other packages
##############

Every package can contain its own set of custom-made elements, yours can too.

AjaXell
*******
AjaXell contains a set of elements to make its page-flow work, such as the
`page-handler` and the `ts-session` element.

A notable element is `ag-toaster`, which allows anyone to show notifications to
the user from their panels.
For more information visit `the AjaXell page <http://kill-all-humans.cern.ch:2974/ts/ajaxell/html/elements/index.html>`_

TS Framework
************
The TS Framework contains a set of panels that are always included in a cell
(i.e. the about panel).

For more information visit `the TS Framework page <http://kill-all-humans.cern.ch:2974/ts/framework/html/elements/index.html>`_
