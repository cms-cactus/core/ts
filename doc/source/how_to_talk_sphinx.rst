Sphinx syntax examples
======================

***************
This is a Title
***************

subtitle
########

subsubtitle
**********************
and so on

basic markup
############
*italic* text.

**bold** text.

`a link <http://xkcd.com/>`.

``*verbatim code*``

TODO: a reference

lists
#####

* This is a bulleted list.
* It has two items, the second
  item uses two lines. (note the indentation)

1. This is a numbered list.
2. It has two items too.

   * sub 1
   * sub 2

#. This is another list
#. Two lists separated only by whitespace are concatenated

terms
#####

term (up to a line of text)
   Definition of the term, which must be indented

   and can even consist of multiple paragraphs

next term
   Description.

line blocks
###########

| These lines are
| broken exactly like in
| the source file.

tables
######

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | ...        | ...      |          |
+------------------------+------------+----------+----------+

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======

links
#####

This is a paragraph that contains `a link`_.

.. _a link: http://example.com/

Lorem ipsum [#f1]_ dolor sit amet ... [#f2]_

.. rubric:: Footnotes

.. [#f1] Text of the first footnote.
.. [#f2] Text of the second footnote.

Lorem ipsum [Ref]_ dolor sit amet.

.. [Ref] Book or article reference, URL or whatever.

..
   This whole indented block
   is a comment.

   Still in the comment.

Source code
###########
basic
*****

This is a normal text paragraph. The next paragraph is a code sample::

  It is not processed in any way, except
  that the indentation is removed.

  It can span multiple lines.

This is a normal text paragraph again.

fancy
*****

.. code-block:: html
    :linenos:

    <h1>code block example</h1>
    <auto-update data="{{my_variable}}" callback="cpp_callback" handle-as="text"></auto-update>
    <span>{{my_variable}}</span>

from external file
******************

.. literalinclude:: //opt/xdaq/htdocs/ts/common-elements/command-input/demo/index.html
   :language: html
   :emphasize-lines: 13,22-27
   :linenos:

custom HTML
###########

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/HBUb8frm2Tw?list=PLNYkxOF6rcIDdS7HWIC_BYRunV6MHs5xo" frameborder="0" allowfullscreen></iframe>

Fancy Math
##########

Since Pythagoras, we know that :math:`a^2 + b^2 = c^2`.

.. math::

   (a + b)^2 = a^2 + 2ab + b^2

   (a - b)^2 = a^2 - 2ab + b^2

   (a + b)^2  &=  (a + b)(a + b) \\
              &=  a^2 + 2ab + b^2

.. math::
   :nowrap:

   \begin{eqnarray}
      y    & = & ax^2 + bx + c \\
      f(x) & = & x^2 + 2xy + y^2
   \end{eqnarray}

Euler's identity, equation :eq:`euler`, was elected one of the most
beautiful mathematical formulas.

.. math:: e^{i\pi} + 1 = 0
   :label: euler
