.. cms documentation documentation master file, created by
   sphinx-quickstart on Tue Feb 23 15:57:06 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the CMS Trigger Supervisor Software Documentation!
=============================================================

Contents:

.. toctree::
   :maxdepth: 2

   building_panels/index
   how_to_talk_sphinx


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
