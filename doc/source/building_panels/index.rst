Building your own Cell Panels
=============================

Welcome
#######

This documentation will show you how to develop panels.

Scope of this document
**********************
This document contains both basic info and quickstarters, but also very
detailed descriptions of the inner workings of the technologies used.

All examples are taken from the Subsystem Supervisor unless otherwise specified.
(https://svnweb.cern.ch/trac/cactus/browser/trunk/cactusprojects/subsystem/supervisor)

Quickstart section
##################

.. toctree::
   :maxdepth: 2

   quickstart_section/dev_setup
   quickstart_section/the_structure_of_a_panel

   quickstart_section/0_hello_world
   quickstart_section/1_ajax_and_databinding
   quickstart_section/2_flexbox_layout
   quickstart_section/3_simple_form
   quickstart_section/4_auto_update
   quickstart_section/5_table
   quickstart_section/6_charts
   quickstart_section/7_theming
   quickstart_section/8_notifications

..
   quickstart_section/21_working_with_arrays

Available resources
###################

.. toctree::
   :maxdepth: 2

   resources/bower-components
   resources/common-elements
   resources/other_packages

..
    Hacker section
    ##############

    .. toctree::
       :maxdepth: 2

       advanced_section/dev_env
       advanced_section/API_design
       advanced_section/panels_and_web_components
       advanced_section/available_packages
       advanced_section/extending_available_packages
       advanced_section/2_editable_table
       advanced_section/responsive_design
