Demo 2: The flexbox layout
==========================

To setup a layout of your panels you can use the flexbox layout system.
This is a set of new CSS directives that seeks to make designing layouts much
simpler. It would deprecate the use of `float: left` and other nonsense.

*******************************
Make the flexbox-layout element
*******************************
In your cell, run:

.. code-block:: bash
    :linenos:

    cd src/html/elements
    ./new-element.js
    name of the new element: flexbox-layout
    creating new element <flexbox-layout>...
    removing any .svn folders in  flexbox-layout
    Finished

***********************************
Register the flexbox-layout element
***********************************

Edit src/html/elements/elements.html and add the following line

.. code-block:: bash
    :linenos:

    <link rel="import" href="flexbox-layout/flexbox-layout.html">

This will tell AjaXell to load our new element.

*******************************
Edit the flexbox-layout element
*******************************

Edit src/html/elements/flexbox-layout/flexbox-layout.html

.. code-block:: html
    :linenos:

    <link rel="import" href="/extern/bower_components/polymer/polymer.html">
    <link rel="import" href="/ts/common-elements/reset-css/reset-css.html">
    <link rel="import" href="/ts/common-elements/ts-colors/ts-colors.html">
    <link rel="import" href="/ts/common-elements/iron-flex-layout-attributes/iron-flex-layout-attributes.html">

    <!--
    `flexbox-layout` is a demo showcasing the capabilities of iron-flex-layout-attributes.

    @demo demo/index.html

    -->
    <dom-module id="flexbox-layout">
      <template>
        <style include="reset-css"></style>
        <style include="ts-colors"></style>

        <!-- this will allow you to use flexbox easily -->
        <!-- surf to /ts/common-elements/iron-flex-layout-attributes/index.html -->
        <style include="iron-flex-layout-attributes"></style>

        <link rel="stylesheet" type="text/css" href="css/flexbox-layout-min.css?__inline=true">
      </template>
      <script src="javascript/flexbox-layout-min.js?__inline=true"></script>
    </dom-module>

We'll add more stuff as we go along.
Note the import for `ts-colors`. It will allow us to do easily add colors to our stuff.
It implements the material design color pallete (https://www.google.com/design/spec/style/color.html#color-color-palette).

The following code will contain a blue box:

.. code-block:: html
    :linenos:

    <div blue-400>this will have a blue background</div>
    <div blue-100>this will have a light blue background</div>


Now edit src/html/elements/flexbox-layout/css/flexbox-layout.scss

.. code-block:: scss
    :linenos:

    :host {
      display: block;
    }

    [square] {
      width: 100px;
      height: 100px;
      margin: 1em;
      img {
        width: 100px;
        height: 100px;
      }
    }


Now edit src/html/elements/flexbox-layout/javascript/flexbox-layout.js

.. code-block:: javascript
    :linenos:

    Polymer({
        is: "flexbox-layoutexample"
    });

Flexbox layouts
###############

Horizontal layout
*****************

Add the following code to the HTML template

.. code-block:: html
    :linenos:

    <h2>Horizontal layout</h2>
    <div horizontal layout blue-200>
      <div square blue-100></div>
      <div square blue-100></div>
      <div square blue-100></div>
    </div>

It will render this:

.. image:: horizontallayout.png
   :width: 700px

Horizontal layout with flex
***************************

The flex attribute instructs an element to take as much space as possible in
the direction of the layout (horizontal or vertical).

If there are multiple elements with the flex attribute the available space will
be divided equally between them.

There are also the flex-2, flex-3, ..., flex-12 attributes. When multiple
elements in the same layout have flex attributes, this will assign a greater
weight to the elements. An element with the flex-2 attribute will be twice as
big as the element with the flex attribute in the same layout.

Add the following code to the HTML template

.. code-block:: html
   :linenos:

    <div horizontal layout blue-200>
      <div square blue-100></div>
      <div square blue-100 flex>this has the flex attribute</div>
      <div square blue-100></div>
    </div>
    <div horizontal layout blue-200>
      <div square blue-100 flex>this has the flex attribute</div>
      <div square blue-100></div>
      <div square blue-100 flex-2>this has the flex-2 attribute</div>
    </div>

It will render this:

.. image:: horizontallayoutwithflex.png
   :width: 700px

vertical alignment
******************

When you have for example a horizontal layout you might like to also control how
the elements in the layout behave vertically. Same goes the other way, you might
like to control horizontal behavior of elements in a vertical layout.

Add the following code to the HTML template

.. code-block:: html
   :linenos:

    <div horizontal layout blue-200 align-start style="height: 250px;margin: 10px;">
      <div square blue-100>align-start</div>
    </div>
    <div horizontal layout blue-200 align-center style="height: 250px;margin: 10px;">
      <div square blue-100>align-center</div>
    </div>
    <div horizontal layout blue-200 align-end style="height: 250px;margin: 10px;">
      <div square blue-100>align-end</div>
    </div>

It will render this:

.. image:: verticalalignment.png
   :width: 700px

flex alignment
**************

You can also specify options for behavior of elements in the layout direction.
For example you might want to horizontally center a set of horizontally aligned
elements.

Add the following code to the HTML template

.. code-block:: html
   :linenos:

    <div horizontal layout blue-200 flex-left style="margin: 10px;">
      <div square blue-100>flex-left</div>
      <div square blue-100>flex-left</div>
      <div square blue-100>flex-left</div>
      <div square blue-100>flex-left</div>
    </div>
    <div horizontal layout blue-200 flex-center style="margin: 10px;">
      <div square blue-100>flex-center</div>
      <div square blue-100>flex-center</div>
      <div square blue-100>flex-center</div>
      <div square blue-100>flex-center</div>
    </div>
    <div horizontal layout blue-200 flex-end style="margin: 10px;">
      <div square blue-100>flex-end</div>
      <div square blue-100>flex-end</div>
      <div square blue-100>flex-end</div>
      <div square blue-100>flex-end</div>
    </div>
    <div horizontal layout blue-200 flex-stretch-with-space style="margin: 10px;">
      <div square blue-100>flex-stretch-with-space</div>
      <div square blue-100>flex-stretch-with-space</div>
      <div square blue-100>flex-stretch-with-space</div>
      <div square blue-100>flex-stretch-with-space</div>
    </div>
    <div horizontal layout blue-200 flex-stretch style="margin: 10px;">
      <div square blue-100>flex-stretch</div>
      <div square blue-100>flex-stretch</div>
      <div square blue-100>flex-stretch</div>
      <div square blue-100>flex-stretch</div>
    </div>

It will render this:

.. image:: flexalignment.png
   :width: 700px

wrapped content blocks
######################

You can instruct a layout to break its set of elements into multiple lines.

Add the following code to the HTML template

.. code-block:: html
    :linenos:

    <div horizontal layout wrap red-100>
      <div square red-300><img src="../../pics/fish.png" /></div>
      <div square red-300><img src="../../pics/cat1.png" /></div>
      <div square red-300><img src="../../pics/cat2.png" /></div>
      <div square red-300><img src="../../pics/cat3.png" /></div>
      <div square red-300><img src="../../pics/cat4.png" /></div>
      <div square red-300><img src="../../pics/cat5.png" /></div>
      <div square red-300><img src="../../pics/cat6.png" /></div>
      <div square red-300><img src="../../pics/cat7.png" /></div>
      <div square red-300><img src="../../pics/cat8.png" /></div>
      <div square red-300><img src="../../pics/cat9.png" /></div>
      <div square red-300><img src="../../pics/cat10.png" /></div>
      <div square red-300><img src="../../pics/cat11.png" /></div>
      <div square red-300><img src="../../pics/cat12.png" /></div>
      <div square red-300><img src="../../pics/cat13.png" /></div>
      <div square red-300><img src="../../pics/cat14.png" /></div>
      <div square red-300><img src="../../pics/cat15.png" /></div>
      <div square red-300><img src="../../pics/cat16.png" /></div>
    </div>

It will render this:

.. image:: flexwrap.png
   :width: 700px

header and footer, vertically centered content
##############################################

This layout generates a header and a footer, the content has the flex attribute
and takes the rest of the vertical space.

.. code-block:: html
    :linenos:

    <div vertical layout style="height:800px;">
      <div blue-300>I am the top bar, I only take the vertical space I need</div>
      <div blue-50 flex vertical layout flex-center>
        <span>the light-blue box takes all the space it can get, because of the flex attribute</span>

        <div horizontal layout
             green-300
             flex-stretch-with-space
             align-center
             style="height: 10em;margin: 1em;">
          <span blue-grey-500 style="margin:1em;padding:1em;line-height:1em;">test</span>
          <span blue-grey-500 style="margin:1em;padding:1em;line-height:2em;">test</span>
          <span blue-grey-500 style="margin:1em;padding:1em;line-height:3em;">test</span>
          <span blue-grey-500 style="margin:1em;padding:1em;line-height:4em;">test</span>
          <span blue-grey-500 style="margin:1em;padding:1em;line-height:5em;">test</span>
          <span blue-grey-500 style="margin:1em;padding:1em;line-height:6em;">test</span>
        </div>

      </div>
      <div blue-300 style="height: 40px;">I am the bottom bar, I'm 40px high</div>
    </div>

It will render this:

.. image:: headerandfooter.png
   :width: 700px

Impossibly complex layout 1
###########################

This code:

.. code-block:: html
    :linenos:

    <!-- big box, children will be put next to each other -->
    <div horizontal layout style="height: 300px;">
        <div blue-400>left</div>

        <!-- second big box, children will be put on top of each other -->
        <div vertical layout flex>
            <div blue-300>top</div>

            <div horizontal layout flex>

                <div vertical layout flex>

                    <div horizontal layout flex>
                        <div blue-200>left</div>
                        <div blue-100 flex>left</div>
                        <div blue-50 flex-5>
                            <p>note that this box is 5 times larger than the box to the left, thanks to the flex-5 attribute</p>
                            <p><a href="https://www.google.be">Google</a></p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div blue-100>right</div>
                    </div>

                    <div blue-300>bottom</div>
                </div>

                <div blue-200>right</div>
            </div>

        </div>

    </div>

Will render this:

.. image:: complex1.png
   :width: 700px

Impossibly complex layout 2
###########################

This code:

.. code-block:: html
    :linenos:

    <div horizontal layout>
        <div blue-300>left</div>
        <div blue-200>left</div>
        <div vertical layout>
            <div blue-200>top</div>
            <div blue-100>top</div>
            <div blue-50>
                <p>client</p>
                <p><a href="">Google</a></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div blue-100>bottom</div>
        </div>
        <div blue-200>right</div>
    </div>

Will render this:

.. image:: complex2.png
   :width: 700px

Now execute Grunt to build our new Polymer element.

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

*****************************
Make the flexbox-layout panel
*****************************

Make a new c++ file /src/common/panels/FlexboxLayout.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/FlexboxLayout.h"
    #include "ajax/PolymerElement.h"

    using namespace subsystempanels;
    FlexboxLayout::FlexboxLayout( tsframework::CellAbstractContext* context, log4cplus::Logger& logger)
    :tsframework::CellPanel(context,logger) {
      logger_ = log4cplus::Logger::getInstance(logger.getName() +".FlexboxLayout");
    }

    void FlexboxLayout::layout(cgicc::Cgicc& cgi) {
      remove();
      add(new ajax::PolymerElement("flexbox-layout"));
    }

Make the include/subsystem/supervisor/panels/FlexboxLayout.h file.

.. code-block:: cpp
    :linenos:

    #ifndef _subsystem_supervisor_panels_FlexboxLayout_h_
    #define _subsystem_supervisor_panels_FlexboxLayout_h_

    #include "ts/framework/CellPanel.h"
    #include "log4cplus/logger.h"
    #include "cgicc/Cgicc.h"

    namespace subsystempanels {
      class FlexboxLayout: public tsframework::CellPanel {
        public:
          FlexboxLayout(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
          void layout(cgicc::Cgicc& cgi);
      };
    }
    #endif

Register the new class in the Makefile.

.. code-block:: makefile
    :linenos:

    Sources=\
    	version.cc\
    	Cell.cc\
    	CellContext.cc\
    	Configuration.cc\
    	...
    	panels/FlexboxLayout.cc \
    	...

Now register your new panel in the menu so users can access it.

Edit src/common/Cell.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/FlexboxLayout.h"
    ...
    void subsystemsupervisor::Cell::init() {
      ...
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      ...
      panelF->add<subsystempanels::FlexboxLayout>("Flexbox layout examples");

Now you can compile your cell and you should see the "Flexbox layout examples"
panel in the menu under the 'control-panels' section.

Also your element has created some documentation.
Surf to <hostname>:<port>/<package-name>/html/index.html and you will see the
package documentation for your cell. `flexbox-layout` will be in there, and clicking
it brings up the documentation for your `flexbox-layout` element.
