The structure of a panel
========================

A panel consists of C++ code rendering the data and one or more Polymer elements
rendering the GUI.

A Polymer element consists of HTML, CSS, and JavaScript code. Each of these you
can develop in a separate file.

***
C++
***

The main task of the C++ code is to provide the front-end code with data. This
is something very important to realize. It will keep your code clean and easier
to understand and change later on.

A minimal C++ panel looks like this:

.. code-block:: cpp
    :linenos:

    #include "MyPanel.h"
    #include "ajax/toolbox.h"
    #include "ajax/PolymerElement.h"

    #include "log4cplus/loggingmacros.h"
    #include <iostream>

    MyPanel::MyPanel( tsframework::CellAbstractContext* context, log4cplus::Logger& logger ) : tsframework::CellPanel(context,logger) {
        logger_ = log4cplus::Logger::getInstance(logger.getName() +".MyPanel");
    }
    MyPanel::~MyPanel() {
        remove();
    }

    void MyPanel::layout(cgicc::Cgicc& cgi)
    {
        remove();
        setEvent("user-clicked-button", ajax::Eventable::OnClick, this, &MyPanel::clicky);

        ajax::PolymerElement* mypanel = new ajax::PolymerElement("my-panel");
        add(mypanel);
    }

    void MyPanel::clicky(cgicc::Cgicc& cgi,std::ostream& out) {
        out << "This was executed because you clicked the button";
    }

This code outputs '<my-panel></my-panel>' on page load. This is the name of our
polymer element that renders the GUI for this panel.

It also registers a callback 'user-clicked-button'. When the server receives
that callback it will execute clicky() and return whatever is piped into 'out'.

****
HTML
****

The main file of our Polymer element is the HTML file. It defines the visual
structure and inserts our CSS and JavaScript code. It looks something like this:

.. code-block:: html
    :linenos:

    <link rel="import" href="/extern/bower_components/polymer/polymer.html">
    <link rel="import" href="/ts/common-elements/reset-css/reset-css.html">
    <link rel="import" href="/extern/bower_components/paper-button/paper-button.html">
    <!--
    `<my-element>` is the Polymer element of the MyPanel panel.

    It features a button the user can click. And when the user clicks this button
    the server will say it clicked the button.

    @authors me
    -->
    <dom-module id="my-element">
        <template>
            <link rel="import" type="css" href="css/my-element-min.css?__inline=true">
            <style include="reset-css"></style>

            <paper-button on-click="_doClickButton">Click me please</paper-button>
            <ts-ajax id="ajax"
                     data="{{ajax_result}}"
                     callback="user-clicked-button"
                     handle-as="text"></ts-ajax>
        </template>
        <script src="javascript/my-element-min.js?__inline=true"></script>
    </dom-module>


**********
JavaScript
**********

The JavaScript of your element is what makes your element spring to life.
It adds interactivity to your element. A basic JavaScript file looks like this:

.. code-block:: javascript
    :linenos:

    Polymer({
      is: "my-element",
      properties: {
        ajax_result: {
          type: String,
          value: "you haven't clicked the button yet..."
        }
      },
      _doClickButton: function() {
        this.$.ajax.generateRequest();
      }
    });

****
SASS
****

You may have heard about CSS, it allows you to style your HTML markup.
It is very powerful. But it misses some features.
One big missing features is the ability to nest your selectors. Or sometimes
you want to create for-loops. Maybe you would like to set a variable for a color
you use a lot...

This is where SASS comes in (http://sass-lang.com/). SASS is CSS with superpowers.
You write your styles using SASS, and the Grunt build tool will translate it to
normal CSS for you.

Also note that we use another tool called autoprefixer (https://css-tricks.com/autoprefixer/).
This will allow you to not worry about using vendor-prefixes (for example
-webkit-transition vs transition) to keep your CSS compatible with older browsers.

A minimal CSS file looks like this:

.. code-block:: scss
    :linenos:

    :host {
        display: block;
    }
