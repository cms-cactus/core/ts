Demo 1: Ajax and data binding
=============================

Probably you would like your C++ code to supply some data to your panel.
We will use the `ts-ajax` element in the `common-elements` package to retrieve
our data, then we will use data-binding to display the data in our panel.

*****************************
Make the data-binding element
*****************************
In your cell, run:

.. code-block:: bash
    :linenos:

    cd src/html/elements
    ./new-element.js
    name of the new element: data-binding
    creating new element <data-binding>...
    removing any .svn folders in  data-binding
    Finished

*********************************
Register the data-binding element
*********************************

Edit src/html/elements/elements.html and add the following line

.. code-block:: bash
    :linenos:

    <link rel="import" href="data-binding/data-binding.html">

This will tell AjaXell to load our new element.

*****************************
Edit the data-binding element
*****************************

Edit src/html/elements/data-binding/data-binding.html

.. code-block:: html
    :linenos:
    :emphasize-lines: 23-24

    <link rel="import" href="/extern/bower_components/polymer/polymer.html">
    <link rel="import" href="/ts/common-elements/reset-css/reset-css.html">
    <link rel="import" href="/ts/common-elements/ts-ajax/ts-ajax.html">
    <link rel="import" href="/extern/bower_components/paper-button/paper-button.html">

    <!--
    `data-binding` retrieves some data from C++ and displays it to the user

    Example:

        <data-binding></data-binding>

    -->
    <dom-module id="data-binding">
      <template>
        <style include="reset-css"></style>
        <link rel="stylesheet" type="text/css" href="css/data-binding-min.css?__inline=true">

        <p>This is the data-binding example. It will fetch some data using the
        `ts-ajax` element and display it here.</p>

        <h1>example 1</h1>
        <ts-ajax data="{{example1}}" callback="example1function" handle-as="text" auto></ts-ajax>
        <span>[[example1]]</span>

        <h1>example 2</h1>
        <ts-ajax id="example2" data="{{example2}}" callback="example2function" handle-as="json"></ts-ajax>
        <paper-button raised on-click="doCallback">Do example2function callback</paper-button><br>

        <template is="dom-repeat" items="[[example2]]" as="item">
          <span>[[item]]</span><br>
        </template>

      </template>
      <script src="javascript/data-binding-min.js?__inline=true"></script>
    </dom-module>

Note the {{...}} and [[...]] code. This is our data binding code.
Consider the highlighted lines.
Line 23 tells Polymer to link the `data` variable from ts-ajax with our own
`example1` variable. This way, if ts-ajax changes its `data` variable our own
`example1` variable will change too.

When we use the {{...}} syntax this change goes both ways. [[...]] goes one way only.
Use the latter to display some final result as we did in line 24, where we don't
anticipate a source of change.

Now edit src/html/elements/data-binding/css/data-binding.scss

.. code-block:: scss
    :linenos:

    :host {
      display: block;
    }

Now edit src/html/elements/data-binding/javascript/data-binding.js

.. code-block:: javascript
    :linenos:

    Polymer({
        is: 'data-binding',

        properties: {
          example1: {
            type: String,
            value: "no data from C++ yet..."
          },
          example2: {
            type: Array,
            value: function() {
              return ["no data from C++ yet..."];
            }
          }
        },

        doCallback: function() {
          // this.$ is a shorthand selector,
          // it allows us to select an element in our template by id
          this.$.example2.generateRequest();
        }
    });

Now execute Grunt to build our new Polymer element.

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

***************************
Make the data-binding panel
***************************

Make a new c++ file /src/common/panels/DataBinding.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/DataBinding.h"
    #include "ajax/PolymerElement.h"
    #include "json/json.h"
    #include <sstream>

    using namespace subsystempanels;
    DataBinding::DataBinding( tsframework::CellAbstractContext* context, log4cplus::Logger& logger)
    :tsframework::CellPanel(context,logger) {
      logger_ = log4cplus::Logger::getInstance(logger.getName() +".DataBinding");
    }

    void DataBinding::layout(cgicc::Cgicc& cgi) {
      remove();
      setEvent("example1function", ajax::Eventable::OnClick, this, &DataBinding::example1);
      setEvent("example2function", ajax::Eventable::OnClick, this, &DataBinding::example2);
      add(new ajax::PolymerElement("data-binding"));
    }

    void DataBinding::example1(cgicc::Cgicc& cgi,std::ostream& out) {
      out << "This text is generated using C++!";
    }

    void DataBinding::example2(cgicc::Cgicc& cgi,std::ostream& out) {
      Json::Value root(Json::arrayValue);
      for (size_t i = 0; i < 10; i++) {
        std::stringstream ss;
        ss << "This is text " << i << " generated by C++";
        root.append(ss.str());
      }
      out << root;
    }

Notice that in the HTML code earlier we specified callback="example1function" in
one of the ts-ajax elements.
Notice the #include "json/json.h" line. We import the jsoncpp library this way
in order to create an array of strings in example2().

Edit your Makefile to add jsoncpp as a dependency.

.. code-block:: makefile
    :linenos:

    DependentLibraries = tsframework tsajaxell ... jsoncpp

Make the include/subsystem/supervisor/panels/DataBinding.h file.

.. code-block:: cpp
    :linenos:

    #ifndef _subsystem_supervisor_panels_DataBinding_h_
    #define _subsystem_supervisor_panels_DataBinding_h_

    #include "ts/framework/CellPanel.h"
    #include "log4cplus/logger.h"
    #include "cgicc/Cgicc.h"

    namespace subsystempanels {
      class DataBinding: public tsframework::CellPanel {
        public:
          DataBinding(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
          void layout(cgicc::Cgicc& cgi);
        private:
         void example1(cgicc::Cgicc& cgi,std::ostream& out);
         void example2(cgicc::Cgicc& cgi,std::ostream& out);
      };
    }
    #endif

Register the new class in the Makefile.

.. code-block:: makefile
    :linenos:

    Sources=\
    	version.cc\
    	Cell.cc\
    	CellContext.cc\
    	Configuration.cc\
    	...
    	panels/DataBinding.cc \
    	...

Now register your new panel in the menu so users can access it.

Edit src/common/Cell.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/DataBinding.h"
    ...
    void subsystemsupervisor::Cell::init() {
      ...
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      ...
      panelF->add<subsystempanels::DataBinding>("ts-ajax and data-binding");

That's right, you can have spaces in your menu names.

Now you can compile your cell and you should see the "ts-ajax and data-binding"
panel in the menu under the 'control-panels' section.

.. image:: databinding.png
   :width: 700px

Also your element has created some documentation.
Surf to <hostname>:<port>/<package-name>/html/index.html and you will see the
package documentation for your cell. `data-binding` will be in there, and clicking
it brings up the documentation for your `data-binding` element.

Be sure to check out the documentation for the `ts-ajax` element at
<hostname>:<port>/ts/common-elements/html/index.html
