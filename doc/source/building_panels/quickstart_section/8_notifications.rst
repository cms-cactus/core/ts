Demo 8: Showing notifications
=============================

************************************
Make the notifications-demo element
************************************
In your cell, run:

.. code-block:: bash
    :linenos:

    cd src/html/elements
    ./new-element.js
    name of the new element: notifications-demo
    creating new element <notifications-demo>...
    removing any .svn folders in  notifications-demo
    Finished

You now have a working `notifications-demo` element. We'll edit it soon.

****************************************
Register the notifications-demo element
****************************************

Edit src/html/elements/elements.html and add the following line

.. code-block:: bash
    :linenos:

    <link rel="import" href="notifications-demo/notifications-demo.html">

This will tell AjaXell to load our new element.

************************************
Edit the notifications-demo element
************************************

Edit src/html/elements/notifications-demo/notifications-demo.html

.. literalinclude:: /../supervisor/src/html/elements/notifications-demo/notifications-demo.html
   :language: html
   :linenos:

Now edit src/html/elements/notifications-demo/css/notifications-demo.scss

.. literalinclude:: /../supervisor/src/html/elements/notifications-demo/css/notifications-demo.scss
   :language: scss
   :linenos:

Now edit src/html/elements/notifications-demo/javascript/notifications-demo.js

.. literalinclude:: /../supervisor/src/html/elements/notifications-demo/javascript/notifications-demo.js
   :language: javascript
   :linenos:

Now execute Grunt to build our new Polymer element.

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

**********************************
Make the notifications-demo panel
**********************************

Make a new c++ file /src/common/panels/NotificationsDemo.cc

.. literalinclude:: /../supervisor/src/common/panels/NotificationsDemo.cc
   :language: cpp
   :linenos:

Make the include/subsystem/supervisor/panels/NotificationsDemo.h file.

.. literalinclude:: /../supervisor/include/subsystem/supervisor/panels/NotificationsDemo.h
   :language: cpp
   :linenos:

Register the new class in the Makefile.

.. code-block:: makefile
    :linenos:

    Sources=\
    	version.cc\
    	Cell.cc\
    	CellContext.cc\
    	Configuration.cc\
    	...
    	panels/NotificationsDemo.cc \
    	...

Now register your new panel in the menu so users can access it.

Edit src/common/Cell.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/NotificationsDemo.h"
    ...
    void subsystemsupervisor::Cell::init() {
      ...
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      ...
      panelF->add<subsystempanels::NotificationsDemo>("Notifications demo");

Now you can compile your cell and you should see the NotificationsDemo panel in the
menu under the 'control-panels' section.

.. image:: NotificationsDemo.png
   :width: 700px
