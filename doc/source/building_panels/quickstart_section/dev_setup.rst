Setting up the Cell
===================

Starting from this point we will assume you already have a working cell and you
now have arrived at the point you wish to develop panels for it.

Front-end code (HTML, CSS, and JavaScript) have a separate build cycle, separate
from the makefile. These are the steps necessary to setup this build system.

**************************
Making your life easier
**************************

We have copied the files that will be created in this page into a tarball.
This will absolve you from having to create any files.

To use it, run:

.. code-block:: bash
    :linenos:

    svn export svn+ssh://svn.cern.ch/reps/cactus/trunk/cactuscore/ts/doc/cell-skeleton.tar
    tar -xzvf cell-skeleton.tar

Now you can continue following this tutorial, but you don't have to create files
anymore.


**************************
Making directories
**************************

Extend the file structure of your cell to include the following folders and files:

.. image:: filetree.png
   :width: 256px

The html folder will contain the build output of the source files in the
/src/html folder.

If you have static resources you wish to serve in your panel, put them in the
/html folder. /src/html is only meant for source files that need to be processed
in some way.

************************
Setting up Grunt
************************

A panel is composed of different code languages, namely HTML, CSS, and JavaScript.
When you develop a panel, each of these languages are housed in their own files.

This makes things easier for you to read and allows your editor to use code
highlighting and syntax checkers.

It also allows us to perform optimizations on your code.
The JavaScript will be optimized and the SASS code will be compiled into CSS and
enhanced for compatibility.

The build system will put all generated files into the /html folder.

Grunt (http://gruntjs.com/) is the tool we'll use to accomplish all this.

if you do not have npm installed, run

.. code-block:: bash
    :linenos:

    sudo yum install -y npm

now run

.. code-block:: bash
    :linenos:

    cd src/html
    sudo npm install -g grunt-cli

This will install the grunt command line tools on your system, you only need
to do this once since it's a global install (the -g option).

Put this in the /src/html/gruntfile.js file

.. container:: toggle

    .. container:: header

        **/src/html/gruntfile.js**

    .. literalinclude:: /../supervisor/src/html/gruntfile.js
       :language: javascript
       :linenos:

This specifies what Grunt has to do, where to find source files, and where to put
built files.

************************
Setting up documentation
************************

Your cell will automatically generate documentation. So anyone running your cell
can browse to <hostname>:<port>/<package-path>/html/index.html and explore what
elements your cell contains.

Make the /src/html/elements/makeIndex.js file and edit the first few lines.

.. container:: toggle

    .. container:: header

        **/src/html/elements/makeIndex.js**

    .. literalinclude:: /../supervisor/src/html/elements/makeIndex.js
       :language: javascript
       :emphasize-lines: 2-4
       :linenos:

This will generate our package documentation using a template html file.

Create /src/html/elements/index_template.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/index_template.html**

    .. literalinclude:: /../supervisor/src/html/elements/index_template.html
       :linenos:

Now make a file html/index.html

.. container:: toggle

    .. container:: header

        **/html/index.html**

    .. literalinclude:: /../supervisor/html/index.html
       :language: html
       :linenos:

This will redirect the user to the elements folder holding the documentation
when they visit <hostname>:<port>/<package-name>/html/index.html

***********
Using Grunt
***********

Now you should be able to run

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

And you should see whatever elements are present in /src/html/elements are built
and put into /html/elements (you probably don't have any elements now).

Your makefile will copy the /html folder into the RPM's. The src/html folder will
not be copied and will not be present in production systems.
Anything that does not need building can be safely copied into the /html folder.
No build system will delete that folder.

Now that you have an update /html folder you can run

.. code-block:: bash
    :linenos:

    make rpm

The makefile will make a new rpm containing the updated /html folder.

*****************************
Setting up a template
*****************************

You will probably want to make some elements of your own now, but where to start?
We'll give you a script that, using some template element, can generate a general
element definition for you.

Create the file /src/html/elements/new-element.js file

.. container:: toggle

    .. container:: header

        **/src/html/elements/new-element.js**

    .. literalinclude:: /../supervisor/src/html/elements/new-element.js
       :language: javascript
       :linenos:

This will copy a folder `template-element` and rename the appropriate code to
the element name you specified.

Now create /src/html/elements/template-element/template-element.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/template-element/template-element.html**

    .. literalinclude:: /../supervisor/src/html/elements/template-element/template-element.html
       :language: html
       :linenos:

Notice the big comment just before the `dom-module` line. This will be used to
generate documentation for your element. Be sure to update the description of
the element in this comment.

Now create /src/html/elements/template-element/description.json

.. container:: toggle

    .. container:: header

        **/src/html/elements/template-element/description.json**

    .. literalinclude:: /../supervisor/src/html/elements/template-element/description.json
       :language: json
       :linenos:

This file gives a description for the package documentation that will be generated
by Grunt. Change the description to something sensible when you generate a new
element with new-element.js. delete the `demo` line if you, at one point, decide
to not provide a demo.

Now create /src/html/elements/template-element/index.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/template-element/index.html**

    .. literalinclude:: /../supervisor/src/html/elements/template-element/index.html
       :language: html
       :linenos:

Now create /src/html/elements/template-element/javascript/template-element.js

.. container:: toggle

    .. container:: header

        **/src/html/elements/template-element/javascript/template-element.js**

    .. literalinclude:: /../supervisor/src/html/elements/template-element/javascript/template-element.js
       :language: javascript
       :linenos:

Note that this template serves as a boilerplate, and probably contains a lot of
code you won't actually use. Delete lines you do not need in new elements you
generate with this new-element.js script.
Also notice the comments in the `properties` section and above every function
definition. These are used to generate documentation for your element and follow
the JSDoc syntax (http://usejsdoc.org/about-getting-started.html).

Now create /src/html/elements/template-element/demo/index.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/template-element/demo/index.html**

    .. literalinclude:: /../supervisor/src/html/elements/template-element/demo/index.html
       :language: html
       :linenos:

This file is the demo. By default the demo only shows the element without any
adjustments or data supplied to it. Adjust the demo if your element needs extra
work or data before it becomes functional.

Now create /src/html/elements/template-element/css/template-element.scss

.. container:: toggle

    .. container:: header

        **/src/html/elements/template-element/css/template-element.scss**

    .. literalinclude:: /../supervisor/src/html/elements/template-element/css/template-element.scss
       :language: scss
       :linenos:

In a generated element, you most probably won't need any of this code except the
very first block (:host {display: block}). The rest serves as code examples.
Notice that --my-custom-color and --my-mixin-name also appeared in the comment
in template-element.html.

Now you should be able to run

.. code-block:: bash
    :linenos:

    cd src/html/elements
    chmod +x new-element.js
    ./new-element.js
    name of the new element: my-new-element
    creating new element <my-new-element>...
    removing any .svn folders in  my-new-element
    Finished

And you will see a new folder my-new-element in /src/html/elements, ready for
you to be developed further into whatever you want to build today.

********************************
Registering your elements in C++
********************************
When you open a web browser and navigate to your cell, your browser needs to be
instructed to load your elements. AjaXell can do this for you, but you need to
provide a list of elements.

Create a file /src/html/elements/elements.html
Now, you don't have any elements yet. So this file will be empty for now.
But here is an example how it would look like if you would have two elements
`my-first-element` and `my-second-element`:

.. code-block:: html
    :linenos:

    <link rel="import" href="my-first-element/my-first-element.html">
    <link rel="import" href="my-second-element/my-second-element.html">

Now open the Cell.cc file and append the following line in the start of the
Cell::init() function

.. code-block:: cpp
    :emphasize-lines: 3
    :linenos:

    void subsystemsupervisor::Cell::init()
    {
      getContext()->addImport("/<package-path>/html/elements/elements.html");
      ...

<package-path> will depend on the name of your cell. For the subsystem supervisor
the addImport line would be this

.. code-block:: cpp
    :linenos:

    getContext()->addImport("/subsystem/supervisor/html/elements/elements.html");
