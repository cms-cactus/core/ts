Demo 5: Tables
==============

************************************
Make the table-example element
************************************
In your cell, run:

.. code-block:: bash
    :linenos:

    cd src/html/elements
    ./new-element.js
    name of the new element: table-example
    creating new element <table-example>...
    removing any .svn folders in  table-example
    Finished

****************************************
Register the table-example element
****************************************

Edit src/html/elements/elements.html and add the following line

.. code-block:: bash
    :linenos:

    <link rel="import" href="table-example/table-example.html">

This will tell AjaXell to load our new element.

************************************
Edit the table-example element
************************************

Edit src/html/elements/table-example/table-example.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/table-example/table-example.html**

    .. code-block:: html
       :linenos:

       <link rel="import" href="/extern/bower_components/polymer/polymer.html">
       <link rel="import" href="/ts/common-elements/reset-css/reset-css.html">
       <link rel="import" href="/ts/common-elements/iron-flex-layout-attributes/iron-flex-layout-attributes.html">
       <link rel="import" href="/extern/bower_components/vaadin-grid/vaadin-grid.html">
       <link rel="import" href="/extern/bower_components/paper-material/paper-material.html">

       <!--
       This panel uses [vaadin-grid](/extern/bower_components/vaadin-grid/index.html)

       `table-example` demonstrates the uses of the vaadin-grid element.

       -->
       <dom-module id="table-example">
         <template>
           <style include="reset-css"></style>
           <style include="iron-flex-layout-attributes"></style>

           <link rel="stylesheet" type="text/css" href="css/table-example-min.css?__inline=true">

         </template>
         <script src="javascript/table-example-min.js?__inline=true"></script>
       </dom-module>

We'll add more stuff as we go along.

Now edit /src/html/elements/table-example/css/table-example.scss

.. container:: toggle

    .. container:: header

        **/src/html/elements/table-example/css/table-example.scss**

    .. literalinclude:: /../supervisor/src/html/elements/table-example/css/table-example.scss
       :language: scss
       :linenos:


Now execute Grunt to build our new Polymer element.

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

**********************************
Make the table-example panel
**********************************

Make a new c++ file /src/common/panels/TableExample.cc

.. container:: toggle

    .. container:: header

        **/src/common/panels/TableExample.cc**

    .. code-block:: cpp
       :linenos:

       #include "subsystem/supervisor/panels/TableExample.h"

       #include "ajax/toolbox.h"
       #include "ajax/PolymerElement.h"
       #include "ajax/toolbox.h"
       #include "json/json.h"

       #include "log4cplus/loggingmacros.h"

       #include <iostream>
       #include <time.h>
       #include <string>
       using namespace subsystempanels;

       TableExample::TableExample( tsframework::CellAbstractContext* context, log4cplus::Logger& logger ) : tsframework::CellPanel(context,logger) {
           logger_ = log4cplus::Logger::getInstance(logger.getName() +".TableExample");
       }
       TableExample::~TableExample() {
           remove();
       }

       void TableExample::layout(cgicc::Cgicc& cgi)
       {
           remove();
           add(new ajax::PolymerElement("table-example"));
       }


Make the /include/subsystem/supervisor/panels/TableExample.h file.

.. container:: toggle

    .. container:: header

        **/include/subsystem/supervisor/panels/TableExample.h**

    .. code-block:: cpp
       :linenos:

       #ifndef _subsystem_supervisor_panels_TableExample_h_
       #define _subsystem_supervisor_panels_TableExample_h_

       #include "ts/framework/CellPanel.h"

       #include "ts/framework/CellAbstractContext.h"

       #include "log4cplus/logger.h"

       #include "cgicc/Cgicc.h"

       #include <iostream>

       namespace subsystempanels
       {

       class TableExample: public tsframework::CellPanel
       {
       	public:
         TableExample(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
         ~TableExample();

         void layout(cgicc::Cgicc& cgi);

        private:

       };
       }//subsystempanels
       #endif


Register the new class in the Makefile.

.. code-block:: makefile
    :linenos:

    Sources=\
    	version.cc\
    	Cell.cc\
    	CellContext.cc\
    	Configuration.cc\
    	...
    	panels/TableExample.cc \
    	...

Now register your new panel in the menu so users can access it.

Edit src/common/Cell.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/TableExample.h"
    ...
    void subsystemsupervisor::Cell::init() {
      ...
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      ...
      panelF->add<subsystempanels::TableExample>("Table example");

Now you can compile your cell and you should see the "Form example"
panel in the menu under the 'control-panels' section.

**********************************
Table1: A basic table with sorting
**********************************

The first example will fetch a JSON file containing 1000 rows.
Also we will make it sortable, so we will write our own sorting function.

C++ code
########

Edit /src/common/panels/TableExample.cc

.. container:: toggle

    .. container:: header

        **/src/common/panels/TableExample.cc**

    .. code-block:: cpp
       :linenos:

       void TableExample::layout(cgicc::Cgicc& cgi)
       {
           remove();
           setEvent("getTable1", ajax::Eventable::OnClick, this, &TableExample::getTable1);
           add(new ajax::PolymerElement("table-example"));
       }

       void TableExample::getTable1(cgicc::Cgicc& cgi,std::ostream& out) {
           Json::Value root;

           Json::Value items(Json::arrayValue);
           for (int i = 0; i < 1000; i++) {
             Json::Value row;

             std::ostringstream msg;
             msg << "row" << i;
             row["some string"] = msg.str();

             msg << "@cern.ch";
             row["email"] = msg.str();

             row["group"] = i % 3;

             items.append(row);
           }
           root["items"] = items;

           Json::Value columns(Json::arrayValue);

           Json::Value column1;
           column1["name"] = "some string";
           column1["sortable"] = true;
           columns.append(column1);

           Json::Value column2;
           column2["name"] = "email";
           column2["sortable"] = true;
           columns.append(column2);

           Json::Value column3;
           column3["name"] = "group";
           column3["sortable"] = true;
           columns.append(column3);

           root["columns"] = columns;

           out << root;
       }

The getTable1 call will provide the data of our first table.
It will provide both column definitions and table rows.

Edit /include/subsystem/supervisor/panels/TableExample.h.

.. container:: toggle

   .. container:: header

       **/include/subsystem/supervisor/panels/TableExample.h**

   .. code-block:: cpp
      :emphasize-lines: 8
      :linenos:

      namespace subsystempanels
      {

      class TableExample: public tsframework::CellPanel
      {

       private:
        void getTable1(cgicc::Cgicc& cgi,std::ostream& out);

      };
      }//subsystempanels
      #endif

HTML
#################

Edit src/html/elements/table-example/table-example.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/table-example/table-example.html**

    .. code-block:: html
       :linenos:

       <dom-module id="table-example">
         <template>

           <h1>basic table, also sortable</h1>
           <p>
             This dataset has 1000 records, however this table is smart and will only render the rows currently in view.
             This will keep the page performant.
           </p>
           <p>
             Click on one of the colums to sort it, shift + click a column to use secondary sort
           </p>
           <paper-material elevation="1">
             <ts-ajax data="{{table1}}" callback="getTable1" handle-as="json" auto></ts-ajax>
             <vaadin-grid id="table1"
                          selection-mode="multi"
                          items="{{table1.items}}"
                          columns="{{table1.columns}}"></vaadin-grid>
           </paper-material>

         </template>
       </dom-module>

ts-ajax will execute the getTable1 callback (notice the 'auto' attribute).
the table will populate itself as soon as ts-ajax completed the callback.

Making it sortable
##################

In the C++ code, our generated JSON contains the "sortable": true, but we still
have to supply a sorting function ourselves.

This gets a bit complicated as we made more than one column sortable, so we have
to support secondary sorting (the user can shift + click on a column to do that),
and we have data that we need to parse to make properly sortable (like the email column).

Edit /src/html/elements/table-example/javascript/table-example.js

.. container:: toggle

    .. container:: header

        **/src/html/elements/table-example/javascript/table-example.js**

    .. code-block:: javascript
       :linenos:

       Polymer({
         attached: function() {
           this.$.table1.addEventListener('sort-order-changed', this.table1sort.bind(this));
         },

         table1sort: function() {
           // secondary sort
           var secondarySort = function() {return 0};
           if (this.$.table1.sortOrder[1]) {
             var columnNameSecondary = this.$.table1.columns[this.$.table1.sortOrder[1].column].name;
             var directionSecondary = this.$.table1.sortOrder[1].direction == 'asc' ? -1 : 1;
             secondarySort = function(row1, row2) {
               return (row1[columnNameSecondary] < row2[columnNameSecondary]) ? directionSecondary : -directionSecondary;
             }

             if (columnNameSecondary == "some string") {
               secondarySort = function(row1, row2) {
                 var a = parseInt(row1[columnNameSecondary].substring(3));
                 var b = parseInt(row2[columnNameSecondary].substring(3));
                 return (a < b) ? directionSecondary : -directionSecondary;
               }
             }
             if (columnNameSecondary == "email") {
               secondarySort = function(row1, row2) {
                 var a = parseInt( row1[columnNameSecondary].substring(3).substring(0, row1[columnNameSecondary].indexOf("@") - 3) );
                 var b = parseInt( row2[columnNameSecondary].substring(3).substring(0, row2[columnNameSecondary].indexOf("@") - 3) );
                 return (a < b) ? directionSecondary : -directionSecondary;
               }
             }
           }

           // primary sort
           var columnName = this.$.table1.columns[this.$.table1.sortOrder[0].column].name;
           var direction = this.$.table1.sortOrder[0].direction == 'asc' ? -1 : 1;

           var sort = function(row1, row2) {
             var result = (row1[columnName] < row2[columnName]) ? direction : -direction;
             if (row1[columnName] == row2[columnName]) {
               result = secondarySort(row1, row2);
             }
             return result;
           }

           if (columnName == "some string") {
             sort = function(row1, row2) {
               var a = parseInt(row1[columnName].substring(3));
               var b = parseInt(row2[columnName].substring(3));
               var result = (a < b) ? direction : -direction;
               if (a == b) {
                 result = secondarySort(row1, row2);
               }
               return result;
             }
           }
           if (columnName == "email") {
             sort = function(row1, row2) {
               var a = parseInt( row1[columnName].substring(3).substring(0, row1[columnName].indexOf("@") - 3) );
               var b = parseInt( row2[columnName].substring(3).substring(0, row2[columnName].indexOf("@") - 3) );
               var result = (a < b) ? direction : -direction;
               if (a == b) {
                 result = secondarySort(row1, row2);
               }
               return result;
             }
           }

           this.$.table1.items.sort(sort);
         }
       });


The result will look like this:

.. image:: table1.png
  :width: 700px

**********************************
Table2: An AJAX table
**********************************

This dataset will be 5000 rows big, and is loaded asynchronously.
This means the table will make requests for more data as the user scrolls.

C++ code
########

Edit /src/common/panels/TableExample.cc

.. container:: toggle

    .. container:: header

        **/src/common/panels/TableExample.cc**

    .. code-block:: cpp
       :linenos:

       void TableExample::layout(cgicc::Cgicc& cgi)
       {
           remove();
           setEvent("getTable1", ajax::Eventable::OnClick, this, &TableExample::getTable1);
           setEvent("getTable2", ajax::Eventable::OnClick, this, &TableExample::getTable2);
           add(new ajax::PolymerElement("table-example"));
       }

       void TableExample::getTable2(cgicc::Cgicc& cgi,std::ostream& out) {
         std::map<std::string,std::string> values(ajax::toolbox::getSubmittedValues(cgi));
         int index;
         std::stringstream indexs(values.find("index")->second);
         indexs >> index;
         int count;
         std::stringstream counts(values.find("count")->second);
         counts >> count;
         if (indexs.fail() || counts.fail()) {
             return;
         }

         Json::Value rows(Json::arrayValue);
         for (size_t i = index; i <= count + index; i++) {
           Json::Value row;

           row["random number"] = rand();
           std::ostringstream msg;
           msg << "item " << i;
           row["some string"] = msg.str();

           rows.append(row);
         }

         out << rows;
       }

Edit /include/subsystem/supervisor/panels/TableExample.h.

.. container:: toggle

   .. container:: header

       **/include/subsystem/supervisor/panels/TableExample.h**

   .. code-block:: cpp
      :emphasize-lines: 8
      :linenos:

      namespace subsystempanels
      {

      class TableExample: public tsframework::CellPanel
      {

       private:
      	void getTable2(cgicc::Cgicc& cgi,std::ostream& out);

      };
      }//subsystempanels
      #endif

Unlike the first example, were we sent both items and column info, we only send
the items in this example. The column info will now be declared in JavaScript.

HTML & Javascript
#################

Edit src/html/elements/table-example/table-example.html

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/table-example.html**

   .. code-block:: html
      :linenos:

      <dom-module id="table-example">
        <template>

          <h1>Asynchronous table</h1>
          <p>
            This dataset is not fetched in one go from the server, but in chunks.
            Use this if the data is expensive to render server side.
            Scroll fast to see the data being loaded.
          </p>
          <p>
            Unfortunately, async data means we cannot use sorting...
          </p>
          <paper-button raised primary on-click="table2_scrollend">Scroll to end</paper-button>
          <paper-button raised primary on-click="table2_scrollstart">Scroll to start</paper-button>
          <paper-button raised primary on-click="table2_scroll3000">Scroll to line 3000</paper-button>
          <paper-material elevation="1">
            <ts-ajax id="ajax_table2"
                     data="{{tsajax_table2}}"
                     callback="getTable2"
                     handle-as="json"
                     parameters='["index", "count"]'></ts-ajax>
            <vaadin-grid id="table2"
                         selection-mode="multi"
                         items="{{table2items}}"
                         columns='{{table2columns}}'
                         size="5000"></vaadin-grid>
          </paper-material>

        </template>
      </dom-module>


Now edit /src/html/elements/table-example/javascript/table-example.js

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/javascript/table-example.js**

   .. code-block:: javascript
      :linenos:

      Polymer({
        properties: {
          /**
           * The callback for the asynchronous data request
           */
          table2callback: Function,
          /**
           * The ajax response from ts-ajax
           */
          tsajax_table2: {
            type: Object,
            observer: 'newTable2data'
          },
          /**
           * The `items` dataset for our table. It is a function because it will fetch
           * data for us rather than just be a dumb array containing all the data.
           */
          table2items: {
            type: Function,
            value: function() {
              return function(params, callback) {
                this.table2callback = callback;
                var ajax = this.$.ajax_table2;
                ajax.index = params.index;
                ajax.count = params.count;
                ajax.generateRequest();
              }.bind(this);
            }
          },
          /**
           * The columns of our data
           */
          table2columns: {
            type: Array,
            value: function() {
              return [{name: "some string"}, {name: "random number"}]
            }
          }
        },

        newTable2data: function(newdata) {
          if (this.table2callback) {
            // note that the callback can also take a second parameter that updates the size
            // this can be used to implement infinite scrolling or datasets with changing sizes
            this.table2callback(newdata);
          }
        },

        table2_scrollend: function() {
          this.$.table2.scrollToEnd();
        },
        table2_scrollstart: function() {
          this.$.table2.scrollToStart();
        },
        table2_scroll3000: function() {
          this.$.table2.scrollToRow(3000);
        },
      });

Notice the asynchronous data fetching makes our JavaScript quite a bit more
complex.
Use this approach when generating data on server-side is slow or otherwise expensive.

The result will look like this:

.. image:: table2.png
   :width: 700px

**********************************
Table3: Frozen columns & styling
**********************************

This table will have a 'frozen' column that will always be visible.
Also some columns are 'hidable', so we will have the option to hide them.

C++ code
########

Edit /src/common/panels/TableExample.cc

.. container:: toggle

    .. container:: header

        **/src/common/panels/TableExample.cc**

    .. code-block:: cpp
       :linenos:

       void TableExample::layout(cgicc::Cgicc& cgi)
       {
           remove();
           setEvent("getTable1", ajax::Eventable::OnClick, this, &TableExample::getTable1);
           setEvent("getTable2", ajax::Eventable::OnClick, this, &TableExample::getTable2);
           setEvent("getTable3", ajax::Eventable::OnClick, this, &TableExample::getTable3);
           add(new ajax::PolymerElement("table-example"));
       }

       void TableExample::getTable3(cgicc::Cgicc& cgi,std::ostream& out) {
         std::map<std::string,std::string> values(ajax::toolbox::getSubmittedValues(cgi));
         int index;
         std::stringstream indexs(values.find("index")->second);
         indexs >> index;
         int count;
         std::stringstream counts(values.find("count")->second);
         counts >> count;
         if (indexs.fail() || counts.fail()) {
             return;
         }

         std::string states[] = {"ok", "warning", "error"};
         Json::Value rows(Json::arrayValue);
         for (size_t i = index; i <= count + index; i++) {
           Json::Value row;

           std::ostringstream msg;
           msg << "cell #" << i;
           row["cell"] = msg.str();

           row["CPU (%)"] = rand() % 100;
           row["memory (%)"] = rand() % 100;
           row["network (%)"] = rand() % 100;
           row["PID"] = rand() % 700;
           row["RSS"] = rand();
           row["VSZ"] = rand();
           row["state"] = states[i % 3];

           time_t     now = time(0);
           struct tm  tstruct;
           char       buf[80];
           tstruct = *localtime(&now);
           // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
           // for more information about date/time format
           strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
           row["timestamp"] = buf;

           rows.append(row);
         }

         out << rows;
       }

Edit /include/subsystem/supervisor/panels/TableExample.h.

.. container:: toggle

   .. container:: header

       **/include/subsystem/supervisor/panels/TableExample.h**

   .. code-block:: cpp
      :emphasize-lines: 8
      :linenos:

      namespace subsystempanels
      {

      class TableExample: public tsframework::CellPanel
      {

       private:
      	void getTable3(cgicc::Cgicc& cgi,std::ostream& out);

      };
      }//subsystempanels
      #endif


HTML & Javascript
#################

Edit src/html/elements/table-example/table-example.html

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/table-example.html**

   .. code-block:: html
      :linenos:

      <dom-module id="table-example">
        <template>

          <h1>Frozen columns &amp; styling</h1>
          <p>
            The first column in this dataset will always be visible.
            Errors and warnings will be very visible.
          </p>
          <p>
            You can also choose to hide some columns using the icon in the upper right.
          </p>
          <paper-material elevation="1">
            <ts-ajax id="ajax_table3"
                   data="{{tsajax_table3}}"
                   callback="getTable3"
                   handle-as="json"
                   parameters='["index", "count"]'></ts-ajax>
            <vaadin-grid id="table3"
                         selection-mode="multi"
                         items="{{table3items}}"
                         columns='{{table3columns}}'
                         size="5000"
                         row-class-generator="[[table3rowclass]]"
                         frozen-columns="1"></vaadin-grid>
          </paper-material>

        </template>
      </dom-module>


Now edit /src/html/elements/table-example/javascript/table-example.js

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/javascript/table-example.js**

   .. code-block:: javascript
      :linenos:

      Polymer({
        properties: {
          /**
           * The callback for the asynchronous data request
           */
          table3callback: Function,
          /**
           * The ajax response from ts-ajax
           */
          tsajax_table3: {
            type: Object,
            observer: 'newTable3data'
          },
          /**
           * The `items` dataset for our table. It is a function because it will fetch
           * data for us rather than just be a dumb array containing all the data.
           */
          table3items: {
            type: Function,
            value: function() {
              return function(params, callback) {
                this.table3callback = callback;
                var ajax = this.$.ajax_table3;
                ajax.index = params.index;
                ajax.count = params.count;
                ajax.generateRequest();
              }.bind(this);
            }
          },
          /**
           * The columns of our data
           */
          table3columns: {
            type: Array,
            value: function() {
              return [
                {name: "cell"},
                {name: "CPU (%)", hidable: true},
                {name: "memory (%)", hidable: true},
                {name: "network (%)", hidable: true},
                {name: "PID", hidable: true},
                {name: "RSS", hidable: true},
                {name: "VSZ", hidable: true},
                {name: "state"},
                {name: "timestamp", hidable: true}
              ];
            }
          },

          table3rowclass: {
            type: Function,
            value: function() {
              return function(row) {
                var bgcolor = "";
                var color = "";
                // var fontweight = "";
                var DOMrow = row.element;

                if (row.data.state == "error") {
                  bgcolor = "#EC4343";
                  color = "white";
                  DOMrow.childNodes[8].style.textTransform = "uppercase";
                } else if (row.data.state == "warning") {
                  bgcolor = "#EC9943";
                  color = "white";
                  DOMrow.childNodes[8].style.textTransform = "uppercase";
                }

                DOMrow.style.color = color;
                for (var i = 0; i < DOMrow.childNodes.length; i++) {
                  DOMrow.childNodes[i].style.backgroundColor = bgcolor;
                }

                return "";
              }
            }
          }
        },

        newTable3data: function(newdata) {
          if (this.table3callback && newdata) {
            this.table3callback(newdata);
          }
        }

      });


The result will look like this:

.. image:: table3.png
   :width: 700px

**********************************
Table4: Custom cell content
**********************************

Sometimes you want to display something fancy instead of just data.
This example will replace the 'progress' value (0-100) with an html progress bar.

C++ code
########

Edit /src/common/panels/TableExample.cc

.. container:: toggle

    .. container:: header

        **/src/common/panels/TableExample.cc**

    .. code-block:: cpp
       :linenos:

       void TableExample::layout(cgicc::Cgicc& cgi)
       {
           remove();
           setEvent("getTable1", ajax::Eventable::OnClick, this, &TableExample::getTable1);
           setEvent("getTable2", ajax::Eventable::OnClick, this, &TableExample::getTable2);
           setEvent("getTable3", ajax::Eventable::OnClick, this, &TableExample::getTable3);
           setEvent("getTable4", ajax::Eventable::OnClick, this, &TableExample::getTable4);
           add(new ajax::PolymerElement("table-example"));
       }

       void TableExample::getTable4(cgicc::Cgicc& cgi,std::ostream& out) {
           Json::Value root;

           Json::Value items(Json::arrayValue);
           for (int i = 1; i <= 100; i++) {
             Json::Value row;

             std::ostringstream msg;
             msg << "row " << i;
             row["rowNumber"] = msg.str();

             row["progress"] = rand() % 100;

             row["extra info"] = "some extra info about row #" + msg.str();

             items.append(row);
           }
           root["items"] = items;

           Json::Value columns(Json::arrayValue);

           Json::Value column1;
           column1["name"] = "rowNumber";
           columns.append(column1);

           Json::Value column3;
           column3["name"] = "progress";
           columns.append(column3);

           root["columns"] = columns;

           out << root;
       }

Edit /include/subsystem/supervisor/panels/TableExample.h.

.. container:: toggle

   .. container:: header

       **/include/subsystem/supervisor/panels/TableExample.h**

   .. code-block:: cpp
      :emphasize-lines: 8
      :linenos:

      namespace subsystempanels
      {

      class TableExample: public tsframework::CellPanel
      {

       private:
      	void getTable4(cgicc::Cgicc& cgi,std::ostream& out);

      };
      }//subsystempanels
      #endif


HTML & Javascript
#################

Edit src/html/elements/table-example/table-example.html

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/table-example.html**

   .. code-block:: html
      :linenos:

      <dom-module id="table-example">
        <template>

          <h1>Custom HTML instead of pure data</h1>
          <p>
            Instead of some progress number (e.g. 10 or 80), you can show a progress bar.
          </p>
          <paper-material elevation="1">
            <ts-ajax data="{{table4}}" callback="getTable4" handle-as="json" auto></ts-ajax>
            <vaadin-grid id="table4"
                         items="{{table4.items}}"
                         columns="{{table4.columns}}"></vaadin-grid>
          </paper-material>

        </template>
      </dom-module>


Now edit /src/html/elements/table-example/javascript/table-example.js

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/javascript/table-example.js**

   .. code-block:: javascript
      :linenos:

      Polymer({
        properties: {
          table4: {
            type: Object,
            observer: "table4Changed"
          }
        },

        table4Changed: function(table4) {
          if (table4) {
            table4.columns[1].renderer = this.table4ProgressRenderer;
          }
        },

        table4ProgressRenderer: function(cell) {
          cell.element.innerHTML = '';
          var child = document.createElement('progress');
          child.setAttribute('value', cell.data);
          child.setAttribute('max', 100);
          cell.element.appendChild(child);
        }

      });

The result will look like this:

.. image:: table4.png
   :width: 700px

**********************************
Table5: Details on selection
**********************************

You can show some extra details when one of the rows is clicked.

HTML & Javascript
#################

Edit src/html/elements/table-example/table-example.html

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/table-example.html**

   .. code-block:: html
      :linenos:

      <dom-module id="table-example">
        <template>

          <h1>details on selection</h1>
          <p>
            Select an item, and a detail view will appear
          </p>
          <paper-material elevation="1">
            <ts-ajax data="{{table5}}" callback="getTable4" handle-as="json" auto></ts-ajax>
            <vaadin-grid id="table5"
                         items="{{table5.items}}"
                         columns="{{table5.columns}}"></vaadin-grid>
          </paper-material>

        </template>
      </dom-module>


Now edit /src/html/elements/table-example/javascript/table-example.js

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/javascript/table-example.js**

   .. code-block:: javascript
      :linenos:

      Polymer({
        properties: {
          table5_detailSelected: {
            type: Number,
            value: 0
          }
        },

        attached: function() {

          this.$.table5.addEventListener('selected-items-changed', function() {
            this.$.table5.setRowDetailsVisible(this.table5_detailSelected, false);
            var selected = this.$.table5.selection.selected();
            if (selected.length == 1) {
              this.$.table5.setRowDetailsVisible(selected[0], true);
              this.table5_detailSelected = selected[0];
            }
          }.bind(this));

          this.$.table5.rowDetailsGenerator = function(rowIndex) {
            var elem = document.createElement('table-detail');

            this.$.table5.getItem(rowIndex, function(error, item) {
              if (!error) {
                elem.item = item;
              }
            });

            return elem;
          }.bind(this);
        }

      });

Now our code will show a 'table-detail' element when a row is selected.
We still need to make this element.

Create src/html/elements/table-example/table-detail.html

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/table-detail.html**

   .. code-block:: html
      :linenos:

      <link rel="import" href="/extern/bower_components/paper-material/paper-material.html">

      <dom-module id="table-detail">
        <template>
          <link rel="stylesheet" type="text/css" href="css/table-detail-min.css?__inline=true">

          <paper-material elevation="1">
            <p>
              [[getExtraInfo(item)]]
            </p>
          </paper-material>

        </template>
        <script src="javascript/table-detail-min.js?__inline=true"></script>
      </dom-module>

Now create /src/html/elements/table-example/javascript/table-detail.js

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/javascript/table-detail.js**

   .. code-block:: javascript
      :linenos:

      Polymer({
        is: "table-detail",
        properties: {
          item: {
            type: Object
          }
        },

        getExtraInfo: function(item) {
          // data binding can't handle spaces
          return item['extra info'];
        }
      });

Now create /src/html/elements/table-example/css/table-detail.scss

.. container:: toggle

   .. container:: header

       **/src/html/elements/table-example/css/table-detail.scss**

   .. code-block:: scss
      :linenos:

      :host {
        display: block;
        height: 8em;
      }

      paper-material {
        padding: 1em;
        margin: 1em;
      }

The result will look like this:

.. image:: table5.png
   :width: 700px
