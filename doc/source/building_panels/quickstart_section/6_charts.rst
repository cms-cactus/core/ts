Demo 6: Charts
==============

For a complete list of options available for each type of chart, look at the
official NVD3 docs (https://nvd3-community.github.io/nvd3/examples/documentation.html)

************************************
Make the chart-examples element
************************************
In your cell, run:

.. code-block:: bash
    :linenos:

    cd src/html/elements
    ./new-element.js
    name of the new element: chart-examples
    creating new element <chart-examples>...
    removing any .svn folders in  chart-examples
    Finished

****************************************
Register the chart-examples element
****************************************

Edit src/html/elements/elements.html and add the following line

.. code-block:: bash
    :linenos:

    <link rel="import" href="chart-examples/chart-examples.html">

This will tell AjaXell to load our new element.

************************************
Edit the chart-examples element
************************************

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/chart-examples/chart-examples.html**

    .. code-block:: html
       :linenos:

       <link rel="import" href="/extern/bower_components/polymer/polymer.html">
       <link rel="import" href="/ts/common-elements/reset-css/reset-css.html">
       <link rel="import" href="/ts/common-elements/iron-flex-layout-attributes/iron-flex-layout-attributes.html">
       <link rel="import" href="/ts/common-elements/math-equation/math-equation.html">
       <link rel="import" href="/ts/common-elements/charts/line-chart/line-chart.html">
       <link rel="import" href="/ts/common-elements/charts/cumulative-line-chart/cumulative-line-chart.html">
       <link rel="import" href="/ts/common-elements/charts/focus-line-chart/focus-line-chart.html">
       <link rel="import" href="/ts/common-elements/charts/horizontal-stacked-bar-chart/horizontal-stacked-bar-chart.html">
       <link rel="import" href="/ts/common-elements/charts/historical-bar-chart/historical-bar-chart.html">
       <link rel="import" href="/ts/common-elements/charts/discrete-bar-chart/discrete-bar-chart.html">
       <link rel="import" href="/ts/common-elements/charts/stacked-bar-chart/stacked-bar-chart.html">
       <link rel="import" href="/ts/common-elements/charts/pie-chart/pie-chart.html">
       <link rel="import" href="/ts/common-elements/charts/scatter-chart/scatter-chart.html">
       <link rel="import" href="/ts/common-elements/charts/stacked-area-chart/stacked-area-chart.html">
       <link rel="import" href="/ts/common-elements/charts/parallel-chart/parallel-chart.html">
       <link rel="import" href="/ts/common-elements/charts/candlestick-chart/candlestick-chart.html">

       <!--
       `chart-exampless` gives some examples of how to use chart elements in the
       common-elements package.
       -->
       <dom-module id="chart-exampless">
         <template>
           <style include="reset-css"></style>
           <style include="iron-flex-layout-attributes"></style>
           <link rel="stylesheet" type="text/css" href="css/chart-exampless-min.css?__inline=true">

         </template>
         <script src="javascript/chart-exampless-min.js?__inline=true"></script>
       </dom-module>


We'll add more stuff as we go along.

Now edit /src/html/elements/chart-examples/css/chart-examples.scss

.. container:: toggle

    .. container:: header

        **/src/html/elements/chart-examples/css/chart-examples.scss**

    .. literalinclude:: /../supervisor/src/html/elements/chart-examples/css/chart-examples.scss
       :language: scss
       :linenos:


Now execute Grunt to build our new Polymer element.

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

**********************************
Make the chart-examples panel
**********************************

Make a new c++ file /src/common/panels/ChartExamples.cc

.. container:: toggle

    .. container:: header

        **/src/common/panels/ChartExamples.cc**

    .. code-block:: cpp
       :linenos:

       #include "subsystem/supervisor/panels/ChartExamples.h"
       #include "ajax/PolymerElement.h"
       #include "json/json.h"
       #include <math.h>

       using namespace subsystempanels;
       ChartExamples::ChartExamples( tsframework::CellAbstractContext* context, log4cplus::Logger& logger)
       :tsframework::CellPanel(context,logger) {
         logger_ = log4cplus::Logger::getInstance(logger.getName() +".ChartExamples");
       }

       void ChartExamples::layout(cgicc::Cgicc& cgi) {
         remove();
         add(new ajax::PolymerElement("chart-examples"));
       }



Make the /include/subsystem/supervisor/panels/ChartExamples.h file.

.. container:: toggle

    .. container:: header

        **/include/subsystem/supervisor/panels/ChartExamples.h**

    .. code-block:: cpp
       :linenos:

       #ifndef _subsystem_supervisor_panels_ChartExamples_h_
       #define _subsystem_supervisor_panels_ChartExamples_h_

       #include "ts/framework/CellPanel.h"
       #include "log4cplus/logger.h"
       #include "cgicc/Cgicc.h"

       namespace subsystempanels {
         class ChartExamples: public tsframework::CellPanel {
           public:
             ChartExamples(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
             void layout(cgicc::Cgicc& cgi);
           private:
         };
       }
       #endif



Register the new class in the Makefile.

.. code-block:: makefile
    :linenos:

    Sources=\
    	version.cc\
    	Cell.cc\
    	CellContext.cc\
    	Configuration.cc\
    	...
    	panels/ChartExamples.cc \
    	...

Now register your new panel in the menu so users can access it.

Edit src/common/Cell.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/ChartExamples.h"
    ...
    void subsystemsupervisor::Cell::init() {
      ...
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      ...
      panelF->add<subsystempanels::ChartExamples>("Chart examples");

Now you can compile your cell and you should see the "Form example"
panel in the menu under the 'control-panels' section.

**********************************
Chart1: A line chart
**********************************

This example will make a basic line chart, and will show a simple line.
Data is rendered client-side for simplicity.

Also note the use of the `math-equation` element do display LaTeX math.

HTML
#################

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/chart-examples/chart-examples.html**

    .. code-block:: html
       :linenos:

       <dom-module id="chart-examples">
         <template>

           <div horizontal layout>
             <paper-material elevation="1" flex vertical layout>
               <math-equation big>f(x) = x^5 + 3.5x^4 - 2.4x^3 -12.5x^2 + 1.5x + 9</math-equation>
               <line-chart flex data="{{chart1data}}" config="{{chart1config}}" configure-chart="{{chart1JSConfig}}"></line-chart>
             </paper-material>
           </div>

         </template>
       </dom-module>


JavaScript
##################

For this first example, we generate our data client side.

Edit /src/html/elements/chart-examples/javascript/chart-examples.js

.. container:: toggle

    .. container:: header

        **/src/html/elements/chart-examples/javascript/chart-examples.js**

    .. code-block:: javascript
       :linenos:

        Polymer({
          is: 'chart-examples',
          properties: {
            chart1data: {
              type: Array,
              value: function() {
                var data = []
                for (var i = -3.5; i < 2.2; i = i+0.01) {
                  data.push({
                    x: i,
                    y: Math.pow(i,5) + 3.5*Math.pow(i,4) - 2.5*Math.pow(i,3) - 12.5*Math.pow(i,2) + 1.5*i + 9
                  });
                }

                return [
                  {
                    values: data,
                    key: 'fifth degree polynomial',
                    color: '#00671A'
                  }
                ];
              }
            },

            chart1config: {
              type: Object,
              value: function() {
                return {
                  useInteractiveGuideline: true,
                  margin: {
                    left: 70
                  }
                }
              }
            },

            chart1JSConfig: {
              type: Function,
              value: function() {
                return function() {
                  var superscript = "⁰¹²³⁴⁵⁶⁷⁸⁹";
                  var formatPower = function(d) {
                    return (d + "").split("").map(function(c) {
                      return superscript[c];
                    }).join("");
                  };
                  this._chart.xAxis
                    .axisLabel("x")
                    .tickFormat(d3.format(',.2f'));

                  this._chart.yAxis
                    .axisLabel("x" + formatPower(5) + " + 3.5x" + formatPower(4) + " - 2.5x" + formatPower(3) + " - 12.5x" + formatPower(2) + " + 1.5x + 9")
                    .tickFormat(d3.format(',.2f'));
                }
              }
            }
          }
        });


The result will look like this:

.. image:: chart1.png
  :width: 700px

**********************************
Chart2: A more advanced line chart
**********************************

This line chart will feature multiple plots, missing data gaps, and a date for
the x axis.

Data is still generated client side.

HTML & Javascript
#################

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

   .. container:: header

       **/src/html/elements/chart-examples/chart-examples.html**

   .. code-block:: html
      :linenos:

      <dom-module id="chart-examples">
        <template>

          <div horizontal layout>
            <paper-material elevation="1" flex vertical layout>
              <math-equation big>f(x) = x^5 + 3.5x^4 - 2.4x^3 -12.5x^2 + 1.5x + 9</math-equation>
              <line-chart flex data="{{chart1data}}" config="{{chart1config}}" configure-chart="{{chart1JSConfig}}"></line-chart>
            </paper-material>
            <paper-material elevation="1" flex horizontal layout>
              <line-chart flex data="{{chart2data}}" config="{{chart1config}}" configure-chart="{{chart2JSConfig}}"></line-chart>
            </paper-material>
          </div>

        </template>
      </dom-module>

Note that we put the second chart next to the first chart, this to demonstrate
the ability to use the layout attributes.


Now edit /src/html/elements/chart-examples/javascript/chart-examples.js

.. container:: toggle

   .. container:: header

       **/src/html/elements/chart-examples/javascript/chart-examples.js**

   .. code-block:: javascript
      :linenos:

      Polymer({
        is: 'chart-examples',
        properties: {
          chart2data: {
            type: Array,
            value: function() {
              var sin = [],
                  sin2 = [],
                  cos = [],
                  rand = [],
                  rand2 = []
                  ;
              for (var i = 0; i < 100; i++) {
                  sin.push({x: i, y: i % 10 == 5 ? null : Math.sin(i/10) }); //the nulls are to show how defined works
                  sin2.push({x: i, y: Math.sin(i/5) * 0.4 - 0.25});
                  cos.push({x: i, y: .5 * Math.cos(i/10)});
                  rand.push({x:i, y: Math.random() / 10});
                  rand2.push({x: i, y: Math.cos(i/10) + Math.random() / 10 })
              }
              return [
                  {
                      area: true,
                      values: sin,
                      key: "Sine Wave",
                      color: "#ff7f0e",
                      strokeWidth: 4,
                      classed: 'dashed'
                  },
                  {
                      values: cos,
                      key: "Cosine Wave",
                      color: "#2ca02c"
                  },
                  {
                      values: rand,
                      key: "Random Points",
                      color: "#2222ff"
                  },
                  {
                      values: rand2,
                      key: "Random Cosine",
                      color: "#667711",
                      strokeWidth: 3.5
                  },
                  {
                      area: true,
                      values: sin2,
                      key: "Fill opacity",
                      color: "#EF9CFB",
                      fillOpacity: .1
                  }
              ];
            },

          },

          chart2JSConfig: {
            type: Function,
            value: function() {
              return function() {
                this._chart.xAxis
                  .showMaxMin(false)
                  .tickFormat(function(d) { return d3.time.format('%x')(new Date(d)) });

                this._chart.yAxis
                  .tickFormat(d3.format(',.2f'));
              }
            }
          }
        }
      });

The result will look like this:

.. image:: chart2.png
   :width: 700px

**********************************
Chart3: A cumulative line chart
**********************************

This type of chart shows a relative change of data.
You will notice the data in this chart starts from 0.
The user can also click on any point to instruct the chart to take that point
in the x axis as the new relative zero.


HTML & Javascript
#################

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

   .. container:: header

       **/src/html/elements/chart-examples/chart-examples.html**

   .. code-block:: html
      :linenos:

      <dom-module id="chart-examples">
        <template>

          <div horizontal layout>
            <paper-material elevation="1" flex horizontal layout>
              <cumulative-line-chart flex data="{{chart3data}}" config="{{chart3config}}" configure-chart="{{chart3JSConfig}}"></cumulative-line-chart>
            </div>
          </div>

        </template>
      </dom-module>


Now edit /src/html/elements/chart-examples/javascript/chart-examples.js

.. container:: toggle

   .. container:: header

       **/src/html/elements/chart-examples/javascript/chart-examples.js**

   .. code-block:: javascript
      :linenos:

      Polymer({
        properties: {
          chart3data: {
            type: Array,
            value: function() {
              return [
                {
                    key: "Long",
                    values: [ [ 1083297600000 , -2.974623048543] , [ 1085976000000 , -1.7740300785979] , [ 1088568000000 , 4.4681318138177] , [ 1091246400000 , 7.0242541001353] , [ 1093924800000 , 7.5709603667586] , [ 1096516800000 , 20.612245065736] , [ 1099195200000 , 21.698065237316] , [ 1101790800000 , 40.501189458018] , [ 1104469200000 , 50.464679413194] , [ 1107147600000 , 48.917421973355] , [ 1109566800000 , 63.750936549160] , [ 1112245200000 , 59.072499126460] , [ 1114833600000 , 43.373158880492] , [ 1117512000000 , 54.490918947556] , [ 1120104000000 , 56.661178852079] , [ 1122782400000 , 73.450103545496] , [ 1125460800000 , 71.714526354907] , [ 1128052800000 , 85.221664349607] , [ 1130734800000 , 77.769261392481] , [ 1133326800000 , 95.966528716500] , [ 1136005200000 , 107.59132116397] , [ 1138683600000 , 127.25740096723] , [ 1141102800000 , 122.13917498830] , [ 1143781200000 , 126.53657279774] , [ 1146369600000 , 132.39300992970] , [ 1149048000000 , 120.11238242904] , [ 1151640000000 , 118.41408917750] , [ 1154318400000 , 107.92918924621] , [ 1156996800000 , 110.28057249569] , [ 1159588800000 , 117.20485334692] , [ 1162270800000 , 141.33556756948] , [ 1164862800000 , 159.59452727893] , [ 1167541200000 , 167.09801853304] , [ 1170219600000 , 185.46849659215] , [ 1172638800000 , 184.82474099990] , [ 1175313600000 , 195.63155213887] , [ 1177905600000 , 207.40597044171] , [ 1180584000000 , 230.55966698196] , [ 1183176000000 , 239.55649035292] , [ 1185854400000 , 241.35915085208] , [ 1188532800000 , 239.89428956243] , [ 1191124800000 , 260.47781917715] , [ 1193803200000 , 276.39457482225] , [ 1196398800000 , 258.66530682672] , [ 1199077200000 , 250.98846121893] , [ 1201755600000 , 226.89902618127] , [ 1204261200000 , 227.29009273807] , [ 1206936000000 , 218.66476654350] , [ 1209528000000 , 232.46605902918] , [ 1212206400000 , 253.25667081117] , [ 1214798400000 , 235.82505363925] , [ 1217476800000 , 229.70112774254] , [ 1220155200000 , 225.18472705952] , [ 1222747200000 , 189.13661746552] , [ 1225425600000 , 149.46533007301] , [ 1228021200000 , 131.00340772114] , [ 1230699600000 , 135.18341728866] , [ 1233378000000 , 109.15296887173] , [ 1235797200000 , 84.614772549760] , [ 1238472000000 , 100.60810015326] , [ 1241064000000 , 141.50134895610] , [ 1243742400000 , 142.50405083675] , [ 1246334400000 , 139.81192372672] , [ 1249012800000 , 177.78205544583] , [ 1251691200000 , 194.73691933074] , [ 1254283200000 , 209.00838460225] , [ 1256961600000 , 198.19855877420] , [ 1259557200000 , 222.37102417812] , [ 1262235600000 , 234.24581081250] , [ 1264914000000 , 228.26087689346] , [ 1267333200000 , 248.81895126250] , [ 1270008000000 , 270.57301075186] , [ 1272600000000 , 292.64604322550] , [ 1275278400000 , 265.94088520518] , [ 1277870400000 , 237.82887467569] , [ 1280548800000 , 265.55973314204] , [ 1283227200000 , 248.30877330928] , [ 1285819200000 , 278.14870066912] , [ 1288497600000 , 292.69260960288] , [ 1291093200000 , 300.84263809599] , [ 1293771600000 , 326.17253914628] , [ 1296450000000 , 337.69335966505] , [ 1298869200000 , 339.73260965121] , [ 1301544000000 , 346.87865120765] , [ 1304136000000 , 347.92991526628] , [ 1306814400000 , 342.04627502669] , [ 1309406400000 , 333.45386231233] , [ 1312084800000 , 323.15034181243] , [ 1314763200000 , 295.66126882331] , [ 1317355200000 , 251.48014579253] , [ 1320033600000 , 295.15424257905] , [ 1322629200000 , 294.54766764397] , [ 1325307600000 , 295.72906119051] , [ 1327986000000 , 325.73351347613] , [ 1330491600000 , 340.16106061186] , [ 1333166400000 , 345.15514071490] , [ 1335758400000 , 337.10259395679] , [ 1338436800000 , 318.68216333837] , [ 1341028800000 , 317.03683945246] , [ 1343707200000 , 318.53549659997] , [ 1346385600000 , 332.85381464104] , [ 1348977600000 , 337.36534373477] , [ 1351656000000 , 350.27872156161] , [ 1354251600000 , 349.45128876100]]
                    ,
                    mean: 250
                },
                {
                    key: "Short",
                    values: [ [ 1083297600000 , -0.77078283705125] , [ 1085976000000 , -1.8356366650335] , [ 1088568000000 , -5.3121322073127] , [ 1091246400000 , -4.9320975829662] , [ 1093924800000 , -3.9835408823225] , [ 1096516800000 , -6.8694685316805] , [ 1099195200000 , -8.4854877428545] , [ 1101790800000 , -15.933627197384] , [ 1104469200000 , -15.920980069544] , [ 1107147600000 , -12.478685045651] , [ 1109566800000 , -17.297761889305] , [ 1112245200000 , -15.247129891020] , [ 1114833600000 , -11.336459046839] , [ 1117512000000 , -13.298990907415] , [ 1120104000000 , -16.360027000056] , [ 1122782400000 , -18.527929522030] , [ 1125460800000 , -22.176516738685] , [ 1128052800000 , -23.309665368330] , [ 1130734800000 , -21.629973409748] , [ 1133326800000 , -24.186429093486] , [ 1136005200000 , -29.116707312531] , [ 1138683600000 , -37.188037874864] , [ 1141102800000 , -34.689264821198] , [ 1143781200000 , -39.505932105359] , [ 1146369600000 , -45.339572492759] , [ 1149048000000 , -43.849353192764] , [ 1151640000000 , -45.418353922571] , [ 1154318400000 , -44.579281059919] , [ 1156996800000 , -44.027098363370] , [ 1159588800000 , -41.261306759439] , [ 1162270800000 , -47.446018534027] , [ 1164862800000 , -53.413782948909] , [ 1167541200000 , -50.700723647419] , [ 1170219600000 , -56.374090913296] , [ 1172638800000 , -61.754245220322] , [ 1175313600000 , -66.246241587629] , [ 1177905600000 , -75.351650899999] , [ 1180584000000 , -81.699058262032] , [ 1183176000000 , -82.487023368081] , [ 1185854400000 , -86.230055113277] , [ 1188532800000 , -84.746914818507] , [ 1191124800000 , -100.77134971977] , [ 1193803200000 , -109.95435565947] , [ 1196398800000 , -99.605672965057] , [ 1199077200000 , -99.607249394382] , [ 1201755600000 , -94.874614950188] , [ 1204261200000 , -105.35899063105] , [ 1206936000000 , -106.01931193802] , [ 1209528000000 , -110.28883571771] , [ 1212206400000 , -119.60256203030] , [ 1214798400000 , -115.62201315802] , [ 1217476800000 , -106.63824185202] , [ 1220155200000 , -99.848746318951] , [ 1222747200000 , -85.631219602987] , [ 1225425600000 , -63.547909262067] , [ 1228021200000 , -59.753275364457] , [ 1230699600000 , -63.874977883542] , [ 1233378000000 , -56.865697387488] , [ 1235797200000 , -54.285579501988] , [ 1238472000000 , -56.474659581885] , [ 1241064000000 , -63.847137745644] , [ 1243742400000 , -68.754247867325] , [ 1246334400000 , -69.474257009155] , [ 1249012800000 , -75.084828197067] , [ 1251691200000 , -77.101028237237] , [ 1254283200000 , -80.454866854387] , [ 1256961600000 , -78.984349952220] , [ 1259557200000 , -83.041230807854] , [ 1262235600000 , -84.529748348935] , [ 1264914000000 , -83.837470195508] , [ 1267333200000 , -87.174487671969] , [ 1270008000000 , -90.342293007487] , [ 1272600000000 , -93.550928464991] , [ 1275278400000 , -85.833102140765] , [ 1277870400000 , -79.326501831592] , [ 1280548800000 , -87.986196903537] , [ 1283227200000 , -85.397862121771] , [ 1285819200000 , -94.738167050020] , [ 1288497600000 , -98.661952897151] , [ 1291093200000 , -99.609665952708] , [ 1293771600000 , -103.57099836183] , [ 1296450000000 , -104.04353411322] , [ 1298869200000 , -108.21382792587] , [ 1301544000000 , -108.74006900920] , [ 1304136000000 , -112.07766650960] , [ 1306814400000 , -109.63328199118] , [ 1309406400000 , -106.53578966772] , [ 1312084800000 , -103.16480871469] , [ 1314763200000 , -95.945078001828] , [ 1317355200000 , -81.226687340874] , [ 1320033600000 , -90.782206596168] , [ 1322629200000 , -89.484445370113] , [ 1325307600000 , -88.514723135326] , [ 1327986000000 , -93.381292724320] , [ 1330491600000 , -97.529705609172] , [ 1333166400000 , -99.520481439189] , [ 1335758400000 , -99.430184898669] , [ 1338436800000 , -93.349934521973] , [ 1341028800000 , -95.858475286491] , [ 1343707200000 , -95.522755836605] , [ 1346385600000 , -98.503848862036] , [ 1348977600000 , -101.49415251896] , [ 1351656000000 , -101.50099325672] , [ 1354251600000 , -99.487094927489]]
                    ,
                    mean: -60
                },
                {
                    key: "Gross",
                    mean: 125,
                    values: [ [ 1083297600000 , -3.7454058855943] , [ 1085976000000 , -3.6096667436314] , [ 1088568000000 , -0.8440003934950] , [ 1091246400000 , 2.0921565171691] , [ 1093924800000 , 3.5874194844361] , [ 1096516800000 , 13.742776534056] , [ 1099195200000 , 13.212577494462] , [ 1101790800000 , 24.567562260634] , [ 1104469200000 , 34.543699343650] , [ 1107147600000 , 36.438736927704] , [ 1109566800000 , 46.453174659855] , [ 1112245200000 , 43.825369235440] , [ 1114833600000 , 32.036699833653] , [ 1117512000000 , 41.191928040141] , [ 1120104000000 , 40.301151852023] , [ 1122782400000 , 54.922174023466] , [ 1125460800000 , 49.538009616222] , [ 1128052800000 , 61.911998981277] , [ 1130734800000 , 56.139287982733] , [ 1133326800000 , 71.780099623014] , [ 1136005200000 , 78.474613851439] , [ 1138683600000 , 90.069363092366] , [ 1141102800000 , 87.449910167102] , [ 1143781200000 , 87.030640692381] , [ 1146369600000 , 87.053437436941] , [ 1149048000000 , 76.263029236276] , [ 1151640000000 , 72.995735254929] , [ 1154318400000 , 63.349908186291] , [ 1156996800000 , 66.253474132320] , [ 1159588800000 , 75.943546587481] , [ 1162270800000 , 93.889549035453] , [ 1164862800000 , 106.18074433002] , [ 1167541200000 , 116.39729488562] , [ 1170219600000 , 129.09440567885] , [ 1172638800000 , 123.07049577958] , [ 1175313600000 , 129.38531055124] , [ 1177905600000 , 132.05431954171] , [ 1180584000000 , 148.86060871993] , [ 1183176000000 , 157.06946698484] , [ 1185854400000 , 155.12909573880] , [ 1188532800000 , 155.14737474392] , [ 1191124800000 , 159.70646945738] , [ 1193803200000 , 166.44021916278] , [ 1196398800000 , 159.05963386166] , [ 1199077200000 , 151.38121182455] , [ 1201755600000 , 132.02441123108] , [ 1204261200000 , 121.93110210702] , [ 1206936000000 , 112.64545460548] , [ 1209528000000 , 122.17722331147] , [ 1212206400000 , 133.65410878087] , [ 1214798400000 , 120.20304048123] , [ 1217476800000 , 123.06288589052] , [ 1220155200000 , 125.33598074057] , [ 1222747200000 , 103.50539786253] , [ 1225425600000 , 85.917420810943] , [ 1228021200000 , 71.250132356683] , [ 1230699600000 , 71.308439405118] , [ 1233378000000 , 52.287271484242] , [ 1235797200000 , 30.329193047772] , [ 1238472000000 , 44.133440571375] , [ 1241064000000 , 77.654211210456] , [ 1243742400000 , 73.749802969425] , [ 1246334400000 , 70.337666717565] , [ 1249012800000 , 102.69722724876] , [ 1251691200000 , 117.63589109350] , [ 1254283200000 , 128.55351774786] , [ 1256961600000 , 119.21420882198] , [ 1259557200000 , 139.32979337027] , [ 1262235600000 , 149.71606246357] , [ 1264914000000 , 144.42340669795] , [ 1267333200000 , 161.64446359053] , [ 1270008000000 , 180.23071774437] , [ 1272600000000 , 199.09511476051] , [ 1275278400000 , 180.10778306442] , [ 1277870400000 , 158.50237284410] , [ 1280548800000 , 177.57353623850] , [ 1283227200000 , 162.91091118751] , [ 1285819200000 , 183.41053361910] , [ 1288497600000 , 194.03065670573] , [ 1291093200000 , 201.23297214328] , [ 1293771600000 , 222.60154078445] , [ 1296450000000 , 233.35556801977] , [ 1298869200000 , 231.22452435045] , [ 1301544000000 , 237.84432503045] , [ 1304136000000 , 235.55799131184] , [ 1306814400000 , 232.11873570751] , [ 1309406400000 , 226.62381538123] , [ 1312084800000 , 219.34811113539] , [ 1314763200000 , 198.69242285581] , [ 1317355200000 , 168.90235629066] , [ 1320033600000 , 202.64725756733] , [ 1322629200000 , 203.05389378105] , [ 1325307600000 , 204.85986680865] , [ 1327986000000 , 229.77085616585] , [ 1330491600000 , 239.65202435959] , [ 1333166400000 , 242.33012622734] , [ 1335758400000 , 234.11773262149] , [ 1338436800000 , 221.47846307887] , [ 1341028800000 , 216.98308827912] , [ 1343707200000 , 218.37781386755] , [ 1346385600000 , 229.39368622736] , [ 1348977600000 , 230.54656412916] , [ 1351656000000 , 243.06087025523] , [ 1354251600000 , 244.24733578385]]
                },
                {
                    key: "S&P 1500",
                    values: [ [ 1083297600000 , -1.7798428181819] , [ 1085976000000 , -0.36883324836999] , [ 1088568000000 , 1.7312581046040] , [ 1091246400000 , -1.8356125950460] , [ 1093924800000 , -1.5396564170877] , [ 1096516800000 , -0.16867791409247] , [ 1099195200000 , 1.3754263993413] , [ 1101790800000 , 5.8171640898041] , [ 1104469200000 , 9.4350145241608] , [ 1107147600000 , 6.7649081510160] , [ 1109566800000 , 9.1568499314776] , [ 1112245200000 , 7.2485090994419] , [ 1114833600000 , 4.8762222306595] , [ 1117512000000 , 8.5992339354652] , [ 1120104000000 , 9.0896517982086] , [ 1122782400000 , 13.394644048577] , [ 1125460800000 , 12.311842010760] , [ 1128052800000 , 13.221003650717] , [ 1130734800000 , 11.218481009206] , [ 1133326800000 , 15.565352598445] , [ 1136005200000 , 15.623703865926] , [ 1138683600000 , 19.275255326383] , [ 1141102800000 , 19.432433717836] , [ 1143781200000 , 21.232881244655] , [ 1146369600000 , 22.798299192958] , [ 1149048000000 , 19.006125095476] , [ 1151640000000 , 19.151889158536] , [ 1154318400000 , 19.340022855452] , [ 1156996800000 , 22.027934841859] , [ 1159588800000 , 24.903300681329] , [ 1162270800000 , 29.146492833877] , [ 1164862800000 , 31.781626082589] , [ 1167541200000 , 33.358770738428] , [ 1170219600000 , 35.622684613497] , [ 1172638800000 , 33.332821711366] , [ 1175313600000 , 34.878748635832] , [ 1177905600000 , 40.582332613844] , [ 1180584000000 , 45.719535502920] , [ 1183176000000 , 43.239344722386] , [ 1185854400000 , 38.550955100342] , [ 1188532800000 , 40.585368816283] , [ 1191124800000 , 45.601374057981] , [ 1193803200000 , 48.051404337892] , [ 1196398800000 , 41.582581696032] , [ 1199077200000 , 40.650580792748] , [ 1201755600000 , 32.252222066493] , [ 1204261200000 , 28.106390258553] , [ 1206936000000 , 27.532698196687] , [ 1209528000000 , 33.986390463852] , [ 1212206400000 , 36.302660526438] , [ 1214798400000 , 25.015574480172] , [ 1217476800000 , 23.989494069029] , [ 1220155200000 , 25.934351445531] , [ 1222747200000 , 14.627592011699] , [ 1225425600000 , -5.2249403809749] , [ 1228021200000 , -12.330933408050] , [ 1230699600000 , -11.000291508188] , [ 1233378000000 , -18.563864948088] , [ 1235797200000 , -27.213097001687] , [ 1238472000000 , -20.834133840523] , [ 1241064000000 , -12.717886701719] , [ 1243742400000 , -8.1644613083526] , [ 1246334400000 , -7.9108408918201] , [ 1249012800000 , -0.77002391591209] , [ 1251691200000 , 2.8243816569672] , [ 1254283200000 , 6.8761411421070] , [ 1256961600000 , 4.5060912230294] , [ 1259557200000 , 10.487179794349] , [ 1262235600000 , 13.251375597594] , [ 1264914000000 , 9.2207594803415] , [ 1267333200000 , 12.836276936538] , [ 1270008000000 , 19.816793904978] , [ 1272600000000 , 22.156787167211] , [ 1275278400000 , 12.518039090576] , [ 1277870400000 , 6.4253587440854] , [ 1280548800000 , 13.847372028409] , [ 1283227200000 , 8.5454736090364] , [ 1285819200000 , 18.542801953304] , [ 1288497600000 , 23.037064683183] , [ 1291093200000 , 23.517422401888] , [ 1293771600000 , 31.804723416068] , [ 1296450000000 , 34.778247386072] , [ 1298869200000 , 39.584883855230] , [ 1301544000000 , 40.080647664875] , [ 1304136000000 , 44.180050667889] , [ 1306814400000 , 42.533535927221] , [ 1309406400000 , 40.105374449011] , [ 1312084800000 , 37.014659267156] , [ 1314763200000 , 29.263745084262] , [ 1317355200000 , 19.637463417584] , [ 1320033600000 , 33.157645345770] , [ 1322629200000 , 32.895053150988] , [ 1325307600000 , 34.111544824647] , [ 1327986000000 , 40.453985817473] , [ 1330491600000 , 46.435700783313] , [ 1333166400000 , 51.062385488671] , [ 1335758400000 , 50.130448220658] , [ 1338436800000 , 41.035476682018] , [ 1341028800000 , 46.591932296457] , [ 1343707200000 , 48.349391180634] , [ 1346385600000 , 51.913011286919] , [ 1348977600000 , 55.747238313752] , [ 1351656000000 , 52.991824077209] , [ 1354251600000 , 49.556311883284]]
                }
              ];
            }
          },

          chart3config: {
            type: Object,
            value: function() {
              return {
                showLegend : true,

                x: function(d) { return d[0] },
                y: function(d) { return d[1]/100 },
                color: d3.scale.category10().range(),
                average: function(d) { return d.mean/100; },
                duration: 300,
                clipVoronoi: false,
                useInteractiveGuideline: true,

                //showcase that the config is idiot-proof
                donut : true
              }
            }
          },

          chart3JSConfig: {
            type: Function,
            value: function() {
              return function() {
                this._chart.xAxis
                  .tickFormat(function(d) { return d3.time.format('%m/%d/%y')(new Date(d)) });

                this._chart.yAxis
                  .tickFormat(d3.format(',.1%'));
              }
            }
          }
        }
      });


The result will look like this:

.. image:: chart3.png
   :width: 700px

**********************************
Chart4: A focusable line chart
**********************************

This line chart features a bar on the bottom the user can use to zoom in or out
of the dataset.

Also this chart has been configured with an upper limit on the y axis to prevent
very high initial values from scaling down the rest of the chart.

This example will be the first to have data generated on server-side.

C++ code
########

Edit /src/common/panels/ChartExamples.cc

.. container:: toggle

    .. container:: header

        **/src/common/panels/ChartExamples.cc**

    .. code-block:: cpp
       :linenos:

       void ChartExamples::layout(cgicc::Cgicc& cgi) {
         remove();
         setEvent("getChart4", ajax::Eventable::OnClick, this, &ChartExamples::getChart4);
         add(new ajax::PolymerElement("chart-examples"));
       }

       void ChartExamples::getChart4(cgicc::Cgicc& cgi,std::ostream& out) {
         int pix = 0;
         Json::Value root(Json::arrayValue);

         Json::Value stream0;
         stream0["area"] = true;
         stream0["color"] = "#00695C";
         stream0["key"] = "prime density π(x)/x";
         Json::Value stream0values(Json::arrayValue);
         for (int i = 2; i < 200; i++) {
           if (ChartExamples::isPrime(i)) {
             pix += 1;
           }
           Json::Value item;
           item["x"] = i;
           item["y"] = (float)pix/(float)i;
           stream0values.append(item);
         }
         stream0["values"] = stream0values;
         root.append(stream0);

         Json::Value stream1;
         stream1["area"] = false;
         stream1["color"] = "#E57373";
         stream1["key"] = "1/(ln(x)-1)";
         Json::Value stream1values(Json::arrayValue);
         for (int i = 2; i < 200; i++) {
           Json::Value item;
           item["x"] = i;
           item["y"] = (float)1/(float)(log(i) - 1);
           stream1values.append(item);
         }
         stream1["values"] = stream1values;
         root.append(stream1);

         out << root;
       }

       bool ChartExamples::isPrime(int num) {
         if (num <= 1)
           return false;
         else if (num == 2)
           return true;
         else if (num % 2 == 0)
           return false;
         else {
           bool prime = true;
           int divisor = 3;
           double num_d = static_cast<double>(num);
           int upperLimit = static_cast<int>(sqrt(num_d) + 1);

           while (divisor <= upperLimit) {
             if (num % divisor == 0)
               prime = false;
             divisor += 2;
           }
           return prime;
         }
       }

Edit /include/subsystem/supervisor/panels/ChartExamples.h.

.. container:: toggle

   .. container:: header

       **/include/subsystem/supervisor/panels/ChartExamples.h**

   .. code-block:: cpp
      :emphasize-lines: 4-5
      :linenos:

      namespace subsystempanels {
        class ChartExamples: public tsframework::CellPanel {
          private:
            void getChart4(cgicc::Cgicc& cgi,std::ostream& out);
            bool isPrime(int num);
        };
      }


HTML & Javascript
#################

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

   .. container:: header

       **/src/html/elements/chart-examples/chart-examples.html**

   .. code-block:: html
      :linenos:

      <dom-module id="chart-examples">
        <template>

          <div horizontal layout>
            <paper-material elevation="1" flex vertical layout>
              <ts-ajax data="{{chart4data}}" callback="getChart4" handle-as="json" auto></ts-ajax>
              <p>
                The number of primes with value lower than x ( <math-equation big inline>\pi(x)</math-equation> ) can be approximated using <math-equation big inline>\frac{1}{\ln(x) -1}</math-equation>
              </p>
              <focus-line-chart flex data="{{chart4data}}" config="{{chart4config}}" configure-chart="{{chart4JSConfig}}"></focus-line-chart>
            </div>
          </div>

        </template>
      </dom-module>


Now edit /src/html/elements/chart-examples/javascript/chart-examples.js

.. container:: toggle

   .. container:: header

       **/src/html/elements/chart-examples/javascript/chart-examples.js**

   .. code-block:: javascript
      :linenos:

      Polymer({
        properties: {
          chart4data: {
            type: Array,
            value: function() {
              return [];
            }
          },

          chart4config: {
            type: Object,
            value: function() {
              return {
                brushExtent: [2,100],
                useInteractiveGuideline: true
              }
            }
          },

          chart4JSConfig: {
            type: Function,
            value: function() {
              return function() {
                this._chart.xAxis
                  .tickFormat(d3.format(',f'));
                this._chart.x2Axis
                  .tickFormat(d3.format(',f'));

                this._chart.yAxis
                  .tickFormat(d3.format(',.2f'));
                this._chart.y2Axis
                  .tickFormat(d3.format(',.2f'));

                this._chart.yDomain([0.2,1]);
              }
            }
          }
        }
      });

The result will look like this:

.. image:: chart4.png
   :width: 700px

**********************************
Chart5: Bar charts
**********************************

For simplicity we will generate the data on client-side again.

In this example we will make a bar chart, a horizontal bar chart, a
discrete bar chart, and a historical bar chart.

HTML & Javascript
#################

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

   .. container:: header

       **/src/html/elements/chart-examples/chart-examples.html**

   .. code-block:: html
      :linenos:

      <dom-module id="chart-examples">
        <template>

          <div vertical layout>
            <div horizontal layout flex>
              <paper-material elevation="1" flex horizontal layout>
                <discrete-bar-chart flex data="{{chart5data}}" config="{{chart5config}}" configure-chart="{{chart5JSConfig}}"></discrete-bar-chart>
              </paper-material>
              <paper-material elevation="1" flex horizontal layout>
                <historical-bar-chart flex data="{{chart6data}}" config="{{chart6config}}" configure-chart="{{chart6JSConfig}}"></historical-bar-chart>
              </paper-material>
            </div>
            <div horizontal layout flex>
              <paper-material elevation="1" flex horizontal layout>
                <stacked-bar-chart flex data="{{chart5data}}" config="{{chart5config}}" configure-chart="{{chart5JSConfig}}"></stacked-bar-chart>
              </paper-material>
              <paper-material elevation="1" flex horizontal layout>
                <horizontal-stacked-bar-chart flex data="{{chart5data}}" config="{{chart5config}}" configure-chart="{{chart5JSConfig}}"></horizontal-stacked-bar-chart>
              </paper-material>
            </div>
          </div>

        </template>
      </dom-module>


Now edit /src/html/elements/chart-examples/javascript/chart-examples.js

.. container:: toggle

   .. container:: header

       **/src/html/elements/chart-examples/javascript/chart-examples.js**

   .. code-block:: javascript
      :linenos:

      Polymer({
        properties: {
          chart5data: {
            type: Array,
            value: function() {
              return[{
                key: "Cumulative Return",
                values: [{
                  "label": "A",
                  "value": 29.765957771107
                }, {
                  "label": "B",
                  "value": 0
                }, {
                  "label": "C",
                  "value": 32.807804682612
                }, {
                  "label": "D",
                  "value": 196.45946739256
                }, {
                  "label": "E",
                  "value": 0.19434030906893
                }, {
                  "label": "F",
                  "value": 98.079782601442
                }, {
                  "label": "G",
                  "value": 13.925743130903
                }, {
                  "label": "H",
                  "value": 5.1387322875705
                }]
              }];
            }
          },

          chart5config: {
            type: Object,
            value: function() {
              return {
                duration: 250,
                x: function(d) { return d.label },
                y: function(d) { return d.value },
              }
            }
          },

          chart5JSConfig: {
            type: Function,
            value: function() {
              return function() {
              }
            }
          },

          chart6data: {
            type: Array,
            value: function() {
              var sin = [];
              for (var i = 0; i < 100; i++) {
                sin.push({x: i, y: Math.sin(i/10) * Math.random() * 100});
              }
              return [{
                values: sin,
                key: "Sine Wave",
                color: "#ff7f0e"
              }];
            }
          },

          chart6config: {
            type: Object,
            value: function() {
              return {
                margin: {left: 100, bottom: 100},
                useInteractiveGuideline: true,
                duration: 250
              }
            }
          },

          chart6JSConfig: {
            type: Function,
            value: function() {
              return function() {
                this._chart.xAxis
                  .axisLabel("Time (s)")
                  .tickFormat(d3.format(',.1f'));
                this._chart.yAxis
                  .axisLabel('Voltage (v)')
                  .tickFormat(d3.format(',.2f'));
                this._chart.showXAxis(true);
              }
            }
          }
        }
      });


The result will look like this:

.. image:: chart5.png
   :width: 700px


**********************************
Chart6: Pie charts
**********************************

HTML & Javascript
#################

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

  .. container:: header

      **/src/html/elements/chart-examples/chart-examples.html**

  .. code-block:: html
     :linenos:

     <dom-module id="chart-examples">
       <template>

         <div vertical layout>
           <div horizontal layout flex>
             <paper-material elevation="1" flex horizontal layout>
               <pie-chart flex data="{{chart7data}}" config="{{chart7config}}"></pie-chart>
             </paper-material>
             <paper-material elevation="1" flex horizontal layout>
               <pie-chart flex data="{{chart7data}}" config="{{chart7config}}" configure-chart="{{chart7JSConfig}}"></pie-chart>
             </paper-material>
           </div>
         </div>

       </template>
     </dom-module>


Now edit /src/html/elements/chart-examples/javascript/chart-examples.js

.. container:: toggle

  .. container:: header

      **/src/html/elements/chart-examples/javascript/chart-examples.js**

  .. code-block:: javascript
     :linenos:

     Polymer({
       properties: {
         chart7data: {
           type: Array,
           value: function() {
             var sin = [];
             for (var i = 0; i < 100; i++) {
               sin.push({x: i, y: Math.sin(i/10) * Math.random() * 100});
             }
             return [
               {key: "One", y: 5},
               {key: "Two", y: 2},
               {key: "Three", y: 9},
               {key: "Four", y: 7},
               {key: "Five", y: 4},
               {key: "Six", y: 3},
               {key: "Seven", y: 0.5}
             ];
           }
         },

         chart7config: {
           type: Object,
           value: function() {
             return {
               x: function(d) { return d.key },
               y: function(d) { return d.y },
               donut: true,
               padAngle: 0.08,
               cornerRadius: 5,
               labelsOutside: true
             }
           }
         },

         chart7JSConfig: {
           type: Function,
           value: function() {
             return function() {
               this._chart.pie
                 .startAngle(function(d) { return d.startAngle/2 -Math.PI/2 })
                 .endAngle(function(d) { return d.endAngle/2 -Math.PI/2 });
             }
           }
         }
       }
     });


The result will look like this:

.. image:: chart6.png
  :width: 700px

**********************************
Chart7: Scatter charts
**********************************

HTML & Javascript
#################

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

  .. container:: header

   **/src/html/elements/chart-examples/chart-examples.html**

  .. code-block:: html
    :linenos:

    <dom-module id="chart-examples">
      <template>

        <div horizontal layout flex>
          <paper-material elevation="1" flex horizontal layout>
            <scatter-chart flex data="{{chart8data}}" config="{{chart8config}}" configure-chart="{{chart8JSConfig}}"></scatter-chart>
          </paper-material>
        </div>

      </template>
    </dom-module>


Now edit /src/html/elements/chart-examples/javascript/chart-examples.js

.. container:: toggle

  .. container:: header

   **/src/html/elements/chart-examples/javascript/chart-examples.js**

  .. code-block:: javascript
    :linenos:

    Polymer({
      properties: {
        chart8data: {
          type: Array,
          value: function() {
            var groups = 4;
            var points = 40;
            var data = [],
                shapes = ['thin-x', 'circle', 'cross', 'triangle-up', 'triangle-down', 'diamond', 'square'],
                random = d3.random.normal();
            for (i = 0; i < groups; i++) {
                data.push({
                    key: 'Group ' + i,
                    values: []
                });
                for (j = 0; j < points; j++) {
                    data[i].values.push({
                        x: random(),
                        y: random(),
                        size: Math.round(Math.random() * 100) / 100,
                        shape: shapes[j % shapes.length]
                    });
                }
            }
            return data;
          }
        },

        chart8config: {
          type: Object,
          value: function() {
            return {
              showDistX: true,
              showDistY: true,
              useVoronoi: true,
              duration: 300,
              color: d3.scale.category10().range()
            }
          }
        },

        chart8JSConfig: {
          type: Function,
          value: function() {
            return function() {
              nv.utils.symbolMap.set('thin-x', function(size) {
                size = Math.sqrt(size);
                return 'M' + (-size/2) + ',' + (-size/2) +
                        'l' + size + ',' + size +
                        'm0,' + -(size) +
                        'l' + (-size) + ',' + size;
              });
              this._chart.xAxis
                .tickFormat(d3.format('.02f'));
              this._chart.yAxis
                .tickFormat(d3.format('.02f'));
            }
          }
        }
      }
    });


The result will look like this:

.. image:: chart7.png
  :width: 700px

**********************************
Chart8: Stacked area chart
**********************************

HTML & Javascript
#################

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

  .. container:: header

   **/src/html/elements/chart-examples/chart-examples.html**

  .. code-block:: html
    :linenos:

    <dom-module id="chart-examples">
      <template>

        <div horizontal layout flex>
          <paper-material elevation="1" flex horizontal layout>
            <stacked-area-chart flex data="{{chart9data}}" config="{{chart9config}}" configure-chart="{{chart9JSConfig}}"></stacked-area-chart>
          </paper-material>
        </div>

      </template>
    </dom-module>


Now edit /src/html/elements/chart-examples/javascript/chart-examples.js

.. container:: toggle

  .. container:: header

   **/src/html/elements/chart-examples/javascript/chart-examples.js**

  .. code-block:: javascript
    :linenos:

    Polymer({
      properties: {
        chart9data: {
          type: Array,
          value: function() {
            return [{
              "key": "Consumer Discretionary",
              "values": [
                [1138683600000, 27.38478809681],
                [1141102800000, 27.371377218208],
                [1143781200000, 26.309915460827],
                [1146369600000, 26.425199957521],
                [1149048000000, 26.823411519395],
                [1151640000000, 23.850443591584],
                [1154318400000, 23.158355444054],
                [1156996800000, 22.998689393694],
                [1159588800000, 27.977128511299],
                [1162270800000, 29.073672469721],
                [1164862800000, 28.587640408904],
                [1167541200000, 22.788453687638],
                [1170219600000, 22.429199073597],
                [1172638800000, 22.324103271051],
                [1175313600000, 17.558388444186],
                [1177905600000, 16.769518096208],
                [1180584000000, 16.214738201302],
                [1183176000000, 18.729632971228],
                [1185854400000, 18.814523318848],
                [1188532800000, 19.789986451358],
                [1191124800000, 17.070049054933],
                [1193803200000, 16.121349575715],
                [1196398800000, 15.141659430091],
                [1199077200000, 17.175388025298],
                [1201755600000, 17.286592443521],
                [1204261200000, 16.323141626569],
                [1206936000000, 19.231263773952],
                [1209528000000, 18.446256391094],
                [1212206400000, 17.822632399764],
                [1214798400000, 15.539366475979],
                [1217476800000, 15.255131790216],
                [1220155200000, 15.660963922593],
                [1222747200000, 13.254482273697],
                [1225425600000, 11.920796202299],
                [1228021200000, 12.122809090925],
                [1230699600000, 15.691026271393],
                [1233378000000, 14.720881635107],
                [1235797200000, 15.387939360044],
                [1238472000000, 13.765436672229],
                [1241064000000, 14.6314458648],
                [1243742400000, 14.292446536221],
                [1246334400000, 16.170071367016],
                [1249012800000, 15.948135554337],
                [1251691200000, 16.612872685134],
                [1254283200000, 18.778338719091],
                [1256961600000, 16.75602606542],
                [1259557200000, 19.385804443147],
                [1262235600000, 22.950590240168],
                [1264914000000, 23.61159018141],
                [1267333200000, 25.708586989581],
                [1270008000000, 26.883915999885],
                [1272600000000, 25.893486687065],
                [1275278400000, 24.678914263176],
                [1277870400000, 25.937275793023],
                [1280548800000, 29.46138169384],
                [1283227200000, 27.357322961862],
                [1285819200000, 29.057235285673],
                [1288497600000, 28.549434189386],
                [1291093200000, 28.506352379723],
                [1293771600000, 29.449241421597],
                [1296450000000, 25.796838168807],
                [1298869200000, 28.740145449189],
                [1301544000000, 22.091744141872],
                [1304136000000, 25.079662545409],
                [1306814400000, 23.674906973064],
                [1309406400000, 23.41800274293],
                [1312084800000, 23.243644138871],
                [1314763200000, 31.591854066817],
                [1317355200000, 31.497112374114],
                [1320033600000, 26.672380820431],
                [1322629200000, 27.297080015495],
                [1325307600000, 20.174315530051],
                [1327986000000, 19.631084213899],
                [1330491600000, 20.366462219462],
                [1333166400000, 17.429019937289],
                [1335758400000, 16.75543633539],
                [1338436800000, 16.182906906042]
              ]
            }, {
              "key": "Consumer Staples",
              "values": [
                [1138683600000, 7.2800122043237],
                [1141102800000, 7.1187787503354],
                [1143781200000, 8.351887016482],
                [1146369600000, 8.4156698763993],
                [1149048000000, 8.1673298604231],
                [1151640000000, 5.5132447126042],
                [1154318400000, 6.1152537710599],
                [1156996800000, 6.076765091942],
                [1159588800000, 4.6304473798646],
                [1162270800000, 4.6301068469402],
                [1164862800000, 4.3466656309389],
                [1167541200000, 6.830104897003],
                [1170219600000, 7.241633040029],
                [1172638800000, 7.1432372054153],
                [1175313600000, 10.608942063374],
                [1177905600000, 10.914964549494],
                [1180584000000, 10.933223880565],
                [1183176000000, 8.3457524851265],
                [1185854400000, 8.1078413081882],
                [1188532800000, 8.2697185922474],
                [1191124800000, 8.4742436475968],
                [1193803200000, 8.4994601179319],
                [1196398800000, 8.7387319683243],
                [1199077200000, 6.8829183612895],
                [1201755600000, 6.984133637885],
                [1204261200000, 7.0860136043287],
                [1206936000000, 4.3961787956053],
                [1209528000000, 3.8699674365231],
                [1212206400000, 3.6928925238305],
                [1214798400000, 6.7571718894253],
                [1217476800000, 6.4367313362344],
                [1220155200000, 6.4048441521454],
                [1222747200000, 5.4643833239669],
                [1225425600000, 5.3150786833374],
                [1228021200000, 5.3011272612576],
                [1230699600000, 4.1203601430809],
                [1233378000000, 4.0881783200525],
                [1235797200000, 4.1928665957189],
                [1238472000000, 7.0249415663205],
                [1241064000000, 7.006530880769],
                [1243742400000, 6.994835633224],
                [1246334400000, 6.1220222336254],
                [1249012800000, 6.1177436137653],
                [1251691200000, 6.1413396231981],
                [1254283200000, 4.8046006145874],
                [1256961600000, 4.6647600660544],
                [1259557200000, 4.544865006255],
                [1262235600000, 6.0488249316539],
                [1264914000000, 6.3188669540206],
                [1267333200000, 6.5873958262306],
                [1270008000000, 6.2281189839578],
                [1272600000000, 5.8948915746059],
                [1275278400000, 5.5967320482214],
                [1277870400000, 0.99784432084837],
                [1280548800000, 1.0950794175359],
                [1283227200000, 0.94479734407491],
                [1285819200000, 1.222093988688],
                [1288497600000, 1.335093106856],
                [1291093200000, 1.3302565104985],
                [1293771600000, 1.340824670897],
                [1296450000000, 0],
                [1298869200000, 0],
                [1301544000000, 0],
                [1304136000000, 0],
                [1306814400000, 0],
                [1309406400000, 0],
                [1312084800000, 0],
                [1314763200000, 0],
                [1317355200000, 4.4583692315],
                [1320033600000, 3.6493043348059],
                [1322629200000, 3.8610064091761],
                [1325307600000, 5.5144800685202],
                [1327986000000, 5.1750695220792],
                [1330491600000, 5.6710066952691],
                [1333166400000, 8.5658461590953],
                [1335758400000, 8.6135447714243],
                [1338436800000, 8.0231460925212]
              ]
            }, {
              "key": "Energy",
              "values": [
                [1138683600000, 1.544303464167],
                [1141102800000, 1.4387289432421],
                [1143781200000, 0],
                [1146369600000, 0],
                [1149048000000, 0],
                [1151640000000, 1.328626801128],
                [1154318400000, 1.2874050802627],
                [1156996800000, 1.0872743105593],
                [1159588800000, 0.96042562635813],
                [1162270800000, 0.93139372870616],
                [1164862800000, 0.94432167305385],
                [1167541200000, 1.277750166208],
                [1170219600000, 1.2204893886811],
                [1172638800000, 1.207489123122],
                [1175313600000, 1.2490651414113],
                [1177905600000, 1.2593129913052],
                [1180584000000, 1.373329808388],
                [1183176000000, 0],
                [1185854400000, 0],
                [1188532800000, 0],
                [1191124800000, 0],
                [1193803200000, 0],
                [1196398800000, 0],
                [1199077200000, 0],
                [1201755600000, 0],
                [1204261200000, 0],
                [1206936000000, 0],
                [1209528000000, 0],
                [1212206400000, 0],
                [1214798400000, 0],
                [1217476800000, 0],
                [1220155200000, 0],
                [1222747200000, 1.4516108933695],
                [1225425600000, 1.1856025268225],
                [1228021200000, 1.3430470355439],
                [1230699600000, 2.2752595354509],
                [1233378000000, 2.4031560010523],
                [1235797200000, 2.0822430731926],
                [1238472000000, 1.5640902826938],
                [1241064000000, 1.5812873972356],
                [1243742400000, 1.9462448548894],
                [1246334400000, 2.9464870223957],
                [1249012800000, 3.0744699383222],
                [1251691200000, 2.9422304628446],
                [1254283200000, 2.7503075599999],
                [1256961600000, 2.6506701800427],
                [1259557200000, 2.8005425319977],
                [1262235600000, 2.6816184971185],
                [1264914000000, 2.681206271327],
                [1267333200000, 2.8195488011259],
                [1270008000000, 0],
                [1272600000000, 0],
                [1275278400000, 0],
                [1277870400000, 1.0687057346382],
                [1280548800000, 1.2539400544134],
                [1283227200000, 1.1862969445955],
                [1285819200000, 0],
                [1288497600000, 0],
                [1291093200000, 0],
                [1293771600000, 0],
                [1296450000000, 1.941972859484],
                [1298869200000, 2.1142247697552],
                [1301544000000, 2.3788590206824],
                [1304136000000, 2.5337302877545],
                [1306814400000, 2.3163370395199],
                [1309406400000, 2.0645451843195],
                [1312084800000, 2.1004446672411],
                [1314763200000, 3.6301875804303],
                [1317355200000, 2.454204664652],
                [1320033600000, 2.196082370894],
                [1322629200000, 2.3358418255202],
                [1325307600000, 0],
                [1327986000000, 0],
                [1330491600000, 0],
                [1333166400000, 0.39001201038526],
                [1335758400000, 0.30945472725559],
                [1338436800000, 0.31062439305591]
              ]
            }, {
              "key": "Financials",
              "values": [
                [1138683600000, 13.356778764352],
                [1141102800000, 13.611196863271],
                [1143781200000, 6.895903006119],
                [1146369600000, 6.9939633271352],
                [1149048000000, 6.7241510257675],
                [1151640000000, 5.5611293669516],
                [1154318400000, 5.6086488714041],
                [1156996800000, 5.4962849907033],
                [1159588800000, 6.9193153169279],
                [1162270800000, 7.0016334389777],
                [1164862800000, 6.7865422443273],
                [1167541200000, 9.0006454225383],
                [1170219600000, 9.2233916171431],
                [1172638800000, 8.8929316009479],
                [1175313600000, 10.345937520404],
                [1177905600000, 10.075914677026],
                [1180584000000, 10.089006188111],
                [1183176000000, 10.598330295008],
                [1185854400000, 9.968954653301],
                [1188532800000, 9.7740580198146],
                [1191124800000, 10.558483060626],
                [1193803200000, 9.9314651823603],
                [1196398800000, 9.3997715873769],
                [1199077200000, 8.4086493387262],
                [1201755600000, 8.9698309085926],
                [1204261200000, 8.2778357995396],
                [1206936000000, 8.8585045600123],
                [1209528000000, 8.7013756413322],
                [1212206400000, 7.7933605469443],
                [1214798400000, 7.0236183483064],
                [1217476800000, 6.9873088186829],
                [1220155200000, 6.8031713070097],
                [1222747200000, 6.6869531315723],
                [1225425600000, 6.138256993963],
                [1228021200000, 5.6434994016354],
                [1230699600000, 5.495220262512],
                [1233378000000, 4.6885326869846],
                [1235797200000, 4.4524349883438],
                [1238472000000, 5.6766520778185],
                [1241064000000, 5.7675774480752],
                [1243742400000, 5.7882863168337],
                [1246334400000, 7.2666010034924],
                [1249012800000, 7.519182132226],
                [1251691200000, 7.849651451445],
                [1254283200000, 10.383992037985],
                [1256961600000, 9.0653691861818],
                [1259557200000, 9.6705248324159],
                [1262235600000, 10.856380561349],
                [1264914000000, 11.27452370892],
                [1267333200000, 11.754156529088],
                [1270008000000, 8.2870811422456],
                [1272600000000, 8.0210264360699],
                [1275278400000, 7.5375074474865],
                [1277870400000, 8.3419527338039],
                [1280548800000, 9.4197471818443],
                [1283227200000, 8.7321733185797],
                [1285819200000, 9.6627062648126],
                [1288497600000, 10.187962234549],
                [1291093200000, 9.8144201733476],
                [1293771600000, 10.275723361713],
                [1296450000000, 16.796066079353],
                [1298869200000, 17.543254984075],
                [1301544000000, 16.673660675084],
                [1304136000000, 17.963944353609],
                [1306814400000, 16.637740867211],
                [1309406400000, 15.84857094609],
                [1312084800000, 14.767303362182],
                [1314763200000, 24.778452182432],
                [1317355200000, 18.370353229999],
                [1320033600000, 15.2531374291],
                [1322629200000, 14.989600840649],
                [1325307600000, 16.052539160125],
                [1327986000000, 16.424390322793],
                [1330491600000, 17.884020741105],
                [1333166400000, 7.1424929577921],
                [1335758400000, 7.8076213051482],
                [1338436800000, 7.2462684949232]
              ]
            }, {
              "key": "Health Care",
              "values": [
                [1138683600000, 14.212410956029],
                [1141102800000, 13.973193618249],
                [1143781200000, 15.218233920665],
                [1146369600000, 14.38210972745],
                [1149048000000, 13.894310878491],
                [1151640000000, 15.593086090032],
                [1154318400000, 16.244839695188],
                [1156996800000, 16.017088850646],
                [1159588800000, 14.183951830055],
                [1162270800000, 14.148523245697],
                [1164862800000, 13.424326059972],
                [1167541200000, 12.974450435753],
                [1170219600000, 13.23247041802],
                [1172638800000, 13.318762655574],
                [1175313600000, 15.961407746104],
                [1177905600000, 16.287714639805],
                [1180584000000, 16.246590583889],
                [1183176000000, 17.564505594809],
                [1185854400000, 17.872725373165],
                [1188532800000, 18.018998508757],
                [1191124800000, 15.584518016603],
                [1193803200000, 15.480850647181],
                [1196398800000, 15.699120036984],
                [1199077200000, 19.184281817226],
                [1201755600000, 19.691226605207],
                [1204261200000, 18.982314051295],
                [1206936000000, 18.707820309008],
                [1209528000000, 17.459630929761],
                [1212206400000, 16.500616076782],
                [1214798400000, 18.086324003979],
                [1217476800000, 18.929464156258],
                [1220155200000, 18.233728682084],
                [1222747200000, 16.315776297325],
                [1225425600000, 14.63289219025],
                [1228021200000, 14.667835024478],
                [1230699600000, 13.946993947308],
                [1233378000000, 14.394304684397],
                [1235797200000, 13.724462792967],
                [1238472000000, 10.930879035806],
                [1241064000000, 9.8339915513708],
                [1243742400000, 10.053858541872],
                [1246334400000, 11.786998438287],
                [1249012800000, 11.780994901769],
                [1251691200000, 11.305889670276],
                [1254283200000, 10.918452290083],
                [1256961600000, 9.6811395055706],
                [1259557200000, 10.971529744038],
                [1262235600000, 13.330210480209],
                [1264914000000, 14.592637568961],
                [1267333200000, 14.605329141157],
                [1270008000000, 13.936853794037],
                [1272600000000, 12.189480759072],
                [1275278400000, 11.676151385046],
                [1277870400000, 13.058852800017],
                [1280548800000, 13.62891543203],
                [1283227200000, 13.811107569918],
                [1285819200000, 13.786494560787],
                [1288497600000, 14.04516285753],
                [1291093200000, 13.697412447288],
                [1293771600000, 13.677681376221],
                [1296450000000, 19.961511864531],
                [1298869200000, 21.049198298158],
                [1301544000000, 22.687631094008],
                [1304136000000, 25.469010617433],
                [1306814400000, 24.883799437121],
                [1309406400000, 24.203843814248],
                [1312084800000, 22.138760964038],
                [1314763200000, 16.034636966228],
                [1317355200000, 15.394958944556],
                [1320033600000, 12.625642461969],
                [1322629200000, 12.973735699739],
                [1325307600000, 15.786018336149],
                [1327986000000, 15.227368020134],
                [1330491600000, 15.899752650734],
                [1333166400000, 18.994731295388],
                [1335758400000, 18.450055817702],
                [1338436800000, 17.863719889669]
              ]
            }, {
              "key": "Industrials",
              "values": [
                [1138683600000, 7.1590087090398],
                [1141102800000, 7.1297210970108],
                [1143781200000, 5.5774588290586],
                [1146369600000, 5.4977254491156],
                [1149048000000, 5.5138153113634],
                [1151640000000, 4.3198084032122],
                [1154318400000, 3.9179295839125],
                [1156996800000, 3.8110093051479],
                [1159588800000, 5.5629020916939],
                [1162270800000, 5.7241673711336],
                [1164862800000, 5.4715049695004],
                [1167541200000, 4.9193763571618],
                [1170219600000, 5.136053947247],
                [1172638800000, 5.1327258759766],
                [1175313600000, 5.1888943925082],
                [1177905600000, 5.5191481293345],
                [1180584000000, 5.6093625614921],
                [1183176000000, 4.2706312987397],
                [1185854400000, 4.4453235132117],
                [1188532800000, 4.6228003109761],
                [1191124800000, 5.0645764756954],
                [1193803200000, 5.0723447230959],
                [1196398800000, 5.1457765818846],
                [1199077200000, 5.4067851597282],
                [1201755600000, 5.472241916816],
                [1204261200000, 5.3742740389688],
                [1206936000000, 6.251751933664],
                [1209528000000, 6.1406852153472],
                [1212206400000, 5.8164385627465],
                [1214798400000, 5.4255846656171],
                [1217476800000, 5.3738499417204],
                [1220155200000, 5.1815627753979],
                [1222747200000, 5.0305983235349],
                [1225425600000, 4.6823058607165],
                [1228021200000, 4.5941481589093],
                [1230699600000, 5.4669598474575],
                [1233378000000, 5.1249037357],
                [1235797200000, 4.3504421250742],
                [1238472000000, 4.6260881026002],
                [1241064000000, 5.0140402458946],
                [1243742400000, 4.7458462454774],
                [1246334400000, 6.0437019654564],
                [1249012800000, 6.4595216249754],
                [1251691200000, 6.6420468254155],
                [1254283200000, 5.8927271960913],
                [1256961600000, 5.4712108838003],
                [1259557200000, 6.1220254207747],
                [1262235600000, 5.5385935169255],
                [1264914000000, 5.7383377612639],
                [1267333200000, 6.1715976730415],
                [1270008000000, 4.0102262681174],
                [1272600000000, 3.769389679692],
                [1275278400000, 3.5301571031152],
                [1277870400000, 2.7660252652526],
                [1280548800000, 3.1409983385775],
                [1283227200000, 3.0528024863055],
                [1285819200000, 4.3126123157971],
                [1288497600000, 4.594654041683],
                [1291093200000, 4.5424126126793],
                [1293771600000, 4.7790043987302],
                [1296450000000, 7.4969154058289],
                [1298869200000, 7.9424751557821],
                [1301544000000, 7.1560736250547],
                [1304136000000, 7.9478117337855],
                [1306814400000, 7.4109214848895],
                [1309406400000, 7.5966457641101],
                [1312084800000, 7.165754444071],
                [1314763200000, 5.4816702524302],
                [1317355200000, 4.9893656089584],
                [1320033600000, 4.498385105327],
                [1322629200000, 4.6776090358151],
                [1325307600000, 8.1350814368063],
                [1327986000000, 8.0732769990652],
                [1330491600000, 8.5602340387277],
                [1333166400000, 5.1293714074325],
                [1335758400000, 5.2586794619016],
                [1338436800000, 5.1100853569977]
              ]
            }, {
              "key": "Information Technology",
              "values": [
                [1138683600000, 13.242301508051],
                [1141102800000, 12.863536342042],
                [1143781200000, 21.034044171629],
                [1146369600000, 21.419084618803],
                [1149048000000, 21.142678863691],
                [1151640000000, 26.568489677529],
                [1154318400000, 24.839144939905],
                [1156996800000, 25.456187462167],
                [1159588800000, 26.350164502826],
                [1162270800000, 26.47833320519],
                [1164862800000, 26.425979547847],
                [1167541200000, 28.191461582256],
                [1170219600000, 28.930307448808],
                [1172638800000, 29.521413891117],
                [1175313600000, 28.188285966466],
                [1177905600000, 27.704619625832],
                [1180584000000, 27.490862424829],
                [1183176000000, 28.770679721286],
                [1185854400000, 29.060480671449],
                [1188532800000, 28.240998844973],
                [1191124800000, 33.004893194127],
                [1193803200000, 34.075180359928],
                [1196398800000, 32.548560664833],
                [1199077200000, 30.629727432728],
                [1201755600000, 28.642858788159],
                [1204261200000, 27.973575227842],
                [1206936000000, 27.393351882726],
                [1209528000000, 28.476095288523],
                [1212206400000, 29.29667866426],
                [1214798400000, 29.222333802896],
                [1217476800000, 28.092966093843],
                [1220155200000, 28.107159262922],
                [1222747200000, 25.482974832098],
                [1225425600000, 21.208115993834],
                [1228021200000, 20.295043095268],
                [1230699600000, 15.925754618401],
                [1233378000000, 17.162864628346],
                [1235797200000, 17.084345773174],
                [1238472000000, 22.246007102281],
                [1241064000000, 24.530543998509],
                [1243742400000, 25.084184918242],
                [1246334400000, 16.606166527358],
                [1249012800000, 17.239620011628],
                [1251691200000, 17.336739127379],
                [1254283200000, 25.478492475753],
                [1256961600000, 23.017152085245],
                [1259557200000, 25.617745423683],
                [1262235600000, 24.061133998642],
                [1264914000000, 23.223933318644],
                [1267333200000, 24.425887263937],
                [1270008000000, 35.501471156693],
                [1272600000000, 33.775013878676],
                [1275278400000, 30.417993630285],
                [1277870400000, 30.023598978467],
                [1280548800000, 33.327519522436],
                [1283227200000, 31.963388450371],
                [1285819200000, 30.498967232092],
                [1288497600000, 32.403696817912],
                [1291093200000, 31.47736071922],
                [1293771600000, 31.53259666241],
                [1296450000000, 41.760282761548],
                [1298869200000, 45.605771243237],
                [1301544000000, 39.986557966215],
                [1304136000000, 43.846330510051],
                [1306814400000, 39.857316881857],
                [1309406400000, 37.675127768208],
                [1312084800000, 35.775077970313],
                [1314763200000, 48.631009702577],
                [1317355200000, 42.830831754505],
                [1320033600000, 35.611502589362],
                [1322629200000, 35.320136981738],
                [1325307600000, 31.564136901516],
                [1327986000000, 32.074407502433],
                [1330491600000, 35.053013769976],
                [1333166400000, 26.434568573937],
                [1335758400000, 25.305617871002],
                [1338436800000, 24.520919418236]
              ]
            }, {
              "key": "Materials",
              "values": [
                [1138683600000, 5.5806167415681],
                [1141102800000, 5.4539047069985],
                [1143781200000, 7.6728842432362],
                [1146369600000, 7.719946716654],
                [1149048000000, 8.0144619912942],
                [1151640000000, 7.942223133434],
                [1154318400000, 8.3998279827444],
                [1156996800000, 8.532324572605],
                [1159588800000, 4.7324285199763],
                [1162270800000, 4.7402397487697],
                [1164862800000, 4.9042069355168],
                [1167541200000, 5.9583963430882],
                [1170219600000, 6.3693899239171],
                [1172638800000, 6.261153903813],
                [1175313600000, 5.3443942184584],
                [1177905600000, 5.4932111235361],
                [1180584000000, 5.5747393101109],
                [1183176000000, 5.3833633060013],
                [1185854400000, 5.5125898831832],
                [1188532800000, 5.8116112661327],
                [1191124800000, 4.3962296939996],
                [1193803200000, 4.6967663605521],
                [1196398800000, 4.7963004350914],
                [1199077200000, 4.1817985183351],
                [1201755600000, 4.3797643870182],
                [1204261200000, 4.6966642197965],
                [1206936000000, 4.3609995132565],
                [1209528000000, 4.4736290996496],
                [1212206400000, 4.3749762738128],
                [1214798400000, 3.3274661194507],
                [1217476800000, 3.0316184691337],
                [1220155200000, 2.5718140204728],
                [1222747200000, 2.7034994044603],
                [1225425600000, 2.2033786591364],
                [1228021200000, 1.9850621240805],
                [1230699600000, 0],
                [1233378000000, 0],
                [1235797200000, 0],
                [1238472000000, 0],
                [1241064000000, 0],
                [1243742400000, 0],
                [1246334400000, 0],
                [1249012800000, 0],
                [1251691200000, 0],
                [1254283200000, 0.44495950017788],
                [1256961600000, 0.33945469262483],
                [1259557200000, 0.38348269455195],
                [1262235600000, 0],
                [1264914000000, 0],
                [1267333200000, 0],
                [1270008000000, 0],
                [1272600000000, 0],
                [1275278400000, 0],
                [1277870400000, 0],
                [1280548800000, 0],
                [1283227200000, 0],
                [1285819200000, 0],
                [1288497600000, 0],
                [1291093200000, 0],
                [1293771600000, 0],
                [1296450000000, 0.52216435716176],
                [1298869200000, 0.59275786698454],
                [1301544000000, 0],
                [1304136000000, 0],
                [1306814400000, 0],
                [1309406400000, 0],
                [1312084800000, 0],
                [1314763200000, 0],
                [1317355200000, 0],
                [1320033600000, 0],
                [1322629200000, 0],
                [1325307600000, 0],
                [1327986000000, 0],
                [1330491600000, 0],
                [1333166400000, 0],
                [1335758400000, 0],
                [1338436800000, 0]
              ]
            }, {
              "key": "Telecommunication Services",
              "values": [
                [1138683600000, 3.7056975170243],
                [1141102800000, 3.7561118692318],
                [1143781200000, 2.861913700854],
                [1146369600000, 2.9933744103381],
                [1149048000000, 2.7127537218463],
                [1151640000000, 3.1195497076283],
                [1154318400000, 3.4066964004508],
                [1156996800000, 3.3754571113569],
                [1159588800000, 2.2965579982924],
                [1162270800000, 2.4486818633018],
                [1164862800000, 2.4002308848517],
                [1167541200000, 1.9649579750349],
                [1170219600000, 1.9385263638056],
                [1172638800000, 1.9128975336387],
                [1175313600000, 2.3412869836298],
                [1177905600000, 2.4337870351445],
                [1180584000000, 2.62179703171],
                [1183176000000, 3.2642864957929],
                [1185854400000, 3.3200396223709],
                [1188532800000, 3.3934212707572],
                [1191124800000, 4.2822327088179],
                [1193803200000, 4.1474964228541],
                [1196398800000, 4.1477082879801],
                [1199077200000, 5.2947122916128],
                [1201755600000, 5.2919843508028],
                [1204261200000, 5.1989783050309],
                [1206936000000, 3.5603057673513],
                [1209528000000, 3.3009087690692],
                [1212206400000, 3.1784852603792],
                [1214798400000, 4.5889503538868],
                [1217476800000, 4.401779617494],
                [1220155200000, 4.2208301828278],
                [1222747200000, 3.89396671475],
                [1225425600000, 3.0423832241354],
                [1228021200000, 3.135520611578],
                [1230699600000, 1.9631418164089],
                [1233378000000, 1.8963543874958],
                [1235797200000, 1.8266636017025],
                [1238472000000, 0.93136635895188],
                [1241064000000, 0.92737801918888],
                [1243742400000, 0.97591889805002],
                [1246334400000, 2.6841193805515],
                [1249012800000, 2.5664341140531],
                [1251691200000, 2.3887523699873],
                [1254283200000, 1.1737801663681],
                [1256961600000, 1.0953582317281],
                [1259557200000, 1.2495674976653],
                [1262235600000, 0.36607452464754],
                [1264914000000, 0.3548719047291],
                [1267333200000, 0.36769242398939],
                [1270008000000, 0],
                [1272600000000, 0],
                [1275278400000, 0],
                [1277870400000, 0],
                [1280548800000, 0],
                [1283227200000, 0],
                [1285819200000, 0.85450741275337],
                [1288497600000, 0.91360317921637],
                [1291093200000, 0.89647678692269],
                [1293771600000, 0.87800687192639],
                [1296450000000, 0],
                [1298869200000, 0],
                [1301544000000, 0.43668720882994],
                [1304136000000, 0.4756523602692],
                [1306814400000, 0.46947368328469],
                [1309406400000, 0.45138896152316],
                [1312084800000, 0.43828726648117],
                [1314763200000, 2.0820861395316],
                [1317355200000, 0.9364411075395],
                [1320033600000, 0.60583907839773],
                [1322629200000, 0.61096950747437],
                [1325307600000, 0],
                [1327986000000, 0],
                [1330491600000, 0],
                [1333166400000, 0],
                [1335758400000, 0],
                [1338436800000, 0]
              ]
            }, {
              "key": "Utilities",
              "values": [
                [1138683600000, 0],
                [1141102800000, 0],
                [1143781200000, 0],
                [1146369600000, 0],
                [1149048000000, 0],
                [1151640000000, 0],
                [1154318400000, 0],
                [1156996800000, 0],
                [1159588800000, 0],
                [1162270800000, 0],
                [1164862800000, 0],
                [1167541200000, 0],
                [1170219600000, 0],
                [1172638800000, 0],
                [1175313600000, 0],
                [1177905600000, 0],
                [1180584000000, 0],
                [1183176000000, 0],
                [1185854400000, 0],
                [1188532800000, 0],
                [1191124800000, 0],
                [1193803200000, 0],
                [1196398800000, 0],
                [1199077200000, 0],
                [1201755600000, 0],
                [1204261200000, 0],
                [1206936000000, 0],
                [1209528000000, 0],
                [1212206400000, 0],
                [1214798400000, 0],
                [1217476800000, 0],
                [1220155200000, 0],
                [1222747200000, 0],
                [1225425600000, 0],
                [1228021200000, 0],
                [1230699600000, 0],
                [1233378000000, 0],
                [1235797200000, 0],
                [1238472000000, 0],
                [1241064000000, 0],
                [1243742400000, 0],
                [1246334400000, 0],
                [1249012800000, 0],
                [1251691200000, 0],
                [1254283200000, 0],
                [1256961600000, 0],
                [1259557200000, 0],
                [1262235600000, 0],
                [1264914000000, 0],
                [1267333200000, 0],
                [1270008000000, 0],
                [1272600000000, 0],
                [1275278400000, 0],
                [1277870400000, 0],
                [1280548800000, 0],
                [1283227200000, 0],
                [1285819200000, 0],
                [1288497600000, 0],
                [1291093200000, 0],
                [1293771600000, 0],
                [1296450000000, 0],
                [1298869200000, 0],
                [1301544000000, 0],
                [1304136000000, 0],
                [1306814400000, 0],
                [1309406400000, 0],
                [1312084800000, 0],
                [1314763200000, 0],
                [1317355200000, 0],
                [1320033600000, 0],
                [1322629200000, 0],
                [1325307600000, 0],
                [1327986000000, 0],
                [1330491600000, 0],
                [1333166400000, 0],
                [1335758400000, 0],
                [1338436800000, 0]
              ]
            }];
          }
        },

        chart9config: {
          type: Object,
          value: function() {
            return {
              useInteractiveGuideline: true,
              x: function(d) { return d[0] },
              y: function(d) { return d[1] },
              duration: 300,
              controlLabels: {stacked: "Stacked"}
            }
          }
        },

        chart9JSConfig: {
          type: Function,
          value: function() {
            return function() {
              this._chart.xAxis
                .tickFormat(function(d) { return d3.time.format('%x')(new Date(d)) });
              this._chart.yAxis
                .tickFormat(d3.format(',.4f'));
              this._chart.legend.vers('furious');
            }
          }
        }
      }
    });


The result will look like this:

.. image:: chart8.png
  :width: 700px

**********************************
Chart9: A Candlestick charts
**********************************

HTML & Javascript
#################

Edit src/html/elements/chart-examples/chart-examples.html

.. container:: toggle

  .. container:: header

   **/src/html/elements/chart-examples/chart-examples.html**

  .. code-block:: html
    :linenos:

    <dom-module id="chart-examples">
      <template>

        <div horizontal layout flex>
          <paper-material elevation="1" flex horizontal layout>
            <candlestick-chart flex data="{{chart11data}}" config="{{chart11config}}" configure-chart="{{chart11JSConfig}}"></candlestick-chart>
          </paper-material>
        </div>

      </template>
    </dom-module>


Now edit /src/html/elements/chart-examples/javascript/chart-examples.js

.. container:: toggle

  .. container:: header

   **/src/html/elements/chart-examples/javascript/chart-examples.js**

  .. code-block:: javascript
    :linenos:

    Polymer({
      properties: {
        chart11data: {
          type: Array,
          value: function() {
            return [{
              values: [{
                "date": 15854,
                "open": 165.42,
                "high": 165.8,
                "low": 164.34,
                "close": 165.22,
                "volume": 160363400,
                "adjusted": 164.35
              }, {
                "date": 15855,
                "open": 165.35,
                "high": 166.59,
                "low": 165.22,
                "close": 165.83,
                "volume": 107793800,
                "adjusted": 164.96
              }, {
                "date": 15856,
                "open": 165.37,
                "high": 166.31,
                "low": 163.13,
                "close": 163.45,
                "volume": 176850100,
                "adjusted": 162.59
              }, {
                "date": 15859,
                "open": 163.83,
                "high": 164.46,
                "low": 162.66,
                "close": 164.35,
                "volume": 168390700,
                "adjusted": 163.48
              }, {
                "date": 15860,
                "open": 164.44,
                "high": 165.1,
                "low": 162.73,
                "close": 163.56,
                "volume": 157631500,
                "adjusted": 162.7
              }, {
                "date": 15861,
                "open": 163.09,
                "high": 163.42,
                "low": 161.13,
                "close": 161.27,
                "volume": 211737800,
                "adjusted": 160.42
              }, {
                "date": 15862,
                "open": 161.2,
                "high": 162.74,
                "low": 160.25,
                "close": 162.73,
                "volume": 200225500,
                "adjusted": 161.87
              }, {
                "date": 15863,
                "open": 163.85,
                "high": 164.95,
                "low": 163.14,
                "close": 164.8,
                "volume": 188337800,
                "adjusted": 163.93
              }, {
                "date": 15866,
                "open": 165.31,
                "high": 165.4,
                "low": 164.37,
                "close": 164.8,
                "volume": 105667100,
                "adjusted": 163.93
              }, {
                "date": 15867,
                "open": 163.3,
                "high": 164.54,
                "low": 162.74,
                "close": 163.1,
                "volume": 159505400,
                "adjusted": 162.24
              }, {
                "date": 15868,
                "open": 164.22,
                "high": 164.39,
                "low": 161.6,
                "close": 161.75,
                "volume": 177361500,
                "adjusted": 160.9
              }, {
                "date": 15869,
                "open": 161.66,
                "high": 164.5,
                "low": 161.3,
                "close": 164.21,
                "volume": 163587800,
                "adjusted": 163.35
              }, {
                "date": 15870,
                "open": 164.03,
                "high": 164.67,
                "low": 162.91,
                "close": 163.18,
                "volume": 141197500,
                "adjusted": 162.32
              }, {
                "date": 15873,
                "open": 164.29,
                "high": 165.22,
                "low": 163.22,
                "close": 164.44,
                "volume": 136295600,
                "adjusted": 163.57
              }, {
                "date": 15874,
                "open": 164.53,
                "high": 165.99,
                "low": 164.52,
                "close": 165.74,
                "volume": 114695600,
                "adjusted": 164.87
              }, {
                "date": 15875,
                "open": 165.6,
                "high": 165.89,
                "low": 163.38,
                "close": 163.45,
                "volume": 206149500,
                "adjusted": 162.59
              }, {
                "date": 15876,
                "open": 161.86,
                "high": 163.47,
                "low": 158.98,
                "close": 159.4,
                "volume": 321255900,
                "adjusted": 158.56
              }, {
                "date": 15877,
                "open": 159.64,
                "high": 159.76,
                "low": 157.47,
                "close": 159.07,
                "volume": 271956800,
                "adjusted": 159.07
              }, {
                "date": 15880,
                "open": 157.41,
                "high": 158.43,
                "low": 155.73,
                "close": 157.06,
                "volume": 222329000,
                "adjusted": 157.06
              }, {
                "date": 15881,
                "open": 158.48,
                "high": 160.1,
                "low": 157.42,
                "close": 158.57,
                "volume": 162262200,
                "adjusted": 158.57
              }, {
                "date": 15882,
                "open": 159.87,
                "high": 160.5,
                "low": 159.25,
                "close": 160.14,
                "volume": 134848000,
                "adjusted": 160.14
              }, {
                "date": 15883,
                "open": 161.1,
                "high": 161.82,
                "low": 160.95,
                "close": 161.08,
                "volume": 129483700,
                "adjusted": 161.08
              }, {
                "date": 15884,
                "open": 160.63,
                "high": 161.4,
                "low": 159.86,
                "close": 160.42,
                "volume": 160402900,
                "adjusted": 160.42
              }, {
                "date": 15887,
                "open": 161.26,
                "high": 162.48,
                "low": 161.08,
                "close": 161.36,
                "volume": 131954800,
                "adjusted": 161.36
              }, {
                "date": 15888,
                "open": 161.12,
                "high": 162.3,
                "low": 160.5,
                "close": 161.21,
                "volume": 154863700,
                "adjusted": 161.21
              }, {
                "date": 15889,
                "open": 160.48,
                "high": 161.77,
                "low": 160.22,
                "close": 161.28,
                "volume": 75216400,
                "adjusted": 161.28
              }, {
                "date": 15891,
                "open": 162.47,
                "high": 163.08,
                "low": 161.3,
                "close": 163.02,
                "volume": 122416900,
                "adjusted": 163.02
              }, {
                "date": 15894,
                "open": 163.86,
                "high": 164.39,
                "low": 163.08,
                "close": 163.95,
                "volume": 108092500,
                "adjusted": 163.95
              }, {
                "date": 15895,
                "open": 164.98,
                "high": 165.33,
                "low": 164.27,
                "close": 165.13,
                "volume": 119298000,
                "adjusted": 165.13
              }, {
                "date": 15896,
                "open": 164.97,
                "high": 165.75,
                "low": 164.63,
                "close": 165.19,
                "volume": 121410100,
                "adjusted": 165.19
              }, {
                "date": 15897,
                "open": 167.11,
                "high": 167.61,
                "low": 165.18,
                "close": 167.44,
                "volume": 135592200,
                "adjusted": 167.44
              }, {
                "date": 15898,
                "open": 167.39,
                "high": 167.93,
                "low": 167.13,
                "close": 167.51,
                "volume": 104212700,
                "adjusted": 167.51
              }, {
                "date": 15901,
                "open": 167.97,
                "high": 168.39,
                "low": 167.68,
                "close": 168.15,
                "volume": 69450600,
                "adjusted": 168.15
              }, {
                "date": 15902,
                "open": 168.26,
                "high": 168.36,
                "low": 167.07,
                "close": 167.52,
                "volume": 88702100,
                "adjusted": 167.52
              }, {
                "date": 15903,
                "open": 168.16,
                "high": 168.48,
                "low": 167.73,
                "close": 167.95,
                "volume": 92873900,
                "adjusted": 167.95
              }, {
                "date": 15904,
                "open": 168.31,
                "high": 169.27,
                "low": 168.2,
                "close": 168.87,
                "volume": 103620100,
                "adjusted": 168.87
              }, {
                "date": 15905,
                "open": 168.52,
                "high": 169.23,
                "low": 168.31,
                "close": 169.17,
                "volume": 103831700,
                "adjusted": 169.17
              }, {
                "date": 15908,
                "open": 169.41,
                "high": 169.74,
                "low": 169.01,
                "close": 169.5,
                "volume": 79428600,
                "adjusted": 169.5
              }, {
                "date": 15909,
                "open": 169.8,
                "high": 169.83,
                "low": 169.05,
                "close": 169.14,
                "volume": 80829700,
                "adjusted": 169.14
              }, {
                "date": 15910,
                "open": 169.79,
                "high": 169.86,
                "low": 168.18,
                "close": 168.52,
                "volume": 112914000,
                "adjusted": 168.52
              }, {
                "date": 15911,
                "open": 168.22,
                "high": 169.08,
                "low": 167.94,
                "close": 168.93,
                "volume": 111088600,
                "adjusted": 168.93
              }, {
                "date": 15912,
                "open": 168.22,
                "high": 169.16,
                "low": 167.52,
                "close": 169.11,
                "volume": 107814600,
                "adjusted": 169.11
              }, {
                "date": 15915,
                "open": 168.68,
                "high": 169.06,
                "low": 168.11,
                "close": 168.59,
                "volume": 79695000,
                "adjusted": 168.59
              }, {
                "date": 15916,
                "open": 169.1,
                "high": 169.28,
                "low": 168.19,
                "close": 168.59,
                "volume": 85209600,
                "adjusted": 168.59
              }, {
                "date": 15917,
                "open": 168.94,
                "high": 169.85,
                "low": 168.49,
                "close": 168.71,
                "volume": 142388700,
                "adjusted": 168.71
              }, {
                "date": 15918,
                "open": 169.99,
                "high": 170.81,
                "low": 169.9,
                "close": 170.66,
                "volume": 110438400,
                "adjusted": 170.66
              }, {
                "date": 15919,
                "open": 170.28,
                "high": 170.97,
                "low": 170.05,
                "close": 170.95,
                "volume": 91116700,
                "adjusted": 170.95
              }, {
                "date": 15922,
                "open": 170.57,
                "high": 170.96,
                "low": 170.35,
                "close": 170.7,
                "volume": 54072700,
                "adjusted": 170.7
              }, {
                "date": 15923,
                "open": 170.37,
                "high": 170.74,
                "low": 169.35,
                "close": 169.73,
                "volume": 87495000,
                "adjusted": 169.73
              }, {
                "date": 15924,
                "open": 169.19,
                "high": 169.43,
                "low": 168.55,
                "close": 169.18,
                "volume": 84854700,
                "adjusted": 169.18
              }, {
                "date": 15925,
                "open": 169.98,
                "high": 170.18,
                "low": 168.93,
                "close": 169.8,
                "volume": 102181300,
                "adjusted": 169.8
              }, {
                "date": 15926,
                "open": 169.58,
                "high": 170.1,
                "low": 168.72,
                "close": 169.31,
                "volume": 91757700,
                "adjusted": 169.31
              }, {
                "date": 15929,
                "open": 168.46,
                "high": 169.31,
                "low": 168.38,
                "close": 169.11,
                "volume": 68593300,
                "adjusted": 169.11
              }, {
                "date": 15930,
                "open": 169.41,
                "high": 169.9,
                "low": 168.41,
                "close": 169.61,
                "volume": 80806000,
                "adjusted": 169.61
              }, {
                "date": 15931,
                "open": 169.53,
                "high": 169.8,
                "low": 168.7,
                "close": 168.74,
                "volume": 79829200,
                "adjusted": 168.74
              }, {
                "date": 15932,
                "open": 167.41,
                "high": 167.43,
                "low": 166.09,
                "close": 166.38,
                "volume": 152931800,
                "adjusted": 166.38
              }, {
                "date": 15933,
                "open": 166.06,
                "high": 166.63,
                "low": 165.5,
                "close": 165.83,
                "volume": 130868200,
                "adjusted": 165.83
              }, {
                "date": 15936,
                "open": 165.64,
                "high": 166.21,
                "low": 164.76,
                "close": 164.77,
                "volume": 96437600,
                "adjusted": 164.77
              }, {
                "date": 15937,
                "open": 165.04,
                "high": 166.2,
                "low": 164.86,
                "close": 165.58,
                "volume": 89294400,
                "adjusted": 165.58
              }, {
                "date": 15938,
                "open": 165.12,
                "high": 166.03,
                "low": 164.19,
                "close": 164.56,
                "volume": 159530500,
                "adjusted": 164.56
              }, {
                "date": 15939,
                "open": 164.9,
                "high": 166.3,
                "low": 164.89,
                "close": 166.06,
                "volume": 101471400,
                "adjusted": 166.06
              }, {
                "date": 15940,
                "open": 166.55,
                "high": 166.83,
                "low": 165.77,
                "close": 166.62,
                "volume": 90888900,
                "adjusted": 166.62
              }, {
                "date": 15943,
                "open": 166.79,
                "high": 167.3,
                "low": 165.89,
                "close": 166,
                "volume": 89702100,
                "adjusted": 166
              }, {
                "date": 15944,
                "open": 164.36,
                "high": 166,
                "low": 163.21,
                "close": 163.33,
                "volume": 158619400,
                "adjusted": 163.33
              }, {
                "date": 15945,
                "open": 163.26,
                "high": 164.49,
                "low": 163.05,
                "close": 163.91,
                "volume": 108113000,
                "adjusted": 163.91
              }, {
                "date": 15946,
                "open": 163.55,
                "high": 165.04,
                "low": 163.4,
                "close": 164.17,
                "volume": 119200500,
                "adjusted": 164.17
              }, {
                "date": 15947,
                "open": 164.51,
                "high": 164.53,
                "low": 163.17,
                "close": 163.65,
                "volume": 134560800,
                "adjusted": 163.65
              }, {
                "date": 15951,
                "open": 165.23,
                "high": 165.58,
                "low": 163.7,
                "close": 164.39,
                "volume": 142322300,
                "adjusted": 164.39
              }, {
                "date": 15952,
                "open": 164.43,
                "high": 166.03,
                "low": 164.13,
                "close": 165.75,
                "volume": 97304000,
                "adjusted": 165.75
              }, {
                "date": 15953,
                "open": 165.85,
                "high": 166.4,
                "low": 165.73,
                "close": 165.96,
                "volume": 62930500,
                "adjusted": 165.96
              }]
            }];
          }
        },

        chart11config: {
          type: Object,
          value: function() {
            return {
              x: function(d) { return d['date'] },
              y: function(d) { return d['close'] },
              duration: 250,
              margin: {left: 75, bottom: 50}
            }
          }
        },

        chart11JSConfig: {
          type: Function,
          value: function() {
            return function() {
              this._chart.xAxis
                .axisLabel("Dates")
                .tickFormat(function(d) {
                    // I didn't feel like changing all the above date values
                    // so I hack it to make each value fall on a different date
                    return d3.time.format('%x')(new Date(new Date() - (20000 * 86400000) + (d * 86400000)));
                });
              this._chart.yAxis
                .axisLabel('Stock Price')
                .tickFormat(function(d,i){ return '$' + d3.format(',.1f')(d); });
            }
          }
        }
      }
    });


The result will look like this:

.. image:: chart9.png
  :width: 700px
