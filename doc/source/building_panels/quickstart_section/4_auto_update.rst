Demo 4: Refresh
===============

The `refresh-example` element behaves much like the `ts-ajax` element we saw in the
previous demo. The big difference is that this element implements periodic
updating.

************************************
Make the refresh-example element
************************************
In your cell, run:

.. code-block:: bash
    :linenos:

    cd src/html/elements
    ./new-element.js
    name of the new element: refresh-example
    creating new element <refresh-example>...
    removing any .svn folders in  refresh-example
    Finished

****************************************
Register the refresh-example element
****************************************

Edit src/html/elements/elements.html and add the following line

.. code-block:: bash
    :linenos:

    <link rel="import" href="refresh-example/refresh-example.html">

This will tell AjaXell to load our new element.

************************************
Edit the refresh-example element
************************************

Edit src/html/elements/refresh-example/refresh-example.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/refresh-example/refresh-example.html**

    .. literalinclude:: /../supervisor/src/html/elements/refresh-example/refresh-example.html
       :language: html
       :linenos:

Now edit /src/html/elements/refresh-example/css/refresh-example.scss

.. container:: toggle

    .. container:: header

        **/src/html/elements/refresh-example/css/refresh-example.scss**

    .. literalinclude:: /../supervisor/src/html/elements/refresh-example/css/refresh-example.scss
       :language: scss
       :linenos:

Now edit /src/html/elements/refresh-example/javascript/refresh-example.js

.. container:: toggle

    .. container:: header

        **/src/html/elements/refresh-example/javascript/refresh-example.js**

    .. literalinclude:: /../supervisor/src/html/elements/refresh-example/javascript/refresh-example.js
       :language: javascript
       :linenos:


Now execute Grunt to build our new Polymer element.

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

**********************************
Make the refresh-example panel
**********************************

Make a new c++ file /src/common/panels/RefreshExample.cc

.. container:: toggle

    .. container:: header

        **/src/common/panels/RefreshExample.cc**

    .. literalinclude:: /../supervisor/src/common/panels/RefreshExample.cc
       :language: cpp
       :linenos:

Make the /include/subsystem/supervisor/panels/RefreshExample.h file.

.. container:: toggle

    .. container:: header

        **/include/subsystem/supervisor/panels/RefreshExample.h**

    .. literalinclude:: /../supervisor/include/subsystem/supervisor/panels/RefreshExample.h
       :language: cpp
       :linenos:

Register the new class in the Makefile.

.. code-block:: makefile
    :linenos:

    Sources=\
    	version.cc\
    	Cell.cc\
    	CellContext.cc\
    	Configuration.cc\
    	...
    	panels/RefreshExample.cc \
    	...

Now register your new panel in the menu so users can access it.

Edit src/common/Cell.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/RefreshExample.h"
    ...
    void subsystemsupervisor::Cell::init() {
      ...
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      ...
      panelF->add<subsystempanels::RefreshExample>("Refresh example");

Now you can compile your cell and you should see the "Form example"
panel in the menu under the 'control-panels' section.

Also your element has created some documentation.
Surf to <hostname>:<port>/<package-name>/html/index.html and you will see the
package documentation for your cell. `refresh-example` will be in there, and clicking
it brings up the documentation for your `refresh-example` element.
