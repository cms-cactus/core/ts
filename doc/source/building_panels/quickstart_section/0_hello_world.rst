Demo 0: Hello World
===================

****************************
Make the hello-world element
****************************
In your cell, run:

.. code-block:: bash
    :linenos:

    cd src/html/elements
    ./new-element.js
    name of the new element: hello-world
    creating new element <hello-world>...
    removing any .svn folders in  hello-world
    Finished

You now have a working `hello-world` element. We'll edit it soon.

********************************
Register the hello-world element
********************************

Edit src/html/elements/elements.html and add the following line

.. code-block:: bash
    :linenos:

    <link rel="import" href="hello-world/hello-world.html">

This will tell AjaXell to load our new element.

****************************
Edit the hello-world element
****************************

This is just a `hello, world!` example. We don't need much of the stuff our
template generated.

Edit src/html/elements/hello-world/hello-world.html

.. code-block:: html
    :linenos:

    <link rel="import" href="/extern/bower_components/polymer/polymer.html">
    <link rel="import" href="/ts/common-elements/reset-css/reset-css.html">

    <!--
    `hello-world` is the simplest panel imaginable. It displays 'Hello, World!',
    nothing more.

    Example:

        <hello-world></hello-world>

    @demo demo/index.html
    -->
    <dom-module id="hello-world">
      <template>
        <style include="reset-css"></style>
        <link rel="stylesheet" type="text/css" href="css/hello-world-min.css?__inline=true">

        <h1>Hello, World!</h1>

      </template>
      <script src="javascript/hello-world-min.js?__inline=true"></script>
    </dom-module>

Note that we didn't delete the reset-css include. This is recommended to do,
`reset-css` provides us with some general css (fonts, theme colors, ...).

Now edit src/html/elements/hello-world/css/hello-world.scss

.. code-block:: scss
    :linenos:

    :host {
      display: block;
    }

It is recommended to always have a display directive in the :host{} section.
This tells the browser how the element will behave inside a page.
The most used are 'block', 'inline-block', and 'inline'.
'block' elements try to take as much width as possible, while 'inline' elements
only take the width and height they need. 'inline-block' is an inline element
that can still have a manually set width or height

Now edit src/html/elements/hello-world/javascript/hello-world.js

.. code-block:: javascript
    :linenos:

    Polymer({
        is: 'hello-world'
    });

This is the minimal required javascript for a Polymer element. It only declares
the existence of the `hello-world` element.

Now execute Grunt to build our new Polymer element.

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

**************************
Make the hello-world panel
**************************

Make a new c++ file /src/common/panels/HelloWorld.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/HelloWorld.h"
    #include "ajax/PolymerElement.h"

    using namespace subsystempanels;
    HelloWorld::HelloWorld( tsframework::CellAbstractContext* context, log4cplus::Logger& logger)
    :tsframework::CellPanel(context,logger) {
      logger_ = log4cplus::Logger::getInstance(logger.getName() +".HelloWorld");
    }

    void HelloWorld::layout(cgicc::Cgicc& cgi) {
      remove();
      add(new ajax::PolymerElement("hello-world"));
    }

The remove(); function clears any previously existing output buffer from the
HelloWorld panel. If you remove that line and you request the panel twice, you
get 2 `hello-world` elements.

Make the include/subsystem/supervisor/panels/HelloWorld.h file.

.. code-block:: cpp
    :linenos:

    #ifndef _subsystem_supervisor_panels_HelloWorld_h_
    #define _subsystem_supervisor_panels_HelloWorld_h_

    #include "ts/framework/CellPanel.h"
    #include "log4cplus/logger.h"
    #include "cgicc/Cgicc.h"

    namespace subsystempanels {
      class HelloWorld: public tsframework::CellPanel {
        public:
          HelloWorld(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
          void layout(cgicc::Cgicc& cgi);
      };
    }
    #endif

Register the new class in the Makefile.

.. code-block:: makefile
    :linenos:

    Sources=\
    	version.cc\
    	Cell.cc\
    	CellContext.cc\
    	Configuration.cc\
    	...
    	panels/HelloWorld.cc \
    	...

Now register your new panel in the menu so users can access it.

Edit src/common/Cell.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/HelloWorld.h"
    ...
    void subsystemsupervisor::Cell::init() {
      ...
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      ...
      panelF->add<subsystempanels::HelloWorld>("HelloWorld");

Now you can compile your cell and you should see the HelloWorld panel in the
menu under the 'control-panels' section.

.. image:: helloworld.png
   :width: 700px

Also your element has created some documentation.
Surf to <hostname>:<port>/<package-name>/html/index.html and you will see the
package documentation for your cell. `hello-world` will be in there, and clicking
it brings up the documentation for your `hello-world` element.
