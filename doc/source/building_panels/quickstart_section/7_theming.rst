Demo 7: Using the theme
=============================

************************************
Make the theme-demo element
************************************
In your cell, run:

.. code-block:: bash
    :linenos:

    cd src/html/elements
    ./new-element.js
    name of the new element: theme-demo
    creating new element <theme-demo>...
    removing any .svn folders in  theme-demo
    Finished

You now have a working `theme-demo` element. We'll edit it soon.

****************************************
Register the theme-demo element
****************************************

Edit src/html/elements/elements.html and add the following line

.. code-block:: bash
    :linenos:

    <link rel="import" href="theme-demo/theme-demo.html">

This will tell AjaXell to load our new element.

************************************
Edit the theme-demo element
************************************

Edit src/html/elements/theme-demo/theme-demo.html

.. literalinclude:: /../supervisor/src/html/elements/theme-demo/theme-demo.html
   :language: html
   :linenos:

Now edit src/html/elements/theme-demo/css/theme-demo.scss

.. literalinclude:: /../supervisor/src/html/elements/theme-demo/css/theme-demo.scss
   :language: scss
   :linenos:

Now edit src/html/elements/theme-demo/javascript/theme-demo.js

.. literalinclude:: /../supervisor/src/html/elements/theme-demo/javascript/theme-demo.js
   :language: javascript
   :linenos:

Now execute Grunt to build our new Polymer element.

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

**********************************
Make the theme-demo panel
**********************************

Make a new c++ file /src/common/panels/ThemeDemo.cc

.. literalinclude:: /../supervisor/src/common/panels/ThemeDemo.cc
   :language: cpp
   :linenos:

Make the include/subsystem/supervisor/panels/ThemeDemo.h file.

.. literalinclude:: /../supervisor/include/subsystem/supervisor/panels/ThemeDemo.h
   :language: cpp
   :linenos:

Register the new class in the Makefile.

.. code-block:: makefile
    :linenos:

    Sources=\
    	version.cc\
    	Cell.cc\
    	CellContext.cc\
    	Configuration.cc\
    	...
    	panels/ThemeDemo.cc \
    	...

Now register your new panel in the menu so users can access it.

Edit src/common/Cell.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/ThemeDemo.h"
    ...
    void subsystemsupervisor::Cell::init() {
      ...
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      ...
      panelF->add<subsystempanels::ThemeDemo>("Theme demo");

Now you can compile your cell and you should see the ThemeDemo panel in the
menu under the 'control-panels' section.

.. image:: ThemeDemo.png
   :width: 700px
