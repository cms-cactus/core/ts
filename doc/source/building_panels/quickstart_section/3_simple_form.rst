Demo 3: Sending data to the server
==================================

The `ts-ajax` element can also be used to send data back to the server.
To demonstrate this we'll build a simple form.

*******************************
Make the form-example element
*******************************
In your cell, run:

.. code-block:: bash
    :linenos:

    cd src/html/elements
    ./new-element.js
    name of the new element: form-example
    creating new element <form-example>...
    removing any .svn folders in  form-example
    Finished

***********************************
Register the form-example element
***********************************

Edit src/html/elements/elements.html and add the following line

.. code-block:: bash
    :linenos:

    <link rel="import" href="form-example/form-example.html">

This will tell AjaXell to load our new element.

*******************************
Edit the form-example element
*******************************

Edit src/html/elements/form-example/form-example.html

.. container:: toggle

    .. container:: header

        **/src/html/elements/form-example/form-example.html**

    .. literalinclude:: /../supervisor/src/html/elements/form-example/form-example.html
       :language: html
       :linenos:

Now edit /src/html/elements/form-example/css/form-example.scss

.. container:: toggle

    .. container:: header

        **/src/html/elements/form-example/css/form-example.scss**

    .. literalinclude:: /../supervisor/src/html/elements/form-example/css/form-example.scss
       :language: scss
       :linenos:

Now edit /src/html/elements/form-example/javascript/form-example.js

.. container:: toggle

    .. container:: header

        **/src/html/elements/form-example/javascript/form-example.js**

    .. literalinclude:: /../supervisor/src/html/elements/form-example/javascript/form-example.js
       :language: javascript
       :linenos:


Now execute Grunt to build our new Polymer element.

.. code-block:: bash
    :linenos:

    cd src/html
    grunt

*****************************
Make the form-example panel
*****************************

Make a new c++ file /src/common/panels/FormExample.cc

.. container:: toggle

    .. container:: header

        **/src/common/panels/FormExample.cc**

    .. literalinclude:: /../supervisor/src/common/panels/FormExample.cc
       :language: cpp
       :linenos:

Make the /include/subsystem/supervisor/panels/FormExample.h file.

.. container:: toggle

    .. container:: header

        **/include/subsystem/supervisor/panels/FormExample.h**

    .. literalinclude:: /../supervisor/include/subsystem/supervisor/panels/FormExample.h
       :language: cpp
       :linenos:

Register the new class in the Makefile.

.. code-block:: makefile
    :linenos:

    Sources=\
    	version.cc\
    	Cell.cc\
    	CellContext.cc\
    	Configuration.cc\
    	...
    	panels/FormExample.cc \
    	...

Now register your new panel in the menu so users can access it.

Edit src/common/Cell.cc

.. code-block:: cpp
    :linenos:

    #include "subsystem/supervisor/panels/FormExample.h"
    ...
    void subsystemsupervisor::Cell::init() {
      ...
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      ...
      panelF->add<subsystempanels::FormExample>("Form example");

Now you can compile your cell and you should see the "Form example"
panel in the menu under the 'control-panels' section.

Also your element has created some documentation.
Surf to <hostname>:<port>/<package-name>/html/index.html and you will see the
package documentation for your cell. `form-example` will be in there, and clicking
it brings up the documentation for your `form-example` element.
