The Development environment
===========================

*****
Grunt
*****

You are free to change this build setup as you like, the only thing you need to
do is make it produce valid Polymer elements in the /html folder.

A valid Polymer element looks like this:

.. code-block:: html
    :linenos:

    <link rel="import" href="/extern/bower_components/polymer/polymer.html">

    <dom-module id="my-element-name">
      <template>
        <style>
          :host {
            display: block;
          }
        </style>

      </template>
      <script>
        Polymer({
          is: 'my-element-name',

        });
      </script>
    </dom-module>


*********************
HTML import structure
*********************
