Panels and Web Components
=========================

.. toctree::
   :maxdepth: 2

   webcomponents
   a_panel_as_a_web_component
