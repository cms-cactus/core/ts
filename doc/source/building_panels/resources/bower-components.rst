The bower-components package
############################

The bower-components package contains any front-end package that is pulled from
the internet (i.e. not made by us).

It currently contains:
 * `The Polymer library <https://www.polymer-project.org/1.0/>`_
 * `iron-elements <https://elements.polymer-project.org/browse?package=iron-elements>`_
 * `paper-elements <https://elements.polymer-project.org/browse?package=paper-elements>`_
 * `gold-elements <https://elements.polymer-project.org/browse?package=gold-elements>`_
 * `neon-elements <https://elements.polymer-project.org/browse?package=neon-elements>`_
 * `platinum-elements <https://elements.polymer-project.org/browse?package=platinum-elements>`_
 * `juicy-jsoneditor <https://github.com/Juicy/juicy-jsoneditor>`_
 * `juicy-ace-editor <https://github.com/Juicy/juicy-ace-editor>`_
 * `vaadin-grid <https://vaadin.com/docs/-/part/elements/vaadin-grid/overview.html>`_
 * `paper-datatable <https://github.com/David-Mulder/paper-datatable>`_
 * `jQuery 2.2.2 <https://jquery.com/>`_
 * `moment.js <http://momentjs.com/>`_
 * `page.js <https://visionmedia.github.io/page.js/>`_
 * `cytoscape.js <http://js.cytoscape.org/>`_
 * `file-saver.js <https://github.com/eligrey/FileSaver.js/>`_
 * `saveSvgAsPng <https://github.com/exupero/saveSvgAsPng>`_
 * `KaTeX <https://github.com/Khan/KaTeX>`_

iron-elements
*************
The iron-elements are a set of web components aiming to provide a basic set of
tools and enhancements to standard elements, for example to provide them with
data-binding capabilities.

These elements do not make assumptions about the used layout or styling, and are
expected to maintain a spartan view, if they render a view at all.

Iron-elements aim to extend basic html elements (e.g. <iron-input> to extend
<input>), provide façade elements for javascript functionality (e.g. <iron-ajax>
to easily make AJAX requests), or provide new functionality that would be
considered basic functionality (e.g. <iron-icon> to display an icon).

paper-elements
**************
Paper-elements is a set of elements that focus on bringing Material Design\cite{materialdesign}
to web components.

Paper-elements aims to extend iron-elements with material design (e.g. <iron-input>
becomes <paper-input>), and introduce new elements that are unique to material
design (e.g. <paper-toast>)


gold-elements
*************
Gold elements are input elements for specific use cases (e.g. email, phone numbers,
credit card numbers, \ldots).

They all extend the `paper-input` element and provide specific validation and
formatting functionality.

platinum-elements
*****************
Platinum-elements are a set of Web Components focused on providing a façade for
web-app capabilities like Service Workers, server push, and bluetooth connectivity.

neon-elements
*************
neon-elements are a set of Web Components designed to be façades for the
JavaScript animation API to make them available by purely writing HTML.

These elements do not use CSS Transitions, CSS Animations, or SVG, rather they
use the new Web Animations API (\url{https://www.w3.org/TR/web-animations/}).

These are among the most advanced Web Components in the packages available to
panel developers.
More info about their usage is provided here: \url{https://youtu.be/-tX0e29GQa4}.

juicy-jsoneditor
****************
juicy-json-editor is a web-based tool to view, edit, format, and validate JSON.
It has various modes such as a tree editor, a code editor, and a plain text editor.

juicy-ace-editor
****************
juicy-ace-editor is a web component that provides easy access to the Ace library.
Ace is an embeddable code editor written in JavaScript.
It matches the features and performance of native editors such as Sublime, Vim
and TextMate.
It can be easily embedded in any web page and JavaScript application.
Ace is maintained as the primary editor for Cloud9 IDE and is the successor of
the Mozilla Skywriter (Bespin) project.

vaadin-grid
***********
Vaadin Grid is a fully featured datagrid for showing table data.
It performs great even with huge data sets, fully supporting paging and lazy
loading from any data source like a REST API.
Grid allows you sort and filter data and customize how each cell gets rendered.

paper-datatable
***************
A material design implementation of a data table.
Currently none of the panels use paper-datatable, and use vaadin-grid instead.
This because the development on this element seems dead.

moment.js
*********
Moment.js is a JavaScript library that makes parsing, validating, manipulating,
and displaying dates in JavaScript easy.

page.js
*******
Tiny Express-inspired client-side router.
It is used by AjaXell to manage the loading of panels.

cytoscape.js
************
A JavaScript library designed to paint network graphs.

file-saver.js
*************
FileSaver.js implements the HTML5 W3C saveAs() FileSaver interface in browsers
that do not natively support it.
There is a FileSaver.js demo that demonstrates saving various media types.

FileSaver.js is the solution to saving files on the client-side, and is perfect
for webapps that need to generate files, or for saving sensitive information 
that shouldn't be sent to an external server.

saveSvgAsPng
************
A JavaScript library that can save an SVG element to a PNG file.

KaTeX
*****
KaTeX is a fast, easy-to-use JavaScript library for TeX math rendering on the web.
