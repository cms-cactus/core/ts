The common-elements package
###########################

The common-elements package contains web components that are made in-house and
are useful across multiple projects (e.g. chart elements).

For more information visit `the common-elements page <http://kill-all-humans.cern.ch:2974/ts/common-elements/index.html>`_
