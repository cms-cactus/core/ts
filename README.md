Trigger Supervisor Main page
============================

Documentation of the Trigger Supervisor

The Trigger Supervisor is a software framework to set up, test, operate and monitor the trigger components on one hand and to manage their interplay and the information exchange with the run control part of the data acquisition system on the other. The Trigger Supervisor is conceived to provide a simple and homogeneous client interface to the online software infrastructure of the trigger subsystems.
The Trigger Supervisor is written in C++ on top of the XDAQ framework.


