#include "ts/toolbox/Gzip.h"

#include <string>
#include <vector>
#include <zlib.h>
#include "xgi/Method.h"

std::string tstoolbox::Gzip::encodeOnRequest( xgi::Input* in, std::string const stringIn ) {
  // bool doGZIP = false;
  std::string const acceptEncoding = in->getenv("ACCEPT_ENCODING");
  // ajax::toolbox::getSubmittedValue ( cgi,"ACCEPT_ENCODING" )
  if (!acceptEncoding.empty() && (acceptEncoding.find("gzip") != std::string::npos)) {
    // doGZIP = true;
    return tstoolbox::Gzip::encode(stringIn);
  } else {
    return stringIn;
  }
}

std::string tstoolbox::Gzip::encode ( std::string const stringIn )
{
  size_t srcSize = stringIn.size();
  Bytef const* src = reinterpret_cast<Bytef const*>(stringIn.c_str());
  // NOTE: The magic number below is a rounded-up result of reading
  // the zlib docs (for compress()).
  size_t dstSize = srcSize + (.1 * srcSize) + 16;
  std::vector<Bytef> dst(dstSize);

  z_stream zStream;
  // Not pretty, but necessary here.
  zStream.next_in = const_cast<Bytef*>(src);
  zStream.avail_in = srcSize;
  zStream.next_out = &dst[0];
  zStream.avail_out = dstSize;
  zStream.zalloc = Z_NULL;
  zStream.zfree = Z_NULL;
  zStream.opaque = Z_NULL;

  // NOTE: The '8' below is the value of DEF_MEM_LEVEL, the default
  // memory-usage level. This is defined in zutil.h, which is not
  // installed (for whatever reason) by the zlib-devel RPM.
  // NOTE: The '+ 16' changes the behaviour from zlib to gzip format.
  int res = deflateInit2(&zStream,
                         Z_DEFAULT_COMPRESSION,
                         Z_DEFLATED,
                         MAX_WBITS + 16,
                         8,
                         Z_DEFAULT_STRATEGY);

  if (res != Z_OK)
    {
      std::string const msg = "Failed to initialize zlib.";
      // XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }
  else
    {
      res = deflate(&zStream, Z_FINISH);
      if ((res != Z_STREAM_END) && (res != Z_OK))
        {
          deflateEnd(&zStream);
          std::string const msg = "Failed to compress string using zlib "
            "(looks like a buffer size problem).";
          // XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
        }
      dstSize = zStream.total_out;
      res = deflateEnd(&zStream);
      if (res != Z_OK)
        {
          std::string const msg = "Failed to compress string using zlib.";
          // XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
        }
    }

  // Turn the result back into an std::string.
  std::string const stringOut(dst.begin(), dst.begin() + dstSize);
  return stringOut;
}
