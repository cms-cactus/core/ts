/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun. (original DirectoryStorage: Thomas Themel)
 *************************************************************************/

#include "ts/toolbox/FsTools.h"
#include "ts/toolbox/Base64.h"
#include "ts/toolbox/HttpMessenger.h"

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include <zlib.h>
#include <glob.h>
#include <curl/curl.h>
#include <curl/easy.h>

using namespace tstoolbox;
using namespace std;

string FsTools::folder ( const string& filename )
{
	return boost::filesystem::path ( filename ).branch_path().string();
}

void FsTools::createFolder ( const string& name )
{
	boost::filesystem::create_directory ( name );
}

bool FsTools::fileExists ( const string& name )
{
	return boost::filesystem::exists ( name ) && !boost::filesystem::is_directory ( name );
}

bool FsTools::folderExists ( const string& name )
{
	return boost::filesystem::exists ( name ) && boost::filesystem::is_directory ( name );
}

bool FsTools::isEmpty ( const string& name )
{
	return boost::filesystem::is_empty ( name );
}

bool FsTools::isFolder ( const string& name )
{
	return boost::filesystem::is_directory ( name );
}

void FsTools::rename ( const string& source, const string& target )
{
	boost::filesystem::rename ( source, target );
}

unsigned long FsTools::remove ( const string& name )
{
	return boost::filesystem::remove_all ( name );
}

void FsTools::listAllFiles ( vector<string>& result,
		string& errors,
		const string& folder,
		const string& regex )
{
	boost::filesystem::path current_dir ( folder );
	boost::regex pattern ( regex );

	for ( boost::filesystem::directory_iterator iter ( current_dir ), end;
			iter != end;
			++iter )
	{
		try
		{
			string name = iter->path().filename().string();

			if ( isFolder ( iter->path().string() ) )
			{
				listAllFiles ( result, errors, iter->path().string(), regex );
			}
			else if ( regex_match ( name, pattern ) )
			{
				result.push_back ( iter->path().string() );
			}
		}
		catch ( exception& e )
		{
			//
			// unfortunately, boost::filesystem has issues with many legal file names
			//
			errors += e.what();
			errors += "\n";
		}
	}
}

std::string FsTools::getTextFile ( const std::string& name )
{
	std::ifstream stream ( name.c_str() );

	if ( !stream )
	{
		XCEPT_RAISE ( xcept::Exception, "File " + name + " not available!" );
	}

	// read the entire file (hopefully)
	//
	std::string result;
	getline ( stream, result, '\0' );
	// file was not entirely read! This should not happen with text files!
	//
	iAssert ( !stream.good() );

	if ( !stream )
	{
		XCEPT_RAISE ( xcept::Exception, "Error while reading file " + name + " !" );
	}

	stream.close();
	return result;
}

std::string FsTools::encodeBase64 ( const std::string& filename )
{
	std::ifstream stream ( filename.c_str(), std::ios_base::in | std::ios_base::binary );

	if ( !stream )
	{
		XCEPT_RAISE ( xcept::Exception, "File " + filename + " not available!" );
	}

	std::ostringstream result;

	if ( !Base64::encode ( stream, result ) )
	{
		XCEPT_RAISE ( xcept::Exception, "Error while encoding file " + filename + " !" );
	}

	stream.close();
	return result.str();
}

void FsTools::decodeBase64 ( const std::string& filename,
		const std::string& content )
{
	std::ofstream stream ( filename.c_str(), std::ios_base::out | std::ios_base::binary );

	if ( !stream )
	{
		XCEPT_RAISE ( xcept::Exception, "Cannot write to file " + filename + "!" );
	}

	std::istringstream input ( content );

	if ( !Base64::decode ( input, stream ) )
	{
		XCEPT_RAISE ( xcept::Exception, "Error while decoding to file " + filename + " !" );
	}

	stream.close();
}

std::string FsTools::zCompress ( const std::string& sourceFilename, std::string destFilename)
{
	const int CHUNK_SIZE = 256 * 1024;
	const int LEVEL = Z_DEFAULT_COMPRESSION;

	if ( "" == destFilename )
	{
		destFilename = sourceFilename + ".zlib";
	}

	FILE* source = fopen ( sourceFilename.c_str(), "rb" );

	if ( !source )
	{
		XCEPT_RAISE ( xcept::Exception, "Zlib: Cannot read from file " + sourceFilename + "!" );
	}

	FILE* dest = fopen ( destFilename.c_str(), "wb" );

	if ( !dest )
	{
		XCEPT_RAISE ( xcept::Exception, "Zlib: Cannot write to file " + destFilename + "!" );
	}

	try             // DEBUG: quick hack
	{
		//
		// Compress from file source to file dest until EOF on source.
		// def() returns Z_OK on success, Z_MEM_ERROR if memory could not be
		// allocated for processing, Z_STREAM_ERROR if an invalid compression
		// level is supplied, Z_VERSION_ERROR if the version of zlib.h and the
		// version of the library linked do not match, or Z_ERRNO if there is
		// an error reading or writing the files.
		//
		int ret, flush;
		unsigned have;
		z_stream strm;
		unsigned char in[CHUNK_SIZE];
		unsigned char out[CHUNK_SIZE];
		// allocate deflate state
		strm.zalloc = Z_NULL;
		strm.zfree = Z_NULL;
		strm.opaque = Z_NULL;
		zErrorCheck ( deflateInit ( &strm, LEVEL ) );

		// compress until end of file
		do
		{
			strm.avail_in = fread ( in, 1, CHUNK_SIZE, source );

			if ( ferror ( source ) )
			{
				( void ) deflateEnd ( &strm );
				zErrorCheck ( Z_ERRNO );
				return "";
			}

			flush = feof ( source ) ? Z_FINISH : Z_NO_FLUSH;
			strm.next_in = in;

			// run deflate() on input until output buffer not full, finish
			// compression if all of source has been read in
			do
			{
				strm.avail_out = CHUNK_SIZE;
				strm.next_out = out;
				ret = deflate ( &strm, flush ); // no bad return value
				iAssert ( ret != Z_STREAM_ERROR ); // state not clobbered
				have = CHUNK_SIZE - strm.avail_out;

				if ( fwrite ( out, 1, have, dest ) != have || ferror ( dest ) )
				{
					( void ) deflateEnd ( &strm );
					zErrorCheck ( Z_ERRNO );
					return "";
				}
			}
			while ( strm.avail_out == 0 );

			iAssert ( strm.avail_in == 0 );  // all input will be used
			// done when last data in file processed
		}
		while ( flush != Z_FINISH );

		iAssert ( ret == Z_STREAM_END );     // stream will be complete
		// clean up and return
		( void ) deflateEnd ( &strm );
	}
	catch ( ... )
	{
		if ( source )
		{
			fclose ( source );
		}

		if ( dest )
		{
			fclose ( dest );
		}

		throw;
	}

	if ( source )
	{
		fclose ( source );
	}

	if ( dest )
	{
		fclose ( dest );
	}

	return destFilename;
}

void FsTools::zDecompress ( const std::string& sourceFilename, const std::string& destFilename )
{
	const int CHUNK_SIZE = 256 * 1024;
	FILE* source = fopen ( sourceFilename.c_str(), "rb" );

	if ( !source )
	{
		XCEPT_RAISE ( xcept::Exception, "Zlib: Cannot read from file " + sourceFilename + "!" );
	}

	FILE* dest = fopen ( destFilename.c_str(), "wb" );

	if ( !dest )
	{
		XCEPT_RAISE ( xcept::Exception, "Zlib: Cannot write to file " + destFilename + "!" );
	}

	try             // DEBUG: quick hack
	{
		//
		// Decompress from file source to file dest until stream ends or EOF.
		// inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
		// allocated for processing, Z_DATA_ERROR if the deflate data is
		// invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
		// the version of the library linked do not match, or Z_ERRNO if there
		// is an error reading or writing the files.
		int ret = 0;
		unsigned have;
		z_stream strm;
		unsigned char in[CHUNK_SIZE];
		unsigned char out[CHUNK_SIZE];
		/* allocate inflate state */
		strm.zalloc = Z_NULL;
		strm.zfree = Z_NULL;
		strm.opaque = Z_NULL;
		strm.avail_in = 0;
		strm.next_in = Z_NULL;
		zErrorCheck ( inflateInit ( &strm ) );

		/* decompress until deflate stream ends or end of file */
		do
		{
			strm.avail_in = fread ( in, 1, CHUNK_SIZE, source );

			if ( ferror ( source ) )
			{
				( void ) inflateEnd ( &strm );
				zErrorCheck ( Z_ERRNO );
				return;
			}

			if ( strm.avail_in == 0 )
			{
				break;
			}

			strm.next_in = in;

			/* run inflate() on input until output buffer not full */
			do
			{
				strm.avail_out = CHUNK_SIZE;
				strm.next_out = out;
				ret = inflate ( &strm, Z_NO_FLUSH );
				iAssert ( ret != Z_STREAM_ERROR ); /* state not clobbered */

				switch ( ret )
				{
				case Z_NEED_DICT:
					ret = Z_DATA_ERROR;     /* and fall through */
				case Z_DATA_ERROR:
				case Z_MEM_ERROR:
					( void ) inflateEnd ( &strm );
					zErrorCheck ( ret );
				}

				have = CHUNK_SIZE - strm.avail_out;

				if ( fwrite ( out, 1, have, dest ) != have || ferror ( dest ) )
				{
					( void ) inflateEnd ( &strm );
					zErrorCheck ( Z_ERRNO );
					return;
				}
			}
			while ( strm.avail_out == 0 );

			/* done when inflate() says it's done */
		}
		while ( ret != Z_STREAM_END );

		/* clean up and return */
		( void ) inflateEnd ( &strm );
		zErrorCheck ( ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR );
	}
	catch ( ... )
	{
		if ( source )
		{
			fclose ( source );
		}

		if ( dest )
		{
			fclose ( dest );
		}

		throw;
	}

	if ( source )
	{
		fclose ( source );
	}

	if ( dest )
	{
		fclose ( dest );
	}
}

void FsTools::zErrorCheck ( int zlibResult )
{
	switch ( zlibResult )
	{
	case Z_ERRNO:

		if ( ferror ( stdin ) )
		{
			XCEPT_RAISE ( xcept::Exception, "Zlib: Error reading stdin" );
		}

		if ( ferror ( stdout ) )
		{
			XCEPT_RAISE ( xcept::Exception, "Zlib: Error writing stdout" );
		}

	case Z_STREAM_ERROR:
		XCEPT_RAISE ( xcept::Exception, "Zlib: Invalid compression level" );
	case Z_DATA_ERROR:
		XCEPT_RAISE ( xcept::Exception, "Zlib: Invalid or incomplete deflate data" );
	case Z_MEM_ERROR:
		XCEPT_RAISE ( xcept::Exception, "Zlib: Out of memory" );
	case Z_VERSION_ERROR:
		XCEPT_RAISE ( xcept::Exception, "Zlib: Version mismatch!" );
	}
}

std::string FsTools::packData ( const std::string& path, const std::string& folder )
{
	iAssert ( folderExists ( path + "/" + folder ) );
	// Unfortunately, for tar I have to change to the folder to get rid of the path :(
	//
	std::string filename = folder + ".tgz";
	int exitCode = Tools::loopedSystemCall ( "cd " + path + "; tar -czf " + filename + " " + folder );

	if ( 0 != exitCode )
	{
		XCEPT_RAISE ( xcept::Exception, "Error while compressing folder " + folder + " in " + path
				+ " (exit code = " + Tools::asString ( exitCode ) + ")!" );
	}

	return encodeBase64 ( filename );
}

void FsTools::unpackData ( const std::string& path, const std::string& folder, const std::string& content )
{
	iAssert ( folderExists ( path ) );
	std::string tgzFile = path + "/" + folder + ".tgz";
	decodeBase64 ( tgzFile, content );
	int exitCode = Tools::loopedSystemCall ( "tar -xzf " + tgzFile + " -C " + path + " --overwrite --no-same-owner -m" );

	if ( 0 != exitCode )
	{
		XCEPT_RAISE ( xcept::Exception, "Error while unpacking " + tgzFile + " to folder " + path
				+ " (exit code = " + Tools::asString ( exitCode ) + ")!" );
	}
}

std::vector<std::string> FsTools::listFiles ( const std::string& pathPattern, bool filesOnly, int globFlags)
{
	glob_t globbuf;
	std::vector<std::string> result;
	glob ( pathPattern.c_str(), GLOB_BRACE | GLOB_MARK | GLOB_TILDE | globFlags, NULL, &globbuf );

	if ( globbuf.gl_pathc > 0 )
	{
		for ( unsigned int i=0; i < globbuf.gl_pathc; ++i )
		{
			std::string file = globbuf.gl_pathv[i];

			if ( !filesOnly || ( !file.empty() && ( '/' != file[file.size()-1] ) ) )
			{
				result.push_back ( file );
			}
		}
	}

	globfree ( &globbuf );
	return result;
}


std::string FsTools::expandFolderName ( const std::string& name )
{
	if ( "" != name )
	{
		std::vector<std::string> folder = listFiles ( name, false );
		iAssertMsg ( 1 == folder.size(), "Folder '" + name + "' does not exist (or contains wildcards)!" );
		return folder[0];
	}

	return name;
}


void FsTools::syncFile ( const std::string& name,
		const std::string& url,
		const std::string& sourceHostUrl,
		const std::string& myHostUrl )
{
	// file does exist already somewhere: compare host names (without the port name!)
	//
	// Attention: As we compare without the port name, the alias (e.g. '/tmp') must be identical for all
	//            cells on the same machine!
	//
	if ( Tools::hostName ( sourceHostUrl, false ) != Tools::hostName ( myHostUrl, false ) )
	{
		//
		// not on this machine (at least maybe not in the latest version): download it
		//
		// the file is downloaded to a different file name to prevent troubles in case the source host is the
		// same machine (in case of an alias), which would mean that the file is both read and written at the
		// same time.
		//
		std::string tempName = name + ".tmp-download";
		HttpMessenger fileToDownload;
		fileToDownload.downloadFile ( url, tempName );
		remove ( name );                                                    // in case a previous version exists
		rename ( tempName, name );
	}
}

