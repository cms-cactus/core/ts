#include "config/version.h"

#include "ts/toolbox/version.h"

GETPACKAGEINFO ( tstoolbox )

void tstoolbox::checkPackageDependencies() throw ( config::PackageInfo::VersionException )
{
  CHECKDEPENDENCY ( config );
}

std::set<std::string, std::less<std::string> > tstoolbox::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;
  ADDDEPENDENCY ( dependencies,config );
  return dependencies;
}
