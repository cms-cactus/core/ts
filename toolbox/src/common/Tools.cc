/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun.
 *************************************************************************/
#include "ts/toolbox/Tools.h"

#include "ts/toolbox/Timer.h"

#include "xdata/Serializable.h"

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include <stdio.h>
#include <iostream>
#include <fstream>

namespace tstoolbox {


// --------------------------------------------------------
std::string Tools::vector2json ( const std::vector<std::string>& items )
{
  return ( items.empty() ) ? "[]"
      : "[\"" + printList ( items, "\",\"" ) + "\"]";  // TODO: escape '"'
}


// --------------------------------------------------------
std::string Tools::map2json ( const std::map<std::string, std::string>& items )
{
  std::ostringstream out;
  out << "{";

  for ( std::map<std::string, std::string>::const_iterator i = items.begin(); i != items.end(); ++i )
  {
    if ( i != items.begin() )
    {
      out << ",";
    }

    out << "\"" << i->first << "\":\"" << i->second << "\"";     // TODO: escape '"'
  }

  out << "}";
  return out.str();
}


// --------------------------------------------------------
std::vector<unsigned int> Tools::string2IntVector ( const std::string& paramName,
    const std::string& param,
    const size_t minLength,
    const size_t fillLength,
    const int maxLegalValue)
{
  if ( param.size() < minLength ) { // too short ... missing values

    XCEPT_RAISE ( tsexception::ParameterError, paramName + " contains not enough values!" );
  }

  std::vector<unsigned int> vec = std::vector<unsigned int> ( std::max ( param.size(), fillLength ), 0 );

  for ( unsigned int i=0; i < param.size(); ++i )
  {
    char character = param[i];
    int value = ( character - '0' );
    vec[i] = value;

    if ( ( value < 0 ) || ( value > maxLegalValue ) ) { // no legal value

      XCEPT_RAISE ( tsexception::ParameterError, paramName + " contains illegal values!" );
    }
  }

  return vec;
}


// --------------------------------------------------------
std::string Tools::extractParam ( const std::string& source,
    const std::string& before,
    const std::string& after )
{
  size_t posBefore, posBegin, posEnd;
  std::string result = "";
  posBefore = ( "" == before ) ? 0 : source.find ( before );

  if ( posBefore != std::string::npos )
  {
    posBegin = posBefore + before.length();
    posEnd = ( "" == after ) ? source.length() : source.find ( after, posBegin ); // length == pos _after_ substr

    if ( posEnd != std::string::npos )
    {
      result = source.substr ( posBegin, posEnd-posBegin );
    }
  }

  return result;
}


// --------------------------------------------------------
std::string Tools::fillString ( const std::string& in, const int width, const std::string& fillChar)
{
  std::ostringstream out;
  out << in;

  for ( int i=in.size(); i < width; ++i )
  {
    out << fillChar;
  }

  return out.str();
}


// --------------------------------------------------------
std::string Tools::xmlEncode ( const std::string& xml,bool encodeQuot)
{
  // "&amp;" must be the first one!! (necessary for xmlDecode(xmlEncode(xmlEncode(msg))))
  //
  std::string result = boost::algorithm::replace_all_copy (
      boost::algorithm::replace_all_copy (
          boost::algorithm::replace_all_copy (
              boost::algorithm::replace_all_copy ( xml, "&", "&amp;" ),
              "<", "&lt;" ),
              ">", "&gt;" ),
              "'", "&apos;" );
  return ( encodeQuot ) ? boost::algorithm::replace_all_copy ( result, "\"", "&quot;" ) : result;
}


// --------------------------------------------------------
std::string Tools::xmlDecode ( const std::string& xml )
{
  // "&amp;" must be the last one!! (see xmlEncode())
  //
  return boost::algorithm::replace_all_copy (
      boost::algorithm::replace_all_copy (
          boost::algorithm::replace_all_copy (
              boost::algorithm::replace_all_copy (
                  boost::algorithm::replace_all_copy ( xml, "&lt;", "<" ),
                  "&gt;", ">" ),
                  "&apos;", "'" ),
                  "&quot;", "\"" ),
                  "&amp;", "&" );
}



// --------------------------------------------------------
bool Tools::startsWith ( const std::string& line,const std::string& substring )
{
  return ( 0 == line.find ( substring ) );
}


// --------------------------------------------------------
std::vector<std::string> Tools::split ( const std::string& toSplit, const std::string& delimiters )
{
  std::vector<std::string> result;
  boost::split ( result, toSplit, boost::is_any_of ( delimiters ) );
  return result;
}


// --------------------------------------------------------
bool Tools::checkAvailableMemory ( size_t availableMB )
{
  try
  {
    char* dummy = new char[availableMB * 1024 * 1024];
    delete[] dummy;
  }
  catch ( ... )
  {
    return false;
  }

  return true;
}


// --------------------------------------------------------
bool Tools::checkAvailableMemory ( size_t availableMB, log4cplus::Logger& log )
{
  if ( !Tools::checkAvailableMemory ( availableMB ) )
  {
    return false;
  }

  // test with increased values
  //
  while ( Tools::checkAvailableMemory ( availableMB += 100 ) )
  {
  }

  LOG4CPLUS_INFO ( log, "checkAvailableMemory(): Less than " << availableMB << " MB." );
  return true;
}


// --------------------------------------------------------
std::string Tools::execWithPipe(const std::string& command, bool AddNewline)
{
  FILE* fpipe;
  char line[256];

  if ( ! ( fpipe = ( FILE* ) popen ( command.c_str(), "r" ) ) ) {
    
    XCEPT_RAISE ( Tools::ExecutionFailed, "Trying to execute '" + command + "'");
  }

  std::ostringstream out;

  while ( fgets ( line, sizeof line, fpipe ) ) {
    
    if ( AddNewline )
      out << "<p>" <<  line << "</p>";
    else
      out << line;
  }

  if (!feof (fpipe))
    XCEPT_RAISE ( Tools::ExecutionFailed, "Trying to read output to '" + command + "'" );

  pclose(fpipe);

  return out.str();
}


// --------------------------------------------------------
int Tools::loopedSystemCall ( const std::string& command,int numAttempts )
{
  int exitCode = 1;

  for ( int i=0; ( i < numAttempts ) && ( 0 != exitCode ); ++i ) {
    
    exitCode = system ( command.c_str() );
  }

  return exitCode;
}


// --------------------------------------------------------
std::string Tools::hostName ( const std::string& url,bool includePort )
{
  std::string name = url;

  if ( name.find ( "//" ) != std::string::npos )
  {
    name = Tools::extractParam ( name, "//", "" );
  }

  if ( name.find ( "/" )  != std::string::npos )
  {
    name = Tools::extractParam ( name, "", "/" );
  }

  if ( !includePort && ( name.find ( ":" ) != std::string::npos ) )
  {
    name = Tools::extractParam ( name, "", ":" );
  }

  return name;
}


// --------------------------------------------------------
void Tools::storeException ( boost::scoped_ptr<xcept::Exception>& holder, xcept::Exception& e )
{
  boost::scoped_ptr<xcept::Exception> ex ( e.clone() );
  holder.swap ( ex );
}

} // end ns toolbox


