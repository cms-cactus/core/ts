#include "ts/toolbox/ReadWriteLock.h"

#include "ts/exception/CellException.h"

#include "toolbox/utils.h"

#include <sstream>

tstoolbox::ReadWriteLock::ReadWriteLock()
{
  int rc = pthread_rwlock_init ( &rwlock_, NULL );

  if ( rc )
  {
    std::ostringstream msg;
    msg << "Can not create pthread rwlock. Error '" << rc << "'";
    XCEPT_RAISE ( tsexception::LockError,msg.str() );
  }
}

tstoolbox::ReadWriteLock::~ReadWriteLock()
{
  int rc = pthread_rwlock_destroy ( &rwlock_ );

  if ( rc )
  {
    std::ostringstream msg;
    msg << "Can not destroy pthread rwlock. Error '" << rc << "'";
    XCEPT_RAISE ( tsexception::LockError,msg.str() );
  }
}

void tstoolbox::ReadWriteLock::rlock()
{
  int rc = pthread_rwlock_rdlock ( &rwlock_ );

  if ( rc )
  {
    std::ostringstream msg;
    msg << "Can not lock pthread-read rwlock. Error '" << rc << "'";
    XCEPT_RAISE ( tsexception::LockError,msg.str() );
  }
}

void tstoolbox::ReadWriteLock::unlock()
{
  int rc = pthread_rwlock_unlock ( &rwlock_ );

  if ( rc )
  {
    std::ostringstream msg;
    msg << "Can not unlock pthread-read rwlock. Error '" << rc << "'";
    XCEPT_RAISE ( tsexception::LockError,msg.str() );
  }
}

void tstoolbox::ReadWriteLock::wlock()
{
  int rc = pthread_rwlock_wrlock ( &rwlock_ );

  if ( rc )
  {
    std::ostringstream msg;
    msg << "Can not lock pthread-write rwlock. Error '" << rc << "'";
    XCEPT_RAISE ( tsexception::LockError,msg.str() );
  }
}


