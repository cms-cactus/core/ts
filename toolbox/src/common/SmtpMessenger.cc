#include "ts/toolbox/SmtpMessenger.h"

#include<stdio.h>
#include<errno.h>

#include <unistd.h>
#include <string.h>

#include "ts/exception/CellException.h"

using namespace std;

namespace tstoolbox {

// --------------------------------------------------------
SmtpMessenger::SmtpMessenger ( const string& to, const string& subject, const string& message ) :
  to_ ( to ),
  from_ ( getDefaultSender() ),
  subject_ ( subject ),
  message_ ( message )
{
  if ( to_.empty() )
  {
    XCEPT_RAISE ( tsexception::SendMailError,"Can not send mail. Destination address is empty" );
  }

  if ( subject_.empty() )
  {
    XCEPT_RAISE ( tsexception::SendMailError,"Can not send mail. Subject is empty" );
  }
}


// --------------------------------------------------------
SmtpMessenger::~SmtpMessenger()
{
}


// --------------------------------------------------------
string SmtpMessenger::getDefaultSender()
{
  return string( "triggersupervisor.noreply@" + getHostname() );
}


// --------------------------------------------------------
string SmtpMessenger::getHostname()
{
  char hostname[256];
  int res = gethostname ( hostname,255 );

  if ( res < 0 )  {
    
    string msg;
    msg = "Cannot get hostname. Error message: ";
    msg += strerror ( res );
    XCEPT_RAISE ( tsexception::SendMailError,msg );
  }

  return string(hostname);
}

// --------------------------------------------------------
void SmtpMessenger::send(std::string aFrom)
{
  FILE* mailpipe = popen ( "/usr/lib/sendmail -t", "w" );

  if ( mailpipe != NULL )
  {
    fprintf ( mailpipe, "To: %s\n", to_.c_str() );
    
    if ( aFrom == "" )
      fprintf ( mailpipe, "From: %s\n", from_.c_str() );
    else
      fprintf ( mailpipe, "From: %s\n", aFrom.c_str() );
    fprintf ( mailpipe, "Subject: %s\n\n", subject_.c_str() );
    fwrite ( message_.c_str(), 1, strlen ( message_.c_str() ), mailpipe );
    fwrite ( ".\n", 1, 2, mailpipe );
    pclose ( mailpipe );
  }
  else
  {
    string err = "Can not send mail:";
    err += strerror ( errno );
    XCEPT_RAISE ( tsexception::SendMailError,err );
  }
}

} // end ns tstoolbox