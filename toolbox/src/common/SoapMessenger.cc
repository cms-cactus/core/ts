/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/
#include "ts/toolbox/SoapMessenger.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "pt/PeerTransportAgent.h"
#include "pt/Messenger.h"
#include "pt/SOAPMessenger.h"
#include "pt/exception/Exception.h"

#include "xdaq/XceptSerializer.h"

#include "xcept/tools.h"

#include "ts/exception/CellException.h"
#include "ts/toolbox/CellToolbox.h"
#include <thread>
#include <chrono>

tstoolbox::SoapMessenger::SoapMessenger ( log4cplus::Logger& log )
  :logger_ ( log4cplus::Logger::getInstance ( log.getName() +".SoapMessenger" ) )
{
  ;
}

tstoolbox::SoapMessenger::~SoapMessenger()
{
  ;
}

log4cplus::Logger& tstoolbox::SoapMessenger::getLogger()
{
  return logger_;
}

xoap::MessageReference tstoolbox::SoapMessenger::send ( xoap::MessageReference msg, const std::string& local_url, const std::string& url, const std::string& urn )
{
  try
  {
    msg->getMimeHeaders()->setHeader ( "SOAPAction", urn );
  }
  catch ( xcept::Exception& e )
  {
    XCEPT_RETHROW ( tsexception::SoapEncodingError, "Can not add SOAP header 'SOAPAction: " + urn + "'",e );
  }

  xoap::MessageReference r;
  int count ( 0 );
  timeval start;
  gettimeofday ( &start,0 );
  const int MAX_TIMEOUT_MS=10000;
  const int MAX_ATTEMPTS=10;

  while ( true )
  {
    try
    {
      ++count;
      pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
      pt::Address::Reference destAddress = pta->createAddress ( url, "soap" );
      pt::Address::Reference localAddress = pt::getPeerTransportAgent()->createAddress ( local_url, "soap" );
      pt::Messenger::Reference mr = pta->getMessenger ( destAddress,localAddress );
      pt::SOAPMessenger& m = dynamic_cast<pt::SOAPMessenger&> ( *mr );
      r = m.send ( msg );
      break;
    }
    catch ( xcept::Exception& e )
    {
      std::ostringstream msg;
      msg << "SOAP Communication attempt failed (trying again). ";
      msg << xcept::stdformat_exception_history ( e );
      LOG4CPLUS_WARN ( getLogger(),msg.str() );
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      timeval end;
      gettimeofday ( &end,0 );

      if ( count >= MAX_ATTEMPTS || tstoolbox::timeval_diff ( end,start ) >MAX_TIMEOUT_MS )
      {
        XCEPT_RETHROW ( tsexception::CommunicationError,"Several SOAP communication attempts have failed. It is possible that the server at '" + url + "' is down",e );
      }
    }
  }

  xoap::SOAPBody rb = r->getSOAPPart().getEnvelope().getBody();

  if ( rb.hasFault() )
  {
    std::ostringstream err;
    err << "SOAP reply contains SOAPFault. ";

    if ( rb.getFault().hasDetail() )
    {
      xoap::SOAPElement detail = rb.getFault().getDetail();
      xcept::Exception rae;
      xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
      XCEPT_RETHROW ( tsexception::SoapFault, err.str(), rae );
    }
    else
    {
      err << rb.getFault().getFaultString();
      XCEPT_RAISE ( tsexception::SoapFault,err.str() );
    }
  }

  return r;
}

