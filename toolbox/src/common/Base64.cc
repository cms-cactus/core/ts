/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun. (original author: Bob Trower)
 *
 * I reduced the original code to the bare minimum and modified it in
 * order to use c++ streams (instead of reading and writing from files
 * only).
 *
 *************************************************************************/


#include "ts/toolbox/Base64.h"

#include <istream>
#include <ostream>

/*
 ** Translation Table as described in RFC1113
 */
static const char cb64[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/*
 ** Translation Table to decode (created by author)
 */
static const char cd64[]="|$$$}rstuvwxyz{$$$$$$$>?@ABCDEFGHIJKLMNOPQRSTUVW$$$$$$XYZ[\\]^_`abcdefghijklmnopq";

void tstoolbox::Base64::encodeblock ( unsigned char in[3], unsigned char out[4], int len )
{
	out[0] = cb64[ in[0] >> 2 ];
	out[1] = cb64[ ( ( in[0] & 0x03 ) << 4 ) | ( ( in[1] & 0xf0 ) >> 4 ) ];
	out[2] = ( unsigned char ) ( len > 1 ? cb64[ ( ( in[1] & 0x0f ) << 2 ) | ( ( in[2] & 0xc0 ) >> 6 ) ] : '=' );
	out[3] = ( unsigned char ) ( len > 2 ? cb64[ in[2] & 0x3f ] : '=' );
}


void tstoolbox::Base64::decodeblock ( unsigned char in[4], unsigned char out[3] )
{
	out[ 0 ] = ( unsigned char ) ( in[0] << 2 | in[1] >> 4 );
	out[ 1 ] = ( unsigned char ) ( in[1] << 4 | in[2] >> 2 );
	out[ 2 ] = ( unsigned char ) ( ( ( in[2] << 6 ) & 0xc0 ) | in[3] );
}


bool tstoolbox::Base64::encode ( std::istream& infile, std::ostream& outfile, int linesize)
{
	unsigned char in[3], out[4];
	int i, len; // blocksout = 0;

	while ( infile.good() )
	{
		len = 0;

		for ( i = 0; i < 3; i++ )
		{
			in[i] = ( unsigned char ) infile.get();

			if ( !infile.eof() )
			{
				len++;
			}
			else
			{
				in[i] = 0;
			}
		}

		if ( len )
		{
			tstoolbox::Base64::encodeblock ( in, out, len );

			for ( i = 0; i < 4; i++ )
			{
				outfile.put ( out[i] );
			}

			//                    blocksout++;
		}

		// jhammer: This is not required by the RFC!! (only for MIME / SMTP)
        		  //
        		  //                if (blocksout >= (linesize/4) || infile.eof()) {
		//                    if (blocksout) {
		//                        outfile << "\r\n";
		//                    }
		//                    blocksout = 0;
		//                }
	}

	return infile.eof();
}


bool tstoolbox::Base64::decode ( std::istream& infile, std::ostream& outfile )
{
	unsigned char in[4], out[3], v;
	int i, len;

	while ( infile.good() )
	{
		for ( len = 0, i = 0; i < 4 && !infile.eof(); i++ )
		{
			v = 0;

			while ( !infile.eof() && v == 0 )
			{
				v = ( unsigned char ) infile.get();
				v = ( unsigned char ) ( ( v < 43 || v > 122 ) ? 0 : cd64[ v - 43 ] );

				if ( v )
				{
					v = ( unsigned char ) ( ( v == '$' ) ? 0 : v - 61 );
				}
			}

			if ( !infile.eof() )
			{
				len++;

				if ( v )
				{
					in[ i ] = ( unsigned char ) ( v - 1 );
				}
			}
			else
			{
				in[i] = 0;
			}
		}

		if ( len )
		{
			tstoolbox::Base64::decodeblock ( in, out );

			for ( i = 0; i < len - 1; i++ )
			{
				outfile.put ( out[i] );
			}
		}
	}

	return infile.eof();
}


