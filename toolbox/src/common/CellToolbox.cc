/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril                                *
 *************************************************************************/

#include "toolbox/string.h"
#include "toolbox/Runtime.h"
#include "toolbox/FileSystemInfo.h"
#include "toolbox/exception/Exception.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/exception/CellException.h"

#include <string>
#include <iomanip>

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/SOAPName.h"

#include "xdata/soap/Serializer.h"
#include "xdata/soap/NamespaceURI.h"
#include "xdata/Serializable.h"
#include "xdata/SimpleType.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"

#include "xdaq/XceptSerializer.h"

#include "xcept/Exception.h"
#include "xdata/exception/Exception.h"

#include "log4cplus/logger.h"

#include "cgicc/Cgicc.h"

#include <sys/time.h>
#include <unistd.h>

namespace tstoolbox {

std::string getCommand ( xoap::MessageReference msg )
{
  std::string com;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      com = xoap::XMLCh2String ( command->getLocalName() );
    }
  }

  return com;
}

std::string getNameSpace ( xoap::MessageReference msg )
{
  std::string namespaceURI;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      namespaceURI = xoap::XMLCh2String ( command->getNamespaceURI() );
    }
  }

  return namespaceURI;
}

bool isAsync ( xoap::MessageReference msg )
{
  bool async = false;
  bool throw_exception = true;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      if ( command->hasAttributes() )
      {
        DOMNamedNodeMap* nodeMap = command->getAttributes();
        std::string attrs = "async";
        XMLCh* attrx =  XMLString::transcode ( attrs.c_str() );
        DOMNode* asyncNode = nodeMap->getNamedItem ( attrx );

        if ( asyncNode!=NULL )
        {
          const XMLCh* asyncValue = asyncNode->getNodeValue();
          std::string asyncString = xoap::XMLCh2String ( asyncValue );

          if ( asyncString=="true" )
          {
            //if it exist and async = "true" then return true;
            async = true;
            throw_exception = false;
          }
          else
          {
            async = false;
            throw_exception = false;
          }
        }

        XMLString::release ( &attrx );
      }
    }
  }

  if ( throw_exception )
  {
    std::string msg = "The async attribute was not found";
    XCEPT_RAISE ( tsexception::SoapParsingError, msg );
  }

  return async;
}

std::string getCid ( xoap::MessageReference msg )
{
  std::string cid;
  bool throw_exception = true;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      if ( command->hasAttributes() )
      {
        DOMNamedNodeMap* nodeMap = command->getAttributes();
        std::string attrs = "cid";
        XMLCh* attrx =  XMLString::transcode ( attrs.c_str() );
        DOMNode* asyncNode = nodeMap->getNamedItem ( attrx );

        if ( asyncNode!=NULL )
        {
          const XMLCh* asyncValue = asyncNode->getNodeValue();
          cid = xoap::XMLCh2String ( asyncValue );
          throw_exception = false;
        }

        XMLString::release ( &attrx );
      }
    }
  }

  if ( throw_exception )
  {
    std::string msg = "The cid attribute was not found";
    XCEPT_RAISE ( tsexception::SoapParsingError, msg );
  }

  return cid;
}

std::string getSid ( xoap::MessageReference msg )
{
  std::string sid;
  bool throw_exception = true;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      if ( command->hasAttributes() )
      {
        DOMNamedNodeMap* nodeMap = command->getAttributes();
        std::string attrs = "sid";
        XMLCh* attrx =  XMLString::transcode ( attrs.c_str() );
        DOMNode* asyncNode = nodeMap->getNamedItem ( attrx );

        if ( asyncNode!=NULL )
        {
          const XMLCh* asyncValue = asyncNode->getNodeValue();
          sid = xoap::XMLCh2String ( asyncValue );
          throw_exception = false;
        }

        XMLString::release ( &attrx );
      }
    }
  }

  if ( throw_exception )
  {
    std::string msg = "The sid attribute was not found";
    XCEPT_RAISE ( tsexception::SoapParsingError, msg );
  }

  return sid;
}


double getTimeus()
{
  struct timeval ts;
  double us;
  gettimeofday ( &ts, NULL );
  us = 1e6 * ( ( double ) ts.tv_sec );
  us += ( double ) ts.tv_usec;
  return us;
}

std::string getCallbackUrl ( xoap::MessageReference msg )
{
  std::string fun;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  //cout<< toolbox::toString("hola1");
  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    //cout << toolbox::toString("hola2");
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      //cout << toolbox::toString("hola3%s",coma.c_str());
      //we are at the command level
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        //cout << toolbox::toString("hola4");
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          //cout << toolbox::toString("hola5,%s",com.c_str());
          if ( com=="callbackUrl" )
          {
            DOMNode* command3 = command2->getFirstChild();

            if ( command3 )
            {
              fun=xoap::XMLCh2String ( command3->getNodeValue() );
            }
            else
            {
              fun="";
            }

            return fun;
          }
        }
      }
    }
  }

  return fun;
}

std::string getCallbackUrn ( xoap::MessageReference msg )
{
  std::string fun;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  //cout<< toolbox::toString("hola1");
  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    //cout << toolbox::toString("hola2");
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      //cout << toolbox::toString("hola3%s",coma.c_str());
      //we are at the command level
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        //cout << toolbox::toString("hola4");
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          //cout << toolbox::toString("hola5,%s",com.c_str());
          if ( com=="callbackUrn" )
          {
            DOMNode* command3 = command2->getFirstChild();

            if ( command3 )
            {
              fun=xoap::XMLCh2String ( command3->getNodeValue() );
            }
            else
            {
              fun="";
            }

            return fun;
          }
        }
      }
    }
  }

  return fun;
}

std::string getCallbackFun ( xoap::MessageReference msg )
{
  std::string fun;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  //cout<< toolbox::toString("hola1");
  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    //cout << toolbox::toString("hola2");
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      //cout << toolbox::toString("hola3%s",coma.c_str());
      //we are at the command level
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        //cout << toolbox::toString("hola4");
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          //cout << toolbox::toString("hola5,%s",com.c_str());
          if ( com=="callbackFun" )
          {
            DOMNode* command3 = command2->getFirstChild();

            if ( command3 )
            {
              fun=xoap::XMLCh2String ( command3->getNodeValue() );
            }
            else
            {
              fun="";
            }

            return fun;
          }
        }
      }
    }
  }

  return fun;
}

std::string getOperation ( xoap::MessageReference msg )
{
  xoap::SOAPBody body = msg->getSOAPPart().getEnvelope().getBody();

  if ( body.hasFault() )
  {
    std::ostringstream err;
    err << "SOAPFault found while getting Operation identifier from message. ";

    if ( body.getFault().hasDetail() )
    {
      xoap::SOAPElement detail = body.getFault().getDetail();
      xcept::Exception rae;
      xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
      XCEPT_RETHROW ( tsexception::SoapFault, err.str(), rae );
    }
    else
    {
      err << body.getFault().getFaultString();
      XCEPT_RAISE ( tsexception::SoapFault,err.str() );
    }
  }

  std::string fun;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  //cout<< toolbox::toString("hola1");
  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    //cout << toolbox::toString("hola2");
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      //cout << toolbox::toString("hola3%s",coma.c_str());
      //we are at the command level
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        //cout << toolbox::toString("hola4");
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          //cout << toolbox::toString("hola5,%s",com.c_str());
          if ( com=="operation" )
          {
            DOMNode* command3 = command2->getFirstChild();
            fun=xoap::XMLCh2String ( command3->getNodeValue() );
            //cout << toolbox::toString("hola6,%s",fun.c_str());
          }
        }
      }
    }
  }

  return fun;
}

std::string getOpid ( xoap::MessageReference msg )
{
  //cout<< toolbox::toString("h1") << endl;
  std::string fun;
  //cout<< toolbox::toString("h2")<< endl;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  //cout<< toolbox::toString("h3");
  DOMNodeList* bodyList = node->getChildNodes();

  //cout<< toolbox::toString("hola1")<< endl;
  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    //cout << toolbox::toString("hola2")<< endl;
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      //cout << toolbox::toString("hola3%s",coma.c_str())<< endl;
      //we are at the command level
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        //cout << toolbox::toString("hola4")<< endl;
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          //cout << toolbox::toString("hola5,%s",com.c_str())<< endl;
          if ( com=="operation" )
          {
            //cout << "1" << endl;
            //DOMNode* command3 = command2->getFirstChild();
            //cout << "2" << endl;
            if ( command2->hasChildNodes() )
            {
              DOMNode* command3 = command2->getFirstChild();
              //cout << "3" << endl;
              fun=xoap::XMLCh2String ( command3->getNodeValue() );
              //cout << "4" << endl;
              //cout << toolbox::toString("hola6,%s",fun.c_str());
            }
          }
        }
      }
    }
  }

  return fun;
}

xdata::Serializable* getXdaqParameter ( xoap::MessageReference msg )
{
  xdata::Serializable* ret ( 0 );
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          DOMNodeList* bodyList3 = command2->getChildNodes();

          for ( unsigned int j = 0; j < bodyList3->getLength(); j++ )
          {
            try
            {
              DOMNode* command3 = bodyList3->item ( j );
              xdata::soap::Serializer serializer;
              ret = analyse ( command3 );
              serializer.import ( ret, command3 ); //ret,
            }
            catch ( xdata::exception::Exception& xde )
            {
              XCEPT_RETHROW ( tsexception::SoapParsingError, "Failed to deserialize payload", xde );
            }
          }
        }
      }
    }
  }

  if ( !ret )
  {
    XCEPT_RAISE ( tsexception::SoapParsingError,"Can not find parameter value coming from XDAQ application" );
  }

  return ret;
}

xdata::Serializable* getPayload ( xoap::MessageReference msg )
{
  xoap::SOAPBody body = msg->getSOAPPart().getEnvelope().getBody();

  if ( body.hasFault() )
  {
    std::ostringstream err;
    err << "SOAPFault found while getting payload from message. ";

    if ( body.getFault().hasDetail() )
    {
      xoap::SOAPElement detail = body.getFault().getDetail();
      xcept::Exception rae;
      xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
      XCEPT_RETHROW ( tsexception::SoapFault, err.str(), rae );
    }
    else
    {
      err << body.getFault().getFaultString();
      XCEPT_RAISE ( tsexception::SoapFault,err.str() );
    }
  }

  xdata::Serializable* ret ( 0 );
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  //cout<< toolbox::toString("hola1");
  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    //cout << toolbox::toString("hola2");
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      //cout << toolbox::toString("hola3%s",coma.c_str());
      //we are at the command level
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        //cout << toolbox::toString("hola4");
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          //cout << toolbox::toString("hola5,%s",com.c_str());
          if ( com=="payload" )
          {
            try
            {
              xdata::soap::Serializer serializer;
              //cout << "hola6" << endl;
              //we must ask the type of the embedded object
              ret = analyse ( command2 );
              serializer.import ( ret, command2 ); //ret, command2);
              //cout << "hola8" << endl;
            }
            catch ( xdata::exception::Exception& xde )
            {
              XCEPT_RETHROW ( tsexception::SoapParsingError, "Failed to decode payload", xde );
            }
          }
        }
      }
    }
  }

  if ( !ret )
  {
    XCEPT_RAISE ( tsexception::SoapParsingError,"Can not find 'payload' in SOAP reply" );
  }

  return ret;
}

xdata::Serializable* getWarningLevel ( xoap::MessageReference msg )
{
  xoap::SOAPBody body = msg->getSOAPPart().getEnvelope().getBody();

  if ( body.hasFault() )
  {
    std::ostringstream err;
    err << "SOAPFault found while getting warning level from message. ";

    if ( body.getFault().hasDetail() )
    {
      xoap::SOAPElement detail = body.getFault().getDetail();
      xcept::Exception rae;
      xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
      XCEPT_RETHROW ( tsexception::SoapFault, err.str(), rae );
    }
    else
    {
      err << body.getFault().getFaultString();
      XCEPT_RAISE ( tsexception::SoapFault,err.str() );
    }
  }

  std::string msgstrrr;
  //std::string mmm;
  //xoap::dumpTree(msg->getEnvelope(),mmm);
  //cout << "message:" << mmm <<endl;
  xdata::Serializable* ret = 0;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  //cout<< toolbox::toString("hola1");
  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    //cout << toolbox::toString("hola2");
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      //cout << toolbox::toString("hola3%s",coma.c_str());
      //we are at the command level
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        //cout << toolbox::toString("hola4");
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          //cout << toolbox::toString("hola5,%s",com.c_str());
          if ( com=="warningLevel" )
          {
            try
            {
              xdata::soap::Serializer serializer;
              //cout << "hola6" << endl;
              //we must ask the type of the embedded object
              ret = analyse ( command2 );
              //xdata::String id;
              //cout << "hola7" << endl;
              //string a;
              //xoap::dumpTree(command2, a);
              //cout << "node payload: " << a << endl;
              //DOMElement* el2 = (DOMElement*) command2;
              //std::string type = xoap::XMLCh2String( el2->getAttributeNS (xoap::XStr(XSI_NAMESPACE_URI), xoap::XStr("type")));//(XStr("xsi:type")));//NS
              //cout << "TYPE: " << type << endl;
              serializer.import ( ret, command2 ); //ret, command2);
              //cout << "hola8" << endl;
            }
            catch ( xdata::exception::Exception& xde )
            {
              XCEPT_RETHROW ( tsexception::SoapParsingError, "Failed to decode warning level", xde );
            }
          }
        }
      }
    }
  }

  return ret;
}

xdata::Serializable* getWarningMessage ( xoap::MessageReference msg )
{
  xoap::SOAPBody body = msg->getSOAPPart().getEnvelope().getBody();

  if ( body.hasFault() )
  {
    std::ostringstream err;
    err << "SOAPFault found while getting warning message. ";

    if ( body.getFault().hasDetail() )
    {
      xoap::SOAPElement detail = body.getFault().getDetail();
      xcept::Exception rae;
      xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
      XCEPT_RETHROW ( tsexception::SoapFault, err.str(), rae );
    }
    else
    {
      err << body.getFault().getFaultString();
      XCEPT_RAISE ( tsexception::SoapFault,err.str() );
    }
  }

  std::string msgstrrr;
  xdata::Serializable* ret = 0;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  //cout<< toolbox::toString("hola1");
  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    //cout << toolbox::toString("hola2");
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      //cout << toolbox::toString("hola3%s",coma.c_str());
      //we are at the command level
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        //cout << toolbox::toString("hola4");
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          //cout << toolbox::toString("hola5,%s",com.c_str());
          if ( com=="warningMessage" )
          {
            try
            {
              xdata::soap::Serializer serializer;
              //cout << "hola6" << endl;
              //we must ask the type of the embedded object
              ret = analyse ( command2 );
              //xdata::String id;
              //cout << "hola7" << endl;
              //string a;
              //xoap::dumpTree(command2, a);
              //cout << "node payload: " << a << endl;
              //DOMElement* el2 = (DOMElement*) command2;
              //std::string type = xoap::XMLCh2String( el2->getAttributeNS (xoap::XStr(XSI_NAMESPACE_URI), xoap::XStr("type")));//(XStr("xsi:type")));//NS
              //cout << "TYPE: " << type << endl;
              serializer.import ( ret, command2 ); //ret, command2);
              //cout << "hola8" << endl;
            }
            catch ( xdata::exception::Exception& xde )
            {
              XCEPT_RETHROW ( tsexception::SoapParsingError, "Failed to decode warning message", xde );
            }
          }
        }
      }
    }
  }

  //  if (!ret)
  //  	ret = new xdata::String("");
  return ret;
}

xdata::Serializable* analyse ( DOMNode* command2 )
{
  xdata::Serializable* ret ( 0 );

  if ( command2->hasAttributes() )
  {
    DOMNamedNodeMap* nodeMap = command2->getAttributes();
    std::string attrs = "xsi:type";
    XMLCh* attrx =  XMLString::transcode ( attrs.c_str() );
    DOMNode* asyncNode = nodeMap->getNamedItem ( attrx );

    if ( asyncNode!=NULL )
    {
      const XMLCh* value = asyncNode->getNodeValue();
      std::string typev = xoap::XMLCh2String ( value );

      if ( typev=="xsd:integer" )
      {
        ret = new xdata::Integer();
      }
      else if ( typev=="xsd:unsignedShort" )
      {
        ret = new xdata::UnsignedShort();
      }
      else if ( typev=="xsd:unsignedLong" )
      {
        ret = new xdata::UnsignedLong();
      }
      else if ( typev=="xsd:float" )
      {
        ret = new xdata::Float();
      }
      else if ( typev=="xsd:double" )
      {
        ret = new xdata::Double();
      }
      else if ( typev=="xsd:boolean" )
      {
        ret = new xdata::Boolean();
      }
      else if ( typev=="xsd:unsignedInt" )
      {
        ret = new xdata::UnsignedInteger();
      }
      else if ( typev=="xsd:string" )
      {
        ret = new xdata::String ( "" );
      }
      else if ( typev=="soapenc:Struct" )
      {
        ret = analyseSoapBag ( command2 );
      }
      else if ( typev=="soapenc:Array" )
      {
        ret = analyseSoapVector ( command2 );
      }
      else
      {
        XCEPT_RAISE ( tsexception::SoapEncodingError, "Not supported type " + typev );
      }
    }

    XMLString::release ( &attrx );
  }
  else
  {
    std::string msg = "Payload in response message has no type, therefore it can not be deserialized";
    XCEPT_RAISE ( tsexception::SoapEncodingError, msg );
  }

  return ret;
}

xdata::Serializable* analyseSoapBag ( DOMNode* command2 )
{
  xdata::Serializable* ret;
  std::string msg = "Sorry, by the time being I can not deserialize bags. :(";
  XCEPT_RAISE ( tsexception::SoapParsingError, msg );
  return ret;
}

xdata::Serializable* analyseSoapVector ( DOMNode* command2 )
{
  xdata::Serializable* ret;
  std::string msg = "Sorry, by the time being I can not deserialize vectors. :(";
  XCEPT_RAISE ( tsexception::SoapParsingError, msg );
  //ret = new xdata::Bag<xdata::Integer>;
  return ret;
}

std::string getOpComName ( xoap::MessageReference msg )
{
  //cout<< toolbox::toString("h1") << endl;
  std::string com;
  bool ok = false;
  //cout<< toolbox::toString("h2")<< endl;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  //cout<< toolbox::toString("h3");
  DOMNodeList* bodyList = node->getChildNodes();

  //cout<< toolbox::toString("hola1")<< endl;
  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    //cout << toolbox::toString("hola2")<< endl;
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      //cout << toolbox::toString("hola3%s",coma.c_str())<< endl;
      //we are at the command level
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        //cout << toolbox::toString("hola4")<< endl;
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          //cout << toolbox::toString("hola5,%s",com.c_str())<< endl;
          if ( com=="command" )
          {
            //cout << "1" << endl;
            //DOMNode* command3 = command2->getFirstChild();
            //cout << "2" << endl;
            if ( command2->hasChildNodes() )
            {
              DOMNode* command3 = command2->getFirstChild();
              //cout << "3" << endl;
              com=xoap::XMLCh2String ( command3->getNodeValue() );
              //cout << "4" << endl;
              //cout << toolbox::toString("hola6,%s",com.c_str());
              return com;
            }
          }
        }
      }
    }
  }

  if ( ok == false )
  {
    XCEPT_RAISE ( tsexception::SoapParsingError,"The <command> element does not exist inside this SOAP request" );
  }

  return com;
}

std::map<std::string, xdata::Serializable*>
getOpComParamList ( xoap::MessageReference msg )
{
  std::map<std::string, xdata::Serializable*> mapParam;
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  for ( unsigned int i = 0; i < bodyList->getLength(); i++ )
  {
    DOMNode* command = bodyList->item ( i );

    if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
    {
      std::string coma = xoap::XMLCh2String ( command->getLocalName() );
      DOMNodeList* bodyList2 = command->getChildNodes();

      for ( unsigned int j = 0; j < bodyList2->getLength(); j++ )
      {
        DOMNode* command2 = bodyList2->item ( j );

        if ( command2->getNodeType() == DOMNode::ELEMENT_NODE )
        {
          std::string com = xoap::XMLCh2String ( command2->getLocalName() );

          if ( com=="param" )
          {
            xdata::Serializable* ret;
            std::string name;

            try
            {
              xdata::soap::Serializer serializer;
              ret = analyse ( command2 );
              //cout << ret->type() << " " << ret->toString()<< endl;
              serializer.import ( ret, command2 ); //ret, command2);
              //cout << ret->type() << " " << ret->toString()<< endl;
            }
            catch ( xdata::exception::Exception& xde )
            {
              XCEPT_RETHROW ( tsexception::SoapParsingError, "Failed to decode parameter list", xde );
            }

            if ( command2->hasAttributes() )
            {
              DOMNamedNodeMap* nodeMap = command2->getAttributes();
              std::string attrs = "name";
              XMLCh* attrx =  XMLString::transcode ( attrs.c_str() );
              DOMNode* asyncNode = nodeMap->getNamedItem ( attrx );

              if ( asyncNode!=NULL )
              {
                const XMLCh* asyncValue = asyncNode->getNodeValue();
                name = xoap::XMLCh2String ( asyncValue );
              }
              else
              {
                XCEPT_RAISE ( tsexception::SoapParsingError,"The <param> element does not have a 'name' attribute" );
              }

              XMLString::release ( &attrx );
            }
            else
            {
              XCEPT_RAISE ( tsexception::SoapParsingError,"The <param> element does not have a 'name' attribute" );
            }

            mapParam[name] = ret;
          }
        }
      }
    }
  }

  return mapParam;
}


xoap::MessageReference doSoapOpSendComand ( const std::string& ns,
    const std::string& cid, const std::string& sid, bool async, const std::string& opid, const std::string& command, std::map<std::string,xdata::Serializable*> param, const std::string& cb,const std::string& url,const std::string& urn )
{
  std::string asyncs;

  if ( async )
  {
    asyncs = "true";
  }
  else
  {
    asyncs = "false";
  }

  xoap::MessageReference msg = xoap::createMessage();

  try
  {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration ( "soap-enc","http://schemas.xmlsoap.org/soap/encoding/" );
    envelope.addNamespaceDeclaration ( "xsi","http://www.w3.org/2001/XMLSchema-instance" );
    envelope.addNamespaceDeclaration ( "xsd","http://www.w3.org/2001/XMLSchema" );
    xoap::SOAPBody body = envelope.getBody();
    xoap::SOAPName opSendCommand = envelope.createName ( "OpSendCommand", "cell", ns );
    xoap::SOAPElement element = body.addBodyElement ( opSendCommand );
    element.addNamespaceDeclaration ( "cell",ns );
    xoap::SOAPName commandId = envelope.createName ( "cid" );
    element.addAttribute ( commandId, cid );
    xoap::SOAPName sesionId = envelope.createName ( "sid" );
    element.addAttribute ( sesionId, sid );
    xoap::SOAPName asynchronous = envelope.createName ( "async" );
    element.addAttribute ( asynchronous, asyncs );
    xoap::SOAPName opSoapName = envelope.createName ( "operation" );
    xoap::SOAPElement operationElement = element.addChildElement ( opSoapName );
    operationElement.addTextNode ( opid );
    xoap::SOAPName commandName = envelope.createName ( "command" );
    xoap::SOAPElement commandElement = element.addChildElement ( commandName );
    commandElement.addTextNode ( command );

    for ( std::map<std::string,xdata::Serializable*>::const_iterator i = param.begin(); i != param.end(); ++i )
    {
      std::string mytype="";

      if ( i->second->type() =="int" )
      {
        mytype="integer";
      }
      else if ( i->second->type() =="string" )
      {
        mytype="string";
      }
      else if ( i->second->type() =="bool" )
      {
        mytype="boolean";
      }
      else if ( i->second->type() =="unsigned long" )
      {
        mytype="unsignedLong";
      }
      else if ( i->second->type() =="unsigned short" )
      {
        mytype="unsignedShort";
      }
      else if ( i->second->type() =="unsigned int" )
      {
        mytype="unsignedInt";
      }
      else
      {
        mytype=i->second->type();
      }

      xoap::SOAPName paramName = envelope.createName ( "param", "cell", ns );
      xoap::SOAPElement paramElement = element.addChildElement ( paramName );
      xoap::SOAPName typeName = envelope.createName ( "type","xsi","http://www.w3.org/2001/XMLSchema-instance" );
      paramElement.addAttribute ( typeName, "xsd:" + mytype );
      xoap::SOAPName nameName = envelope.createName ( "name" );
      paramElement.addAttribute ( nameName, i->first );
      paramElement.addTextNode ( i->second->toString() );
    }

    xoap::SOAPName callbackFunName = envelope.createName ( "callbackFun" );
    xoap::SOAPElement callbackFunElement = element.addChildElement ( callbackFunName );
    callbackFunElement.addTextNode ( cb );
    xoap::SOAPName callbackUrlName = envelope.createName ( "callbackUrl" );
    xoap::SOAPElement callbackUrlElement = element.addChildElement ( callbackUrlName );
    callbackUrlElement.addTextNode ( url );
    xoap::SOAPName callbackUrnName = envelope.createName ( "callbackUrn" );
    xoap::SOAPElement callbackUrnElement = element.addChildElement ( callbackUrnName );
    callbackUrnElement.addTextNode ( urn );
  }
  catch ( xcept::Exception& e )
  {
    std::ostringstream str;
    str << "Can not create the SOAP message in doOpSendCommand";
    XCEPT_RETHROW ( tsexception::SoapEncodingError,str.str(),e );
  }

  return msg;
}

xoap::MessageReference
doSoapOpGetState ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& opid, const std::string& cb,const std::string& url,const std::string& urn )
{
  std::string asyncs;

  if ( async )
  {
    asyncs = "true";
  }
  else
  {
    asyncs = "false";
  }

  xoap::MessageReference msg = xoap::createMessage();

  try
  {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration ( "soap-enc","http://schemas.xmlsoap.org/soap/encoding/" );
    envelope.addNamespaceDeclaration ( "xsi","http://www.w3.org/2001/XMLSchema-instance" );
    envelope.addNamespaceDeclaration ( "xsd","http://www.w3.org/2001/XMLSchema" );
    xoap::SOAPBody body = envelope.getBody();
    xoap::SOAPName opGetState = envelope.createName ( "OpGetState", "cell", ns );
    xoap::SOAPElement element = body.addBodyElement ( opGetState );
    element.addNamespaceDeclaration ( "cell",ns );
    xoap::SOAPName commandId = envelope.createName ( "cid" );
    element.addAttribute ( commandId, cid );
    xoap::SOAPName sesionId = envelope.createName ( "sid" );
    element.addAttribute ( sesionId, sid );
    xoap::SOAPName asynchronous = envelope.createName ( "async" );
    element.addAttribute ( asynchronous, asyncs );
    xoap::SOAPName opSoapName = envelope.createName ( "operation" );
    xoap::SOAPElement operationElement = element.addChildElement ( opSoapName );
    operationElement.addTextNode ( opid );
    xoap::SOAPName callbackFunName = envelope.createName ( "callbackFun" );
    xoap::SOAPElement callbackFunElement = element.addChildElement ( callbackFunName );
    callbackFunElement.addTextNode ( cb );
    xoap::SOAPName callbackUrlName = envelope.createName ( "callbackUrl" );
    xoap::SOAPElement callbackUrlElement = element.addChildElement ( callbackUrlName );
    callbackUrlElement.addTextNode ( url );
    xoap::SOAPName callbackUrnName = envelope.createName ( "callbackUrn" );
    xoap::SOAPElement callbackUrnElement = element.addChildElement ( callbackUrnName );
    callbackUrnElement.addTextNode ( urn );
  }
  catch ( xcept::Exception& e )
  {
    std::ostringstream str;
    str << "Can not create the SOAP message in doOpGetState";
    XCEPT_RETHROW ( tsexception::SoapEncodingError,str.str(),e );
  }

  return msg;
}

xoap::MessageReference
doSoapOpReset ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& op, const std::string& cb,const std::string& url,const std::string& urn )
{
  std::string asyncs;

  if ( async )
  {
    asyncs = "true";
  }
  else
  {
    asyncs = "false";
  }

  xoap::MessageReference msg = xoap::createMessage();

  try
  {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration ( "soap-enc","http://schemas.xmlsoap.org/soap/encoding/" );
    envelope.addNamespaceDeclaration ( "xsi","http://www.w3.org/2001/XMLSchema-instance" );
    envelope.addNamespaceDeclaration ( "xsd","http://www.w3.org/2001/XMLSchema" );
    xoap::SOAPBody body = envelope.getBody();
    xoap::SOAPName opReset = envelope.createName ( "OpReset", "cell", ns );
    xoap::SOAPElement element = body.addBodyElement ( opReset );
    element.addNamespaceDeclaration ( "cell",ns );
    xoap::SOAPName commandId = envelope.createName ( "cid" );
    element.addAttribute ( commandId, cid );
    xoap::SOAPName sesionId = envelope.createName ( "sid" );
    element.addAttribute ( sesionId, sid );
    xoap::SOAPName asynchronous = envelope.createName ( "async" );
    element.addAttribute ( asynchronous, asyncs );
    xoap::SOAPName opSoapName = envelope.createName ( "operation" );
    xoap::SOAPElement operationElement = element.addChildElement ( opSoapName );
    operationElement.addTextNode ( op );
    xoap::SOAPName callbackFunName = envelope.createName ( "callbackFun" );
    xoap::SOAPElement callbackFunElement = element.addChildElement ( callbackFunName );
    callbackFunElement.addTextNode ( cb );
    xoap::SOAPName callbackUrlName = envelope.createName ( "callbackUrl" );
    xoap::SOAPElement callbackUrlElement = element.addChildElement ( callbackUrlName );
    callbackUrlElement.addTextNode ( url );
    xoap::SOAPName callbackUrnName = envelope.createName ( "callbackUrn" );
    xoap::SOAPElement callbackUrnElement = element.addChildElement ( callbackUrnName );
    callbackUrnElement.addTextNode ( urn );
  }
  catch ( xcept::Exception& e )
  {
    std::ostringstream str;
    str << "Cannot create the SOAP message in doOpReset";
    XCEPT_RETHROW ( tsexception::SoapEncodingError,str.str(),e );
  }

  return msg;
}

xoap::MessageReference
doSoapCommand ( const std::string& ns, const std::string& cid, const std::string& sid, bool async,const std::string& command, std::map<std::string,xdata::Serializable*> param, const std::string& cb,const std::string& url,const std::string& urn )
{
  std::string asyncs;

  if ( async )
  {
    asyncs = "true";
  }
  else
  {
    asyncs = "false";
  }

  xoap::MessageReference msg = xoap::createMessage();

  try
  {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration ( "soap-enc","http://schemas.xmlsoap.org/soap/encoding/" );
    envelope.addNamespaceDeclaration ( "xsi","http://www.w3.org/2001/XMLSchema-instance" );
    envelope.addNamespaceDeclaration ( "xsd","http://www.w3.org/2001/XMLSchema" );
    xoap::SOAPBody body = envelope.getBody();
    xoap::SOAPName commandName = envelope.createName ( command, "cell", ns );
    xoap::SOAPElement element = body.addBodyElement ( commandName );
    element.addNamespaceDeclaration ( "cell",ns );
    xoap::SOAPName commandId = envelope.createName ( "cid" );
    element.addAttribute ( commandId, cid );
    xoap::SOAPName sesionId = envelope.createName ( "sid" );
    element.addAttribute ( sesionId, sid );
    xoap::SOAPName asynchronous = envelope.createName ( "async" );
    element.addAttribute ( asynchronous, asyncs );

    for ( std::map<std::string,xdata::Serializable*>::const_iterator i = param.begin(); i != param.end(); ++i )
    {
      std::string mytype="";

      if ( i->second->type() =="int" )
      {
        mytype="integer";
      }
      else if ( i->second->type() =="string" )
      {
        mytype="string";
      }
      else if ( i->second->type() =="bool" )
      {
        mytype="boolean";
      }
      else if ( i->second->type() =="unsigned long" )
      {
        mytype="unsignedLong";
      }
      else if ( i->second->type() =="unsigned short" )
      {
        mytype="unsignedShort";
      }
      else if ( i->second->type() =="unsigned int" )
      {
        mytype="unsignedInt";
      }
      else
      {
        mytype=i->second->type();
      }

      xoap::SOAPName paramName = envelope.createName ( "param", "cell", ns );
      xoap::SOAPElement paramElement = element.addChildElement ( paramName );
      xoap::SOAPName typeName = envelope.createName ( "type","xsi","http://www.w3.org/2001/XMLSchema-instance" );
      paramElement.addAttribute ( typeName, "xsd:" + mytype );
      xoap::SOAPName nameName = envelope.createName ( "name" );
      paramElement.addAttribute ( nameName, i->first );
      paramElement.addTextNode ( i->second->toString() );
    }

    xoap::SOAPName callbackFunName = envelope.createName ( "callbackFun" );
    xoap::SOAPElement callbackFunElement = element.addChildElement ( callbackFunName );
    callbackFunElement.addTextNode ( cb );
    xoap::SOAPName callbackUrlName = envelope.createName ( "callbackUrl" );
    xoap::SOAPElement callbackUrlElement = element.addChildElement ( callbackUrlName );
    callbackUrlElement.addTextNode ( url );
    xoap::SOAPName callbackUrnName = envelope.createName ( "callbackUrn" );
    xoap::SOAPElement callbackUrnElement = element.addChildElement ( callbackUrnName );
    callbackUrnElement.addTextNode ( urn );
  }
  catch ( xcept::Exception& e )
  {
    std::ostringstream str;
    str << "Can not create the SOAP message in doOpSendCommand";
    XCEPT_RETHROW ( tsexception::SoapEncodingError,str.str(),e );
  }

  return msg;
}


std::string newline2br ( const std::string& in )
{
  std::string ret ( in );
  size_t pos=0;
  pos = ret.find ( '\n', pos );

  while ( pos < ret.npos )
  {
    ret.erase ( pos, 1 );
    ret.insert ( pos, "<br />", 6 );
    pos = ret.find ( '\n', pos );
  }

  return ret;
}

std::string expandFileName ( const std::string& pathname )
{
  std::string result;
  std::vector<std::string> expandedFile;

  try
  {
    expandedFile = toolbox::getRuntime()->expandPathName ( pathname );
  }
  catch ( toolbox::exception::Exception& tbe )
  {
    std::string msg = "Cannot expand filename [";
    msg += pathname;
    msg += "]";
    XCEPT_RETHROW ( tsexception::FileError, msg, tbe );
  }

  if ( expandedFile.size() == 1 )
  {
    std::string filename = expandedFile[0];

    if ( ( filename.find ( "file:/" ) == std::string::npos ) && ( filename.find ( "http://" ) ) )
    {
      // MUST have always the "/" in the beginning
      filename.insert ( 0, "file:" );
    }

    // ------------------------
    result = "";

    // if it starts with file: or /
    if ( ( filename.find ( "file:",0 ) == 0 ) || ( filename.find ( "/",0 ) == 0 ) )
    {
      int delimiter = filename.find ( "/" );
      result = filename.substr ( delimiter );
    }
    else
    {
      std::string msg = "Load failed. Unsupported protocol in URL";
      msg += filename;
      XCEPT_RAISE ( tsexception::FileError, msg );
    }
  }
  else
  {
    std::string msg = "Pathname for module is ambiguous [";
    msg += pathname;
    msg += "]";
    XCEPT_RAISE ( tsexception::FileError, msg );
  }

  return result;
}

std::string getEnvironment ( const std::string& variable )
{
  char* cenvironment;
  cenvironment = getenv ( variable.c_str() );

  if ( !cenvironment )
  {
    XCEPT_RAISE ( tsexception::EnvironmentError,"Environment variable '" + variable + "' not found" );
  }

  return cenvironment;
}

std::string getSubmittedValue ( cgicc::Cgicc& cgi, const std::string& attribute )
{
  std::vector<cgicc::FormEntry> formEntries = cgi.getElements();

  for ( std::vector<cgicc::FormEntry>::const_iterator i=formEntries.begin(); i != formEntries.end(); ++i )
    if ( i->getName() ==attribute )
    {
      return i->getValue();
    }

  XCEPT_RAISE ( tsexception::AttributeNotFound,"The attribute '" + attribute + "' has not been found in the CGI request" );
}

std::string getPassword ( const std::string& path,const std::string& filename )
{
  //std::string path = getEnvironment("PWD_PATH");
  std::string file = path + "/" + filename;
  std::ifstream f ( file.c_str() );

  if ( !f.good() )
  {
    XCEPT_RAISE ( tsexception::FileError,"An error has happened on opening file '" + file + "'" );
  }

  //gets all the non-blank characters from the file
  std::ostringstream pwd;
  char ch;

  while ( f.get ( ch ) )
  {
    if ( !isspace ( ch ) )
    {
      pwd << ch;
    }
  }

  return pwd.str();
}

double timeval_diff ( timeval end, timeval start )
{
  struct timeval result;

  /* Perform the carry for the later subtraction by updating y. */
  if ( end.tv_usec < start.tv_usec )
  {
    int nsec = ( start.tv_usec - end.tv_usec ) / 1000000 + 1;
    start.tv_usec -= 1000000 * nsec;
    start.tv_sec += nsec;
  }

  if ( end.tv_usec - start.tv_usec > 1000000 )
  {
    int nsec = ( end.tv_usec - start.tv_usec ) / 1000000;
    start.tv_usec += 1000000 * nsec;
    start.tv_sec -= nsec;
  }

  /* Compute the time remaining to wait.
    tv_usec is certainly positive. */
  result.tv_sec = end.tv_sec - start.tv_sec;
  result.tv_usec = end.tv_usec - start.tv_usec;

  /* Return 1 if result is negative. */
  if ( end.tv_sec < start.tv_sec )
  {
    XCEPT_RAISE ( tsexception::CellException, "First timeval should be bigger than the second" );
  }

  return ( result.tv_sec*1000000 + result.tv_usec ) /1000;
}


std::string formatTimeval ( timeval aTv )
{
 struct tm* lTm;
 char time_string[40];

 lTm = localtime (&aTv.tv_sec);
 strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", lTm);
 
 std::ostringstream ss;
 ss << time_string << "." << (aTv.tv_usec/1000L);
 return ss.str();
}


std::string escape ( const std::string& str )
{
  std::stringstream ss;
  size_t len = str.length();

  for ( size_t i=0 ; i<len ; i++ )
  {
    unsigned char c = str[i];

    if ( static_cast<unsigned int> ( c ) <= '\x7F' )
    {
      ss << c;
    }
    else
    {
      ss << "\\" << static_cast<unsigned int> ( c );
    }
  }

  return ss.str();
}

}
