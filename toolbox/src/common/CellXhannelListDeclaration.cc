/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/
#include "ts/toolbox/CellXhannelListDeclaration.h"
#include "ts/exception/CellException.h"
#include "ts/toolbox/CellToolbox.h"

#include "toolbox/string.h"
#include "toolbox/Runtime.h"

#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/DOMParserFactory.h"

#include <xercesc/util/XMLURL.hpp>

#include <string>
#include <vector>
#include <sstream>

tstoolbox::CellXhannelListDeclaration::CellXhannelListDeclaration() :document_ ( 0 ), parserName_ ( "" )
{
  ;
}

tstoolbox::CellXhannelListDeclaration::~CellXhannelListDeclaration()
{
  if ( document_ )
  {
    xoap::getDOMParserFactory()->destroy ( parserName_ );
  }
}

void tstoolbox::CellXhannelListDeclaration::load ( const std::string& declarationFile )
{
  if ( !document_ && !declarationFile.empty() )
  {
    std::string filename = tstoolbox::expandFileName ( declarationFile );

    if ( ( filename.find ( "file:/" ) == std::string::npos ) && ( filename.find ( "http://" ) ) )
    {
      filename.insert ( 0, "file:" );
    }

    XMLURL* source = 0;

    try
    {
      source = new XMLURL ( filename.c_str() );
    }
    catch ( ... )
    {
      std::ostringstream msg;
      msg << "Failed to load from " << filename;
      XCEPT_RAISE ( tsexception::ParsingError, msg.str() );
    }

    parserName_ = "tstoolbox::CellXhannelListDeclaration";
    xoap::DOMParser* parser_ = xoap::getDOMParserFactory()->get ( parserName_ );

    try
    {
      document_ = parser_->parse ( *source );
      delete source;
    }
    catch ( xcept::Exception& e )
    {
      delete source;
      std::ostringstream msg;
      msg << "Cannot parse XML loaded from '" << filename << "'";
      XCEPT_RETHROW ( tsexception::ParsingError, msg.str(), e );
    }
  }
}

std::vector<std::string>
tstoolbox::CellXhannelListDeclaration::getXhannelNames()
{
  if ( !document_ )
  {
    XCEPT_RAISE ( tsexception::ParsingError, "Load a document before parsing it" );
  }

  std::vector<std::string> result;
  DOMNodeList* xhannels = document_->getElementsByTagName ( xoap::XStr ( "xhannel" ) );

  for ( unsigned int i = 0; i < xhannels->getLength(); ++i )
  {
    DOMNode* xhannel = xhannels->item ( i );
    DOMNamedNodeMap* attributes = xhannel->getAttributes();

    if ( !attributes )
    {
      XCEPT_RAISE ( tsexception::ParsingError,"Missing attributes in xhannel property" );
    }

    if ( DOMNode* attribute = attributes->getNamedItem ( xoap::XStr ( "name" ) ) )
    {
      std::string xhannelName = xoap::XMLCh2String ( attribute->getTextContent() );
      result.push_back ( xhannelName );
    }
    else
    {
      XCEPT_RAISE ( tsexception::ParsingError,"Attribute 'name' missing in 'xhannel' property" );
    }
  }

  return result;
}

std::string
tstoolbox::CellXhannelListDeclaration::getTargetAttribute ( const std::string& xhannelName, const std::string& attr )
{
  if ( !document_ )
  {
    XCEPT_RAISE ( tsexception::ParsingError, "Load a document before parsing it" );
  }

  DOMNodeList* xhannels = document_->getElementsByTagName ( xoap::XStr ( "xhannel" ) );

  for ( unsigned int i = 0; i < xhannels->getLength(); ++i )
  {
    DOMNode* xhannel = xhannels->item ( i );
    DOMNamedNodeMap* attributes = xhannel->getAttributes();

    if ( !attributes )
    {
      XCEPT_RAISE ( tsexception::ParsingError,"Missing attributes in xhannel property" );
    }

    DOMNode* attribute = attributes->getNamedItem ( xoap::XStr ( "name" ) );

    if ( !attribute )
    {
      XCEPT_RAISE ( tsexception::ParsingError,"Attribute 'name' missing in 'xhannel' property" );
    }

    std::string desiredName = xoap::XMLCh2String ( attribute->getTextContent() );

    if ( xhannelName != desiredName )
    {
      continue;
    }

    DOMNode* desired = 	attributes->getNamedItem ( xoap::XStr ( attr.c_str() ) );

    if ( !desired )
    {
      XCEPT_RAISE ( tsexception::ParsingError,"Attribute '" + attr + "' missing in '" + xhannelName + "' xhannel" );
    }

    std::string attrValue = xoap::XMLCh2String ( desired->getTextContent() );
    return attrValue;
  }

  std::ostringstream msg;
  msg << "Attribute '" << attr << "' in xhannel '" << xhannelName << "' not found";
  XCEPT_RAISE ( tsexception::ParsingError,msg.str() );
}
