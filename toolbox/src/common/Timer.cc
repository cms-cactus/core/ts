#include "ts/toolbox/Timer.h"

#include <boost/lexical_cast.hpp>

using namespace tstoolbox;
Timer::Timer()
{
	reset();
}


Timer& Timer::reset()
{
	gettimeofday ( &_startTime, 0 );
	return *this;
}


int Timer::seconds() const
{
	// we always round up
	//
	return ( ( ms() + 999 ) / 1000 );
}


int Timer::ms() const
{
	timeval startTime ( _startTime );
	timeval curTime;
	gettimeofday ( &curTime, 0 );

	// Perform the carry for the later subtraction by updating y.
	//
	if ( curTime.tv_usec < startTime.tv_usec )
	{
		int nsec = ( startTime.tv_usec - curTime.tv_usec ) / 1000000 + 1;
		startTime.tv_usec -= 1000000 * nsec;
		startTime.tv_sec += nsec;
	}

	if ( curTime.tv_usec - startTime.tv_usec > 1000000 )
	{
		int nsec = ( curTime.tv_usec - startTime.tv_usec ) / 1000000;
		startTime.tv_usec += 1000000 * nsec;
		startTime.tv_sec -= nsec;
	}

	// Compute the time remaining to wait.
	// tv_usec is certainly positive.
	//
	return ( ( ( curTime.tv_sec - startTime.tv_sec ) * 1000 ) +
			( ( curTime.tv_usec - startTime.tv_usec ) / 1000 ) );
}


std::string Timer::startTimeAsString ( bool niceFormat )
{
	return timeAsString ( _startTime.tv_sec, niceFormat );
}


std::string Timer::timeAsString ( time_t secondsSince1970,
		bool niceFormatting  )
{
	std::stringstream out;
	struct tm* tmValue;
	tmValue = localtime ( &secondsSince1970 );
	out << tmValue->tm_year+1900;
	out << "/";
	addWithZeroPadding ( out, tmValue->tm_mon+1 );
	out << "/";
	addWithZeroPadding ( out, tmValue->tm_mday );
	out << '-';
	addWithZeroPadding ( out, tmValue->tm_hour );
	out << ":";
	addWithZeroPadding ( out, tmValue->tm_min );
	out << ":";
	addWithZeroPadding ( out, tmValue->tm_sec );
	return ( niceFormatting ) ? niceFormat ( out.str() )
			: out.str();
}


std::string Timer::niceFormat ( const std::string& dateTime )
{
  std::ostringstream out;

  if ( dateTime.size() < 15 ) {
    
    return "";
  }

  out << dateTime.substr ( 0, 4 )
      << '-'
      << dateTime.substr ( 5, 2 )
      << '-'
      << dateTime.substr ( 8, 2 )
      << ' '
      << dateTime.substr ( 11, 2 )
      << ':'
      << dateTime.substr ( 14, 2 )
      << ':'
      << dateTime.substr ( 17, 2 );
      
  return out.str();
}


std::string Timer::timeAsString ( std::string microSecondsSince1970,
		bool niceFormatting )
{
	if ( microSecondsSince1970.length() > 6 )
	{
		return timeAsString ( boost::lexical_cast<time_t> ( microSecondsSince1970.substr ( 0, microSecondsSince1970.length()-6 ) ),
				niceFormatting );
	}
	else
	{
		return "";
	}
}


std::string Timer::startTimeInMicroSecondsAsString()
{
	std::stringstream ss, ms;
	ms.width ( 6 );
	ms.fill ( '0' );
	ms << _startTime.tv_usec;
	ss << _startTime.tv_sec << ms.str();
	return ss.str();
}


void Timer::addWithZeroPadding ( std::stringstream& out, int value )
{
	out << ( ( value < 10 ) ? "0" : "" ) << value;
}
