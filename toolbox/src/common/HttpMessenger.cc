/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril                                    *
 *************************************************************************/

#include "ts/toolbox/HttpMessenger.h"
#include "ts/toolbox/FsTools.h"

//int tstoolbox::HttpMessenger::onlyonce_ = 1;

tstoolbox::HttpMessenger::HttpMessenger() :timeout_ ( 4 ),url_ ( 0 )
{
}

tstoolbox::HttpMessenger::~HttpMessenger()
{
  delete[] url_;
}

int tstoolbox::HttpMessenger::htmlWriter ( char* data, size_t size, size_t nmemb,std::string* writerData )
{
  if ( writerData == 0 )
  {
    return 0;
  }

  writerData->append ( data, size*nmemb );
  return size * nmemb;
}

char* tstoolbox::HttpMessenger::string2cstr ( const std::string& s )
{
  char* cstr = new char[s.length() +1];
  s.copy ( cstr, std::string::npos );
  cstr[s.length() ] = 0;
  return cstr;
}

void tstoolbox::HttpMessenger::init ( CURL *&conn, char* url, std::string& response, char* errorBuffer )
{
  CURLcode code;
  conn = curl_easy_init();

  if ( conn == 0 )
  {
    XCEPT_RAISE ( tsexception::CurlException,"Failed to create CURL connection" );
  }

  code = curl_easy_setopt ( conn, CURLOPT_ERRORBUFFER, errorBuffer );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set error buffer" << code;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  code = curl_easy_setopt ( conn, CURLOPT_URL, url );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set error URL " << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  code = curl_easy_setopt ( conn, CURLOPT_FOLLOWLOCATION, 1 );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set redirection option " << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  code = curl_easy_setopt ( conn, CURLOPT_WRITEFUNCTION, htmlWriter );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set writer " << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  code = curl_easy_setopt ( conn, CURLOPT_WRITEDATA, &response );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set write data " << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  code = curl_easy_setopt ( conn, CURLOPT_TIMEOUT, timeout_ );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set write data " << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  unsigned long nosignal ( 0 );
  code = curl_easy_setopt ( conn, CURLOPT_NOSIGNAL, nosignal );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set write data " << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }
}

void tstoolbox::HttpMessenger::requestHtml ( const std::string& url, std::string& response )
{
  CURLcode code;

  if ( url.empty() )
  {
    XCEPT_RAISE ( tsexception::CurlException, "Trying to request with empty URL" );
  }

  CURL* connection ( 0 );
  char errorBuffer[CURL_ERROR_SIZE];
  url_ = string2cstr ( url );
  init ( connection, url_,response, errorBuffer );
  code = curl_easy_perform ( connection );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed HTTP request to URL '" << url << "' error found (errno = " << code << "): " << errorBuffer << ".";
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  curl_easy_cleanup ( connection );
}

void* tstoolbox::HttpMessenger::myrealloc ( void* ptr, size_t size )
{
  //There might be a realloc() out there that doesn't like reallocing
  //NULL pointers, so we take care of it here
  if ( ptr )
  {
    return realloc ( ptr, size );
  }
  else
  {
    return malloc ( size );
  }
}

size_t tstoolbox::HttpMessenger::fileWriter ( void* ptr, size_t size, size_t nmemb, void* data )
{
  size_t realsize = size * nmemb;
  struct MemoryStruct* mem = ( struct MemoryStruct* ) data;
  mem->memory = ( char* ) myrealloc ( mem->memory, mem->size + realsize + 1 );

  if ( mem->memory )
  {
    memcpy ( & ( mem->memory[mem->size] ), ptr, realsize );
    mem->size += realsize;
    mem->memory[mem->size] = 0;
  }

  return realsize;
}

void tstoolbox::HttpMessenger::requestFile ( const std::string& url, char*& buffer, size_t& size )
{
  CURL* curl_handle ( 0 );
  CURLcode code;
  struct MemoryStruct chunk;
  chunk.memory=0;
  chunk.size = 0;
  //init the curl session
  curl_handle = curl_easy_init();
  //Error Buffer
  char errorBuffer[CURL_ERROR_SIZE];
  code = curl_easy_setopt ( curl_handle, CURLOPT_ERRORBUFFER, errorBuffer );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set error buffer (code " << code << ")." << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  //specify URL to get
  url_ = string2cstr ( url );
  code = curl_easy_setopt ( curl_handle, CURLOPT_URL, url_ );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to specify URL (code " << code << ")." << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  //send all data to this function
  code = curl_easy_setopt ( curl_handle, CURLOPT_WRITEFUNCTION, fileWriter );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to specify Handler (code " << code << ")." << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  //we pass our 'chunk' struct to the callback function
  code = curl_easy_setopt ( curl_handle, CURLOPT_WRITEDATA, ( void* ) &chunk );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to specify buffer (code " << code << ")." << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  //some servers don't like requests that are made without a user-agent field, so we provide one
  code = curl_easy_setopt ( curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0" );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to user-agent libcurl-agent/1.0 (code " << code << ")." << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  code = curl_easy_setopt ( curl_handle, CURLOPT_TIMEOUT, timeout_ );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set write data " << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  unsigned long nosignal ( 0 );
  code = curl_easy_setopt ( curl_handle, CURLOPT_NOSIGNAL, nosignal );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed to set write data " << errorBuffer;
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  //get it!
  code = curl_easy_perform ( curl_handle );

  if ( code != CURLE_OK )
  {
    std::ostringstream msg;
    msg << "Failed HTTP request to URL '" << url << "' error found (errno =" << code << "): " << errorBuffer << ".";
    XCEPT_RAISE ( tsexception::CurlException,msg.str() );
  }

  //cleanup curl stuff
  curl_easy_cleanup ( curl_handle );
  buffer = chunk.memory;
  size = chunk.size;
}

void tstoolbox::HttpMessenger::setTimeout ( const long& timeout )
{
  timeout_ = timeout;
}

size_t tstoolbox::HttpMessenger::_downloadData ( void* ptr, size_t size, size_t nmemb, FILE* stream )
{
        return fwrite ( ptr, size, nmemb, stream );
}

void tstoolbox::HttpMessenger::downloadFile ( const std::string& url, const std::string& destFilename )
{
        // create the folder if necessary (currently: create one level only)
        //
        std::string path = FsTools::folder ( destFilename );

        if ( !FsTools::folderExists ( path ) )
        {
                FsTools::createFolder ( path );
        }

        CURL* curl = curl_easy_init();

        if ( curl )
        {
                FILE* file = fopen ( destFilename.c_str(), "wb" );

                if ( !file )
                {
                        XCEPT_RAISE ( xcept::Exception, "Cannot write to file " + destFilename + "!" );
                }

                curl_easy_setopt ( curl, CURLOPT_URL, url.c_str() );
                curl_easy_setopt ( curl, CURLOPT_WRITEFUNCTION, &tstoolbox::HttpMessenger::_downloadData );
                curl_easy_setopt ( curl, CURLOPT_WRITEDATA, file );
                curl_easy_setopt ( curl, CURLOPT_FOLLOWLOCATION, 1 );
                curl_easy_setopt ( curl, CURLOPT_TIMEOUT, 30 );                                        // max 30 seconds
                curl_easy_setopt ( curl, CURLOPT_FAILONERROR, 1 );
                CURLcode errCode = curl_easy_perform ( curl );

                if ( CURLE_OK != errCode )
                {
                        XCEPT_RAISE ( xcept::Exception, "Cannot download '" + url + "' to file '" + destFilename
                                        + "'! (CURL error: " + curl_easy_strerror ( errCode ) + ")" );
                }

                curl_easy_cleanup ( curl );
                fclose ( file );
        }
}


