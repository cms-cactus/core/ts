#include "ts/toolbox/InterprocessMutex.h"

#include <iostream>
#include <fstream>

#include <errno.h>
#include <unistd.h>

std::string id = "my process mutex";
int main ( int argc, char* argv[] )
{
  std::string id ( "ipc_mutex" );
  int N=100;
  pid_t childpid = fork();

  if ( childpid == 0 ) //child
  {
    tstoolbox::InterprocessMutex m ( id );

    for ( int i=0; i!= N; ++i )
    {
      {
        tstoolbox::InterprocessMutexHandler h ( m );
        std::cout <<"CHILD:lock " << i << std::endl;
        sleep ( 1 );
        std::cout <<"CHILD:unlock " << i << std::endl;
      }
    }
  }
  else if ( childpid > 0 )              //parent
  {
    tstoolbox::InterprocessMutex m ( id );

    for ( int i=0; i!= N; ++i )
    {
      {
        tstoolbox::InterprocessMutexHandler h ( m );
        std::cout <<"PARENT:lock " << i <<  std::endl;
        sleep ( 1 );
        std::cout <<"PARENT:unlock " << i << std::endl;
      }
    }
  }
  else                  //error
  {
    std::cerr << "Error executing fork" << std::endl;
    perror ( "fork" );
  }

  sleep ( 1 );
  return 0;
}
