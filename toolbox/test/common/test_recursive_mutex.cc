#include "ts/toolbox/Mutex.h"

#include <iostream>

#include <sys/types.h>
#include <pthread.h>

tstoolbox::Mutex mutex_ ( true );
volatile bool pass1 = false;
volatile bool pass2 = false;

void* thread1 ( void* arg )
{
  int i=0;
  //while(true) {
  ++i;
  std::cout << "thread1: " << i << " : BEFORE lock(1)" <<std::endl;
  mutex_.lock();
  std::cout << "thread1: " << i << " : AFTER lock(1)" <<std::endl;
  pass2 =true;
  usleep ( 1000 );
  std::cout << "thread1: " << i << " : BEFORE lock(2)" <<std::endl;
  mutex_.lock();
  std::cout << "thread1: " << i << " : AFTER lock(2)" <<std::endl;
  std::cout << "thread1: " << i << " : BEFORE sleep(2)" <<std::endl;
  usleep ( 10 );
  std::cout << "thread1: " << i << " : AFTER sleep(2)" <<std::endl;
  std::cout << "thread1: " << i << " : BEFORE unlock(2)" <<std::endl;
  mutex_.unlock();
  std::cout << "thread1: " << i << " : AFTER unlock(2)" <<std::endl;
  std::cout << "thread1: " << i << " : BEFORE unlock(1)" <<std::endl;
  mutex_.unlock();
  std::cout << "thread1: " << i << " : AFTER unlock(1)" <<std::endl;
  //}
}
void* thread2 ( void* arg )
{
  int i=0;
  //while(true) {
  ++i;

  while ( !pass2 )
  {
    usleep ( 10 );
    std::cout << "thread2: pass2="<<pass2<<std::endl;
  }

  std::cout << "thread2: " << i << " : BEFORE lock()" <<std::endl;
  mutex_.lock();
  std::cout << "thread2: " << i << " : AFTER lock()" <<std::endl;
  //std::cout << "thread2: " << i << " : BEFORE sleep()" <<std::endl;
  //usleep(10);
  //std::cout << "thread2: " << i << " : AFTER sleep()" <<std::endl;
  std::cout << "thread2: " << i << " : BEFORE unlock()" <<std::endl;
  mutex_.unlock();
  std::cout << "thread2: " << i << " : AFTER unlock()" <<std::endl;
  // }
}


std::string id = "my process mutex";
int main ( int argc, char* argv[] )
{
  pthread_t t1;
  pthread_t t2;
  pthread_attr_t pthread_custom_attr;
  pthread_create ( &t1, NULL, thread1,NULL );
  pthread_create ( &t2, NULL, thread2,NULL );
  pthread_join ( t1,NULL );
  pthread_join ( t2,NULL );
  return 0;
}
