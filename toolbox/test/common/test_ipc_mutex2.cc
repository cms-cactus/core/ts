#include "ts/toolbox/InterprocessMutex.h"

#include <iostream>

#include <unistd.h>

#include <sys/types.h>
#include <pthread.h>

std::string id = "my process mutex";
tstoolbox::InterprocessMutex m ( id );

void* thread1 ( void* arg )
{
  while ( true )
  {
    tstoolbox::InterprocessMutexHandler h ( m );
    std::cout << "-->thread1 starts...";
    std::cout.flush();
    sleep ( 1 );
    std::cout << "thread1 finishes<---" << std::endl;
  }
}

void* thread2 ( void* arg )
{
  while ( true )
  {
    tstoolbox::InterprocessMutexHandler h ( m );
    std::cout << "--> thread2 starts...";
    std::cout.flush();
    std::cout << "thread2 finishes<---" << std::endl;
  }
}

int main ( int argc, char* argv[] )
{
  pthread_t t1;
  pthread_t t2;
  pthread_attr_t pthread_custom_attr;
  pthread_create ( &t1, NULL, thread1,NULL );
  pthread_create ( &t2, NULL, thread2,NULL );
  pthread_join ( t1,NULL );
  pthread_join ( t2,NULL );
  return 0;
}
