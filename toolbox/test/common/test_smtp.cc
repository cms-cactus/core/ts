#include "ts/toolbox/SmtpMessenger.h"

#include <iostream>

int main ( int argc, char* argv[] )
{
  //                 to     from     subject          message      smtp server          port 25            no MX
  tstoolbox::SmtpMessenger m ( "marc@cern.ch", "test subject", "test message body" );
  m.send( );
  return 0;
}
