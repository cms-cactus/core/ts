/**
 * @file CellException.h
 *
 * @brief Cell Exception classes
 * 
 * @author I. Magrans
 **/

#ifndef _ts_toolbox_CellException_h_
#define _ts_toolbox_CellException_h_

#include "xcept/Exception.h"

#include <string>

namespace tsexception
{
  //!Base class for exceptions until TS version 1.3
  class CellException: public xcept::Exception
  {
    public:
      CellException ( std::string name, std::string message, std::string module, int line, std::string function ) :xcept::Exception ( name, message, module, line, function ) {};
      CellException ( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) :xcept::Exception ( name, message, module, line, function, e ) {};
  };
}

#define DEFINE_TS_EXCEPTION(T) \
    class T : public tsexception::CellException \
    { \
    public: \
        T(std::string name, std::string message, std::string module, int line, std::string function) \
            : tsexception::CellException(name, message, module, line, function) \
        { \
        } \
          \
        T(std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e) \
            : tsexception::CellException(name, message, module, line, function, e) \
        { \
        } \
    }

namespace tsexception
{

  //!Error sending a mail
  DEFINE_TS_EXCEPTION ( SendMailError );

  //!A Document is not loaded and someone wants to parse it
  DEFINE_TS_EXCEPTION ( ParsingError );

  //!Error while a xhannel is being created
  DEFINE_TS_EXCEPTION ( XhannelError );

  //!Error while a xhannel is being created
  DEFINE_TS_EXCEPTION ( XhannelCreationError );

  //!The Xhannel does not exist
  DEFINE_TS_EXCEPTION ( XhannelDoesNotExist );

  //!A Xhannel is not being used correctly
  DEFINE_TS_EXCEPTION ( XhannelUsageError );

  //!Can not find remote application
  DEFINE_TS_EXCEPTION ( RemoteApplicationNotFound );

  //!Command execution give an error
  DEFINE_TS_EXCEPTION ( CommandExecutionError );

  //!Command can not be added because already exist
  DEFINE_TS_EXCEPTION ( CommandAlreadyExist );

  //!CellCommand does not exist
  DEFINE_TS_EXCEPTION ( CommandDoesNotExist );

  //!CellCommand name is not compliant with the XML tag syntax
  DEFINE_TS_EXCEPTION ( CommandNameError );

  //!The group does not exist in the CellFactory
  DEFINE_TS_EXCEPTION ( GroupDoesNotExist );

  //!Error in the CellFSM class
  DEFINE_TS_EXCEPTION ( FsmError );

  //!An error occured during the execution of an operation
  DEFINE_TS_EXCEPTION ( OperationExecutionError );

  //!The operation instance does not exist
  DEFINE_TS_EXCEPTION ( OperationDoesNotExist );

  //!The proposed oepration id is not acceptable
  DEFINE_TS_EXCEPTION ( ProposedOperationIdError );

  //!CellOepration already exist on the CellOperationFactory
  DEFINE_TS_EXCEPTION ( OperationAlreadyExists );

  //!CellPanel was not found in the CellPanelFactory
  DEFINE_TS_EXCEPTION ( CellPanelClassDoesNotExist );

  //!This is thrown when the curl library gives an error
  DEFINE_TS_EXCEPTION ( CurlException );

  //!An error occured while the SOAP message was being built
  DEFINE_TS_EXCEPTION ( SoapEncodingError );

  //!An error occured while the SOAP message was being parsed
  DEFINE_TS_EXCEPTION ( SoapParsingError );

  //!A SOAP Fault has been received
  DEFINE_TS_EXCEPTION ( SoapFault );

  //!An error occurred while communicating with a remote application
  DEFINE_TS_EXCEPTION ( CommunicationError );

  //!An error related with the sesion information has been found
  DEFINE_TS_EXCEPTION ( SessionError );

  //!An error occured while the SOAP message was being parsed
  DEFINE_TS_EXCEPTION ( ParameterError );

  //!An error occured while trying to access to the DB
  DEFINE_TS_EXCEPTION ( DbAccessError );

  //!An error occured in the returned information from the DB
  DEFINE_TS_EXCEPTION ( DbReturnedObjectError );
  
  //!An error occured while reading or writting a file
  DEFINE_TS_EXCEPTION ( FileError );

  //!Environment variable can not be found
  DEFINE_TS_EXCEPTION ( EnvironmentError );

  //!Environment variable can not be found
  DEFINE_TS_EXCEPTION ( MonitoringError );

  //!Error converting between classes
  DEFINE_TS_EXCEPTION ( ConversionError );

  //!Error while using a lock
  DEFINE_TS_EXCEPTION ( LockError );

  //!XML attribute not found
  DEFINE_TS_EXCEPTION ( AttributeNotFound );

  //!Error while rendering the TS GUI
  DEFINE_TS_EXCEPTION ( GuiError );

  //!Error while intializing the Cell
  DEFINE_TS_EXCEPTION ( CellInitializationError );

  //!Excepetion thrown by tstoolbox::UnexpectedHandler if an unexpected exception is thrown
  DEFINE_TS_EXCEPTION ( UnexpectedException );

  //!Virtual memory size to high an inminent crash is possible
  DEFINE_TS_EXCEPTION ( VirtualMemoryTooHigh );

  //!CPU Usage too high. The node could be hanging due too an infinite loop or a number of requests too high
  DEFINE_TS_EXCEPTION ( CpuUsageTooHigh );

  //!Trying to use a NULL pointer in a method call
  DEFINE_TS_EXCEPTION ( NullPointer );

  //!Within a switch/case the expresion is not covered by any of the cases
  DEFINE_TS_EXCEPTION ( CaseNotCovered );

  //!An error occured while executing a callback
  DEFINE_TS_EXCEPTION ( CallbackExecutionError );

  //!Possible Memory Leak caued a Bad Alloc
  DEFINE_TS_EXCEPTION ( PossibleMemoryLeak );

  //!Unsupported table name syntax
  DEFINE_TS_EXCEPTION ( TStoreUnsupportedQualifiedTableName );

  //!Semaphore crearion error
  DEFINE_TS_EXCEPTION ( SemaphoreCreationError );

  //!Error during semaphore locking/unlocking
  DEFINE_TS_EXCEPTION ( SemaphoreError );

  //!There was an error while executing fork
  DEFINE_TS_EXCEPTION ( ForkError );

}//ns tsexception

#endif
