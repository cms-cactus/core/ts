
#ifndef _ts_framework_Timer_h_
#define _ts_framework_Timer_h_

#include <string>
#include <sstream>

#include <ctime>
#include <sys/time.h>

namespace tstoolbox
{
  class Timer
  {
    public:
      Timer();

      Timer& reset();

      int seconds() const;

      // Original source (modified):
      // http://www.gnu.org/software/libtool/manual/libc/Elapsed-Time.html
      //
      // Included in a similar way in tstoolbox::timeval_diff()
      //
      int ms() const;

      std::string startTimeAsString ( bool niceFormat = false );

      // REVIEW: This could be done better...
      //
      static std::string timeAsString ( time_t secondsSince1970,
                                   bool niceFormatting = false );

      static std::string niceFormat ( const std::string& dateTime );


      static std::string timeAsString ( std::string microSecondsSince1970,
                                   bool niceFormatting = false );

      std::string startTimeInMicroSecondsAsString();

    protected:
      timeval _startTime;


      static void addWithZeroPadding ( std::stringstream& out, int value );

  };
}

#endif
