#ifndef _ts_toolbox_version_h_
#define _ts_toolbox_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define TS_TSTOOLBOX_VERSION_MAJOR 5
#define TS_TSTOOLBOX_VERSION_MINOR 2
#define TS_TSTOOLBOX_VERSION_PATCH 4

//
// Template macros
//
#define TS_TSTOOLBOX_VERSION_CODE PACKAGE_VERSION_CODE(TS_TSTOOLBOX_VERSION_MAJOR,TS_TSTOOLBOX_VERSION_MINOR,TS_TSTOOLBOX_VERSION_PATCH)
#ifndef TS_TSTOOLBOX_PREVIOUS_VERSIONS
#define TS_TSTOOLBOX_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TS_TSTOOLBOX_VERSION_MAJOR,TS_TSTOOLBOX_VERSION_MINOR,TS_TSTOOLBOX_VERSION_PATCH)
#else
#define TS_TSTOOLBOX_FULL_VERSION_LIST  TS_TSTOOLBOX_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TS_TSTOOLBOX_VERSION_MAJOR,TS_TSTOOLBOX_VERSION_MINOR,TS_TSTOOLBOX_VERSION_PATCH)
#endif

namespace tstoolbox
{
  const std::string project = "ts";
  const std::string package  =  "tstoolbox";
  const std::string versions =  TS_TSTOOLBOX_FULL_VERSION_LIST;
  const std::string description = "Trigger Supervisor Toolbox and Exceptions";
  const std::string authors = "Ildefons Magrans de Abril, Marc Magrans de Abril";
  const std::string summary = "Contains the TriggerSupervisor Toolbox and the Exceptions (https://savannah.cern.ch/projects/l1ts/)";
  const std::string link = "https://savannah.cern.ch/projects/l1ts/";

  config::PackageInfo getPackageInfo();
  void checkPackageDependencies() throw ( config::PackageInfo::VersionException );
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif


