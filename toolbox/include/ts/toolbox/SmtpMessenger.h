#ifndef _ts_toolbox_SmtpMessenger_h_
#define _ts_toolbox_SmtpMessenger_h_

#include <string>

namespace tstoolbox
{
  //!Simple SMTP client. If some of the steps fails throws a tsexception::SendMailError. 
  //!The default sender is <triggersupervisor.noreply@hostname>. The default message is empty.
  class SmtpMessenger
  {
    public:
      //!The recipients are comma separated (e.g. "trigger@cern.ch,supervisor@cern.ch").
      SmtpMessenger ( const std::string& to, const std::string& subject, const std::string& message="" );
      ~SmtpMessenger();
      
      static std::string getHostname();

      void send(std::string aFrom = "");
      
    private:
      static std::string getDefaultSender();
      std::string to_;
      std::string from_;
      std::string subject_;
      std::string message_;
  };
} // end ns tstoolbox
#endif
