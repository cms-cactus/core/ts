/**
 * @file CellToolbox.h
 *
 * @author I. Magrans
 **/

#ifndef _CellToolbox_h_
#define _CellToolbox_h_

#include <string>

#include "xoap/MessageReference.h"
#include "cgicc/Cgicc.h"

namespace xdata
{
  class Serializable;
}

namespace tstoolbox
{
  std::string getCommand ( xoap::MessageReference msg );
  std::string getNameSpace ( xoap::MessageReference msg );
  bool isAsync ( xoap::MessageReference msg );
  std::string getCid ( xoap::MessageReference msg );
  std::string getSid ( xoap::MessageReference msg );
  std::string getOpid ( xoap::MessageReference msg );
  std::string getOperation ( xoap::MessageReference msg );
  std::string getCallbackFun ( xoap::MessageReference msg );
  std::string getCallbackUrl ( xoap::MessageReference msg );
  std::string getCallbackUrn ( xoap::MessageReference msg );
  xdata::Serializable* getPayload ( xoap::MessageReference msg );
  xdata::Serializable* getWarningLevel ( xoap::MessageReference msg );
  xdata::Serializable* getWarningMessage ( xoap::MessageReference msg );
  xdata::Serializable* getXdaqParameter ( xoap::MessageReference msg );
  xdata::Serializable* analyse ( DOMNode* com );
  xdata::Serializable* analyseSoapBag ( DOMNode* com );
  xdata::Serializable* analyseSoapVector ( DOMNode* com );
  //methods used in CellOpSendCom object
  std::string getOpComName ( xoap::MessageReference msg );
  std::map<std::string, xdata::Serializable*> getOpComParamList ( xoap::MessageReference msg );

  //do SOAP messages to interact with the CellCommandPort::run(xoap::MessageReference): Monolitic command interface

  xoap::MessageReference doSoapCommand ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& command, std::map<std::string,xdata::Serializable*> param, const std::string& cb,const std::string& url,const std::string& urn );

  //do SOAP messages to interact with the CellCommandPort::run(xoap::MessageReference): Operation interface (documented in /cell/xml/opInterface)

  xoap::MessageReference doSoapOpSendComand ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& opid, const std::string& command, std::map<std::string,xdata::Serializable*> param, const std::string& cb,const std::string& url,const std::string& urn );
  xoap::MessageReference doSoapOpGetState ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& opid, const std::string& cb,const std::string& url,const std::string& urn );
  xoap::MessageReference doSoapOpReset ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& op, const std::string& cb,const std::string& url,const std::string& urn );
  //bool checkError( xoap::MessageReference msg);
  double getTimeus();
  // converts all '\n' characters in string in to xhtml <br />-tags in the returned string
  std::string newline2br ( const std::string& in );

  std::string expandFileName ( const std::string& pathname );
  std::string getEnvironment ( const std::string& variable );
  std::string getSubmittedValue ( cgicc::Cgicc& cgi,const std::string& attribute );
  //!gets the content of the file located at <path>/<filename>. Removes blank characters (i.e. new line, tabs, spaces,etc.)
  std::string getPassword ( const std::string& path,const std::string& filename );
  //!difference of timeval in ms
  double timeval_diff ( timeval end, timeval start );
  
  //!return a formatted timeval as YYYY-MM-DD HH:MM:SS:msec
  std::string formatTimeval ( timeval aTv );  
  //!Escapes non-ASCII characters so they are correctly encoded and serialized
  std::string escape ( const std::string& str );
} //ns tstoolbox

#endif
