#ifndef _ts_toolbox_ReadWriteLock_h_
#define _ts_toolbox_ReadWriteLock_h_

#include <pthread.h>

namespace tstoolbox
{
  //!Implementation of multiple read single write lock.
  //!Initial state is all the locks released
  class ReadWriteLock
  {
    public:

      ReadWriteLock();
      ~ReadWriteLock();

      //! Lock for reader access.
      void rlock();

      //! Unlock reader access.
      void unlock();

      //! Lock for writer access.
      void wlock();

    private:
      ReadWriteLock ( const ReadWriteLock& rwlock );
      const ReadWriteLock& operator= ( const ReadWriteLock& rwlock );

    private:
      pthread_rwlock_t rwlock_;
  };

  //!The handle uses the RAII idiom in order to simplify exception handling and locks
  class ReadWriteLockHandler
  {
    public:
      enum LockType {READ,WRITE};
      ReadWriteLockHandler ( ReadWriteLock& rwlock, const LockType& type ) :rwlock_ ( rwlock )
      {
        if ( type == READ )
        {
          rwlock_.rlock();
        }
        else
        {
          rwlock_.wlock();
        }
      };
      ~ReadWriteLockHandler()
      {
        rwlock_.unlock();
      };
    private:
      ReadWriteLock& rwlock_;
  };
}
#endif
