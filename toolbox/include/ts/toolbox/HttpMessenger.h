/**
 * @file HttpMessenger.h
 *
 * @author I. Magrans
 * @author M. Magrans
 **/

#ifndef _ts_toolbox_HttpMessenger_h_
#define _ts_toolbox_HttpMessenger_h_

#include <iostream>
#include <ostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ts/exception/CellException.h"

#include "curl/curl.h"
#include "curl/easy.h"

namespace tstoolbox
{
  class HttpMessenger
  {
    public:

      HttpMessenger();
      ~HttpMessenger();

      void requestHtml ( const std::string& url, std::string& response );
      void requestFile ( const std::string& url,char*& buffer, size_t& size );

      void downloadFile ( const std::string& url, const std::string& destFilename );
      size_t _downloadData ( void* ptr, size_t size, size_t nmemb, FILE* stream );

      void setTimeout ( const long& timeout );

    private:

      static int htmlWriter ( char* data, size_t size, size_t nmemb,std::string* writerData );
      static size_t fileWriter ( void* ptr, size_t size, size_t nmemb, void* data );

      void init ( CURL *&conn, char* url,std::string& response,char* errorBuffer );
      char* string2cstr ( const std::string& s );
      static void* myrealloc ( void* ptr, size_t size );


      //static int onlyonce_;
      long timeout_;
      char* url_;

    private:
      struct MemoryStruct
      {
        char* memory;
        size_t size;
      };
  };
}//ns tstoolbox
#endif
