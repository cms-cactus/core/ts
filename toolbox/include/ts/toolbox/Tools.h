/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun.
 *************************************************************************/

#ifndef _tstoolbox_ItfTools_h_
#define _tstoolbox_ItfTools_h_

#include "ts/exception/CellException.h"

#include <boost/scoped_ptr.hpp>

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iomanip>
/*
namespace tsframework {
class CellObject;
}
*/
namespace xdata {
class Serializable;
}

namespace log4cplus {
class Logger;
}

namespace tstoolbox
{


#define iAssertMsg(expression, message) XCEPT_ASSERT(expression, tstoolbox::Tools::AssertionFailed, std::string(#expression) + " --" + message);
#define iAssert(expression)             XCEPT_ASSERT(expression, tstoolbox::Tools::AssertionFailed, #expression);

/**
 * This provides a set of tools.
 */
namespace Tools
{
DEFINE_TS_EXCEPTION ( AssertionFailed );
DEFINE_TS_EXCEPTION ( ExecutionFailed );
DEFINE_TS_EXCEPTION ( StdException );
DEFINE_TS_EXCEPTION ( UnknownException );
DEFINE_TS_EXCEPTION ( InvalidArgumentException );

/**
 * Prints a list of elements from T to an ostringstream and returns the string (separated by _separator_).
 * T must provide an operator<<.
 */
template <class T>
std::string printList ( const T& items, const std::string& separator = "," )
{
	std::ostringstream out;

	for ( typename T::const_iterator i = items.begin(); i != items.end(); ++i )
	{
		if ( i != items.begin() )
		{
			out << separator;
		}

		out << *i;
	}

	return out.str();
}


std::string vector2json ( const std::vector<std::string>& items );

std::string map2json ( const std::map<std::string, std::string>& items );

std::vector<unsigned int> string2IntVector ( const std::string& paramName,
		const std::string& param,
		const size_t minLength,
		const size_t fillLength,
		const int maxLegalValue = 1 );

/// extracts the substring from source that is located between BEFORE and AFTER. If one of these is empty,
/// the beginning/end of the string will be used.
///
std::string extractParam ( const std::string& source,
		const std::string& before,
		const std::string& after );

/// Returns the the shortest possible string representation of the given int/bool/... vector.
///
template <typename T>
std::string toMinLengthString ( const std::vector<T>& vec )
{
	std::stringstream result;
	int maxIndex;

	if ( vec.empty() )
	{
		return "0";
	}

	// we transmit only the relevant bits (i.e. up to the highest value that is not != 0)
	//
	for ( maxIndex = vec.size()-1; ( maxIndex > 0 ) && !vec[maxIndex]; --maxIndex )
	{
	}

	for ( int i = 0; i <= maxIndex; ++i )
	{
		//
		// yes, "i <= maxIndex" ... we want to have at least one 0 and we have to include the highest one!!
		//
		result << vec[i];
	}

	return result.str();
}

// Note: string so you can pass something like "&nbsp;"
std::string fillString ( const std::string& in,
		const int width,
		const std::string& fillChar = " " ) ;

template <typename T>
std::string fillStringLeft ( const T& in,
		const int width,
		const char fillChar = ' ' )
{
	std::ostringstream out;
	out << std::setfill ( fillChar ) << std::setw ( width ) << in;
	return out.str();
}

//! As my Attributes class uses "'" to enclose the values, '"' causes no problems for me. Therefore, it is not
//! encoded by default.
//!
std::string xmlEncode ( const std::string& xml,	bool encodeQuot = false );

std::string xmlDecode ( const std::string& xml );

template <typename T>
std::string asString ( const T& value )
{
	std::ostringstream out;
	out << value;
	return out.str();
}

bool startsWith ( const std::string& line,const std::string& substring );

std::vector<std::string> split ( const std::string& toSplit,const std::string& delimiters );

bool checkAvailableMemory ( size_t availableMB = 100 );

bool checkAvailableMemory ( size_t availableMB,	log4cplus::Logger& log );

std::string execWithPipe ( const std::string& command, bool AddNewline = false );

/**
 * Sometimes system() returns a wrong exit code "-1", even though everything seems to be fine. Thus, this method
 * tries to call it several times until it gives up.
 */
int loopedSystemCall ( const std::string& command,int numAttempts = 5 );

std::string hostName ( const std::string& url,		bool includePort = false );

void storeException ( boost::scoped_ptr<xcept::Exception>& holder, xcept::Exception& e );

}


}

#endif
