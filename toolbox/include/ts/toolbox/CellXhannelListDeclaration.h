/**
 * @file CellXhannelListDeclaration.h
 *
 * @author I. Magrans
 **/

#ifndef _ts_toolbox_CellXhannelListDeclaration_h_
#define _ts_toolbox_CellXhannelListDeclaration_h_

#include "xoap/domutils.h"

#include <string>
#include <vector>
#include <map>
namespace tstoolbox
{

  class CellXhannelListDeclaration
  {
    public:
      CellXhannelListDeclaration();
      ~CellXhannelListDeclaration();

      void load ( const std::string& declarationFile );
      //!get the values of the xhannel names
      std::vector<std::string> getXhannelNames();

      std::string getTargetAttribute ( const std::string& xhannelName, const std::string& attr );

    private:
      DOMDocument* document_;
      std::string parserName_;
  };
}//ns tstoolbox
#endif
