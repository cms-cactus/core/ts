#ifndef _ts_toolbox_Mutex_h_
#define _ts_toolbox_Mutex_h_

#include "ts/exception/CellException.h"

#include <sstream>

#include <pthread.h>

namespace tstoolbox
{
  //!Mutex wrapper. The same thred can not lock twice, this will cause a dead-lock
  class Mutex
  {
    public:
      //!By default the mutex is non-recursive
      Mutex ( bool recursive=false )
      {
        pthread_mutexattr_init ( &attr_ );

        if ( recursive )
        {
          pthread_mutexattr_settype ( &attr_, PTHREAD_MUTEX_RECURSIVE );
        }

        pthread_mutex_init ( &mutex_,&attr_ );
      }
      ~Mutex()
      {
        pthread_mutex_destroy ( &mutex_ );
        pthread_mutexattr_destroy ( &attr_ );
      }
      void lock()
      {
        pthread_mutex_lock ( &mutex_ );
      };
      void unlock()
      {
        pthread_mutex_unlock ( &mutex_ );
      };

    private:
      Mutex ( const Mutex& );
      Mutex& operator= ( const Mutex& );
      pthread_mutex_t mutex_;
      pthread_mutexattr_t attr_;
  };

  //!The handle uses the RAII idiom in order to simplify exception handling while using the mutex
  class MutexHandler
  {
    public:
      MutexHandler ( Mutex& mutex ) :mutex_ ( mutex )
      {
        mutex_.lock();
      };
      ~MutexHandler()
      {
        mutex_.unlock();
      };
    private:
      Mutex& mutex_;
      MutexHandler ( const MutexHandler& );
      MutexHandler& operator= ( const MutexHandler& );
  };
}
#endif
