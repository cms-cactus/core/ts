/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun. (original DirectoryStorage: Thomas Themel)
 *************************************************************************/

#ifndef _tstoolbox_FsTools_h_
#define _tstoolbox_FsTools_h_

#include "ts/toolbox/Tools.h"

#include "xcept/Exception.h"

#include <vector>
#include <string>
#include <fstream>

namespace tstoolbox
{


namespace FsTools
{
  //!returns the parent path of the filename
std::string folder ( const std::string& filename );

//!creates a directory
void createFolder ( const std::string& name );

//!checks if a file exists
bool fileExists ( const std::string& name );

//!checks if a folder exists
bool folderExists ( const std::string& name );

//!checks if a directory or a file is empty. works for both files and folders
bool isEmpty ( const std::string& name );

//!checks if it is a directory
bool isFolder ( const std::string& name );

//!renames the source using the target
void rename ( const std::string& source, const std::string& target );

//! removes both files and (also non-empty!) directories
unsigned long remove ( const std::string& name );


//! Contrary to listFiles(), this works for symlinks too! Returns all matching files in all sub folders!
//!
void listAllFiles ( std::vector<std::string>& result, std::string& errors, const std::string& folder, const std::string& regex );

//!stores the data of a file to a string
std::string getTextFile ( const std::string& name );

// template version if you'd prefer to parse the input file while loading already (less memory consumption)
//
template <typename T>
void getTextFile ( const std::string& name,
		T& result )
{
	std::ifstream stream ( name.c_str() );

	if ( !stream )
	{
		XCEPT_RAISE ( xcept::Exception, "File " + name + " not available!" );
	}

	stream >> result;
	// read the entire file (hopefully)
	//
	getline ( stream, result, '\0' );

	if ( !stream )
	{
		XCEPT_RAISE ( xcept::Exception, "Error while reading file " + name + " !" );
	}

	stream.close();
}


template <class T>
void storeTextFile ( const std::string& name,
		const T& value,
		bool append = false )
{
	iAssert ( "" != name );
	std::ofstream file ( name.c_str(), std::ios_base::out | ( append ? std::ios_base::app
			: std::ios_base::trunc ) );

	if ( !file )
	{
		XCEPT_RAISE ( xcept::Exception, "Cannot create/append file " + name + "!" );
	}

	file << value;

	if ( !file )
	{
		XCEPT_RAISE ( xcept::Exception, "Error while writing file " + name + " !" );
	}

	file.close();
}

//!encodes a file using the Base64
std::string encodeBase64 ( const std::string& filename );
//!decodes a file using the Base64
void decodeBase64 ( const std::string& filename, const std::string& content );

//! ZLib compression: See http://zlib.net/zlib_how.html
//!
//! If destFilename == "" then sourceFilename + ".zlib" will be used
//!
//! @return The destination file name.
//!
std::string zCompress ( const std::string& sourceFilename, std::string destFilename = "" );


//! ZLib compression: See http://zlib.net/zlib_how.html
//!
void zDecompress ( const std::string& sourceFilename, const std::string& destFilename );


//! ZLib compression: See http://zlib.net/zlib_how.html
//!
void zErrorCheck ( int zlibResult );

//!creates an archive
//!Do not pass the path or folder with a trailing '/' !!
std::string packData ( const std::string& path, const std::string& folder );

//!extracts from an archive
void unpackData ( const std::string& path,
		const std::string& folder,
		const std::string& content );

/**
 * This method returns a vector with all files (and folders if you want) that match the pattern. Be aware that
 * each item *includes* the folder!! (necessary as the folder could contain wildcards too)
 *
 * Does NOT expand/find symlinks!
 */
//!like ls but it won't show the contents of subdirectories
std::vector<std::string> listFiles ( const std::string& pathPattern,
		bool filesOnly = true,
		int globFlags = 0 );

/**
 * Expand the folder name (e.g. get rid of the tilde; to ease the removal from the expanded file names).
 * Furthermore, this call adds a '/' at the end (if necessary). Will throw.
 */
std::string expandFolderName ( const std::string& name );

// REVIEW: Is sourceHostUrl really needed??
//
//!downloads a file and if it already exists in a previous version, deletes the previous version file
void syncFile ( const std::string& name,
		const std::string& url,
		const std::string& sourceHostUrl,
		const std::string& myHostUrl );

//! @see http://curl.haxx.se/libcurl/c/
//!

};
}

#endif
