#ifndef _tsitf_Gzip_h_
#define _tsitf_Gzip_h_

#include <string>
#include "xgi/Method.h"

namespace tstoolbox {
  namespace Gzip {
    std::string encodeOnRequest( xgi::Input* in, std::string const stringIn );
    /*
     ** encode a string using the GZIP algorithm
     */
    std::string encode ( std::string const stringIn );
  }
}

#endif
