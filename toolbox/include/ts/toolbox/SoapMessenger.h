/**
 * @file SoapMessenger.h
 *
 * @author I. Magrans
 **/

#ifndef _tstoolbox_SoapMessenger_h_
#define _tstoolbox_SoapMessenger_h_

#include "xoap/MessageReference.h"

#include "log4cplus/logger.h"

#include <string>
#include <stdlib.h>
#include <iostream>
#include <sstream>

namespace tstoolbox
{
  class SoapMessenger
  {
    public:

      SoapMessenger ( log4cplus::Logger& log );
      ~SoapMessenger();

      xoap::MessageReference send ( xoap::MessageReference msg, const std::string& local_url, const std::string& url, const std::string& urn );

      log4cplus::Logger& getLogger();

    private:
      log4cplus::Logger logger_;


  };
}//ns tstoolbox
#endif
