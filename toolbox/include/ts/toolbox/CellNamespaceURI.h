/**
 * @file CellNamespaceURI.h
 *
 * @brief Definitions of TS namespaces
 * 
 * @author I. Magrans
 **/

#ifndef _TSNamespaceURI_h_
#define _TSNamespaceURI_h_

// Push/Pull converter namespace
#define PPC_NS_URI "urn:ppc-soap:3.0"
//Control cell namespace (testing porposes):
#define CELL_NS_URI "urn:cell-soap:3.0"
//namespace of the Central node of the TS:
#define TS_NS_URI "urn:ts-soap:3.0"
//namespace of the GT leaf:
#define TSGT_NS_URI "urn:tsgt-soap:3.0"
//namespace of the GMT leaf:
#define TSGMT_NS_URI "urn:tsgmt-soap:3.0"
//namespace of the GCT leaf:
#define TSGCT_NS_URI "urn:tsgct-soap:3.0"
//namespace of the RCT leaf:
#define TSRCT_NS_URI "urn:tsrct-soap:3.0"
//namespace of the DTTF leaf:
#define TSDTTF_NS_URI "urn:tsdttf-soap:3.0"
//namespace of the CSCTF leaf:
#define TSCSCTF_NS_URI "urn:tscsctf-soap:3.0"
//namespace of the RPCT (RPCT are the RPC Trigger modules) leaf:
#define TSRPCT_NS_URI "urn:tsrpct-soap:3.0"
//namespace of the ECAL leaf:
#define TSECAL_NS_URI "urn:tsecal-soap:3.0"
//namespace of the HCAL leaf:
#define TSHCAL_NS_URI "urn:tshcal-soap:3.0"
//namespace of the DT leaf:
#define TSDT_NS_URI "urn:tsdt-soap:3.0"
//namespace of the CSC leaf:
#define TSCSC_NS_URI "urn:tscsc-soap:3.0"
//namespace of the RPC (RPC are the RPC TPG modules) leaf:
#define TSRPC_NS_URI "urn:tsrpc-soap:3.0"
//namespace of the CellAccess
#define TSACCESS_NS_URI "urn:tsaccess-soap:3.0"

// Upgraded systems:
//namespace of the S1CALOL2 leaf:
#define TSS1CALOL2_NS_URI "urn:tss1calol2-soap:3.0"

#endif
