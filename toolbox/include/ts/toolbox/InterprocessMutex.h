#ifndef _ts_toolbox_InterprocessMutex_h_
#define _ts_toolbox_InterprocessMutex_h_

#include "ts/exception/CellException.h"

#include <sstream>
#include <iostream>
#include <locale>

#include <errno.h>
#include <limits.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#if defined(__GNU_LIBRARY__) && !defined(_SEM_SEMUN_UNDEFINED)
/* union semun is defined by including <sys/sem.h> */
#else
/* according to X/OPEN we have to define it ourselves */
union semun
{
  int val;                  /* value for SETVAL */
  struct semid_ds* buf;     /* buffer for IPC_STAT, IPC_SET */
  unsigned short* array;    /* array for GETALL, SETALL */
  /* Linux specific part: */
  struct seminfo* __buf;    /* buffer for IPC_INFO */
};
#endif

namespace tstoolbox
{
  //!InterprocessMutex wrapper. The same thred can not lock twice, this will cause a dead-lock
  class InterprocessMutex
  {
    public:
      //!By default the mutex is non-recursive
      InterprocessMutex ( const std::string& id )
      {
        int hash = create_hash ( id );
        //deploy something?
        key_t mykey = ftok ( "/tmp", hash );

        if ( mykey == -1 )
        {
          throwException ( "Error while creating semaphore key for '/tmp' file" );
        }

        int numsems =1;
        int semflags = 0666;
        sid_ = semget ( mykey, numsems, semflags );

        if ( sid_ ==-1 && errno == ENOENT )
        {
          //the semaphore does not exist, we try to create it
          sid_ = semget ( mykey, numsems, IPC_CREAT | IPC_EXCL | semflags );

          if ( sid_ != -1 )
          {
            //Semaphore should be intialized
            union semun semopts;
            semopts.val = 1;

            if ( semctl ( sid_,0,SETVAL,semopts ) == -1 )
            {
              throwException ( "Can not initialize Semaphore" );
            }
          }
          else if ( sid_==-1 && errno ==EEXIST )
          {
            //the semaphore was created in the meanwhile
            sid_ = semget ( mykey, numsems, semflags );

            if ( sid_==-1 )
            {
              //but we can not retrieve it
              std::ostringstream msg;
              msg << "Error retrieving existing semaphore (key=" << mykey << ")";
              throwException ( msg.str() );
            }
          }
          else
          {
            //Can not create Semaphore
            throwException ( "Error trying to create a Semaphore" );
          }
        }
        else if ( sid_ == -1 )
        {
          throwException ( "Error while creating interprocess semaphore" );
        }
      }

      ~InterprocessMutex()
      {
        ;
      };

      void lock()
      {
        struct sembuf sem_lock= { ( unsigned short ) 0, -1, SEM_UNDO};
        bool success = semop ( sid_, &sem_lock, 1 ) != -1;

        if ( !success )
        {
          throwException ( "Error while locking semaphore" );
        }
      };
      void unlock()
      {
        struct sembuf sem_lock= { ( unsigned short ) 0, 1, SEM_UNDO};
        bool success = semop ( sid_, &sem_lock, 1 ) != -1;

        if ( !success )
        {
          throwException ( "Error while unlocking semaphore" );
        }
      };

    private:
      int create_hash ( const std::string& id )
      {
        std::locale loc;                 // the "C" locale
        const std::collate<char>& coll = std::use_facet<std::collate<char> > ( loc );
        long myhash = coll.hash ( id.data(),id.data() +id.length() );
        //std::cout << (myhash & 65535) << std::endl;
        return ( myhash % 65535 );
      };

      void throwException ( const std::string& emsg )
      {
        std::stringstream msg;
        msg << emsg << ": "<< strerror ( errno ) <<" (errno=" << errno << ")";
        XCEPT_RAISE ( tsexception::SemaphoreError, msg.str() );
      };

      InterprocessMutex ( const InterprocessMutex& );
      InterprocessMutex& operator= ( const InterprocessMutex& );

      int sid_;
  };

  //!The handle uses the RAII idiom in order to simplify exception handling while using the mutex
  class InterprocessMutexHandler
  {
    public:
      InterprocessMutexHandler ( InterprocessMutex& mutex ) :mutex_ ( mutex )
      {
        mutex_.lock();
      };
      ~InterprocessMutexHandler()
      {
        mutex_.unlock();
      };
    private:
      InterprocessMutex& mutex_;
      InterprocessMutexHandler ( const InterprocessMutexHandler& );
      InterprocessMutexHandler& operator= ( const InterprocessMutexHandler& );
  };
}
#endif
