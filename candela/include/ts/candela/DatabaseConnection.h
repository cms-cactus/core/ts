#ifndef cactuscore_candela_DATABASECONNECTION_H
#define cactuscore_candela_DATABASECONNECTION_H

#include "log4cplus/logger.h"

#include <string>
#include <vector>
#include <map>
	
/**
 * @brief: tsframework related classes necessary to get the CellXhannelTB object
 */

namespace xdata
{
    class Table; 
}


namespace tsframework
{
    class CellAbstractContext;
    class CellXhannelTB;
}


namespace tscandela
{

	// FORWARD DECLARATIONS

class Table;

class DatabaseConnection
{

  friend class Table;
  friend class DBJob; 

  
      
// STATIC API
public:

    static std::string getUserFromFullyQualifiedName(const std::string& fullyQualifiedTableName);

    static std::string getTableFromFullyQualifiedName(const std::string& fullyQualifiedTableName);

    static std::string getPassword(const std::string& pwdPath, const std::string& user);

    //! Default SQL view to perform queries
    static const std::string defaultTStoreView_;


public:
    /**
     * @brief CTOR: takes a log object, the context from where to get the CellXhannelTB object and the DB user. The DB id is defined in the view files.
     * @param log Logger object
     * @param context CellContext from which to take the CellXhannelTB object
     * @param user DB user
     * @param pwdPath Path where password files are stored. By default, it will be gotten from the $PWD_PATH environment variable
     */
    DatabaseConnection(
        log4cplus::Logger& log,
        tsframework::CellAbstractContext* context,
        const std::string& user	  = "CMS_TRG_R",
        const std::string& pwdPath = "$PWD_PATH"
        );

    /**
     * @brief DTOR
     */
    ~DatabaseConnection();

    /**
     * @brief Gets a Table object
     * @param fullyQualifiedTableName Name of the table to get
     * @return candela::Table object
     */
    Table getTable(const std::string& fullyQualifiedTableName);

    /**
     * @brief Returns all table names that belong to the current schema in a vector of string objects
     * @return vector<string> containing the names of the tables that belong to this user
     */
    std::vector<std::string> getTableNames(const std::string& user);

    /**
     * @brief Returns a Table object pointed by a fkColumn in table tableName
     * @param fullyQualifiedTableName The table name of the Table pointing to another
     * @param fkColumn Column that has a FK-PK relationship with the returned Table
     * @return candela::Table object
     */
    Table getReferencedTable(const std::string& fullyQualifiedTableName, const std::string& fkColumn);


    //! Returns the user for this connection
    std::string getUser();

    //!
    std::string getPasswordPath();

    //! Gets the logger object
    log4cplus::Logger& 	getLogger();    


  private:

    std::string getReferencedColumn(const std::string& owner, const std::string& constraint);

    //! Returns the login password for this user
    std::string getPassword();

    //! Gets the CellXhannelTB* from the context
    tsframework::CellXhannelTB* getDBXhannel();

    //! Gets the CellAbstractContext* object
    tsframework::CellAbstractContext* getContext();

    /**
     * @brief Stores tabName - tabName's PK in m_tableToPK
     * @return Primary key of table fullyQualifiedTableName
     */
    std::string getPK(const std::string& owner, const std::string& fullyQualifiedTableName);

  public:
    /**
     * @brief Stores relation of tabName to a map containing pairs of
     * tabName column names and tabName column types
     * @param fullyQualifiedTableName Full qualified name for the table
     * @return map <string, string> Map containing column name - column type pairs for table fullyQualifiedTableName
     */
    std::map<std::string, std::string> getTableDefinition(const std::string& fullyQualifiedTableName);
    
    /**
     */
    xdata::Table* getTableDefinitionInXdata(const std::string& fullyQualifiedTableName);
    
    /**
     * @brief Stores all tables for a given schema.
     * This is stored in the member mapUserNameToVectorTableName_;
     * @param user User for which all tables will be stored
     */
    void fillAllTableNames(const std::string& user);

    
  private:

	
    //! Logger object
    log4cplus::Logger logger_;

    //! Pointer to CellAbstractContext
    tsframework::CellAbstractContext* context_;

    //! DB user
    std::string user_;

    //! Path to stored password files
    std::string pwdPath_;

    //! DB user's password
    std::string password_;

    //! TStore view name
    std::string tStoreView_;

    //! Map table name to Table pointer
    std::map< std::string, Table* > mapTableNameToTable_;

    //!Map table name to map table column to table type
    std::map< std::string, std::map< std::string,std::string > > mapTableNameToMapColumnNameToType_;

    //! Map pairs table name - table primary key name
    std::map< std::string, std::string> mapTableNameToPKName_;

    //! Map table name to map of pairs FK names to referenced table
    std::map< std::string, std::map<std::string, std::string> > mapTableNameToMapFKNameToRefTable_;

    //! Map table name to map of pairs FK names to referenced column
    std::map< std::string, std::map<std::string, std::string> > mapTableNameToMapFKNameToRefColumn_;

    //! Map user name to vector of table names
    std::map< std::string, std::vector<std::string> > mapUserNameToVectorTableName_;

  };
}//ns candela

#endif

