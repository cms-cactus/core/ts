#ifndef cactuscore_candela_QUERYRESULT_H
#define cactuscore_candela_QUERYRESULT_H

#include "log4cplus/logger.h"

#include <string>
#include <vector>
#include <map>

#include <boost/thread/mutex.hpp>


namespace xdata
{
    class Table;
}

namespace tsframework
{
    class CellAbstractContext;
    class CellXhannelTB;
    class CellXhannelRequest;
}


namespace tscandela
{

    class DatabaseConnection;
    class Row;
    class QueryResultIterator;


class QueryResult
{

    friend class DatabaseConnection;    
    friend class Row;
    friend class Table; 

public:

    typedef QueryResultIterator const_iterator;

    /**
    * CTOR
    * @brief CTOR
    * @param parameters Map with parameters for XML binding
    * @param tStoreView The XML file which contains the query
    * @param parameters Map containing name-value pairs for parameter binding
    *
    * The method calls the QueryResult::execute method to run the query
    */

    QueryResult(){};

    QueryResult(
          DatabaseConnection* db,	  
          std::map<std::string, std::string>& parameters,
          const std::string& queryname,
          const std::string& tStoreView = ""
          );   

    /**
     * DTOR
     */
    ~QueryResult();


    /**
     * @return The number of rows that resulted from the query execution
     */
    size_t size() const;


    /**
     * @return Vector of strings containing the name of all columns in the query result
     */
    std::vector<std::string> getColumns() const;

    /**
     * @return The name of the query
     */
    std::string getName() const;

    /**
     *	@return QueryResultIterator object, pointing to the first row of the xdata::Table
     */
    const_iterator begin();

    /**
     *	@return QueryResultIterator object
     */
    const_iterator end();


    std::string getColumnType(const std::string& columnName);


    void printQueryResultDetails() const;


private:

    /** 
     * @brief Executes a given query
     */
    xdata::Table* execute();

    xdata::Table* getQueryResult() const;

    void throwQueryResultMissingException() const;




private:
    
    DatabaseConnection* db_;

    std::map< std::string, std::string > parameters_;

    std::string queryName_;

    std::string tStoreView_;

    tsframework::CellXhannelTB* dbXhannel_;

    log4cplus::Logger logger_;

    xdata::Table* resultTable_;  

};
} //ns candela

#endif
