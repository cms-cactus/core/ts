#ifndef _cactuscore_candela_DBJOB_H_
#define _cactuscore_candela_DBJOB_H_

#include "ts/candela/utilities.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "xdata/Serializable.h"
#include "xdata/Table.h"

#include <string>
#include <sstream>
#include <map>

// #include <type_traits>

namespace xdata
{
    class Table; 
}

namespace tsframework
{
    class CellXhannelTB;    
    class CellXhannelRequest; 
    class CellXhannelRequestTB;    
}

namespace tscandela
{
    class DatabaseConnection;

class DBJob
{
public:

    DBJob(DatabaseConnection* db, const std::string& tStoreView);

    virtual ~DBJob();   
    
    virtual void run(); 
   

protected:   
    
    virtual void runVirtual() = 0;
    
    virtual void printJobVirtual() = 0;
    
    virtual std::string jobType() = 0;
    
    
    
    tsframework::CellXhannelTB* dbXhannel_;

    std::string user_;

    std::string password_;

    std::string tStoreView_;

    tsframework::CellXhannelRequest* request_; 
    
    tsframework::CellXhannelRequestTB* reqDB_;
    
    DatabaseConnection* db_;
    
    log4cplus::Logger logger_;
    
private:        
    
    void connect();
    
    void getXhannelRequest();

};


class DBJobSelect : public DBJob
{
public:
    
    DBJobSelect(DatabaseConnection* db, 
            const std::string& tStoreView, 
            const std::string& queryName,
            const std::map<std::string, std::string>& parameters);
    
    virtual ~DBJobSelect();        
    
    xdata::Table* getResult();    
    
    
private:
    
    virtual void runVirtual();
    
    virtual void printJobVirtual();
    
    virtual std::string jobType();
    
    void setJobResult(xdata::Table* result);
    
    
    std::string queryName_;
    
    std::map<std::string, std::string> parameters_;
    
    xdata::Table* result_;
    
    
        
};


class DBJobInsert : public DBJob
{
public:  
    
    DBJobInsert(DatabaseConnection* db,
            const std::string& tableName,
            const std::string& tStoreView = "");
    
    /*
    DBJobInsert(DatabaseConnection* db, 
            const std::string& tableName, 
            const std::map<std::string, std::string>& data, 
            const std::string& tStoreView = "");    
    */
    
    virtual ~DBJobInsert();            
    
    
    template <typename T> void
    setData(const std::string& column, const T& data)
    {
        std::string valueType = data_->getColumnType(column);
        xdata::Serializable* valueInXdata = utilities::createSerializable(valueType, data);
        
        data_->setValueAt(0, column, *valueInXdata);  
	delete valueInXdata;              
    }


    /* template <typename T> void */
    /*   setData(size_t row, const std::string& column, const T& data) */
    /* { */
    /*     std::string valueType = data_->getColumnType(column); */
    /*     xdata::Serializable* valueInXdata = utilities::createSerializable(valueType, data); */
        
    /*     data_->setValueAt(row, column, *valueInXdata); */
    /* 	/\* std::cerr << row << '\t' << column << '\t' << valueInXdata->toString() *\/ */
    /* 	/\* 	  << '\t' << valueType << std::endl; *\/ */
    /* } */


    void
      setData(size_t row, const std::string& column, const std::string & data)
    {
        std::string valueType = data_->getColumnType(column);
        xdata::Serializable* valueInXdata = utilities::createSerializable(valueType, data);
        
        data_->setValueAt(row, column, *valueInXdata);
	delete valueInXdata;
    	/* std::cerr << row << '\t' << column << '\t' << valueInXdata->toString() */
    	/* 	  << '\t' << valueType << std::endl; */
    }

    void
      setData(size_t row, const std::string& column, xdata::Serializable * xdata)
    {
      std::string valueType = data_->getColumnType(column);
      std::string valueXType = xdata->type();
      
      /* std::cerr << "Request insert of column " << column << " with type " << valueXType */
      /* 		<< " while expected type " << valueType << std::endl; */
      if ( valueType != valueXType ) {
	std::ostringstream msg;
	msg << "Request insert of column " << column << " with type " << valueXType
	    << " while expected type " << valueType << std::endl;
	XCEPT_RAISE(xcept::Exception, msg.str());
      }
      data_->setValueAt(row, column, *xdata);
    };

    void setDataMap(const std::map<std::string, std::string>& dataMap);
    xdata::Table getTable();
    
    void setDataTable( xdata::Table& dataTable );
    
private:
    
    virtual void runVirtual();
    
    virtual void printJobVirtual();
    
    virtual std::string jobType();
    
    xdata::Table* getTableDefinition(const std::string& tableName);
    
    void mapToXdataTable(const std::map<std::string, std::string>& data);
    
    
    std::string tableName_;
    
    xdata::Table* data_;
    
    static const std::string defaultInsertView_;
 
};


class DBJobDelete : public DBJob
{
public:
    
    DBJobDelete(DatabaseConnection* db,
            const std::string& tableName,            
            xdata::Table* record,
            const std::string& tStoreView = "");
       
    
    virtual ~DBJobDelete();
        
    
private:
    
    virtual void runVirtual();
    
    virtual void printJobVirtual();
    
    virtual std::string jobType();
    
    xdata::Table* toXdataTable(const std::string& pkName, const std::string& pkValue); 
    
    
    std::string tableName_;       
    
    xdata::Table* record_;
    
    static const std::string defaultDeleteView_;         
    

};


} // end ns candela
#endif
