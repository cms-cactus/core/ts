/* 
 * File:   utilities.h
 * Author: Carlos Ghabrous Larrea
 *
 * Created on February 20, 2015, 5:42 PM
 */
#ifndef cactuscore_candela_UTILITIES_H
#define cactuscore_candela_UTILITIES_H

#include <string>
#include <sstream>

namespace xdata
{
    class Serializable;     
}

namespace tscandela
{
namespace  utilities
{
    
xdata::Serializable* createSerializable(const std::string& type, const std::string& value = "");


template <typename T>
xdata::Serializable* 
createSerializable(const std::string& type, const T& value)
{
    std::ostringstream oss; 
    oss << value; 
    xdata::Serializable* result = createSerializable(type, oss.str());
    return result;
}
        

}

}
#endif

