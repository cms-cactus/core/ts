#ifndef cactuscore_candela_TABLE_H
#define cactuscore_candela_TABLE_H

#include "ts/candela/DBJob.h"

#include "log4cplus/logger.h"

#include <string>
#include <vector>
#include <map>


namespace xdata
{
    class Table;     
}

namespace tscandela
{

  class DatabaseConnection;
  class QueryResult;
  


  class Table 
  {
    
  public:

    /**
     * @brief Another CTOR where the argument is schema + tablename
     */
    Table(
	  DatabaseConnection* dbConnection,
	  const std::string& fullyQualifiedTableName);
    
    /**
     * DTOR
     */
    ~Table(){}

    
    /**
     * @brief Gets the schema the Table belongs to
     */
    std::string getOwner() const;

  
    /**
     * @brief Gets the table's name
     */
    std::string getName() const;

  
    /**
     * @brief Returns schema + tablename
     */
    std::string getFullName() const;
  

    /**
     * @brief Returns the name of the column that is the table's primary key
     */
    std::string getPK() const;


    /**
     * @brief Gets metadata from a table: a list of string pairs that contain the column name and column data type
     * @return The list of string pairs containing column names and data types
     */
    std::map< std::string,std::string > getColumnsDefinition() const;
  

    /**
     * @return The fully qualified table names of all tables being referred by this Table object
     */
    // std::set< std::string > getDestinationTableNames() const;


    /**
	* @return The fully qualified table names of all tables referring to this Table object.
	*/
    // std::set< std::string > getSourceTableNames() const;


    /**
     * @return Returns the PK column name of the table being referred by this Table object
     */
    //std::string getDestinationColumnName( const std::string& desttabname ) const;
   

    /**
	 * @return QueryResult object mapping the SQL Query select * from Table
	 */
	const QueryResult selectAll(const std::string& maxRows = "100");


    /**
	 * @brief Returns a list with the different table's primary keys
	 * @param filter A string as used with SQL LIKE operator that allows primary key filtering (ex %mykey%)
	 */
	const QueryResult selectPrimaryKeys(const std::string& filter = "");


	/**
     * @param fkColumn Column pointing to another table "T" via a FK-PK relationship
     * @param fkColumnValue Value of column fkColumn, which will be the PK value in table "T"
     * @return QueryResult object that will get the record of the table pointed by fkColumn
     */
	const QueryResult selectFKRow( const std::string& fkColumn,
    				   const std::string& fkColumnValue
    				 ) const;


    /**
	 * @brief Selects a table record (row) by a primary key value
	 * @returns A QueryResult object with the query's result
	 */
	const QueryResult selectRowByPK(const std::string& pkValue) const;


	/**
	 * @brief performs a select query using a TStore view file
	 * @param queryname Name of the query to perform
	 * @param tStoreView TStore view file where queries are stored
	 * @param parameters Map to do parameter binding
	 */
	const QueryResult select(const std::string& queryName,
			      const std::string& tStoreView,
			      std::map<std::string,std::string>& parameters
			      );    

    // void insert(const std::map<std::string, std::string>& data);

    void update(const std::string& pkValue, const std::map<std::string, std::string>& data);

    void deleteRow(const std::string& pkValue);
      
    class Inserter
    {
        
    public:
        
        Inserter(Table* theTable);
        
        ~Inserter();
        
        Inserter& set(const std::map<std::string, std::string>& dataMap);               
        
        template <typename T>
        Inserter& operator()(const std::string& column, const T& data)
        {                
            insertJob_->setData(column, data);           
            return *this; 
        }

	Inserter& setData(size_t row, const std::string& column, const std::string & data)
        {                
	  insertJob_->setData(row, column, data);           
          return *this; 
        }
        
        Inserter& setData(size_t row, const std::string& column, xdata::Serializable * data)
        {                
	  insertJob_->setData(row, column, data);           
          return *this; 
        }
        
        void insert();
        
    private:
        
        void cleanJob();
        
        Table* theTable_;  
        
        DBJobInsert* insertJob_;        
        
    };

    
  public:
    
    Inserter getInserter();


 private:
     
    DatabaseConnection* dbConnection_;
    
    std::string fullyQualifiedTableName_;
    
    std::string owner_;
    
    std::string tableName_;
    
    mutable std::string pkName_;
    
    log4cplus::Logger logger_;
    // std::vector< std::string > sourceTableNames_;
    // std::vector< std::string > destinationTableNames_;
    
    
    
  };

}
#endif
