#ifndef cactuscore_candela_VERSION_H
#define cactuscore_candela_VERSION_H

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!
#define TS_TSCANDELA_VERSION_MAJOR 5
#define TS_TSCANDELA_VERSION_MINOR 2
#define TS_TSCANDELA_VERSION_PATCH 4


#define TS_TSCANDELA_VERSION_CODE PACKAGE_VERSION_CODE(TS_TSCANDELA_VERSION_MAJOR,TS_TSCANDELA_VERSION_MINOR,TS_TSCANDELA_VERSION_PATCH)

#ifndef TS_TSCANDELA_PREVIOUS_VERSIONS
#define TS_TSCANDELA_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TS_TSCANDELA_VERSION_MAJOR,TS_TSCANDELA_VERSION_MINOR,TS_TSCANDELA_VERSION_PATCH)
#else
#define TS_TSCANDELA_FULL_VERSION_LIST  TS_TSCANDELA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TS_TSCANDELA_VERSION_MAJOR,TS_TSCANDELA_VERSION_MINOR,TS_TSCANDELA_VERSION_PATCH)
#endif

namespace tscandela
{
	const std::string project = "ts";
	const std::string package  = "tscandela";
	const std::string versions = TS_TSCANDELA_FULL_VERSION_LIST;
	const std::string description = "CANDELA: ConfigurAtioN Database accEss LibrAry";
	const std::string authors = "Carlos Ghabrous Larrea";
	const std::string summary = "ConfigurAtioN Database accEss LibrAry";
	const std::string link = "https://svnweb.cern.ch/trac/cactus";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
