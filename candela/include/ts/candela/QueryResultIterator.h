#ifndef cactusprojects_candela_QUERYITERATOR_H
#define cactusprojects_candela_QUERYITERATOR_H

#include <iterator>


namespace tscandela
{

class QueryResult;
class Row;

class QueryResultIterator : public std::iterator<std::input_iterator_tag, QueryResult>
{

    friend class QueryResult;
    friend class Row;

public:

    QueryResultIterator();

    QueryResultIterator(const QueryResult* result);

    ~QueryResultIterator();

    bool operator == (const QueryResultIterator& it) const;

    bool operator != (const QueryResultIterator& it) const;

    QueryResultIterator& operator ++ ();

    QueryResultIterator& operator -- ();

    QueryResultIterator& operator = (const QueryResultIterator& it);

    Row& operator * ();

    Row* operator -> ();

private:

    const QueryResult* result_;
    
    Row* row_;
    
    unsigned long position_;
    
    bool endReached_;
};

} // end of namespace candela
#endif
