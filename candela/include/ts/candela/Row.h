#ifndef cactuscore_candela_ROW_H
#define cactuscore_candela_ROW_H

#include "ts/candela/QueryResult.h"
#include "ts/candela/QueryResultIterator.h"

#include "xdata/Table.h"
#include "xcept/Exception.h"

#include <boost/lexical_cast.hpp>

#include <string>
#include <iostream>


namespace tscandela
{

class Row
{

public:

    /**
     * CTOR
     */
    Row(QueryResultIterator& qIt);

    Row(const Row& row);

    Row& operator =(const Row& row);

    /**
     * DTOR
     */
    ~Row();

    /**
     * @brief Template method that returns the value of one column performing a boost::lexical_cast
     * @param m_rowNum The row number in the xdata::Table we are at
     * @param columnName The name of the column we want to take the value from
     * @return The value stored in row m_rowNum and column columnName in the type T
     */
    template<class T>
    T get(const std::string& columnName)
    {
        std::ostringstream exceptionMsg;

        T result;
        try
        {
            xdata::Table* t = qIt_.result_->getQueryResult();
            result = boost::lexical_cast<T>(t->getValueAt(qIt_.position_, columnName)->toString());
        }
        catch (boost::bad_lexical_cast& blc)
        {
            exceptionMsg << "Bad lexical cast exception while getting column " << columnName
                         << " from query!" << blc.what() << std::endl;
            XCEPT_RAISE(xcept::Exception, exceptionMsg.str());
        }
        catch(xcept::Exception& e)
        {
            exceptionMsg << "Exception while getting column " << columnName << " from query!" << e.message()
                         << std::endl;
            XCEPT_RAISE(xcept::Exception, exceptionMsg.str());
        }

        return result;
    }

private:

    QueryResultIterator& qIt_;
};

} // end namespace candela
#endif
