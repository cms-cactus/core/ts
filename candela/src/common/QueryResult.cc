#include "ts/candela/QueryResult.h"

#include "ts/candela/DatabaseConnection.h"
#include "ts/candela/QueryResultIterator.h"
#include "ts/candela/DBJob.h"

#include "xdata/Table.h"
#include "xdata/TableIterator.h"

#include "xcept/Exception.h"


using namespace std;

namespace tscandela
{

QueryResult::QueryResult(
    DatabaseConnection* db,		
    map<string, string>& parameters,
    const string& queryName,
    const string& tStoreView
):
db_(db),
parameters_(parameters),
queryName_(queryName),
tStoreView_( (tStoreView == "") ? DatabaseConnection::defaultTStoreView_ : tStoreView),
logger_( log4cplus::Logger::getInstance(db->getLogger().getName() + "candela::QueryResult") ),
resultTable_(execute())
{     
    
}



QueryResult::~QueryResult()
{
}


size_t
QueryResult::size() const
{
    if (!resultTable_)
    {
            throwQueryResultMissingException();
    }

    return resultTable_->getRowCount();

}


vector<string>
QueryResult::getColumns() const
{
    if (!resultTable_)
    {
            throwQueryResultMissingException();
    }

    return resultTable_->getColumns();
}


string
QueryResult::getName() const
{
    return queryName_;
}


QueryResult::const_iterator
QueryResult::begin()
{
    return QueryResultIterator(this);
}


QueryResult::const_iterator
QueryResult::end()
{
    QueryResultIterator cIt(this);
    cIt.endReached_ = true;
    cIt.position_ = 0;

    return cIt;
}




xdata::Table*
QueryResult::execute()
{      
    DBJobSelect selectJob(db_, tStoreView_, queryName_, parameters_);
    selectJob.run(); 
    
    xdata::Table* result = selectJob.getResult();
    
    return result;
}



string
QueryResult::getColumnType(const string& columnName)
{
    string columnType("");

    if (!resultTable_)
    {
            throwQueryResultMissingException();
    }

    columnType = resultTable_->getColumnType(columnName);

    return columnType;
}


void
QueryResult::printQueryResultDetails() const
{

    ostringstream queryResult;

    vector<string> columns = resultTable_->getColumns();

    int count = 0;
    for (xdata::TableIterator tIt = resultTable_->begin(); tIt != resultTable_->end(); ++tIt)
    {
        queryResult << "Printing row #" << count << endl;

        for (vector<string>::const_iterator cIt = columns.begin(); cIt != columns.end(); ++cIt)
        {
            queryResult << "COLUMN NAME: " << *cIt << "; VALUE: " << tIt->getField(*cIt)->toString() << endl;
        }
        count++;
    }

    LOG4CPLUS_DEBUG(logger_, queryResult.str());
}


xdata::Table*
QueryResult::getQueryResult() const
{
    return resultTable_;
}



void
QueryResult::throwQueryResultMissingException() const
{
    ostringstream oss;
    oss << "QueryResult result from query " << queryName_ << " is missing!" << endl;
    XCEPT_RAISE(xcept::Exception, oss.str());
}


} // end ns candela
