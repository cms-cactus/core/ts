#include "ts/candela/Table.h"

#include "ts/candela/DatabaseConnection.h"
#include "ts/candela/QueryResult.h"
#include "ts/candela/DBJob.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

using namespace std;


namespace tscandela
{

Table::Table(DatabaseConnection* dbConnection, const string& fullyQualifiedTableName)
:
    dbConnection_(dbConnection),
    fullyQualifiedTableName_(fullyQualifiedTableName)
{
    
    logger_ = log4cplus::Logger::getInstance(dbConnection->getLogger().getName() + ".candela::Table");
    size_t pos = fullyQualifiedTableName.find('.');

    if(pos != string::npos)
    {
        owner_ 		= fullyQualifiedTableName.substr(0,pos);
        tableName_	= fullyQualifiedTableName.substr(pos+1);
    }

}


string
Table::getOwner() const
{
    return owner_;
}



string
Table::getName() const
{
    return tableName_;
}



string
Table::getFullName() const
{
    return owner_ + "." + tableName_;
}



string
Table::getPK() const
{

    if(!pkName_.empty())
    {
        return pkName_;
    }

    pkName_ = dbConnection_->getPK(owner_, tableName_);
    return pkName_;
}
 


const QueryResult
Table::selectPrimaryKeys(const string& filter)
{

  if(pkName_.empty())
  {
      pkName_ = dbConnection_->getPK(owner_, tableName_);
  }

  string queryName 		= "selectPrimaryKeys";

  map<string, string> queryParams;
  queryParams.insert(make_pair("columnName", pkName_));
  queryParams.insert(make_pair("fullTableName", owner_ + "." + tableName_));
  queryParams.insert(make_pair("filter", filter));
  
  QueryResult q(dbConnection_,		  
		  queryParams,
		  queryName);
  
  return q;
}



map<string, string>
Table::getColumnsDefinition() const
{
    map<string, string> mapColumnNameToXDAQType = dbConnection_->getTableDefinition(fullyQualifiedTableName_);
    return mapColumnNameToXDAQType;
}


const QueryResult
Table::selectFKRow(
		const string& fkColumn,
		const string& fkColumnValue
		) const
{
  	Table tempTable = dbConnection_->getReferencedTable(this->fullyQualifiedTableName_, fkColumn);
  	QueryResult q = tempTable.selectRowByPK(fkColumnValue);
  	return q;
}



const QueryResult
Table::selectAll(const string& maxRows)
{
  	string queryName 		= "selectAll";
  	map<string, string> queryParams;
  	queryParams.insert(make_pair("fullTableName", this->fullyQualifiedTableName_));
  	queryParams.insert(make_pair("maxRows", maxRows));

  	QueryResult q(dbConnection_,  			
  		   	queryParams,
  		   	queryName);

    return q;
}



const QueryResult
Table::select(const string& queryName, const string& tStoreViewName, map<string,string>& queryParams)
{
    QueryResult q(dbConnection_,    	  
    	  queryParams,
    	  queryName, 
          tStoreViewName);

    return q;
}



const QueryResult
Table::selectRowByPK(const string& pkValue) const
{
    string queryName("selectRowByPK");

    map<string,string> queryParams;
    queryParams.insert(make_pair("fullTableName", this->fullyQualifiedTableName_));
    queryParams.insert(make_pair("PKColumnName",  this->getPK()));
    queryParams.insert(make_pair("PKValue", 	   pkValue));


    QueryResult q(dbConnection_,    	  
    	  queryParams,
    	  queryName);

    return q;
}


void 
Table::update(const string& pkValue, const map<string, string>& data)
{
}


void
Table::deleteRow(const string& pkValue)
{
    QueryResult q = this->selectRowByPK(pkValue);
    xdata::Table* qResult = q.getQueryResult();
    
    DBJobDelete jobDelete(dbConnection_, this->getName(), qResult );   
    
    jobDelete.run(); 
}


Table::Inserter
Table::getInserter()
{
    return Table::Inserter(this);
}


Table::Inserter::Inserter(Table* theTable)
:
theTable_(theTable),
insertJob_(0)
{
    insertJob_ = new DBJobInsert( theTable_->dbConnection_, theTable_->tableName_);        
}


Table::Inserter::~Inserter()
{    
    if (insertJob_)
    {
        delete insertJob_;
    }
    
    insertJob_ = 0;
    
}


Table::Inserter&
Table::Inserter::set(const map<string, string>& dataMap)
{        
    insertJob_->setDataMap(dataMap);
    
    return *this;  
}


void 
Table::Inserter::insert()
{                
    insertJob_->run();   
        
    cleanJob();
}

void 
Table::Inserter::cleanJob()
{
    if (insertJob_)
    {
        delete insertJob_;
        insertJob_ = 0;
    }
}

} // end ns candela
