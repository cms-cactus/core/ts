#include "ts/candela/utilities.h"

#include "xdata/xdata.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"

    
using namespace std; 

namespace tscandela
{
    

namespace utilities
{
 
xdata::Serializable*
createSerializable(const string& type, const string& value)
{
    xdata::Serializable* result = 0;
    
    if (type == xdata::String().type())
    {
        result = new xdata::String;
    }
    else if (type == xdata::Integer().type())
    {
        result = new xdata::Integer;
    }
    else if (type == xdata::Integer32().type())
    {
        result = new xdata::Integer32;
    }
    else if (type == xdata::Integer64().type())
    {
        result = new xdata::Integer64;
    }
    else if (type == xdata::Float().type())
    {
        result = new xdata::Float;
    }
    else if (type == xdata::Double().type())
    {
        result = new xdata::Double;
    }
    else if (type == xdata::Boolean().type())
    {
        result = new xdata::Boolean;
    }
    else if (type == xdata::TimeVal().type())
    {
        result = new xdata::TimeVal;
    }
    else
    {
        XCEPT_RAISE(xcept::Exception, ("No matching xdata type for type " + type));
    }
    // add here more types...
    
    
    if ( ! value.empty() && value != "nan" && value != "inf")
    {
        result->fromString(value);
    }
    
    return result; 
}

} // end ns utilities
} // end ns candela

