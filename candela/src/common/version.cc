#include "config/version.h"

#include "ts/candela/version.h"


GETPACKAGEINFO(tscandela)

namespace tscandela
{
void checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
}

std::set<std::string, std::less<std::string> > getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY(dependencies,config);
	return dependencies;
}

} // end ns tscandela
