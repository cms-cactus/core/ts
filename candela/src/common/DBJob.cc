#include "ts/candela/DBJob.h"

#include "ts/candela/DatabaseConnection.h"
#include "ts/candela/Table.h"

#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequest.h"
#include "ts/framework/CellXhannelRequestTB.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "xcept/tools.h"

#include <sstream>

#include "xdata/TableIterator.h"

using namespace std; 

namespace tscandela
{
	
DBJob::DBJob(DatabaseConnection* db, const string& tStoreView)
:
dbXhannel_(db->getDBXhannel()),
user_(db->getUser()), 
password_(db->getPassword()),
tStoreView_(tStoreView),
request_(0),
reqDB_(0),
db_(db)
{
    logger_ = log4cplus::Logger::getInstance(db->getLogger().getName() + ".candela::DBJob");
}


DBJob::~DBJob()
{        
    
    if (request_)
    {
        dbXhannel_->removeRequest(request_);
    }

    if (dbXhannel_->isConnected())
    {
        try
        {
            dbXhannel_->disconnect();
        }
        catch(xcept::Exception& e)
        {
            ostringstream  ex; 
            ex << "Exception in xhannelTB disconnect: " <<  xcept::stdformat_exception_history(e) << endl;
            LOG4CPLUS_WARN(logger_, ex.str());
        }       
    }
    
}


void
DBJob::run()
{
    this->connect(); 
    this->getXhannelRequest(); 
    this->runVirtual();   
    this->printJobVirtual();
}



void
DBJob::connect()
{
    if (!dbXhannel_)
    {
        XCEPT_RAISE(xcept::Exception, "Could not get the DB Xhannel from CellContext");
    }

    dbXhannel_->connect(tStoreView_, user_, password_);
}


void
DBJob::getXhannelRequest()
{
    if (!request_)
    {
        request_ = dbXhannel_->createRequest();
    }

    if (!reqDB_)
    {
        reqDB_ = dynamic_cast<tsframework::CellXhannelRequestTB*>(request_);
    }

    if (!reqDB_)
    {
        ostringstream msg;
        msg << "Wrong xhannel request type in DBJob! " << endl;
        XCEPT_RAISE(xcept::Exception, msg.str());
    }    
}


// DBJobSelect

DBJobSelect::DBJobSelect(DatabaseConnection* db, 
        const string& tStoreView, 
        const string& queryName, 
        const map<string, string>& parameters
)
:
DBJob::DBJob(db, tStoreView),
queryName_(queryName),
parameters_(parameters),
result_(0)
{
   
}


DBJobSelect::~DBJobSelect()
{   
    if (result_ != 0)
        delete result_;
}


void
DBJobSelect::runVirtual()
{       
    reqDB_->doGetQuery(queryName_, parameters_);
    
    dbXhannel_->send(reqDB_);
    
    xdata::Table* tblResult = reqDB_->getQueryReply();
    
    setJobResult(tblResult); 
    
    
      
}


xdata::Table*
DBJobSelect::getResult()
{
    xdata::Table* result = result_;
    result_ = 0;
    
    return result;
}


void 
DBJobSelect::setJobResult(xdata::Table* result)
{
    
    if (result_)
    {
        delete result_;
    }
    
    result_ = result;
    
}


void
DBJobSelect::printJobVirtual()
{
    ostringstream msg; 
    msg << "Job type ===> " << this->jobType() << endl;
    msg << "User     ===> " << this->user_ << endl;
    msg << "View     ===> " << this->tStoreView_ << endl;
    msg << "Query    ===> " << this->queryName_ << endl;
    msg << "Params   ===> " << endl;
    
    for (map<string, string>::const_iterator cIt = parameters_.begin();
            cIt != parameters_.end();
            ++cIt)
    {
        msg << "\t" << "Name: " << cIt->first << " --- Value: " << cIt->second << endl;
    }
        
    msg << "Result   ===> " << endl;
    
    vector<string> columns = result_->getColumns();
    for (vector<string>::const_iterator cIt = columns.begin(); cIt != columns.end(); ++cIt)
    {
        msg << "\t" << "Column ===> " << *cIt;
        
        // Because certain queries may not contain results rows (getTableDefinition, for instance)
        if (result_->getRowCount())
        {
            msg << " --- Value ===> " << result_->getValueAt(0, *cIt)->toString();
        }
        msg << endl;
    }
    
    LOG4CPLUS_DEBUG(logger_, msg.str());

}

string
DBJobSelect::jobType()
{
    return "DBJobSelect";
}

        // DBJobInsert


const string DBJobInsert::defaultInsertView_ = "urn:tstore-view-Nested:candela"; 


DBJobInsert::DBJobInsert(DatabaseConnection* db,
        const string& tableName,
        const string& tStoreView)
:
        DBJob(db, ( tStoreView.empty() ? defaultInsertView_ : tStoreView) ),
        tableName_(tableName),
        data_(getTableDefinition(tableName))
{
    /*
    ostringstream msg;
    msg << "XXX New DBJobInsert for table " << tableName_;
    msg << "XXX data table columns: " << endl;
    
    vector<string> cols = data_->getColumns();
    for (vector<string>::const_iterator cIt = cols.begin(); cIt != cols.end(); ++cIt)
    {
        msg << " column " << *cIt << "; type: " << data_->getColumnType(*cIt) << endl;
    }
    LOG4CPLUS_DEBUG(logger_, msg.str() );
    */
}


DBJobInsert::~DBJobInsert()
{
    if (data_)
    {
        delete data_;
    }
    
    data_ = 0; 
}


void
DBJobInsert::runVirtual()
{    
    /*
    ostringstream oss; 
    oss << "XXX Running insertjob with a ";
    if (data_)
    {
        oss << "non "; 
    }
    oss << "empty table: " << endl;
    
    vector <string> cols = data_->getColumns();
    for (vector<string>::const_iterator cIt = cols.begin(); cIt != cols.end(); ++cIt)
    {
        oss << "XXX Column " << *cIt << "; type: " << data_->getColumnType(*cIt) << " value : " << 
                data_->getValueAt(0, *cIt)->toString() << endl;
    }
    LOG4CPLUS_DEBUG(logger_, oss.str() );
    oss.flush();
    */

  if ( ! data_->getRowCount() )
    {
        XCEPT_RAISE(xcept::Exception, "Request insert of empty data table in DBJob!\n" );
    }    
    
    reqDB_->doInsertForTable(tableName_, data_);    
    
    dbXhannel_->send(reqDB_);    
    /*
    oss << "XXX request sent" << endl;    
    LOG4CPLUS_DEBUG(logger_, oss.str() );
    oss.flush();
    */
    
    
    
}


void 
DBJobInsert::setDataMap(const map<string, string>& dataMap)
{
    mapToXdataTable(dataMap);        
}


xdata::Table*
DBJobInsert::getTableDefinition(const string& tableName)
{
    xdata::Table* definitionTable = db_->getTableDefinitionInXdata(tableName_);
    return definitionTable; 
}


xdata::Table
DBJobInsert::getTable()
{
    return *data_; 
}

void
DBJobInsert::setDataTable( xdata::Table& dataTable )
{
  std::vector<std::string> userColumns = dataTable.getColumns();
  std::vector<std::string> dbColumns = data_->getColumns();

  if ( userColumns.size() != dbColumns.size() ) {
    XCEPT_RAISE(xcept::Exception, "xdata::Table has a different number of columns with respect to the database table");
  }

  std::sort ( userColumns.begin(), userColumns.end() );
  std::sort ( dbColumns.begin(), dbColumns.end() ); 
  for ( size_t i = 0; i < userColumns.size(); ++i ) {
    if ( userColumns.at(i) == dbColumns.at(i) )
	XCEPT_RAISE(xcept::Exception, "Columns mismatch among xdata::Table and database table");
  }

  xdata::TableIterator lastRow( dataTable.end() );
  std::vector<std::string>::const_iterator lastColumn(dbColumns.end());

  size_t rowcount = 0;
  for ( xdata::TableIterator row( dataTable.begin() ); row != lastRow; ++row, ++rowcount ) {
    for ( std::vector<std::string>::const_iterator col(dbColumns.begin());
	    col != lastColumn; ++col ) {
      data_->setValueAt( rowcount, *col , *(row->getField( *col )) );
    }
  }
 
}




void
DBJobInsert::mapToXdataTable(const map<string, string>& data)
{         
    ostringstream msg; 
  
    for (map<string, string>::const_iterator cIt = data.begin();
            cIt != data.end();
            ++cIt)
    {   
        string valueXdataType = data_->getColumnType(cIt->first); 
        xdata::Serializable* valueInXdata = utilities::createSerializable(valueXdataType, cIt->second);
        data_->setValueAt( 0, cIt->first, *valueInXdata );
	delete valueInXdata;
        // msg << "XXX Inserted in data_ column " << cIt->first << " of type " << valueXdataType << " with value " << valueInXdata->toString() << endl;        
    }
    
    /*
    msg << "XXX table at the end of mapToXdataTable " << endl;
    vector<string> columns = data_->getColumns();
    for (vector<string>::const_iterator cIt = columns.begin(); cIt != columns.end(); ++cIt)
    {
        msg << "Column " << *cIt << "; value " << data_->getValueAt(0, *cIt) << endl;
    }
    
    LOG4CPLUS_DEBUG(logger_, msg.str() );
     */
        
}


string
DBJobInsert::jobType()
{
    return "DBJobInsert";
}



void
DBJobInsert::printJobVirtual()
{
    ostringstream msg; 
    msg << "Job type ===> " << this->jobType() << endl;
    msg << "User     ===> " << this->user_ << endl;
    msg << "View     ===> " << this->tStoreView_ << endl;   
    msg << "Data     ===> " << endl;
    
    vector<string> columns = data_->getColumns();
    for (vector<string>::const_iterator cIt = columns.begin();
            cIt != columns.end();
            ++cIt)
    {
        msg << "\t" << "Column: " << *cIt << " --- Value: " << data_->getValueAt(0, *cIt)->toString() << endl;
    }
       
    
    LOG4CPLUS_DEBUG(logger_, msg.str());
    
}


        // DBJobDelete

const string DBJobDelete::defaultDeleteView_ = "urn:tstore-view-Nested:candela"; 

DBJobDelete::DBJobDelete(DatabaseConnection* db,
        const string& tableName,       
        xdata::Table* record,
        const string& tStoreView)
:
        DBJob(db, ( tStoreView.empty() ? defaultDeleteView_ : tStoreView) ),
        tableName_(tableName),
        record_(record)
{
   
}


DBJobDelete::~DBJobDelete()
{
}


void
DBJobDelete::runVirtual()
{           
    reqDB_->doDeleteForTable(tableName_, record_);
    
    dbXhannel_->send(reqDB_);     
    
    
}


string
DBJobDelete::jobType()
{
    return "DBJobDelete";
}

void 
DBJobDelete::printJobVirtual()
{ 
    ostringstream msg; 
    msg << "Job type ===> " << this->jobType() << endl;
    msg << "User     ===> " << this->user_ << endl;
    msg << "View     ===> " << this->tStoreView_ << endl;
    msg << "Delete   ===> " << endl;
    
    vector<string> columns = record_->getColumns();
    for (vector<string>::const_iterator cIt = columns.begin(); cIt != columns.end(); ++cIt)
    {
        msg << "\t Column  ===> " << *cIt << " --- Value   ===> " << record_->getValueAt(0, *cIt)->toString() << endl;
    }  
    
    LOG4CPLUS_DEBUG(logger_, msg.str());
}
} // end ns candela
