#include "ts/candela/Row.h"

using namespace std;

namespace tscandela
{

Row::Row(QueryResultIterator& qIt)
:
    qIt_(qIt)
{
}


Row::Row(const Row& row)
:
    qIt_(row.qIt_)
{
    this->operator=(row);
}


Row&
Row::operator =(const Row& row)
{

    if (this != &row)
    {
        qIt_ = row.qIt_;
    }
    return *this;
}


Row::~Row()
{

}

} // end ns candela
