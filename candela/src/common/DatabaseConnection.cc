#include "ts/candela/DatabaseConnection.h"

#include "ts/candela/Table.h"
#include "ts/candela/QueryResult.h"
#include "ts/candela/QueryResultIterator.h"
#include "ts/candela/Row.h"


#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelTB.h"
#include "ts/toolbox/CellToolbox.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "xcept/Exception.h"


using namespace std;


// STATIC API
const string 
tscandela::DatabaseConnection::defaultTStoreView_ = "urn:tstore-view-SQL:candela-main";

namespace tscandela
{

string
DatabaseConnection::getUserFromFullyQualifiedName(const string& fullyQualifiedTableName)
{
    size_t position = fullyQualifiedTableName.find_last_of(".");
    return fullyQualifiedTableName.substr(0, position);
}


string
DatabaseConnection::getTableFromFullyQualifiedName(const string& fullyQualifiedName)
{
    size_t position = fullyQualifiedName.find_last_of(".");
    return fullyQualifiedName.substr(position+1);
}


string
DatabaseConnection::getPassword(const string& pwdPath, const string& user)
{
    string pwdFileRoute("");
    if (pwdPath.find("PWD_PATH") != string::npos)
    {
        pwdFileRoute = tstoolbox::getEnvironment("PWD_PATH");
    } else {
      pwdFileRoute = pwdPath;
    }

    string file("");

    for (unsigned int i = 0; i<user.length(); i++)
    {
        file += char(tolower(user[i]));
    }

    file += ".txt";

    return tstoolbox::getPassword(pwdFileRoute, file);
}

} // end ns tscandela


// PUBLIC API

namespace tscandela
{
DatabaseConnection::DatabaseConnection(log4cplus::Logger& log,
                                        tsframework::CellAbstractContext* context,
                                        const string& user,
                                        const string& pwdPath)
:
context_(context),
user_(user),
pwdPath_(pwdPath),
tStoreView_(DatabaseConnection::defaultTStoreView_)
{
    logger_ = log4cplus::Logger::getInstance(log.getName() + ".DatabaseConnection");    
    password_ = DatabaseConnection::getPassword(pwdPath_, user_);
}



DatabaseConnection::~DatabaseConnection()
{

    for (map<string, Table*>::iterator it = mapTableNameToTable_.begin();
                    it != mapTableNameToTable_.end();
                    ++it)
    {
        delete it->second;
    }

    mapTableNameToTable_.clear();
}



Table
DatabaseConnection::getTable(const string& fullyQualifiedTableName)
{
    ostringstream oss;    

    bool isInCache;

    isInCache = mapTableNameToTable_.find(fullyQualifiedTableName) != mapTableNameToTable_.end();

    // Return copy if cached
    if (isInCache)
    {
        oss << "Found table " << fullyQualifiedTableName << " in cache." << endl;
        LOG4CPLUS_DEBUG(getLogger(), oss.str());
        return Table (*mapTableNameToTable_[fullyQualifiedTableName]);
    }

    string queryName = "checkTableExists";
    map<string, string> queryParams;
    queryParams["fullTableName"] = fullyQualifiedTableName;

    QueryResult q(this, queryParams, queryName);
    bool tableExists = q.begin()->get<bool>("TABLE_EXISTS");

    if (!tableExists)
    {
        oss << "Table " << fullyQualifiedTableName << " doesn't exist!" << endl;
        LOG4CPLUS_WARN(getLogger(), oss.str());
        XCEPT_RAISE(xcept::Exception, oss.str());
    }    

    Table* newTable = new Table(this, fullyQualifiedTableName);
    mapTableNameToTable_.insert( make_pair(fullyQualifiedTableName, newTable) );

    return Table( *mapTableNameToTable_[fullyQualifiedTableName] );
}



Table
DatabaseConnection::getReferencedTable(const string& fullyQualifiedTableName, const string& fkColumn)
{
    ostringstream oss;

    bool isTableInCache;
    isTableInCache = ( mapTableNameToMapFKNameToRefTable_.find(fullyQualifiedTableName)
                                            != mapTableNameToMapFKNameToRefTable_.end() );

    if (isTableInCache)
    {
        bool isFKInCache;
        isFKInCache = ( mapTableNameToMapFKNameToRefTable_[fullyQualifiedTableName].find(fkColumn)
                                        != mapTableNameToMapFKNameToRefTable_[fullyQualifiedTableName].end() );

        // return copy if cached
        if (isFKInCache)
        {
            string tableName = mapTableNameToMapFKNameToRefTable_[fullyQualifiedTableName][fkColumn];
            return getTable(tableName);
        }
    }


    string queryName("getReferencedTable");
    map<string, string> queryParams;
    queryParams["owner"] 		= DatabaseConnection::getUserFromFullyQualifiedName(fullyQualifiedTableName);
    queryParams["tableName"] 	= DatabaseConnection::getTableFromFullyQualifiedName(fullyQualifiedTableName);
    queryParams["fkColumn"] 	= fkColumn;

    QueryResult q(this, queryParams, queryName);
    size_t results = q.size();

    if (results != 1)
    {
        oss << "Wrong number of results (" << results << ") in query " << queryName << ". Expected just one result!" << endl;
        XCEPT_RAISE(xcept::Exception, oss.str());
    }

    QueryResult::const_iterator qCIt = q.begin();
    string refTableOwner 	= qCIt->get<string>("REFERENCED_OWNER");
    string refTable 		= qCIt->get<string>("REFERENCED_TABLE");
    string refConstName 	= qCIt->get<string>("REF_CONST_NAME");

    mapTableNameToMapFKNameToRefTable_[fullyQualifiedTableName][fkColumn] 	= refTableOwner + "." + refTable;

    string refColumn = getReferencedColumn(refTableOwner, refConstName);
    mapTableNameToMapFKNameToRefColumn_[fullyQualifiedTableName][fkColumn] 	= refColumn;

    return getTable(refTableOwner + "." + refTable);
}



vector<string>
DatabaseConnection::getTableNames(const string& user)
{
    vector<string> result = mapUserNameToVectorTableName_[user];

    if (result.empty())
    {
        fillAllTableNames(user);
    }

    return mapUserNameToVectorTableName_[user];
}




// 		PRIVATE API


string
DatabaseConnection::getReferencedColumn(const string& owner, const string& constraint)
{
    ostringstream oss;

    string queryName("getReferencedColumn");
    map<string, string> queryParams;
    queryParams.insert( make_pair("owner", owner) );
    queryParams.insert( make_pair("constraint", constraint) );

    QueryResult q(this, queryParams, queryName);
    size_t results = q.size();

    if (results != 1)
    {
        oss << "Wrong number of results (" << results << ") in query " << queryName << ". Expected just one result!" << endl;
        XCEPT_RAISE(xcept::Exception, oss.str());
    }

    return q.begin()->get<string>("REF_COLUMN");
}


string
DatabaseConnection::getPK(const string& owner, const string& tableName)
{
    ostringstream oss;

    bool isInCache;
    isInCache = mapTableNameToPKName_.find(tableName) != mapTableNameToPKName_.end();

    if (isInCache)
    {
        return mapTableNameToPKName_[tableName];
    }

    string queryName ="fillTableToPK";
    map<string,string> queryParams;
    queryParams.insert(make_pair("tableName", tableName));
    queryParams.insert(make_pair("owner", owner));

    QueryResult q(this, queryParams, queryName);

    string pk("");
    size_t rowCount = q.size();
    if(rowCount != 1)
    {
        pk = "ROWID";
        oss << "No primary key for table " << tableName << ". Will rely on ROWID" << endl;
        LOG4CPLUS_DEBUG(getLogger(), oss.str());
    }
    else
    {
        pk = q.begin()->get<string>("PK_COLUMN_NAME");
    }
    
    mapTableNameToPKName_.insert(make_pair(tableName, pk));

    return pk;
}



map<string, string>
DatabaseConnection::getTableDefinition(const string& fullyQualifiedTableName)
{
    ostringstream oss; 

    bool isInCache;
    isInCache = mapTableNameToMapColumnNameToType_.find(fullyQualifiedTableName) != mapTableNameToMapColumnNameToType_.end();

    if (isInCache)
    {
        return mapTableNameToMapColumnNameToType_[fullyQualifiedTableName];
    }   
    
    string queryName("getTableDefinition");
    map<string,string> queryParams;
    queryParams.insert(make_pair("TableName", fullyQualifiedTableName));

    QueryResult q(this, queryParams, queryName);

    vector<string> columns = q.getColumns();
    map<string, string> result;
    
    for (vector<string>::const_iterator cIt = columns.begin();
                    cIt != columns.end();
                    ++cIt)
    {
        string xdataType = q.getColumnType(*cIt);       
        result.insert(make_pair(*cIt, xdataType));
    }
   
    mapTableNameToMapColumnNameToType_.insert(make_pair(fullyQualifiedTableName, result));
    return result;
}


xdata::Table*
DatabaseConnection::getTableDefinitionInXdata(const string& fullyQualifiedTableName)
{       
    string queryName("getTableDefinition");
    map<string,string> queryParams;
    queryParams.insert(make_pair("TableName",fullyQualifiedTableName));

    QueryResult q(this, queryParams, queryName);
    
    return q.getQueryResult();
}


void
DatabaseConnection::fillAllTableNames(const string& user)
{

    string queryName("fillAllTableNames");
    map<string,string> queryParams;
    queryParams.insert(make_pair("owner", user));

    QueryResult q(this, queryParams, queryName);
    for (QueryResult::const_iterator qCIt = q.begin();
                    qCIt != q.end();
                    ++qCIt)
    {
        string fullTableName = qCIt->get<string>("TABLE_NAME");
        mapUserNameToVectorTableName_[user].push_back(fullTableName);
    }

}


tsframework::CellXhannelTB*
DatabaseConnection::getDBXhannel()
{
    tsframework::CellXhannelTB* xhannelTB =
        dynamic_cast<tsframework::CellXhannelTB*>(getContext()->getXhannel("DB"));

    return xhannelTB;
}


string
DatabaseConnection::getUser()
{
    return user_;
}


string
DatabaseConnection::getPassword()
{
    return password_;
}


string
DatabaseConnection::getPasswordPath()
{
    return pwdPath_;
}


log4cplus::Logger&
DatabaseConnection::getLogger()
{
    return logger_;
}


tsframework::CellAbstractContext*
DatabaseConnection::getContext()
{
    return context_;
}

} // end ns tscandela



/*
 * FOR TESTING PURPOSES
std::map<std::string, Table*> DatabaseConnection::getTableNameToTableCache() { return mapTableNameToTable_;}
std::map<std::string, std::string>  DatabaseConnection::getTableNameToTablePKCache() {return mapTableNameToPKName_;}
std::map<std::string, std::map<std::string, std::string> >  DatabaseConnection::getTableNameToFKNameToRefTableCache() {return mapTableNameToMapFKNameToRefTable_;}
std::map<std::string, std::map<std::string, std::string> >  DatabaseConnection::getTableNameToFKNameToRefColumnCache() {return mapTableNameToMapFKNameToRefColumn_;}
*/
