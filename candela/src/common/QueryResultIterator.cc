#include "ts/candela/QueryResultIterator.h"

#include "ts/candela/QueryResult.h"
#include "ts/candela/Row.h"

using namespace std;

namespace tscandela
{


QueryResultIterator::QueryResultIterator()
:
    result_(0),
    row_(this->operator ->()),
    position_(0),
    endReached_(true)
{

}


QueryResultIterator::QueryResultIterator(const QueryResult* result)
:
result_(result),
position_(0)
{
    if (result->size() == 0)
    {
        endReached_ = true;
    }
    else
    {
        endReached_ = false;
    }
}


QueryResultIterator::~QueryResultIterator()
{

}


bool
QueryResultIterator::operator == (const QueryResultIterator& it) const
{
    return (this->position_ == it.position_) && (this->endReached_ == it.endReached_);
}


bool
QueryResultIterator::operator != (const QueryResultIterator& it) const
{
    return !this->operator==(it);
}


QueryResultIterator&
QueryResultIterator::operator ++ ()
{
    if (endReached_)
    {
        return *this;
    }

    if ( (position_ + 1) == (result_->size() ))
    {
        endReached_ = true;
        position_ = 0;
    }
    else
    {
        position_++;
    }

    return *this;

}


QueryResultIterator&
QueryResultIterator::operator -- ()
{

    if ( endReached_ )
    {
        endReached_ = false;
        position_ = result_->size() - 1;
    }
    else
    {
        if (position_ == 0)
        {
            return *this;
        }
        position_--;
    }

    return *this;
}


QueryResultIterator&
QueryResultIterator::operator = (const QueryResultIterator& it)
{
    if (endReached_)
    {
        endReached_ = false;
        position_ = result_->size() - 1;
    }
    else
    {
        if (position_ == 0)
        {
            return *this;
        }

        position_--;
    }

    return *this;
}


Row&
QueryResultIterator::operator * ()
{
    return *(this->row_);
}


Row*
QueryResultIterator::operator -> ()
{
    this->row_ = new Row(*this);
    return this->row_;

}

} // end ns candela
