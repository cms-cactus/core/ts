#include <iostream>

#include "candela/DatabaseConnection.h"
#include "candela/Table.h"
#include "candela/QueryResult.h"
#include "candela/QueryResultIterator.h"
#include "candela/Row.h"


#include "xdata/Table.h"
#include "xdata/TableIterator.h"

#include "ts/framework/CellAbstractContext.h"

#include "log4cplus/logger.h"

using namespace std;



void makeTraversal(candela::DatabaseConnection& db, const candela::Table& table, candela::QueryResult& q)
{
	for (candela::QueryResult::const_iterator qCIt = q.begin();
			qCIt != q.end();
			++qCIt)
	{
		vector<string> columns = q.getColumns();

		for (vector<string>::const_iterator colCIt = columns.begin();
				colCIt != columns.end();
				++colCIt)
		{
			candela::Table fkTable = db.getReferencedTable(table.getName(), *colCIt);
			candela::QueryResult fkQuery = table.selectFKRow(*colCIt, qCIt->get<string>(*colCIt));

			makeTraversal(db, fkTable, fkQuery);
		}
	}

}

string getRegisterValue(candela::DatabaseConnection& db)
{
	string registerValue("");

	const string currentGTKey("gt_2013_no_ttc");
	const string originColumn("PSBOGTI_SLOT_13_SETUP_FK");

	candela::Table gtSetupTable = db.getTable("GT_SETUP");
	candela::QueryResult gtSetupRow = gtSetupTable.selectRowByPK("gt_2013_no_ttc");

	string psb13FKKey = gtSetupRow.begin()->get<string>(originColumn);
	candela::QueryResult psbSetupRow = gtSetupTable.selectFKRow(originColumn, psb13FKKey);

	registerValue = psbSetupRow.begin()->get<string>("CH1_T0_AUTOALIGN_AUTO");
	return registerValue;
}


void getTriggerMask(candela::DatabaseConnection& db, map<string, string>& maskMap)
{

	candela::Table dttfCurrentRsTable = db.getTable("DTTF_SETTINGS_KEY_CURRENT");
	candela::QueryResult currentKey = dttfCurrentRsTable.selectRowByPK("The Key");

	if (currentKey.size() != 1)
	{
		cout << "Unexpected number of row returned from query " << currentKey.getName() << endl;
		return;
	}

	string dttfHwConfig = currentKey.begin()->get<string>("HW_SETTINGS");
	candela::QueryResult maskQuery = dttfCurrentRsTable.selectFKRow("HW_SETTINGS", dttfHwConfig);

	if (maskQuery.size() != 1)
	{
		cout << "Unexpected number of row returned from query" << maskQuery.getName() << endl;
		return;
	}


	vector<string> columns = maskQuery.getColumns();

	for (vector<string>::const_iterator cIt = columns.begin();
									    cIt != columns.end();
									    ++cIt)
	{
		if ( (*cIt).find("ID") != string::npos )
			continue;

		maskMap[*cIt] = maskQuery.begin()->get<string>(*cIt);
	}

}


void makeSimpleInsert(candela::DatabaseConnection& db)
{

	string fdlTableName("GT_FDL_SETUP");

        map<string, string> columnNameToColumnValue;
	columnNameToColumnValue["ENABLE_STATUS_SLOT_10_FDL"] = "1";
	columnNameToColumnValue["FW_VME_FDL_VERSION"] = "0x1007";
        
	candela::Table fdlSetupTable = db.getTable(fdlTableName);
	fdlSetupTable.getInserter().set(columnNameToColumnValue).insert();

}






void bulkInsert( candela::DatabaseConnection& db )
{

  candela::Table::Inserter inserter = db.getTable("CMS_DT_SC_MON.ALGO_SCALERS").getInserter();

  unsigned long count = std::numeric_limits<int>::max();
  count *= 1000;
  for ( size_t i = 0; i < 1000 ; ++i ) {

    std::ostringstream ost;
    ost << "ciao_" << i;
    std::string lumi( ost.str() );

    inserter.setData(i, "ALGO_COUNT"      , count + i  );
    inserter.setData(i, "ALGO_INDEX"      , i );
    inserter.setData(i, "ALGO_RATE"       , i );
    inserter.setData(i, "SCALER_TYPE"     , i%4 );
    inserter.setData(i, "LUMI_SECTIONS_ID", lumi );
    
  }

  inserter.insert();

}



int main(){
 

	/* Dummy objects needed for the examples to compile */
	tsframework::CellAbstractContext* context = 0;
	log4cplus::Logger log = log4cplus::Logger::getInstance(".CellContext");


	// INSERTER CODE TO BE ADDED HERE


	try
	{
		candela::DatabaseConnection db(log, context, "CMS_DTTF");
		map<string, string> masks;
		getTriggerMask(db, masks);
	}
	catch (exception& e)
	{
		cout << "Could not retrieve the trigger mask: " << e.what() << endl;
	}


	try
	{
		candela::DatabaseConnection db(log, context, "CMS_TRG_R");
		candela::Table gtSetup = db.getTable("CMS_GT.GT_SETUP");
		candela::QueryResult qGtSetup = gtSetup.selectAll();

		makeTraversal(db, gtSetup, qGtSetup);
	}
	catch (exception& e)
	{
		cout << "Could not perform the tree traversal " << e.what() << endl;
	}

	return 0;
}
