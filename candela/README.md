CANDELA Main page
============================

Documentation of the CANDELA library 

CANDELA stands for ConfigurAtioN Database accEss LibrAry. It is a library that provides trigger subsystems with a common, uniform and easy-to-use interface to perform operations with Oracle databases. It is written in C++ and also uses a small set of functionalities from the BOOST libraries. Although the main use case for trigger subsystems is the extraction of parameters from the online configuration database, CANDELA does not restrict its functionalities to that database.
