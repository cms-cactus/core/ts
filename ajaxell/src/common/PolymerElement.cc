/*
 * This widget sends an arbitrary HTML tag (e.g. <example-element></example-element>)
 * Used to link a polymer element to a panel
 */
#include "ajax/PolymerElement.h"

#include <iostream>

std::string name_;
ajax::PolymerElement::PolymerElement() :ajax::Eventable(), ajax::Container()
{
    allowEvent ( ajax::Eventable::OnClick );
    allowEvent ( ajax::Eventable::OnTime );
    allowEvent ( ajax::Eventable::OnChange );
    allowEvent ( ajax::Eventable::OnSubmit );
    allowEvent ( ajax::Eventable::OnOk );
    allowEvent ( ajax::Eventable::OnClose );
    allowEvent ( ajax::Eventable::OnFocus );
    allowEvent ( ajax::Eventable::OnExpand );
    allowEvent ( ajax::Eventable::NoEvent );
}
ajax::PolymerElement::PolymerElement( const std::string& name ) :ajax::Eventable(), ajax::Container()
{
    PolymerElement();
    setElementName(name);
    allowEvent ( ajax::Eventable::OnClick );
    allowEvent ( ajax::Eventable::OnTime );
    allowEvent ( ajax::Eventable::OnChange );
    allowEvent ( ajax::Eventable::OnSubmit );
    allowEvent ( ajax::Eventable::OnOk );
    allowEvent ( ajax::Eventable::OnClose );
    allowEvent ( ajax::Eventable::OnFocus );
    allowEvent ( ajax::Eventable::OnExpand );
    allowEvent ( ajax::Eventable::NoEvent );
}

void ajax::PolymerElement::setElementName ( const std::string& name )
{
  name_ = name;
}
std::string ajax::PolymerElement::getElementName()
{
  return name_;
}

void ajax::PolymerElement::html ( cgicc::Cgicc& cgi, std::ostream& out )
{
  out << "<" << name_ << " ";
  attributes ( out );
  out << ">" << std::endl;

  for ( std::vector<Widget*>::iterator i ( getWidgets().begin() ); i != getWidgets().end(); ++i )
  {
    ( *i )->html ( cgi,out );
  }

  out << "</" << name_ << ">" << std::endl;
}
