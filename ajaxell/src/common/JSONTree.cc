#include "ajax/JSONTree.h"

#include "ajax/JSONTreeNode.h"

ajax::JSONTree::JSONTree() : ajax::Widget(), ajax::Container() {}


void ajax::JSONTree::html ( cgicc::Cgicc& cgi, std::ostream& out )
{
    for ( std::vector<ajax::Widget*>::iterator i ( getWidgets().begin() ); i != getWidgets().end(); ++i )
    {
        ( *i )->html ( cgi,out );
        if (i != getWidgets().end() - 1) {
            out << ",\n";
        }
    }
}

void ajax::JSONTree::add ( ajax::JSONTreeNode* node )
{
    if (node->getSubMenuName() == "") {
        node->setSubMenuName("menu");
    }
  ajax::Container::add ( node );
}
