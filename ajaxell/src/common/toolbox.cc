/*
 * The toolbox contains functions to help the developer in communicating with
 * the web browser (parsing form data, a basic json mapper, and character escaping)
 */
#include "ajax/toolbox.h"
#include "ajax/Exception.h"

#include "ajax/ResultBox.h"

#include "cgicc/Cgicc.h"
#include "cgicc/FormFile.h"
#include "cgicc/CgiEnvironment.h"
#include "cgicc/CgiUtils.h"

std::string ajax::toolbox::getSubmittedValue ( cgicc::Cgicc& cgi,const std::string& name )
{
  //std::cout << "content-type: " << cgi.getEnvironment().getContentType() << std::endl;
  //if (!cgi.getEnvironment().getPostData().empty())
  //  std::cout << "post:  "<< cgi.getEnvironment().getPostData() << std::endl;
  const std::vector<cgicc::FormEntry>& formEntries = cgi.getElements();

  for ( std::vector<cgicc::FormEntry>::const_iterator i=formEntries.begin(); i != formEntries.end(); ++i )
  {
    if ( i->getName() ==name )
    {
      return i->getValue();
    }
  }

  //bug in the cgicc library. see http://www.mail-archive.com/help-cgicc@gnu.org/msg00053.html
  //and https://savannah.cern.ch/task/index.php?8146
  std::string data = cgi.getEnvironment().getPostData();
  std::string fname, fvalue;
  std::string::size_type pos;
  std::string::size_type oldPos = 0;

  if ( !data.empty() )
  {
    // Parse the data in one fell swoop for efficiency
    while ( true )
    {
      // Find the '=' separating the name from its value
      pos = data.find_first_of ( '=', oldPos );

      // If no '=', we're finished
      if ( std::string::npos == pos )
      {
        break;
      }

      // Decode the name
      fname = cgicc::form_urldecode ( data.substr ( oldPos, pos - oldPos ) );
      oldPos = ++pos;
      // Find the '&' separating subsequent name/value pairs
      pos = data.find_first_of ( '&', oldPos );
      // Even if an '&' wasn't found the rest of the string is a value
      fvalue = cgicc::form_urldecode ( data.substr ( oldPos, pos - oldPos ) );

      // Store the pair
      //fFormData.push_back(FormEntry(fname, fvalue));
      if ( fname ==name )
      {
        return fvalue;
      }

      if ( std::string::npos == pos )
      {
        break;
      }

      // Update parse position
      oldPos = ++pos;
    }
  }

  return getCookieValue(cgi, name);
}

std::map<std::string,std::string> ajax::toolbox::getSubmittedValues ( cgicc::Cgicc& cgi )
{
  std::map<std::string,std::string> result;
  std::vector<cgicc::FormEntry> formEntries = cgi.getElements();

  for ( std::vector<cgicc::FormEntry>::const_iterator i=formEntries.begin(); i != formEntries.end(); ++i )
  {
    result.insert ( make_pair ( i->getName(),i->getValue() ) );
  }

  //bug in the cgicc library. see http://www.mail-archive.com/help-cgicc@gnu.org/msg00053.html and https://savannah.cern.ch/task/index.php?8146
  std::string data = cgi.getEnvironment().getPostData();
  std::string fname, fvalue;
  std::string::size_type pos;
  std::string::size_type oldPos = 0;

  if ( !data.empty() && result.empty() )
  {
    // Parse the data in one fell swoop for efficiency
    while ( true )
    {
      // Find the '=' separating the name from its value
      pos = data.find_first_of ( '=', oldPos );

      // If no '=', we're finished
      if ( std::string::npos == pos )
      {
        break;
      }

      // Decode the name
      fname = cgicc::form_urldecode ( data.substr ( oldPos, pos - oldPos ) );
      oldPos = ++pos;
      // Find the '&' separating subsequent name/value pairs
      pos = data.find_first_of ( '&', oldPos );
      // Even if an '&' wasn't found the rest of the string is a value
      fvalue = cgicc::form_urldecode ( data.substr ( oldPos, pos - oldPos ) );
      // Store the pair
      result.insert ( make_pair ( fname, fvalue ) );

      if ( std::string::npos == pos )
      {
        break;
      }

      // Update parse position
      oldPos = ++pos;
    }
  }

  return result;
}

std::string ajax::toolbox::getCookieValue ( cgicc::Cgicc& cgi,const std::string& name )
{
    for(std::vector<cgicc::HTTPCookie>::const_iterator i=cgi.getEnvironment().getCookieList().begin(); i!= cgi.getEnvironment().getCookieList().end(); i++){
        if (i->getName() == name) {
            return i->getValue();
        }
    }
    return "";
}

const cgicc::FormFile& ajax::toolbox::getFile ( cgicc::Cgicc& cgi, const std::string& name )
{
  const std::vector<cgicc::FormFile>& files ( ajax::toolbox::getFiles ( cgi ) );

  for ( std::vector<cgicc::FormFile>::const_iterator i ( files.begin() ); i != files.end(); ++i )
  {
    if ( i->getName() == name )
    {
      return *i;
    }
  }

  throw ajax::FormFileNotFound ( "The file '" + name + "' has not been found in the CGI request" );
}

const std::vector<cgicc::FormFile>& ajax::toolbox::getFiles ( cgicc::Cgicc& cgi )
{
  return cgi.getFiles();
}


std::string ajax::toolbox::map2json ( const std::map<std::string,std::string>& params )
{
  std::string result;

  if ( params.empty() )
  {
    return result;
  }

  result = "{";

  for ( std::map<std::string,std::string>::const_iterator i ( params.begin() ); i != params.end(); )
  {
    std::string attr = i->first;
    std::string val = i->second;
    result += "'" + attr + "':";
    result += val;

    if ( ++i != params.end() )
    {
      result +=  ",";
    }
  }

  result += "}";
  return result;
}

double ajax::toolbox::elapsetime ( ::timeval& end, ::timeval& start )
{
  struct ::timeval result;

  /* Perform the carry for the later subtraction by updating y. */
  if ( end.tv_usec < start.tv_usec )
  {
    int nsec = ( start.tv_usec - end.tv_usec ) / 1000000 + 1;
    start.tv_usec -= 1000000 * nsec;
    start.tv_sec += nsec;
  }

  if ( end.tv_usec - start.tv_usec > 1000000 )
  {
    int nsec = ( end.tv_usec - start.tv_usec ) / 1000000;
    start.tv_usec += 1000000 * nsec;
    start.tv_sec -= nsec;
  }

  /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
  result.tv_sec = end.tv_sec - start.tv_sec;
  result.tv_usec = end.tv_usec - start.tv_usec;

  /* Return -1 if result is negative. */
  if ( end.tv_sec < start.tv_sec )
  {
    return -1.0;
  }

  return ( result.tv_sec*1000000 + result.tv_usec ) /1000;
}

std::string ajax::toolbox::escapeHTML(std::string html) {
    std::string buffer;
    buffer.reserve(html.size());
    for(size_t pos = 0; pos != html.size(); ++pos) {
        switch(html[pos]) {
            case '&':  buffer.append("&amp;");       break;
            case '\"': buffer.append("&quot;");      break;
            case '\'': buffer.append("&apos;");      break;
            case '<':  buffer.append("&lt;");        break;
            case '>':  buffer.append("&gt;");        break;
            default:   buffer.append(&html[pos], 1); break;
        }
    }
    return buffer;
}
