#include "config/version.h"
#include "ts/ajaxell/version.h"

GETPACKAGEINFO ( tsajaxell )

void tsajaxell::checkPackageDependencies()
{
  CHECKDEPENDENCY ( config );
}

std::set<std::string, std::less<std::string> > tsajaxell::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;
  ADDDEPENDENCY ( dependencies,config );
  return dependencies;
}

