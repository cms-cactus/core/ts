#include "ajax/EventHandler.h"

#include "ajax/AutoHandledWidget.h"
#include "ajax/Eventable.h"
#include "ajax/Registry.h"

#include "ajax/NullWidget.h"
#include "ajax/toolbox.h"

ajax::EventHandler::EventHandler ( const std::string& handlerUrl )
  :handlerUrl_ ( handlerUrl )
{
  ;
}

ajax::EventHandler::~EventHandler()
{
  ajax::Registry::getRegistry().remove ( this );

  for (CallbackIterator it1 = callbacks_.begin(); it1 != callbacks_.end(); it1++) {
    for (EventTypeIterator it2 = it1->second.begin(); it2 != it1->second.end(); it2++)
      delete it2->second;
  }
}

void ajax::EventHandler::removeEvent ( const std::string& id,ajax::Eventable::EventType type )
{
  //tstoolbox::ReadWriteLockHandler(rwlock_,tstoolbox::ReadWriteLockHandler::WRITE);
  if ( hasHandler ( id,type ) )
  {
    delete callbacks_[id][type];
    CallbackIterator i ( callbacks_.find ( id ) );
    i->second.erase ( type );

    if ( i->second.empty() )
    {
      callbacks_.erase ( id );
    }
  }
}

void ajax::EventHandler::removeEvents ( ajax::Eventable* widget )
{
  //tstoolbox::ReadWriteLockHandler(rwlock_,tstoolbox::ReadWriteLockHandler::WRITE);
  ajax::Registry::getRegistry().remove ( widget );
  std::string id = widget->getId();

  if ( callbacks_.find ( id ) != callbacks_.end() )
  {
    EventTypeIterator j ( callbacks_[id].begin() );

    for ( ; j != callbacks_[id].end(); ++j )
    {
      delete j->second;
    }

    callbacks_.erase ( id );
  }
}

bool ajax::EventHandler::hasHandler ( const std::string& id, ajax::Eventable::EventType type )
{
  //tstoolbox::ReadWriteLockHandler handler(rwlock_,tstoolbox::ReadWriteLockHandler::READ);
  return hasHandlerPrivate ( id,type );
}

bool ajax::EventHandler::hasHandlerPrivate ( const std::string& id, ajax::Eventable::EventType type )
{
  if ( callbacks_.empty() )
  {
    return false;
  }

  CallbackIterator i ( callbacks_.find ( id ) );

  if ( i != callbacks_.end() )
  {
    EventTypeIterator j ( i->second.find ( type ) );

    if ( j != i->second.end() )
    {
      return true;
    }
  }

  return false;
}
void ajax::EventHandler::callHandler ( cgicc::Cgicc& cgi, std::ostream& out )
{
  //tstoolbox::ReadWriteLockHandler handler(rwlock_,tstoolbox::ReadWriteLockHandler::READ);
  std::string id = ajax::toolbox::getSubmittedValue ( cgi,"_id_" );
  std::string eventName = ajax::toolbox::getSubmittedValue ( cgi,"_eventType_" );
  ajax::Eventable::EventType type = ajax::Eventable::string2event ( eventName );
  CallbackIterator i ( callbacks_.find ( id ) );

  if ( callbacks_.empty() )
  {
    throw ajax::CallbackNotFound ( "There are no callbacks in the EventHandler" );
  }

  if ( i == callbacks_.end() )
  {
    throw ajax::CallbackNotFound ( "Callback not found for _id_ '" + id + "'" );
  }

  EventTypeIterator j ( i->second.find ( type ) );

  if ( j != i->second.end() )
  {
    return j->second->invoke ( cgi,out );
  }
  else
  {
    throw ajax::CallbackNotFound ( "Callback not found for _id_ '" + id + "' and _eventType_ '" + eventName );
  }
}
