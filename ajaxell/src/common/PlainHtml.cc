/*
 * Widget that can be used to send raw output to the browser
 */
#include "ajax/PlainHtml.h"

ajax::PlainHtml::PlainHtml()
  :ajax::Widget()
{
}
ajax::PlainHtml::PlainHtml( const std::string& html) : ajax::Widget()
{
    html_ << html;
}

std::ostringstream& ajax::PlainHtml::getStream()
{
  return html_;
}

void ajax::PlainHtml::html ( cgicc::Cgicc& cgi, std::ostream& out )
{
  out << html_.str();
}
