/*
 * The registry manages the connections between eventhandlers and widgets
 */
#include "ajax/Registry.h"

#include "ajax/Exception.h"
#include "ajax/EventHandler.h"


#include <sstream>

tstoolbox::ReadWriteLock ajax::Registry::rwlock_;
ajax::Registry* ajax::Registry::instance_=0;

ajax::Registry& ajax::Registry::getRegistry()
{
  tstoolbox::ReadWriteLockHandler h ( rwlock_,tstoolbox::ReadWriteLockHandler::WRITE );

  if ( instance_ == 0 )
  {
    instance_ = new ajax::Registry();
  }

  return *instance_;
}


void ajax::Registry::remove ( ajax::Eventable* widget )
{
  tstoolbox::ReadWriteLockHandler h ( rwlock_,tstoolbox::ReadWriteLockHandler::WRITE );
  EventHandlerMap::iterator i = handlers_.find ( widget );

  if ( handlers_.end() != i )
  {
    handlers_.erase ( i );
  }
}

ajax::EventHandler* ajax::Registry::getEventHandler ( ajax::Eventable* widget )
{
  tstoolbox::ReadWriteLockHandler h ( rwlock_,tstoolbox::ReadWriteLockHandler::READ );
  EventHandlerMap::iterator i = handlers_.find ( widget );

  if ( handlers_.end() != i )
  {
    return i->second;
  }
  else
  {
    std::ostringstream msg;
    msg << "EventHandler not found for Widget address '" << widget << "'";
    throw EventHandlerNotFound ( msg.str() );
  }
}

void ajax::Registry::add ( ajax::Eventable* widget,ajax::EventHandler* handler )
{
  tstoolbox::ReadWriteLockHandler h ( rwlock_,tstoolbox::ReadWriteLockHandler::WRITE );
  handlers_[widget] = handler;
}

void ajax::Registry::remove ( ajax::EventHandler* handler )
{
  tstoolbox::ReadWriteLockHandler h ( rwlock_,tstoolbox::ReadWriteLockHandler::WRITE );

  for ( EventHandlerMap::iterator i ( handlers_.begin() ); i != handlers_.end(); )
    if ( i->second == handler )
    {
      handlers_.erase ( i );
      i = handlers_.begin();
    }
    else
    {
      ++i;
    }
}
