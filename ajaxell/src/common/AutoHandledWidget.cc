#include "ajax/AutoHandledWidget.h"

#include "ajax/EventHandler.h"
#include "ajax/Container.h"
#include "ajax/Exception.h"
#include "ajax/toolbox.h"

#include <iostream>
#include <sstream>
#include <cstdlib>
#include <ctime>

ajax::AutoHandledWidget::AutoHandledWidget ( const std::string& url )
  :mutex_ ( true )
{
  eHandler_ = new ajax::EventHandler ( url );
}

ajax::AutoHandledWidget::~AutoHandledWidget()
{
  delete eHandler_;
}

ajax::EventHandler* ajax::AutoHandledWidget::getEventHandler()
{
  return eHandler_;
}

bool ajax::AutoHandledWidget::hasHandler ( const std::string& id, ajax::Eventable::EventType type )
{
  tstoolbox::MutexHandler handler ( mutex_ );

  if ( getEventHandler()->hasHandler ( id,type ) )
  {
    return true;
  }
  else if ( ajax::Container* p = dynamic_cast<ajax::Container*> ( this ) )
  {
    std::vector<ajax::Widget*> all ( p->getAllWidgets() );

    for ( std::vector<ajax::Widget*>::iterator i ( all.begin() ); i != all.end(); ++i )
    {
      if ( ajax::AutoHandledWidget* q = dynamic_cast<ajax::AutoHandledWidget*> ( *i ) )
      {
        if ( q->getEventHandler()->hasHandler ( id,type ) )
        {
          return true;
        }
      }
    }
  }

  return false;
}

void ajax::AutoHandledWidget::callHandler ( cgicc::Cgicc& cgi, std::ostream& out )
{
  tstoolbox::MutexHandler handler ( mutex_ );

  try
  {
    getEventHandler()->callHandler ( cgi,out );
    return;
  }
  catch ( ajax::CallbackNotFound& e ) {}

  if ( ajax::Container* p = dynamic_cast<ajax::Container*> ( this ) )
  {
    std::vector<ajax::Widget*> all ( p->getAllWidgets() );

    for ( std::vector<ajax::Widget*>::iterator i ( all.begin() ); i != all.end(); ++i )
    {
      if ( ajax::AutoHandledWidget* q = dynamic_cast<ajax::AutoHandledWidget*> ( *i ) )
      {
        try
        {
          q->callHandler ( cgi,out );
          return;
        }
        catch ( ajax::CallbackNotFound& e ) {}
      }
    }
  }

  throw ajax::CallbackNotFound ( "Callback not found in AutoHandledWidget with id '" + getId() + "'" );
}

void ajax::AutoHandledWidget::html ( cgicc::Cgicc& cgi, std::ostream& out )
{
  tstoolbox::MutexHandler handler ( mutex_ );

  for ( std::vector<ajax::Widget*>::iterator i ( getWidgets().begin() ); i != getWidgets().end(); ++i )
  {
    ( *i )->html ( cgi,out );
  }
}

void ajax::AutoHandledWidget::remove ( ajax::Widget* widget )
{
  tstoolbox::MutexHandler handler ( mutex_ );
  ajax::Container::remove ( widget );
}

void ajax::AutoHandledWidget::remove ( const std::string& id )
{
  tstoolbox::MutexHandler handler ( mutex_ );
  ajax::Container::remove ( id );
}

void  ajax::AutoHandledWidget::remove()
{
  tstoolbox::MutexHandler handler ( mutex_ );
  ajax::Container::remove();
}
