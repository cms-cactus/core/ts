/*
 * The RGB class helps with modifying colors
 * Input is in hex or RGB format
 * Output is in hex format
 */
#include "ajax/RGB.h"
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <stdio.h>

namespace ajax {
  RGB::RGB() {
    RGB(0,0,0);
  }
  RGB::RGB(const std::string hex) {
    int red;
    int green;
    int blue;


    try {
      std::istringstream(hex.substr(1,2)) >> red;
      std::istringstream(hex.substr(3,2)) >> green;
      std::istringstream(hex.substr(5,2)) >> blue;
    } catch(const std::exception& e) {
      std::cout << "RGB could not parse the string " << hex << "\n";
    }

    RGB(red, green, blue);
  }
  RGB::RGB(const int red, const int green, const int blue) {
    this->red = red;
    this->green = green;
    this->blue = blue;
  }
  std::ostream& operator<<(std::ostream &out, const RGB &rgb) {
    return out << rgb.toHex();
  }

  std::string RGB::toHex() const {
    std::ostringstream oss;
    oss << "#" << std::setfill('0') << std::setw(2) << std::hex << this->red;
    oss << std::setfill('0') << std::setw(2) << std::hex << this->green;
    oss << std::setfill('0') << std::setw(2) << std::hex << this->blue;
    return oss.str();
  }

  RGB RGB::lighten(int percent) {
    int newRed = (int)((float)this->red * (float)(1. + (float)percent/100.));
    if (newRed > 255) {
      newRed = 255;
    } else if(newRed < 0) {
      newRed = 0;
    }
    int newGreen = (int)((float)this->green * (float)(1. + (float)percent/100.));
    if (newGreen > 255) {
      newGreen = 255;
    } else if(newGreen < 0) {
      newGreen = 0;
    }
    int newBlue = (int)((float)this->blue * (float)(1. + (float)percent/100.));
    if (newBlue > 255) {
      newBlue = 255;
    } else if(newBlue < 0) {
      newBlue = 0;
    }
    RGB result(newRed, newGreen, newBlue);
    return result;
  }

  RGB RGB::darken(int percent) {
    return this->lighten(-percent);
  }



}
