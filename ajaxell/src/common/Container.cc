/*
 * Container enables a widget to contain child widgets.
 * This allows the developer to render an HTML node tree in a panel.
 * See the EmbeddingExample in the Subsystem Supervisor for an example.
 */
#include "ajax/Container.h"

#include "ajax/Exception.h"

#include <iostream>
#include <typeinfo>

ajax::Container::Container()
  :isOwner_ ( true )
{
  ;
}

ajax::Container::~Container()
{
  remove();
}

void ajax::Container::add ( ajax::Widget* widget )
{
  widgets_.push_back ( widget );
}

std::vector<ajax::Widget*>& ajax::Container::getWidgets()
{
  return widgets_;
}

void ajax::Container::remove ( ajax::Widget* widget )
{
  for ( std::vector<ajax::Widget*>::iterator i ( getWidgets().begin() ); i!=getWidgets().end(); ++i )
    if ( *i == widget )
    {
      if ( ( *i )->getIsOwned() )
      {
        if ( Container* c = dynamic_cast<Container*> ( widget ) )
        {
          c->remove();
        }

        delete *i;
      }

      getWidgets().erase ( i );
      return;
    }

  throw WidgetDoesNotExist ( "Can not remove widget '" + widget->getId() + "'. The widget can not be found inside the ajax::Container " );
}

void ajax::Container::remove ( const std::string& id )
{
  for ( std::vector<ajax::Widget*>::iterator i ( getWidgets().begin() ); i!=getWidgets().end(); ++i )
    if ( ( *i )->getId() == id )
    {
      if ( ( *i )->getIsOwned() )
      {
        if ( Container* c = dynamic_cast<Container*> ( *i ) )
        {
          c->remove();
        }

        delete *i;
      }

      getWidgets().erase ( i );
      return;
    }

  throw WidgetDoesNotExist ( "Can not remove widget '" + id + "'. The widget can not be found inside the ajax::Container " );
}


void ajax::Container::remove()
{
  for ( std::vector<ajax::Widget*>::iterator i ( getWidgets().begin() ); i != getWidgets().end(); ++i )
    if ( ( *i )->getIsOwned() )
    {
      if ( Container* c = dynamic_cast<Container*> ( *i ) )
      {
        c->remove();
      }

      delete *i;
    }

  widgets_.clear();
}

ajax::Widget* ajax::Container::find ( const std::string& id )
{
  ajax::Widget* result ( 0 );

  for ( std::vector<ajax::Widget*>::iterator i ( getWidgets().begin() ); i!=getWidgets().end(); ++i )
  {
    if ( ( *i )->getId() == id )
    {
      return *i;
    }

    ajax::Container* p = dynamic_cast<ajax::Container*> ( *i );

    if ( p )
    {
      result = p->find ( id );

      if ( result )
      {
        return result;
      }
    }
  }

  return 0;
}

std::vector<ajax::Widget*> ajax::Container::getAllWidgets()
{
  std::vector<ajax::Widget*> result;
  result.insert ( result.end(),getWidgets().begin(),getWidgets().end() );

  for ( std::vector<ajax::Widget*>::iterator i ( getWidgets().begin() ); i != getWidgets().end(); ++i )
  {
    if ( ajax::Container* p = dynamic_cast<ajax::Container*> ( *i ) )
    {
      std::vector<ajax::Widget*> more = p->getAllWidgets();
      result.insert ( result.end(),more.begin(),more.end() );
    }
  }

  return result;
}
