#include "ajax/ResultBox.h"

#include <iostream>

ajax::ResultBox::ResultBox()
  :ajax::Widget(), ajax::Container() //, isResizable_(false)
{
  set ( "elevation","0" );
}


void ajax::ResultBox::html ( cgicc::Cgicc& cgi, std::ostream& out )
{
  out << "<div"; attributes ( out ); out << ">\n";
  for ( std::vector<Widget*>::iterator i ( getWidgets().begin() ); i != getWidgets().end(); ++i ) {
      ( *i )->html ( cgi,out );
  }
  out << "</div>\n";
}


void ajax::ResultBox::innerHtml ( cgicc::Cgicc& cgi,std::ostream& out )
{
  std::vector<ajax::Widget*> wdgts = getWidgets();

  for ( std::vector<ajax::Widget*>::iterator i ( wdgts.begin() ); i != wdgts.end(); ++i )
  {
    ( *i )->html ( cgi,out );
  }
}
