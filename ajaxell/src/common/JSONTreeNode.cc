#include "ajax/JSONTreeNode.h"
#include "ajax/Exception.h"
#include "ajax/ResultBox.h"

#include <cstdlib>
#include <sstream>

ajax::JSONTreeNode::JSONTreeNode() : ajax::Eventable(), ajax::Container()
{
    allowEvent ( ajax::Eventable::OnClick );
    submenuName_ = "";
}


void ajax::JSONTreeNode::html ( cgicc::Cgicc& cgi, std::ostream& out )
{
    int submenusCnt = std::distance(getWidgets().begin(), getWidgets().end());

    if (submenuName_ == "" && submenusCnt > 0) {
        //print an array instead of an object
        printSubmenus(cgi,out);
    } else {
        out << "{\n";
        if (submenusCnt == 0) {
            JSONattributes ( out , true);
        } else {
            JSONattributes ( out , false);
            out << "    \"" << submenuName_ << "\": ";
            printSubmenus(cgi,out);
        }
        out << "}\n";
    }
}

void ajax::JSONTreeNode::printSubmenus ( cgicc::Cgicc& cgi, std::ostream& out ) {
    out << "[\n";
    for ( std::vector<ajax::Widget*>::iterator i ( getWidgets().begin() ); i != getWidgets().end(); ++i )
    {
        ( *i )->html ( cgi,out );
        if (i != getWidgets().end() - 1) {
            out << ",\n";
        }
    }
    out << "]\n";
}

void ajax::JSONTreeNode::setCaption ( const std::string& caption )
{
    set( "caption", caption);
}

void ajax::JSONTreeNode::setSubMenuName ( const std::string& submenuName )
{
    submenuName_ = submenuName;
}
std::string ajax::JSONTreeNode::getSubMenuName()
{
    return submenuName_;
}


void ajax::JSONTreeNode::add ( ajax::JSONTreeNode* node )
{
    ajax::Container::add ( node );
}
