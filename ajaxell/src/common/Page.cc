#include "ajax/Page.h"
#include "ajax/Exception.h"
#include "ajax/toolbox.h"
#include "ajax/RGB.h"

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <typeinfo>

#include "cgicc/HTMLClasses.h"

ajax::Page::Page ( const std::string& url )
  :ajax::AutoHandledWidget ( url ),url_ ( url )
{
}

void ajax::Page::printPreBody ( cgicc::Cgicc& cgi, std::ostream& out )
{
  RGB primaryColor = getPrimaryColor();
  RGB secondaryColor = getSecondaryColor();
  out <<
"<!doctype html>\n"
"<html>\n"
"<head>\n"
"    <meta charset='utf-8'>\n"
"    <meta name='description' content=''>\n"
"    <meta name='viewport' content='width=device-width, initial-scale=1'>\n"
"    <title>" << getCaption() << "</title>\n"
"    <script src='/extern/bower_components/webcomponentsjs/webcomponents-lite.min.js' async></script>\n"
"    <script src='/ts/ajaxell/html/before.js'></script>\n"
"    <script src='/ts/ajaxell/html/after.js'></script>\n"
"    <link rel='import' href='/ts/ajaxell/html/elements/ajaxell-elements.html'>\n"
<< getAppPath() << getImports() <<
"    <link rel='stylesheet' href='/ts/ajaxell/html/ajaxell.css'>\n"
"    <style is='custom-style'>"
"      div[toolbar] {\n"
"        background-color: " << primaryColor << " !important;\n"
"      }\n"
"      :root {\n"
"        --dark-primary-color: " << primaryColor.darken(10) << ";\n"
"        --default-primary-color: " << primaryColor << ";\n"
"        --primary-color: " << primaryColor << ";\n"
"        --light-primary-color: " << primaryColor.lighten(10) << ";\n"
"        --secondary-color: " << secondaryColor << ";\n";

for (size_t i = 1; i <= 6; i++) {
  out << "        --primary-color-light-" << i*10 << ": " << primaryColor.lighten(i*10) << ";\n";
  out << "        --primary-color-dark-" << i*10 << ": " << primaryColor.darken(i*10) << ";\n";
  out << "        --secondary-color-light-" << i*10 << ": " << secondaryColor.lighten(i*10) << ";\n";
  out << "        --secondary-color-dark-" << i*10 << ": " << secondaryColor.darken(i*10) << ";\n";
}

  out <<
"      }\n"
"      [toolbar] paper-progress {\n"
"        --paper-progress-container-color: var(--primary-color);\n"
"        --paper-progress-active-color: var(--secondary-color);\n"
"      }\n"
"    </style>\n"
"</head>\n"
"<body>\n"
"    <ts-session id='session' sessionid='" << getSessionId() << "'></ts-session>\n"
"    <ag-toaster></ag-toaster>"
"    <page-handler></page-handler>\n"
"    <div running-dino> <div dino></div> <div eye></div> <div mouth></div> <div ground></div> <div comets></div> </div>\n"
"    <div vertical layout>\n"
"      <div toolbar vertical layout>\n"
"        <div horizontal layout flex>\n"
"          <iron-icon icon='menu' onclick='toggleMenu()'></iron-icon>\n"
"          <a class='title' href='#!/' onclick='page('/');'>" << getCaption() << "</a>\n"
"          <span id='path' flex></span>\n"
"          <ul menu>\n"
"            <li>\n"
"              <iron-icon icon='help-outline'></iron-icon>\n"
"              <ul>\n"
"                <paper-material elevation='1'>\n"
"                  <li><a target='_blank' href='https://svnweb.cern.ch/trac/cactus'>Support</a></li>\n"
"                  <li><a target='_blank' href='https://twiki.cern.ch/twiki/bin/view/CMS/TriggerSupervisor'>About TS</a></li>\n"
"                  <li><a target='_blank' href='http://cactus.web.cern.ch/cactus/tsdocs/'>Dev Docs</a></li>\n"
"                </paper-material>\n"
"              </ul>\n"
"            </li>\n"
"           </ul>\n"
"           <img src='/ts/ajaxell/html/images/XDAQLogo.png' alt=''/>\n"
"         </div>"
"        <paper-progress id='panelprogress'></paper-progress>\n"
"      </div>\n"
"      <div horizontal layout flex>\n"
"        <div id='menu-container'>\n"
"          <ajaxell-menu id='menu'></ajaxell-menu>\n"
"        </div>\n"
"        <div flex content-container>\n"
"          <div content>\n";

}


void ajax::Page::html ( cgicc::Cgicc& cgi, std::ostream& out )
{
    tstoolbox::MutexHandler handler ( mutex_ );

    printPreBody(cgi, out);

    for ( std::vector<Widget*>::iterator i ( getWidgets().begin() ); i != getWidgets().end(); ++i )
    {
        ( *i )->html ( cgi,out );
    }

    printPostBody(cgi, out);

}


void ajax::Page::printPostBody ( cgicc::Cgicc& cgi, std::ostream& out )
{
    out <<

"            </div>\n"
"          </div>\n"
"        </div>\n"
"      </div>\n"
"   </body>\n"
"</html>" << std::flush;
}


std::string ajax::Page::getUrl()
{
  return url_;
}


std::string ajax::Page::getAppPath()
{
  if (!appPath_.empty()) {
    return "    <link rel='import' href='/" + appPath_ + "/html/elements/elements.html'>\n";
  } else {
    return "";
  }
}


void ajax::Page::setAppPath ( const std::string& appPath )
{
  appPath_=appPath;
}


std::string ajax::Page::getImports()
{
  return imports_;
}


void ajax::Page::setImports ( const std::string& imports )
{
  imports_=imports;
}


void ajax::Page::setPrimaryColor(ajax::RGB color) {
  primaryColor_ = color;
}


void ajax::Page::setPrimaryColor(int red, int green, int blue) {
  RGB color(red, green, blue);
  primaryColor_ = color;
}


void ajax::Page::setSecondaryColor(ajax::RGB color) {
  secondaryColor_ = color;
}


void ajax::Page::setSecondaryColor(int red, int green, int blue) {
  RGB color(red, green, blue);
  secondaryColor_ = color;
}


ajax::RGB ajax::Page::getPrimaryColor() {
  return primaryColor_;
}


ajax::RGB ajax::Page::getSecondaryColor() {
  return secondaryColor_;
}


void ajax::Page::setSessionId ( const std::string& sessionId )
{
  sessionid_=sessionId;
}


std::string ajax::Page::getSessionId() const
{
  return sessionid_;
}
