/*
 * Outputs a single <div> element
 * Do not use this for new projects
 */
#include "ajax/Div.h"

#include "ajax/ResultBox.h"

#include <iostream>

ajax::Div::Div()
  :ajax::Eventable(), ajax::Container()
{
}


void ajax::Div::html ( cgicc::Cgicc& cgi, std::ostream& out )
{
  out << "<div ";
  attributes ( out );
  out << ">" << std::endl;

  for ( std::vector<Widget*>::iterator i ( getWidgets().begin() ); i != getWidgets().end(); ++i )
  {
    ( *i )->html ( cgi,out );
  }

  out << "</div>" << std::endl;
}
