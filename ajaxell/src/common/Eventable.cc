/*
 * Extends a widget so it can respond to events
 * Events include and are limited to:
 * - OnClick
 * - OnChange
 * - OnSubmit
 * - OnOk
 * - OnClose
 * - OnTime
 * - OnFocus
 * - OnExpand
 */
#include "ajax/Eventable.h"
#include "ajax/EventHandler.h"
#include "ajax/NullWidget.h"
#include "ajax/ResultBox.h"
#include "ajax/Exception.h"
#include "ajax/Registry.h"
#include "ajax/toolbox.h"

#include <cctype>
#include <typeinfo>

ajax::Eventable::Eventable()
  :ajax::Widget(), defaultValue_ ( "" )
{
  ;
}

ajax::Eventable::~Eventable()
{
  std::map<EventType, std::pair<std::string,ajax::Widget*> >::iterator i ( events_.begin() );

  for ( ; i != events_.end(); ++i )
  {
    if ( isNullWidget ( i->first ) )
    {
      delete i->second.second;
    }

    try
    {
      EventHandler* handler = ajax::Registry::getRegistry().getEventHandler ( this );
      handler->removeEvents ( this );
    }
    catch ( EventHandlerNotFound& e )
    {
      //The eventhandler has been removed already
      ;
    }
  }
}

bool ajax::Eventable::isNullWidget ( Eventable::EventType type )
{
  if ( isNullWidget_.find ( type ) != isNullWidget_.end() )
    if ( isNullWidget_[type] )
    {
      return true;
    }

  return false;
}

void ajax::Eventable::setEvent ( Eventable::EventType type,const std::string& handlerUrl )
{
  if ( !isAllowed ( type ) )
  {
    std::ostringstream msg;
    msg << "Not allowed type '" << event2string ( type ) << "' for widget '" << typeid ( this ).name() << "' and id '" << getId() << "'";
    throw ajax::NotAllowedEvent ( msg.str() );
  }

  if ( hasEvent ( type ) )
    if ( isNullWidget ( type ) )
    {
      delete events_[type].second;
    }

  events_[type] = std::make_pair ( handlerUrl,new ajax::NullWidget() );
  isNullWidget_[type] = true;
}

void ajax::Eventable::setEvent ( Eventable::EventType type,const std::string& handlerUrl, ajax::ResultBox* result )
{
  if ( !isAllowed ( type ) )
  {
    std::ostringstream msg;
    msg << "Not allowed type '" << event2string ( type ) << "' for widget id '" << typeid ( this ).name() << "' and id '" << getId() << "'";
    throw ajax::NotAllowedEvent ( msg.str() );
  }

  if ( hasEvent ( type ) )
    if ( isNullWidget ( type ) )
    {
      delete events_[type].second;
    }

  events_[type] = std::make_pair ( handlerUrl,result );
  isNullWidget_[type] = false;
}

void ajax::Eventable::removeEvent ( Eventable::EventType type )
{
  if ( hasEvent ( type ) )
  {
    if ( isNullWidget_[type] )
    {
      delete events_[type].second;
    }

    events_.erase ( type );
    isNullWidget_.erase ( type );
  }
}

void ajax::Eventable::removeEvents()
{
  for ( std::map<EventType, std::pair<std::string,ajax::Widget*> >::iterator i ( events_.begin() ); i!=events_.end(); ++i )
  {
    if ( isNullWidget ( i->first ) )
    {
      delete i->second.second;
    }
  }

  events_.clear();
  isNullWidget_.clear();
}

ajax::Eventable::ResultType ajax::Eventable::getResultType ( Eventable::EventType event )
{
  std::map<EventType, std::pair<std::string,ajax::Widget*> >::iterator i;

  if ( ( i = events_.find ( event ) ) != events_.end() )
  {
    ajax::Widget* w = i->second.second;

    if ( dynamic_cast<ajax::ResultBox*> ( w ) )
    {
      return Eventable::ResultBox;
    }
    else
    {
      return Eventable::None;
    }
  }
  else
  {
    return Eventable::None;
  }
}

bool ajax::Eventable::hasEvent ( Eventable::EventType event )
{
  std::map<EventType, std::pair<std::string,ajax::Widget*> >::iterator i;

  if ( ( i = events_.find ( event ) ) != events_.end() )
  {
    return true;
  }
  else
  {
    return false;
  }
}


std::string ajax::Eventable::getOnEventUrl ( Eventable::EventType event )
{
  std::map<EventType, std::pair<std::string,ajax::Widget*> >::iterator i;

  if ( ( i = events_.find ( event ) ) != events_.end() )
  {
    return i->second.first;
  }
  else
  {
    return "";
  }
}



ajax::Widget* ajax::Eventable::getResponseWidget ( Eventable::EventType event )
{
  std::map<EventType, std::pair<std::string,ajax::Widget*> >::iterator i;

  if ( ( i = events_.find ( event ) ) != events_.end() )
    if ( getResultType ( event ) != Eventable::None )
    {
      return i->second.second;
    }

  return 0;
}


std::string ajax::Eventable::event2string ( Eventable::EventType event )
{
  if ( event == Eventable::OnClick )
  {
    return "OnClick";
  }
  else if ( event == Eventable::OnChange )
  {
    return "OnChange";
  }
  else if ( event == Eventable::OnSubmit )
  {
    return "OnSubmit";
  }
  else if ( event == Eventable::OnOk )
  {
    return "OnOk";
  }
  else if ( event == Eventable::OnClose )
  {
    return "OnClose";
  }
  else if ( event == Eventable::OnTime )
  {
    return "OnTime";
  }
  else if ( event == Eventable::OnFocus )
  {
    return "OnFocus";
  }
  else if ( event == Eventable::OnExpand )
  {
    return "OnExpand";
  }
  else if ( event == Eventable::NoEvent )
  {
    return "";
  }

  std::ostringstream msg;
  msg << "There is no a string representation for the eventType '" << event << "'";
  throw ajax::NotDefinedEventType ( msg.str() );
}

ajax::Eventable::EventType ajax::Eventable::string2event ( const std::string& eventName )
{
  if ( eventName == "OnClick" )
  {
    return Eventable::OnClick;
  }
  else if ( eventName == "OnChange" )
  {
    return Eventable::OnChange;
  }
  else if ( eventName == "OnSubmit" )
  {
    return Eventable::OnSubmit;
  }
  else if ( eventName == "OnOk" )
  {
    return Eventable::OnOk;
  }
  else if ( eventName == "OnClose" )
  {
    return Eventable::OnClose;
  }
  else if ( eventName == "OnTime" )
  {
    return Eventable::OnTime;
  }
  else if ( eventName == "OnFocus" )
  {
    return Eventable::OnFocus;
  }
  else if ( eventName == "OnExpand" )
  {
    return Eventable::OnExpand;
  }
  else if ( ( eventName == "NoEvent" ) || ( eventName == "" ) )
  {
    return Eventable::NoEvent;
  }

  std::ostringstream msg;
  msg << "There is no a eventType for the string '" << eventName << "'";
  throw ajax::NotDefinedEventName ( msg.str() );
}

void ajax::Eventable::setDefaultValue ( const std::string& value )
{
  defaultValue_ = value;
}

std::string ajax::Eventable::getDefaultValue()
{
  return defaultValue_;
}

void ajax::Eventable::allowEvent ( Eventable::EventType event )
{
  allowedEvents_.insert ( event );
}

bool ajax::Eventable::isAllowed ( Eventable::EventType event )
{
  std::set<Eventable::EventType>::iterator i ( allowedEvents_.find ( event ) );
  return ( i == allowedEvents_.end() ) ?false:true;
}
