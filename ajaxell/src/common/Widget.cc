/*
 * The widget class enables the developer to set attributes of an element that
 * implements it.
 */
#include "ajax/Widget.h"

#include "ajax/toolbox.h"

#include "cgicc/Cgicc.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <typeinfo>
#include <string.h>

unsigned long ajax::Widget::lastId_ =1;

ajax::Widget::Widget()
  : id_ ( "" ),caption_ ( "" ),isOwned_ ( true )
{
  lastId_++;
  std::ostringstream msg;
  msg << "Wdgt";
  msg << lastId_;
  //	msg << std::fixed;
  //	msg << std::setprecision(0);
  //	msg << rand();
  id_ = msg.str();
}

ajax::Widget::~Widget()
{
  ;
}
void ajax::Widget::setIsOwned ( bool isOwned )
{
  isOwned_ = isOwned;
}

bool ajax::Widget::getIsOwned()
{
  return isOwned_;
}
void ajax::Widget::setId ( const std::string& id )
{
  set("id", id);
}
std::string ajax::Widget::getId()
{
  return id_;
}

void ajax::Widget::setCaption ( const std::string& caption )
{
  caption_ = caption;
}
std::string ajax::Widget::getCaption()
{
  return caption_;
}
void ajax::Widget::set ( const std::string& attribute,const std::string& value )
{
  if ( attribute.compare("id") == 0 ) {
    id_ = value;
  }
  if ( attributes_.find ( attribute ) == attributes_.end() )
  {
    attributes_.insert ( std::make_pair ( attribute,value ) );
  }
  else
  {
    attributes_[attribute]=value;
  }
}

void ajax::Widget::attributes ( std::ostream& out )
{
  for ( std::map<std::string,std::string>::iterator i ( attributes_.begin() ); i != attributes_.end(); ++i )
  {
      out << " " << i->first;
      if (!i->second.empty()) {
          out << "=\"" << i->second << "\"";
      }
      out << " ";
  }
}
void ajax::Widget::JSONattributes ( std::ostream& out, bool omitLastComma )
{
    for ( std::map<std::string,std::string>::iterator i ( attributes_.begin() ); i != attributes_.end(); ++i )
    {
        out << "    \"" << i->first << "\": \"" << i->second << "\"";
        if (omitLastComma) {
            if (!( i != attributes_.end() && ++ ( attributes_.begin() = i ) == attributes_.end() )) {
                out << ",";
            }
        } else {
            out << ",";
        }
        out << "\n";
    }
}

void ajax::Widget::unset ( const std::string& attribute )
{
  std::map<std::string, std::string>::iterator it= attributes_.find ( attribute );

  if ( it!=attributes_.end() )
  {
    attributes_.erase ( it );
  }
}

std::string ajax::Widget::getSessionId ( cgicc::Cgicc& cgi )
{
  return ajax::toolbox::getSubmittedValue ( cgi,"_sessionid_" );
}
