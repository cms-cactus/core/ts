/*
 * Renders a <table> element
 * Do not use for new projects
 */
#include "ajax/Table.h"

#include "ajax/Exception.h"
#include "ajax/NullWidget.h"

#include <sstream>

ajax::Table::Table()
  :ajax::Widget(), rowSize_ ( 0 ), showRules_ ( false ), showHeader_ ( false )
{
  set ( "enableAlternateRows","true" );
  set ( "rowAlternateClass","alternateRow" );
  set ( "cellpadding","0" );
  set ( "border","0" );
}

ajax::Table::~Table()
{
  for ( std::map<std::string, std::vector<ajax::Widget*> >::iterator i ( values_.begin() ); i!=values_.end(); ++i )
    for ( std::vector<ajax::Widget*>::iterator j ( i->second.begin() ); j!=i->second.end(); ++j )
    {
      if ( dynamic_cast<ajax::NullWidget*> ( *j ) )
      {
        delete *j;
      }
      else if ( ( *j )->getIsOwned() )
      {
        delete *j;
      }
    }
}

void ajax::Table::remove()
{
  for ( std::map<std::string, std::vector<ajax::Widget*> >::iterator i ( values_.begin() ); i!=values_.end(); ++i )
    for ( std::vector<ajax::Widget*>::iterator j ( i->second.begin() ); j != i->second.end(); ++j )
    {
      if ( ( *j )->getIsOwned() )
      {
        delete *j;
      }
    }

  values_.clear();
  columns_.clear();
  order_.clear();
  rowSize_ = 0;
}

void ajax::Table::addColumn ( const std::string& column, ajax::Table::ColumnType type )
{
  if ( columns_.find ( column ) != columns_.end() )
  {
    throw ColumnAlreadyExist ( "Column '" + column + "' already exists on table widget '" + getId() + "'" );
  }

  columns_.insert ( std::make_pair ( column,type ) );
  order_.push_back ( column );

  if ( rowSize_ > 0 )
    for ( unsigned int i = 0; i < rowSize_ ; ++i )
    {
      values_[column].push_back ( new ajax::NullWidget() );
    }
}

void ajax::Table::setWidgetAt ( const std::string& column, unsigned int at, ajax::Widget* widget )
{
  std::map<std::string, ColumnType>::iterator i = columns_.find ( column );

  if ( i == columns_.end() )
  {
    throw ColumnDoesNotExist ( "Column '" + column + "' does not exist in table widget '" + getId() + "'" );
  }

  if ( ( at+1 ) > rowSize_ )
  {
    resize ( at+1 );
  }

  //If the stored widget is NullWidget or we are owners we delete the widget
  ajax::Widget* w;
  std::map<std::string, std::vector<ajax::Widget*> >::iterator j ( values_.find ( column ) );

  if ( j == values_.end() )
  {
    throw TableLocationDoesNotExist ( "Resize operation failed in Table widget '" + getId() + "'" );
  }

  w = j->second[at];

  if ( dynamic_cast<ajax::NullWidget*> ( w ) )
  {
    delete w;
  }
  else if ( w->getIsOwned() )
  {
    delete w;
  }

  j->second[at] = widget;
}


ajax::Table::Table::ColumnType ajax::Table::getType ( const std::string& column )
{
  std::map<std::string, ajax::Table::ColumnType>::iterator i ( columns_.find ( column ) );

  if ( i == columns_.end() )
  {
    throw ajax::ColumnDoesNotExist ( "Column '" + column + "' does not exist on table widget '" + getId() + "'" );
  }
  else
  {
    return i->second;
  }
}

ajax::Widget* ajax::Table::getWidgetAt ( const std::string& column, unsigned int at )
{
  std::map<std::string, std::vector<ajax::Widget*> >::iterator j ( values_.find ( column ) );

  if ( j == values_.end() )
  {
    std::ostringstream msg;
    msg << "There is no widget in column '" << column << "', row '" << at << "' for table widget '" << getId() << "'";
    throw ajax::TableLocationDoesNotExist ( msg.str() );
  }

  return j->second[at];
}

void ajax::Table::resize ( unsigned int newSize )
{
  if ( ( newSize <= rowSize_ ) || ( newSize <= 0 ) )
  {
    std::ostringstream msg;
    msg << "New table size '" << newSize << "' should positive and bigger than '" << rowSize_ << "' for table widget '" + getId() + "'";
    throw ajax::IncorrectRowSize ( msg.str() );
  }

  for ( std::map<std::string, ajax::Table::ColumnType>::iterator i ( columns_.begin() ); i!=columns_.end(); ++i )
    for ( unsigned int j = rowSize_; j < newSize ; ++j )
    {
      values_[i->first].push_back ( new ajax::NullWidget() );
    }

  rowSize_ = newSize;
}

std::string ajax::Table::columnType2string ( ajax::Table::ColumnType type )
{
  if ( type == ajax::Table::Html )
  {
    return "html";
  }
  else if ( type == ajax::Table::Number )
  {
    return "Number";
  }
  else
  {
    return "String";
  }
}


void ajax::Table::html ( cgicc::Cgicc& cgi, std::ostream& out )
{
  if ( getShowRules() )
  {
    set ( "rules","all" );
    set ( "style","border:1px solid #ccc; " );
  }
  else
  {
    set ( "rules","none" );
    set ( "style","border:0px solid #ccc; " );
  }

  out << "<table ";
  attributes ( out );
  out << " >" << std::endl;

  if ( getShowHeader() )
  {
    out << "<thead>" << std::endl;
    out << "<tr>" << std::endl;

    for ( std::vector<std::string>::iterator j ( order_.begin() ); j!=order_.end(); ++j )
    {
      std::map<std::string, ajax::Table::ColumnType>::iterator i ( columns_.find ( *j ) );
      out << "<th field=\"" << i->first << "\" ";

      if ( getShowRules() )
      {
        out << "style=\"border-right:1px solid #999; \" " << std::endl;
      }
      else
      {
        out << "style=\"border-right:0px solid #999; \" " << std::endl;
      }

      out << ">" << i->first << "</th>" << std::endl;
    }

    out << "</tr>" << std::endl;
    out << "</thead>" << std::endl;
  }

  out << "<tbody>" << std::endl;

  for ( unsigned int i ( 0 ); i < rowSize_; ++i )
  {
    out << "<tr>" << std::endl;

    for ( std::vector<std::string>::iterator k ( order_.begin() ); k!=order_.end(); ++k )
    {
      std::map<std::string, std::vector<ajax::Widget*> >::iterator j ( values_.find ( *k ) );
      out << "<td>" << std::endl;
      j->second[i]->html ( cgi,out );
      out << "</td>" << std::endl;
    }

    out << "</tr>" << std::endl;
  }

  out << "</tbody>" << std::endl;
  out << "</table>" << std::endl;
}

void ajax::Table::setShowRules ( bool showRules )
{
  showRules_ = showRules;
}

void ajax::Table::setShowHeader ( bool showHeader )
{
  showHeader_ = showHeader;
}

bool ajax::Table::getShowRules()
{
  return showRules_;
}

bool ajax::Table::getShowHeader()
{
  return showHeader_;
}
