Polymer({
  is: "ajaxell-menu",
  properties: {
    /**
     * The message the element will show
     */
    menu: {
      type: Object,
      observer: '_menuChanged'
    },
    recursionLevel: {
      type: Number,
      value: 0,
      reflectToAttribute: true
    },
    recursionRootName: {
      type: String,
      value: ""
    },
    closed: {
      type: Boolean,
      value: true,
      reflectToAttribute: true
    }
  },

  toggle: function() {
    this.closed = !(this.closed);
  },

  attached: function() {
    if (this.recursionLevel==0) {
      this.closed = false;
    }
  },

  _menuChanged: function() {
    var root = Polymer.dom(this.$.container);
    root.innerHTML = "";

    // go over each element in the menu
    for (var i = 0; i < this.menu.length; i++) {
      var currentMenu = this.menu[i];

      if (currentMenu.name != "Home") {

        // if it has a submenu, or it is the root level
        if (currentMenu.menu || this.recursionLevel == 0) {
          // create a submenu title
          var title = document.createElement('p');

          // with an icon if it is on the root level
          var icon = document.createElement('iron-icon');
          if (this.recursionLevel == 0) {
            icon.icon = "ajaxell:" + currentMenu.name.toLowerCase().replace(" ", "-");
          } else {
            icon.icon = "icons:chevron-right";
            title.setAttribute('closed', 'closed');
            // icon.style = "height: 20px";
          }
          title.appendChild(icon);

          title.appendChild(document.createTextNode(currentMenu.name));
          title.addEventListener('click', function() {
            //toggle the submenu
            if (this.getAttribute('closed')) {
              this.removeAttribute('closed');
              this.setAttribute('open', 'open');
            } else if (this.getAttribute('open')) {
              this.removeAttribute('open');
              this.setAttribute('closed', 'closed');
            }
            this.nextSibling.toggle();
          });

          if (!(currentMenu.menu)) {
            var warning = document.createElement('span');
            warning.setAttribute('warning', 'warning');
            warning.appendChild(document.createTextNode('(empty)'));
            title.appendChild(warning);
          }

          root.appendChild(title);

          //create the submenu
          var submenu = document.createElement('ajaxell-menu');
          submenu.recursionLevel = this.recursionLevel + 1;
          //pass the recursion root name
          if (this.recursionLevel == 0) {
            submenu.recursionRootName = currentMenu.name;
          } else {
            submenu.recursionRootName = this.recursionRootName;
          }
          switch (currentMenu.name) {
            case "Commands":
            case "Peers":
            submenu.closed = false;
          }
          root.appendChild(submenu);
          submenu.menu = currentMenu.menu;

        // if it has no submenu it is a clickable link
        } else {
          var link = document.createElement('a');
          if (this.recursionRootName == "Monitoring" && currentMenu.name.indexOf("urn:xdaq-flashlist:") == 0) {
            link.textContent = currentMenu.name.substring(19);
          } else {
            link.textContent = currentMenu.name;
          }

          if (currentMenu.url) {
            link.href = currentMenu.url;
            link.target = "_blank";
          } else {
            // link.href = "javascript:void(0)";
            link.href = "#!/" + this.recursionRootName.split(" ").join("%20") + "/" + currentMenu.name.split(" ").join("%20");
          }

          root.appendChild(link);
        }

      }
    }
  }
});
