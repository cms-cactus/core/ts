Polymer({
    is: 'ag-toaster',

    properties: {

      /**
       * The current toast
       */
      _toast: {
        type: Object,
        value: function() { return {};},
        observer: '_toastChanged'
      },
      _toastHideDelay: Object,
      _toastTimer: Object,
      /**
       * The duration in milliseconds to show the toast.
       */
      duration: {
        type: Number,
        value: 3000
      },
      _stack: {
        type: Array,
        value: function() {return [];}
      },
      /**
       * An Array of past toasts, sorted from most recent
       */
      log: {
        type: Array,
        value: function() {return [];}
      }
    },

    observers: [
      '_stackChanged(_stack.splices)'
    ],

    _toastChanged: function() {
      this.updateStyles();
    },


    _dialogButtonClick: function(event, details) {
      this.log[0].answer = event.model.option;
      this._toast.callback(event.model.option);
      this._toast = {};
      this.$.modal.close();
    },

    _toastButtonClick: function(event, details) {
      this.cancelAsync(this._toastTimer);
      this.log[0].answer = event.model.option;
      this._toast.callback(event.model.option);
      this.$.toast.hide();
      this._toastHideDelay = this.async(function() {
        this._toast = {};
        this._stackChanged();
      }, 200);
    },

    /**
     * Generate a notification based on the given toast.
     * The `Toast` object accepts the following properties:<br>
     * type (String): Can be 'info', 'warning', or 'error'.<br>
     * message (String | HTMLElement): The actual message to show.<br>
     * options (Array of Strings) (optional): Options presented to the user. The chosen option is returned.
     * blocking (optional): This will force the user to stop what they're
     * doing and choose one of the supplied options.
     * callback (function): The function to execute when an option has been selected or a timeout has passed.
     *
     * @param {Toast} toast The toast object.
     */
    throwToast: function(toast, sender) {
      toast.sender = sender;
      toast.timestamp = new Date();
      this.push('_stack', toast);
    },

    _oldToastIsHigherPriority: function() {
      var currentToast = this._toast;
      var newToast = this._stack[0];

      var currentPriority = 0;
      if (currentToast.type == "warning") {
        currentPriority = 1;
      } else if (currentToast.type == "error") {
        currentPriority = 2;
      }

      var newPriority = 0;
      if (newToast.type == "warning") {
        newPriority = 1;
      } else if (newToast.type == "error") {
        newPriority = 2;
      }

      return currentPriority > newPriority;
    },

    _stackChanged: function() {

      // if we have something new to show && the current one is just a toast && current toast is not higher priority
      // otherwise it will just remain in the to-do stack
      if(this._stack.length > 0 && !(this._toast.blocking) && !this._oldToastIsHigherPriority() ) {
        this.cancelAsync(this._toastHideDelay);

        // if a toast is still active, we need to hide it first
        if (this.$.toast.opened) {
          this.cancelAsync(this._toastTimer);
          this.$.toast.hide();
          this._toastTimeout();
          // give the toast time to hide before loading the new one
          this._toastHideDelay = this.async(function() {
            this._nextToast();
          }, 200);
        // otherwise we just show the next one
        } else {
          this._nextToast();
        }
      }
    },

    _toastTimeout: function() {
      if (this._toast.callback) {
        this._toast.callback(null);
      }
    },

    _nextToast: function() {
      this._toast = this._stack[0];
      if (this._toast && this._toast.type && this._toast.message) {
        this.unshift('log', this._toast);

        if (this._toast.options && !(this._toast.callback) ) {
          console.error(this._toast, "toast has options but provides no callback");
          this._toast.callback = function() {console.error("no callback was specified for this toast")};
        }

        if (this._toast.blocking) {
          // show a dialog
          this.$.modal.open();
        } else {
          // show a toast
          this.$.toast.show();
          this._toastTimer = this.async(function() {
            this._toastTimeout();
            this._toastHideDelay = this.async(function() {
              this._toast = {};
              this._stackChanged();
            }, 200);
            this._stackChanged();
          }, this.duration);
        }
      } else {
        console.error(this, "received an invalid toast: ", this._toast);
        this._toast = {};
      }
      this.shift('_stack');
    },

    _modalClosed: function() {
      this._stackChanged();
    },

    /**
     * This will output the entire log to the console.
     */
    showLog: function() {
      console.log('showing toaster logs');
      for (var i = 0; i < this.log.length; i++) {
        var toast = this.log[i];

        var output = toast.message + "\n";
        if (toast.answer) {
          output += "-> user responded: " + toast.answer + "\n";
        }
        output += "-> received at " + toast.timestamp + " by ";

        if (toast.type == "info") {
          console.info(output, toast.sender);
        } else if (toast.type == "warning") {
          console.warn(output, toast.sender);
        } else if (toast.type == "error") {
          console.error(output, toast.sender);
        } else {
          console.log(output, toast.sender);
        }
      }
    },

    attached: function() {
      if (typeof toaster === "undefined") {
        window.toaster = this;
      } else {
        console.error(this, "Trying to register as toaster, but another is already registerd", toaster);
      }
    },
});
