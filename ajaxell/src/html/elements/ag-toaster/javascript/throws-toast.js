/**
 * Use the `throwsToast` behavior to enable an element to throw modal dialogs that get
 * handled by a &lt;ag-toaster&gt; element.
 *
 * @polymerBehavior throwsToast
 */
throwsToast = {

  /**
   * Throw a message to the central toaster.
   * The `Toast` object accepts the following properties:<br>
   * type (String): Can be 'info', 'warning', or 'error'.<br>
   * message (String | HTMLElement): The actual message to show.<br>
   * options (Array of Strings) (optional): This will force the user to stop what they're
   * doing and choose one of the supplied options. The chosen option is returned.
   *
   * @param {Toast} toast Can be 'info', 'warning', or 'error'.
   * @param {function} callback The function to invoke when one of the options has been clicked. Takes the option string as argument.
   * @return {Void | String}
   */
  throwToast: function(toast) {
    if (typeof toaster === "undefined") {
      console.error(this, "Trying to throw toast ", toast, " but toaster is undefined");
    } else {
      toaster.throwToast(toast, this);
    }
  }
};
