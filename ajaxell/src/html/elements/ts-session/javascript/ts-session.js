Polymer({
    is: 'ts-session',
    properties: {
        sessionid: {
            type: String,
            value: "no session",
            notify: true,
            reflectToAttribute: true,
            observer: "_sessionidchanged"
        }
    },
    updateID: function() {
      this.sessionid = window.ts_session.id;
    },
    _sessionidchanged: function() {
      if (this.sessionid != "no session") {
        // we copy the session to a global variable for iron-ajax
        // iron-ajax is patched to send the session id with every request
        window.ts_session = {id: this.sessionid};
      }
    }
});
