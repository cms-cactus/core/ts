Polymer({
  is: "page-handler",
  behaviors: [throwsToast],
  handleResponse: function(e, request) {
    document.getElementById('panelprogress').indeterminate = false;
    var response = request.response;
    if (response.length == 0) {
      this._show404();
    } else {
      if (response.indexOf("<!doctype") !== 0) {

        function allDOMnodes(element) {
          var tagName = element.tagName;
          if (tagName && tagName.indexOf("-") > 0) {
            if (!importDefinitions[tagName]) {
              console.error("Element definition not found!", tagName);
            } else {
              document.getElementById('panelprogress').indeterminate = true;
              if(!Polymer.isInstance(element)) {
                Polymer.Base.importHref( [importDefinitions[tagName]], function() {
                  document.getElementById('panelprogress').indeterminate = false;
                });
              } else {
                document.getElementById('panelprogress').indeterminate = false;
              }
            }

          }
          var childNodes = Polymer.dom(element).childNodes;
          for (var i = 0; i < childNodes.length; i++) {
            var child = childNodes[i];
            allDOMnodes(child);
          }
        }

        var resultbox = Polymer.dom(document.getElementById("tsgui_content_"));
        resultbox.innerHTML = response;
        allDOMnodes(resultbox.node);
      } else {
        console.warn('page-handler: request returned main page, app is out of sync', request);
        //we got back a new session id from the server, inform ts-session
        this.throwToast({
          'type': 'info',
          'message': 'server has refreshed the session id'
        });
        document.getElementById('session').updateID();
        page.replace(page.current);
      }
    }
  },

  attached: function() {
    if (window.location.hash == "") {
      window.location.hash = "#!/";
    }
    this.$.ajax.url = window.location.origin + window.location.pathname;

    page.base("/" + window.location.pathname.split("/")[1] + "/");
    page.start({hashbang: true});
    page('/',               this._handleHome.bind(this) );
    page('/:group/:command',this._handleCommand.bind(this) );
    page('*',               this._show404 );
    page();
  },

  _handleHome: function(context, next) {
    this.$.ajax.body = [
        "_eventType_=OnClick",
        "_id_=tsgui_tree_Control Panels_Home",
        "tsgui_tree_Control Panels_Home=Home"
    ].join("&");
    // Polymer.dom(document.querySelector('#tsgui_content_')).appendChild(document.createElement('ts-loader'));
    this.$.ajax.generateRequest();
    document.getElementById('path').textContent = "";
  },

  _handleCommand: function(context, next) {
    if (!!(context.params.group) && context.params.command) {
      context.params.group = context.params.group.split("%20").join(" ");
      context.params.command = context.params.command.split("%20").join(" ")
      this.$.ajax.body = [
        "_eventType_=OnClick",
        "_id_=" + "tsgui_tree_"+context.params.group+"_"+context.params.command,
        "tsgui_tree_"+context.params.group+"_"+context.params.command+"=" + context.params.command
      ].join("&");
      // Polymer.dom(document.querySelector('#tsgui_content_')).appendChild(document.createElement('ts-loader'));
      this.$.ajax.generateRequest();
      document.getElementById('path').textContent = " ⟩ " + context.params.group + " ⟩ " + context.params.command;
      document.getElementById('panelprogress').indeterminate = true;
    }
  },
  _show404: function() {
    var content = Polymer.dom(document.querySelector('#tsgui_content_'));
    content.innerHTML='';
    if (page.current != "/") {
      content.appendChild(document.createElement('error-404'));
      document.getElementById('path').textContent = " ⟩ ☠️";
    } else {
      document.getElementById('path').textContent = "";
      console.log("This cell does not yet have a default page defined");
    }
  }

});
