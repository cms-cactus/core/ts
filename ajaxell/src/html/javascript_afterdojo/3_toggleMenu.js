toggleMenu = function() {
  var menu = document.getElementById('menu');
  var container = document.getElementById('menu-container');
  if (menu.getAttribute('closed') === null) {
    menu.setAttribute('closed', 'closed');
    container.setAttribute('closed', 'closed');
  } else {
    menu.removeAttribute('closed');
    container.removeAttribute('closed');
  }
}
