window.importDefinitions = {};
window.LazyLoad = function(newDefinition) {
  if (!newDefinition.base) {
    console.error("HTML Imports definition has no base defined", newDefinition);
  }
  if (!newDefinition.provides) {
    console.error("HTML Imports definition does not provide any elements", newDefinition);
  }

  for (var newimport in newDefinition.provides) {
    if (newDefinition.provides.hasOwnProperty(newimport)) {
      importDefinitions[newimport.toUpperCase()] = newDefinition.base + newDefinition.provides[newimport];
    }
  }
}

// example:
// var base = document._currentScript.baseURI;base = base.substring(0, base.lastIndexOf("/") + 1);
// LazyLoad({
//   base: base,
//   provides: {
//     "ts-operations": "ts-operations/ts-operations.html"
//   }
// })
