window.addEventListener('WebComponentsReady', function(e) {
  // this is important, if not executed the main page will stay gray
  document.body.setAttribute('ready', 'ready');

  //setup menu
  document.getElementById('menu').set('menu', tree.menu);

  setTimeout(function () {
    var dino = document.querySelector('div[running-dino]');
    dino.parentElement.removeChild(dino);
    dino = null;
  }, 2000);
});
