#ifndef _ajax_Eventable_h_
#define _ajax_Eventable_h_

#include "ajax/Widget.h"

#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <set>

namespace ajax
{

  class ResultBox;

  class Eventable: public ajax::Widget
  {
    public:

      enum EventType { OnClick, OnChange, OnSubmit, OnOk, OnClose, OnTime, OnFocus, OnExpand, NoEvent};
      enum ResultType {Dialog,ResultBox,None};

    public:
      Eventable();
      ~Eventable();

      virtual void setEvent ( Eventable::EventType type,const std::string& handlerUrl );
      virtual void setEvent ( Eventable::EventType type,const std::string& handlerUrl, ajax::ResultBox* result );

      void removeEvent ( Eventable::EventType type );
      void removeEvents();

      Eventable::ResultType getResultType ( Eventable::EventType event );
      bool hasEvent ( Eventable::EventType event );

      std::string getOnEventUrl ( Eventable::EventType event );

      ajax::Widget* getResponseWidget ( Eventable::EventType event );

      static std::string event2string ( Eventable::EventType event );
      static Eventable::EventType string2event ( const std::string& eventName );

    public:
      //!sets the default value that will be received through the CGI request
      void setDefaultValue ( const std::string& value );

    protected:
      void allowEvent ( Eventable::EventType event );
      bool isAllowed ( Eventable::EventType event );

    protected:
      std::string getDefaultValue();

    private:
      std::string defaultValue_;

    private:
      bool isNullWidget ( Eventable::EventType event );

      std::map<EventType, std::pair<std::string,ajax::Widget*> > events_;
      std::map<EventType,bool> isNullWidget_;
      std::set<Eventable::EventType> allowedEvents_;
  };

}
#endif
