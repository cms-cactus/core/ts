#ifndef _ajax_JSONTreeNode_h_
#define _ajax_JSONTreeNode_h_

#include "ajax/Eventable.h"
#include "ajax/Container.h"

#include <string>
#include <ostream>
#include <set>
#include <vector>

namespace ajax
{

  //!JSONTreeNode has the event OnClick
  class JSONTreeNode: public Eventable, public Container
  {
    public:
      JSONTreeNode();

      //!insert the html code into the stream
      void html ( cgicc::Cgicc& cgi, std::ostream& out );
      void printSubmenus ( cgicc::Cgicc& cgi, std::ostream& out );

      //!A tree node can only contain JSONTreeNodes
      void add ( ajax::JSONTreeNode* node );

      void setCaption ( const std::string& caption );

      void setSubMenuName ( const std::string& submenuName );
      std::string getSubMenuName();

    private:
      void add ( ajax::Widget* node )
      {
        ;
      };
      std::string submenuName_;

  };

}

#endif
