#ifndef _ajax_PlainHtml_h_
#define _ajax_PlainHtml_h_

#include "ajax/Widget.h"

#include <string>
#include <sstream>
#include <set>

namespace ajax
{

  class PlainHtml: public Widget
  {
    public:
      PlainHtml();
      PlainHtml( const std::string& );

      //!returns a output stream that can be filled with the desired html/javascript code
      std::ostringstream& getStream();

      //!Creates the html/javascript to display the Widget
      void html ( cgicc::Cgicc& cgi, std::ostream& out );

    private:
      std::ostringstream html_;

  };

}

#endif
