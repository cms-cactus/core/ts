#ifndef _ajax_Exception_h_
#define _ajax_Exception_h_

//#include "xcept/Exception.h"
#include <exception>
#include <string>

namespace ajax
{

  class Exception: public std::exception
  {
    public:
      Exception ( const std::string& msg ) :msg_ ( msg )
      {
        ;
      };
      Exception() :msg_ ( "Exception" )
      {
        ;
      };
      virtual ~Exception() throw () {};
      virtual const char* what() const throw()
      {
        return msg_.c_str();
      };

    protected:
      std::string msg_;

  };


  class CallbackNotFound: public ajax::Exception
  {
    public:
      CallbackNotFound ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };

  };

  class NotDefinedEventType: public ajax::Exception
  {
    public:
      NotDefinedEventType ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };

  };

  class NotDefinedEventName: public ajax::Exception
  {
    public:
      NotDefinedEventName ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  //class AJAXELL_ROOTNotDefined: public ajax::Exception {
  //public:
  //  NotDefinedEventName(const std::string& msg):Exception(msg) {;};
  //};

  //class XDAQ_ROOTNotDefined: public ajax::Exception
  //{
  //};

  class CanNotOpenTmpDirectory: public ajax::Exception
  {
    public:
      CanNotOpenTmpDirectory ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class ErrorReadingTmpDirectory: public ajax::Exception
  {
    public:
      ErrorReadingTmpDirectory ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class EventableIsNotAWidget: public ajax::Exception
  {
    public:
      EventableIsNotAWidget ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class ColumnDoesNotExist: public ajax::Exception
  {
    public:
      ColumnDoesNotExist ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class ColumnAlreadyExist: public ajax::Exception
  {
    public:
      ColumnAlreadyExist ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class IncorrectRowSize: public ajax::Exception
  {
    public:
      IncorrectRowSize ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class TableLocationDoesNotExist: public ajax::Exception
  {
    public:
      TableLocationDoesNotExist ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class CookieValueNotFound: public ajax::Exception
  {
    public:
      CookieValueNotFound ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class FormValueNotFound: public ajax::Exception
  {
    public:
      FormValueNotFound ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class FormFileNotFound: public ajax::Exception
  {
    public:
      FormFileNotFound ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class ParentDialogNotDefined: public ajax::Exception
  {
    public:
      ParentDialogNotDefined ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class NotAllowedEvent: public ajax::Exception
  {
    public:
      NotAllowedEvent ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class NotifierIdNotFound: public ajax::Exception
  {
    public:
      NotifierIdNotFound ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class WidgetDoesNotExist: public ajax::Exception
  {
    public:
      WidgetDoesNotExist ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };

  class EventHandlerNotFound: public ajax::Exception
  {
    public:
      EventHandlerNotFound ( const std::string& msg ) :Exception ( msg )
      {
        ;
      };
  };
}
#endif
