#ifndef _ajax_EventHandler_h_
#define _ajax_EventHandler_h_

#include "ajax/Widget.h"
#include "ajax/Eventable.h"
#include "ajax/ResultBox.h"
#include "ajax/Exception.h"
#include "ajax/Registry.h"

#include "cgicc/Cgicc.h"

#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iostream>

namespace ajax
{

  class EventHandler
  {

    private:
      class CallbackSignature;
      typedef std::map<ajax::Eventable::EventType,CallbackSignature*> EventTypeMap;
      typedef EventTypeMap::iterator EventTypeIterator;
      typedef std::map<std::string, EventTypeMap > CallbackMap;
      typedef CallbackMap::iterator CallbackIterator;

    public:
      EventHandler ( const std::string& handlerUrl );
    public:
      //!The url of the server that will handle the events is the unique parameter of the constructor

      ~EventHandler();

      //!Add a Event Handler to a certain widget Event.
      //!The event Handler must be a method of type void (::*callback)(cgicc::Cgicc&,std::ostream&)) of whatever class
      //!In this case nothing is returned to the browser
      template <class OBJECT>	void setEvent (
          ajax::Eventable* from,
          ajax::Eventable::EventType type,
          OBJECT* obj,
          void ( OBJECT::*callback ) ( cgicc::Cgicc&,std::ostream& )
      )
      {
        //tstoolbox::ReadWriteLockHandler handler(rwlock_,tstoolbox::ReadWriteLockHandler::WRITE);
        std::string fromId ( from->getId() );

        if ( hasHandlerPrivate ( fromId,type ) )
        {
          delete callbacks_[fromId][type];
        }

        from->setEvent ( type,handlerUrl_ );
        callbacks_[fromId][type] = new Callback<OBJECT> ( obj,callback );
        ajax::Registry& r = ajax::Registry::getRegistry();
        r.add ( from,this );
      }

      template <class OBJECT>	void setEvent (
          std::string fromId,
          ajax::Eventable::EventType type,
          OBJECT* obj,
          void ( OBJECT::*callback ) ( cgicc::Cgicc&,std::ostream& )
      )
      {
        if ( hasHandlerPrivate ( fromId,type ) )
        {
          delete callbacks_[fromId][type];
        }
        callbacks_[fromId][type] = new Callback<OBJECT> ( obj,callback );
      }

      //!Add a Event Handler to a certain widget Event.
      //!The event Handler must be a method of type void (::*callback)(cgicc::Cgicc&,std::ostream&)) of whatever class
      //!The html/javascript code that the event handler appends in the stream is send back to the browser to the ResultBox widget
      template <class OBJECT> void setEvent (
          ajax::Eventable* from,
          ajax::Eventable::EventType type,
          ajax::ResultBox* to,
          OBJECT* obj,
          void ( OBJECT::*callback ) ( cgicc::Cgicc&,std::ostream& )
      )
      {
        //tstoolbox::ReadWriteLockHandler handler(rwlock_,tstoolbox::ReadWriteLockHandler::WRITE);
        std::string fromId ( from->getId() );

        if ( hasHandlerPrivate ( fromId,type ) )
        {
          delete callbacks_[fromId][type];
        }

        from->setEvent ( type,handlerUrl_,to );
        callbacks_[fromId][type] = new Callback<OBJECT> ( obj,callback );
        ajax::Registry::getRegistry().add ( from,this );
      }


      //!remove a concrete event. Deprecated
      void removeEvent ( const std::string& id,ajax::Eventable::EventType type );
      //!remove all events of a concrete widget
      void removeEvents ( ajax::Eventable* widget );

      bool hasHandler ( const std::string& id, ajax::Eventable::EventType type );
      void callHandler ( cgicc::Cgicc& cgi, std::ostream& out );
    private:
      bool hasHandlerPrivate ( const std::string& id, ajax::Eventable::EventType type );
    private:

      class CallbackSignature
      {
        public:
          virtual ~CallbackSignature()
          {
            ;
          };

          virtual void invoke ( cgicc::Cgicc& cgi, std::ostream& out ) = 0;
      };

      template <class OBJECT> class Callback: public CallbackSignature
      {
        public:

          Callback ( OBJECT* object, void ( OBJECT::*func ) ( cgicc::Cgicc&, std::ostream& ) ) : obj_ ( object ), func_ ( func ) {};
          void invoke ( cgicc::Cgicc& cgi, std::ostream& out )
          {
            if ( func_ != 0 )
            {
              ( obj_->*func_ ) ( cgi,out );
            }
          }

        private:
          OBJECT* obj_;
          void ( OBJECT::*func_ ) ( cgicc::Cgicc&, std::ostream& );
      };

    private:
      CallbackMap callbacks_;
      std::string handlerUrl_;
  };

}
#endif
