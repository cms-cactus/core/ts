#ifndef _ajax_Table_h_
#define _ajax_Table_h_

#include "ajax/Widget.h"

#include <string>
#include <ostream>
#include <vector>
#include <map>

namespace ajax
{


  //!This class can be used for layout purpouses
  class Table: public Widget
  {
    public:
      //Only Number and String types can be ordered
      enum ColumnType {Number, String, Html};
    public:

      Table();
      ~Table();

      //!If true show the rulers. true by default
      void setShowRules ( bool showRules );
      //!If true show the header. true by default
      void setShowHeader ( bool showHeader );


      void remove();

      //!adds a column to the table
      void addColumn ( const std::string& column, ajax::Table::ColumnType type );
      //!Set the Widget for a given column an position
      void setWidgetAt ( const std::string& column, unsigned int at, ajax::Widget* widget );

      //!This method creates the html/javascript code that you want to put in the webpage. Usually only the html method of the
      //!Page Table is used.
      void html ( cgicc::Cgicc& cgi, std::ostream& out );

    protected:
      //!gets the type of the column
      ajax::Table::ColumnType getType ( const std::string& column );
      //!gets the widget on a determined position
      ajax::Widget* getWidgetAt ( const std::string& column, unsigned int at );
      void resize ( unsigned int newSize );
      std::string columnType2string ( ajax::Table::ColumnType type );
      bool getShowRules();
      bool getShowHeader();

    protected:
      bool isOwner_;
      unsigned int rowSize_;
      bool showRules_;
      bool showHeader_;

      std::map<std::string, ajax::Table::ColumnType> columns_;
      std::map<std::string, std::vector<ajax::Widget*> > values_;
      std::vector<std::string> order_;
  };

}
#endif /*WIDGET_H_*/
