#ifndef _ajax_toolbox_h_
#define _ajax_toolbox_h_

#include <string>
#include <ostream>
#include <vector>
#include <sstream>
#include <map>
#include <sys/time.h>

namespace cgicc
{
  class Cgicc;
  class FormFile;
}

namespace ajax
{
  class ResultBox;
  class Form;
  class ParametersForm;
  class InputFile;

  namespace toolbox
  {
    //!If the submitted value does not exist returns an empty string
    std::string getSubmittedValue ( cgicc::Cgicc& cgi,const std::string& name );
    //!returns all the form elements
    std::map<std::string,std::string> getSubmittedValues ( cgicc::Cgicc& cgi );
    std::string getCookieValue ( cgicc::Cgicc& cgi,const std::string& name );
    const cgicc::FormFile& getFile ( cgicc::Cgicc& cgi,const std::string& name );
    const std::vector<cgicc::FormFile>& getFiles ( cgicc::Cgicc& cgi );

    std::string map2json ( const std::map<std::string,std::string>& params );
    double elapsetime ( ::timeval& end, ::timeval& start );
    std::string escapeHTML(std::string html);
  }
}
#endif
