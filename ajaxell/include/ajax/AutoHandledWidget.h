#ifndef _ajax_AutoHandledWidget_h_
#define _ajax_AutoHandledWidget_h_

#include "ts/toolbox/Mutex.h"

#include "ajax/Widget.h"
#include "ajax/Container.h"

#include "ajax/EventHandler.h"
#include "ajax/Eventable.h"

#include "cgicc/Cgicc.h"

#include <string>
#include <ostream>
#include <set>

namespace ajax
{

  class AutoHandledWidget: public Widget, public Container
  {
    public:
      //!This widget needs to know the url that will be used with the event handler
      AutoHandledWidget ( const std::string& url );
      ~AutoHandledWidget();

      //!remove a concrete event
      void removeEvent ( const std::string& id,ajax::Eventable::EventType type )
      {
        this->getEventHandler()->removeEvent ( id,type );
      }

      //!removes all the events of a widget
      void removeEvent ( Eventable* widget )
      {
        this->getEventHandler()->removeEvents ( widget );
      }

      //remove a widget if exists
      virtual void remove ( ajax::Widget* widget );
      //!remove from the container the first widget that has the same id. Is the widget has getIsOwned()==true destroy it.
      virtual void remove ( const std::string& id );
      //!clears the container and, destroys the widgest that has getIsOwned == true
      virtual void remove();


      //used by polymerFormExample.cc
      template <class OBJECT>	void setEvent (
          ajax::Eventable* from,
          ajax::Eventable::EventType type,
          OBJECT* obj,
          void ( OBJECT::*callback ) ( cgicc::Cgicc&,std::ostream& )
      )
      {
        this->getEventHandler()->setEvent ( from,type,obj,callback );
      }

      template <class OBJECT>	void setEvent (
          std::string fromId,
          ajax::Eventable::EventType type,
          OBJECT* obj,
          void ( OBJECT::*callback ) ( cgicc::Cgicc&,std::ostream& )
      )
      {
        this->getEventHandler()->setEvent ( fromId,type,obj,callback );
      }

      template <class OBJECT> void setEvent (
          ajax::Eventable* from,
          ajax::Eventable::EventType type,
          ajax::ResultBox* to,
          OBJECT* obj,
          void ( OBJECT::*callback ) ( cgicc::Cgicc&,std::ostream& )
      )
      {
        this->getEventHandler()->setEvent ( from,type,to,obj,callback );
      }

      bool hasHandler ( const std::string& id, ajax::Eventable::EventType type );
      //!Calls the corresponding handler in all the tree. Reads the 'id' and 'eventType' of the Cgicc object
      //! and looks if exists a handler with the corresponding values (or default handler if exist).
      void callHandler ( cgicc::Cgicc& cgi, std::ostream& out );

      //!Default representation of an AutoHandledWidget. Takes all the Widgets in the container and display
      //!them one by one.
      void html ( cgicc::Cgicc& cgi, std::ostream& out );


    private:
      ajax::EventHandler* getEventHandler();
      ajax::EventHandler* eHandler_;
    protected:
      tstoolbox::Mutex mutex_;
  };

}
#endif /*AutoHandledWidget_H_*/
