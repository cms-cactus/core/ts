#ifndef _ajax_h_
#define _ajax_h_

#include "ajax/AutoHandlerWidget.h"
#include "ajax/Container.h"
#include "ajax/Div.h"
#include "ajax/Eventable.h"
#include "ajax/EventHandler.h"
#include "ajax/Exception.h"
#include "ajax/NullWidget.h"
#include "ajax/Page.h"
#include "ajax/PlainHtml.h"
#include "ajax/PolymerElement.h"
#include "ajax/Registry.h"
#include "ajax/ResultBox.h"
#include "ajax/Table.h"
#include "ajax/toolbox.h"
#include "ajax/Widget.h"

#endif
