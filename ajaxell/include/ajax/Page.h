#ifndef _ajax_Page_h_
#define _ajax_Page_h_

#include "ajax/AutoHandledWidget.h"
#include "ts/toolbox/Mutex.h"

#include <string>
#include <ostream>
#include <set>
#include <utility>
#include "ajax/RGB.h"

namespace ajax
{

  //! Users of the library should inherit from this clas to create his own web pages
  class Page: public AutoHandledWidget
  {
    public:
      Page ( const std::string& url );
      ~Page()
      {
        ;
      };

      //!Appends the html/javascript code of the widget intto the stream
      virtual void html ( cgicc::Cgicc& cgi, std::ostream& out );

    public:
      void setSessionId ( const std::string& sessionId );
      std::string getSessionId() const;
      std::string getAppPath();
      void setAppPath(const std::string& appPath);
      std::string getImports();
      void setImports(const std::string& imports);
      void setPrimaryColor(ajax::RGB color);
      void setPrimaryColor(int red, int green, int blue);
      void setSecondaryColor(ajax::RGB color);
      void setSecondaryColor(int red, int green, int blue);

      ajax::RGB getPrimaryColor();
      ajax::RGB getSecondaryColor();

    protected:
      std::string getUrl();

    private:
      std::string url_;
      std::string sessionid_;
      std::string appPath_;
      std::string imports_;
      ajax::RGB primaryColor_;
      ajax::RGB secondaryColor_;
      void printPreBody ( cgicc::Cgicc& cgi, std::ostream& out );
      void printPostBody ( cgicc::Cgicc& cgi, std::ostream& out );

  };

}

#endif
