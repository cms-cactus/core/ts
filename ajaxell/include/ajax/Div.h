#ifndef _ajax_Div_h_
#define _ajax_Div_h_

#include "ajax/Widget.h"
#include "ajax/Container.h"
#include "ajax/Eventable.h"

#include <string>
#include <ostream>

namespace ajax
{

  //!Div widget Widgets are the Containers used to display response to events.
  class Div: public Eventable, public Container
  {
    public:

      Div();

      //!insert the html code into the stream
      void html ( cgicc::Cgicc& cgi, std::ostream& out );


  };

}
#endif
