#ifndef _ajax_JSONTree_h_
#define _ajax_JSONTree_h_

#include "ajax/Widget.h"
#include "ajax/Container.h"

#include <string>
#include <vector>
#include <ostream>
#include <set>
#include <iostream>

namespace ajax
{

  class JSONTreeNode;

  //! This class is used to draw JSONTreeView or acyclic graphs in the usual folder/file representation.
  //! You should add JSONTreeNodes to the JSONTree.
  class JSONTree: public Widget, public Container
  {
    public:
      JSONTree();

      //!Creates the html/javascript code of the widget
      void html ( cgicc::Cgicc& cgi, std::ostream& out );

      //!adds a node to the JSONTree
      void add ( ajax::JSONTreeNode* node );

    private:

      void add ( ajax::Widget& node )
      {
        ;
      };

  };

}

#endif
