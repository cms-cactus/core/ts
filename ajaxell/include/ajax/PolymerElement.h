#ifndef _ajax_PolymerElement_h_
#define _ajax_PolymerElement_h_

#include "ajax/Widget.h"
#include "ajax/Container.h"
#include "ajax/Eventable.h"

#include <string>
#include <ostream>

namespace ajax
{

  class PolymerElement: public Eventable, public Container
  {
    public:

      PolymerElement();
      PolymerElement( const std::string& );
      void setElementName ( const std::string& );
      std::string getElementName ();
      //!insert the html code into the stream
      void html ( cgicc::Cgicc& cgi, std::ostream& out );
    protected:
      std::string name_;
  };

}
#endif
