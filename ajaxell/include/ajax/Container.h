#ifndef _ajax_Container_h_
#define _ajax_Container_h_

#include "ajax/Widget.h"

#include <vector>

namespace ajax
{

  class AutoHandledWidget;
  class EventHandler;
  class Page;

  class Container
  {
      friend class AutoHandledWidget;
      friend class EventHandler;
      friend class Page;

    public:
      Container();
      //!A Widget contained in the vector is deleted if getIsOwned()=true
      virtual ~Container();

      virtual void add ( ajax::Widget* widget );
      //!remove from the container the first widget that has the same id than the passed argument if Widget::getIsOwned() == true. If is
      //!false then remove from the vector only
      virtual void remove ( ajax::Widget* widget );
      //!remove from the container the first widget that has the same id. Is the widget has getIsOwned()==true destroy it.
      virtual void remove ( const std::string& id );
      //!clears the container and, destroys the widgest that has getIsOwned == true
      virtual void remove();

      virtual std::vector<ajax::Widget*>& getWidgets();

      //!returns null pointer if nothing found. Deep search to any number of levels
      ajax::Widget* find ( const std::string& id );

    private:
      //!Gets all the widgets in the tree of containers
      std::vector<ajax::Widget*> getAllWidgets();

    private:
      std::vector<ajax::Widget*> widgets_;
      bool isOwner_;
  };

}
#endif
