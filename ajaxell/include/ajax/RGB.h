#ifndef _ajax_RGB_h_
#define _ajax_RGB_h_

#include <string>

namespace ajax {
  class RGB {
    public:
      RGB();
      RGB(const std::string hex);
      RGB(const int red, const int green, const int blue);
      int red;
      int green;
      int blue;
      std::string toHex() const;
      RGB lighten(int percent);
      RGB darken(int percent);
  };
  std::ostream& operator<<(std::ostream &out, const RGB &rgb);
}

#endif
