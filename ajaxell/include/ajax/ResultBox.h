#ifndef _ajax_ResultBox_h_
#define _ajax_ResultBox_h_

#include "ajax/Widget.h"
#include "ajax/Container.h"


#include <string>
#include <ostream>

namespace ajax
{

  //!ResultBox and Dialog Widgets are the Containers used to display response to events.
  class ResultBox: public Widget, public Container
  {
    public:

      ResultBox();

      //void setIsResizable(bool isResizable);

      //!insert the html code into the stream
      void html ( cgicc::Cgicc& cgi, std::ostream& out );

      //!Helper method that prints the inside content of the ResultBox. To be used inside the event handlers
      void innerHtml ( cgicc::Cgicc& cgi,std::ostream& out );

      //	private:
      //bool getIsResizable();
      //bool isResizable_;
  };

}
#endif
