#ifndef _ajax_Registry_h_
#define _ajax_Registry_h_

#include "ts/toolbox/ReadWriteLock.h"

#include "ajax/Eventable.h"

#include <map>
#include <string>

namespace ajax
{
  //! Singleton class used to register the existence of eventable widgets that has events
  class EventHandler;
  class Registry
  {

    private:
      //typedef std::pair<std::string, NotificationType> NotifierId;
      typedef std::map<Eventable*,EventHandler* > EventHandlerMap;

    public:
      //!Instanciates the Singleton if not and gives a reference to it
      static Registry& getRegistry();

      //!Registers the EventHandler being used ofr a given Eventable Widget
      void add ( ajax::Eventable* widget,ajax::EventHandler* handler );

      //!remove the widget information from the Registry
      void remove ( ajax::Eventable* widget );

      //!remove the EventHandler information from the Registry
      void remove ( ajax::EventHandler* handler );

      //!Gets the EventHandler that corresponds to a given widget
      EventHandler* getEventHandler ( ajax::Eventable* widget );

    private:
      Registry()
      {
        ;
      };
      Registry ( const Registry& );
      const Registry& operator= ( const Registry& );

    private:
      static Registry* instance_;

    private:
      EventHandlerMap handlers_;
      static tstoolbox::ReadWriteLock rwlock_;

  };

}
#endif
