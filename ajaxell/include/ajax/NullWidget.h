#ifndef _ajax_NullWidget_h_
#define _ajax_NullWidget_h_

#include "ajax/Widget.h"

namespace ajax
{

  //!Used internally
  class NullWidget: public ajax::Widget
  {
    public:
      virtual void html ( cgicc::Cgicc& cgi, std::ostream& out )
      {
        ;
      };
  };

}
#endif /*WIDGET_H_*/
