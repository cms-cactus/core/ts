#ifndef _ajax_Widget_h_
#define _ajax_Widget_h_

#include "cgicc/Cgicc.h"

#include <string>
#include <ostream>
#include <set>
#include <map>

namespace cgicc
{
  class Cgicc;
}

namespace ajax
{

  class Widget
  {
    public:

      Widget();
      virtual ~Widget();

      //!Sets the value of the isOwned flag. If isOwned=true the container will destroy the widget after calling Container::remove
      void setIsOwned ( bool isOwned );
      //!Gets teh value of the isOwned flag. If isOwned=true the container will destroy the widget after calling Container::remove
      bool getIsOwned();

      //!Widget unique identifier. The constructor assigns one by default but can be changed.
      void setId ( const std::string& id );
      //!Returns the unique identifier of the widget
      std::string getId();

      //!Caption is a label assigned to any Widget. Not all the Widgets has label, but almost. By Default
      //!is assigned the id as caption of the widget.
      void setCaption ( const std::string& caption );
      std::string getCaption();


      //!This method creates the html/javascript code that you want to put in the webpage. Usually only the html method of the
      //!Page Widget is used.
      virtual void html ( cgicc::Cgicc&,std::ostream& out ) = 0;

      //!Sets a pair name/value for a given attribute. The attributes correspond to the ones of the html code
      void set ( const std::string& attribute, const std::string& value );
      //!Removes an attribute from the current list of attributes
      void unset ( const std::string& attribute );

      //!Retrieve the session id from the CGI request
      std::string getSessionId ( cgicc::Cgicc& cgi );

    protected:
      void attributes ( std::ostream& out );
      void JSONattributes ( std::ostream& out, bool omitLastComma );
    private:
      std::string id_;
      std::string caption_;
      bool isOwned_;
      static unsigned long lastId_;
      std::map<std::string,std::string> attributes_;
  };

}
#endif /*WIDGET_H_*/
