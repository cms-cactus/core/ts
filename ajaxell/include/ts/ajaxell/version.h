#ifndef _ts_ajaxell_version_h_
#define _ts_ajaxell_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define TS_TSAJAXELL_VERSION_MAJOR 5
#define TS_TSAJAXELL_VERSION_MINOR 2
#define TS_TSAJAXELL_VERSION_PATCH 4

//
// Template macros
//
#define TS_TSAJAXELL_VERSION_CODE PACKAGE_VERSION_CODE(TS_TSAJAXELL_VERSION_MAJOR,TS_TSAJAXELL_VERSION_MINOR,TS_TSAJAXELL_VERSION_PATCH)
#ifndef TS_TSAJAXELL_PREVIOUS_VERSIONS
#define TS_TSAJAXELL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TS_TSAJAXELL_VERSION_MAJOR,TS_TSAJAXELL_VERSION_MINOR,TS_TSAJAXELL_VERSION_PATCH)
#else
#define TS_TSAJAXELL_FULL_VERSION_LIST  TS_TSAJAXELL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TS_TSAJAXELL_VERSION_MAJOR,TS_TSAJAXELL_VERSION_MINOR,TS_TSAJAXELL_VERSION_PATCH)
#endif

namespace tsajaxell
{
  const std::string project = "ts";
  const std::string package  =  "tsajaxell";
  const std::string versions =  TS_TSAJAXELL_FULL_VERSION_LIST;
  const std::string description = "AjaXell CGI Widget library";
  const std::string authors = "Ildefons Magrans de Abril, Marc Magrans de Abril";
  const std::string summary = "Contains a CGI C++ widget library to be used within the XDAQ framework (https://savannah.cern.ch/projects/l1ts/)";
  const std::string link = "https://savannah.cern.ch/projects/l1ts/";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}
#endif
