/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons Magrans                                             *
 *************************************************************************/

#include "ts/framework/CellXhannelRequest.h"
#include "ts/framework/CellObject.h"
#include "ts/toolbox/CellToolbox.h"
#include "ts/toolbox/CellNamespaceURI.h"

#include "log4cplus/logger.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include <sys/time.h>

using namespace std; 

namespace tsframework
{

CellXhannelRequest::CellXhannelRequest()
{
    Init();
}


void CellXhannelRequest::Init()
{
    ostringstream id;
    id << typeid ( this ).name() << "." << this << "." << rand();
    requestId_ = id.str();
    msg_request_ = 0;
    msg_reply_ = 0;
}


CellXhannelRequest::~CellXhannelRequest()
{
    if ( msg_request_ ) {
      
        delete msg_request_;
    }

    msg_request_ = 0;

    if ( msg_reply_ ) {
      
        delete msg_reply_;
    }

    msg_reply_ = 0;
}


string CellXhannelRequest::getRequestId()
{
    return requestId_;
}


xoap::MessageReference* CellXhannelRequest::getRequest()
{
    return msg_request_;
}

void CellXhannelRequest::setReply ( xoap::MessageReference msg )
{
    msg_reply_ = new xoap::MessageReference ( msg );
}


xoap::MessageReference*
CellXhannelRequest::getReply()
{
    return msg_reply_;
}


bool CellXhannelRequest::hasResponse()
{
    if ( msg_reply_ != 0 ) {
      
        return true;
    }

    return false;
}


// --------------------------------------------------------
void CellXhannelRequest::setSendTimestamp()
{
  gettimeofday(&mSendTimestamp,NULL);
}


// --------------------------------------------------------
struct timeval CellXhannelRequest::getSendTimestamp()
{
  return mSendTimestamp;
}


// --------------------------------------------------------
void CellXhannelRequest::setReplyTimestamp()
{
  gettimeofday(&mReplyTimestamp,NULL);
}


// --------------------------------------------------------
struct timeval CellXhannelRequest::getReplyTimestamp()
{
  return mReplyTimestamp;
}

} // end ns tsframework

