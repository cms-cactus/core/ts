/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons & Marc Magrans de Abril               		 *
 *************************************************************************/

#include "ts/framework/RefreshFunctional.h"

//#include "xdaq/Application.h"
//#include "xdata/xdata.h"
//
//export template <class OBJECT,class ITEM_CLASS>
//RefreshFunctional< OBJECT,ITEM_CLASS>::RefreshFunctional(OBJECT* object, void (OBJECT::*refreshMethod)(ITEM_CLASS&))
//	:object_(object),refreshMethod_(refreshMethod)
//{
//	;
//}
//
//export template <class OBJECT,class ITEM_CLASS>
//RefreshFunctional< OBJECT,ITEM_CLASS>::~RefreshFunctional()
//{
//	;
//}
//
//export template <class OBJECT,class ITEM_CLASS>
//void RefreshFunctional< OBJECT,ITEM_CLASS>::refresh(xdata::Serializable* value)
//{
//	(object_->*(this->refreshMethod_))(dynamic_cast<ITEM_CLASS&>(*value));
//}

//template class RefreshFunctional<xdaq::Application*,xdata::String>;
//template class RefreshFunctional<xdaq::Application,xdata::Integer>;
//template class RefreshFunctional<xdaq::Application,xdata::Float>;
//template class RefreshFunctional<xdaq::Application,xdata::Double>;
//template class RefreshFunctional<xdaq::Application,xdata::Boolean>;
//template class RefreshFunctional<xdaq::Application,xdata::UnsignedLong>;
//template class RefreshFunctional<xdaq::Application,xdata::UnsignedShort>;





