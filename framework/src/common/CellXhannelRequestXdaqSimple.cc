/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril                                *
 *************************************************************************/

#include "ts/framework/CellXhannelRequestXdaqSimple.h"
#include "ts/framework/CellObject.h"

#include "ts/exception/CellException.h"
 
#include "ts/toolbox/CellToolbox.h"
#include "ts/toolbox/CellNamespaceURI.h"

#include <string>
#include <cstring>
#include <sstream>

#include "log4cplus/logger.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/SOAPConstants.h"
#include "xdaq/exception/Exception.h"

tsframework::CellXhannelRequestXdaqSimple::CellXhannelRequestXdaqSimple ( log4cplus::Logger& log,CellAbstractContext* context )
{
  setContext ( context );
  logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellXhannelRequestXdaqSimple" );
}

tsframework::CellXhannelRequestXdaqSimple::~CellXhannelRequestXdaqSimple()
{
  ;
}

void tsframework::CellXhannelRequestXdaqSimple::doCommand ( const std::string& msg )
{
  char* b = new char[msg.size() +1];
  std::strcpy ( b,msg.c_str() );
  xoap::MessageReference request = xoap::createMessage ( b, strlen ( b ) +1 );
  delete [] b;
  msg_request_ = new xoap::MessageReference ( request ); 
}
  
void tsframework::CellXhannelRequestXdaqSimple::doCommand ( xoap::MessageReference msg )
{
  msg_request_ = new xoap::MessageReference ( msg );
}

xoap::MessageReference
tsframework::CellXhannelRequestXdaqSimple::commandReply()
{
  return *msg_reply_;
}


void tsframework::CellXhannelRequestXdaqSimple::doCommand( const std::string& command, const std::string& nameSpace )
{
  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  envelope.addNamespaceDeclaration("xsi",
                                   "http://www.w3.org/2001/XMLSchema-instance");
  envelope.addNamespaceDeclaration("xsd",
                                   "http://www.w3.org/2001/XMLSchema");
  envelope.addNamespaceDeclaration("soappenc",xoap::SOAPConstants::URI_NS_SOAP_ENCODING);
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName commandName = envelope.createName(command,
                                                   "xdaq",
                                                   nameSpace);
  xoap::SOAPElement commandElement = body.addBodyElement(commandName);
  commandElement.setTextContent(command +">");

  msg_request_ = new xoap::MessageReference ( msg );
}

xdata::Serializable*
tsframework::CellXhannelRequestXdaqSimple::parameterGetReply()
{
  if ( !msg_reply_ )
  {
    XCEPT_RAISE ( tsexception::XhannelUsageError,"Can not parse non-received reply" );
  }

  xdata::Serializable* serial = tstoolbox::getXdaqParameter ( *msg_reply_ );
  return serial;
}


void tsframework::CellXhannelRequestXdaqSimple::doParameterSet(const std::string& name, const std::string& nameSpace, const std::string& type, const std::string& value)
{
  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  envelope.addNamespaceDeclaration("xsi",
                                   "http://www.w3.org/2001/XMLSchema-instance");
  envelope.addNamespaceDeclaration("xsd",
                                   "http://www.w3.org/2001/XMLSchema");
  envelope.addNamespaceDeclaration("soappenc",xoap::SOAPConstants::URI_NS_SOAP_ENCODING);
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName commandName = envelope.createName("ParameterSet",
                                                   "xdaq",
                                                   "urn:xdaq-soap:3.0");
  xoap::SOAPElement commandElement = body.addBodyElement(commandName);
  xoap::SOAPName propertiesName = envelope.createName("properties",
                                                      "p",
                                                      nameSpace);
  xoap::SOAPElement propertiesElement = commandElement.addChildElement(propertiesName);
  xoap::SOAPName propertiesTypeName = envelope.createName("type",
                                                          "xsi",
                                                          "http://www.w3.org/2001/XMLSchema-instance");
  propertiesElement.addAttribute(propertiesTypeName, "soapenc:Struct");
  
  xoap::SOAPName propertyName = envelope.createName(name, "p", nameSpace);
  xoap::SOAPElement propertyElement = propertiesElement.addChildElement(propertyName);
  xoap::SOAPName propertyTypeName = envelope.createName("type",
                                                          "xsi",
                                                          "http://www.w3.org/2001/XMLSchema-instance");
  propertyElement.addAttribute(propertyTypeName, "xsd:"+type);
  propertyElement.setTextContent(value + "</p:" + name + ">");

  msg_request_ = new xoap::MessageReference ( msg );
  
}


void tsframework::CellXhannelRequestXdaqSimple::doParameterGet ( const std::string& name, const std::string& nameSpace, const std::string& type )
{
  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
  envelope.addNamespaceDeclaration("xsi",
                                   "http://www.w3.org/2001/XMLSchema-instance");
  envelope.addNamespaceDeclaration("xsd",
                                   "http://www.w3.org/2001/XMLSchema");
  envelope.addNamespaceDeclaration("soappenc",xoap::SOAPConstants::URI_NS_SOAP_ENCODING);
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName commandName = envelope.createName("ParameterGet",
                                                   "xdaq",
                                                   "urn:xdaq-soap:3.0");
  xoap::SOAPElement commandElement = body.addBodyElement(commandName);
  xoap::SOAPName propertiesName = envelope.createName("properties",
                                                      "p",
                                                      nameSpace);
  xoap::SOAPElement propertiesElement = commandElement.addChildElement(propertiesName);
  xoap::SOAPName propertiesTypeName = envelope.createName("type",
                                                          "xsi",
                                                          "http://www.w3.org/2001/XMLSchema-instance");
  propertiesElement.addAttribute(propertiesTypeName, "soapenc:Struct");
  
  xoap::SOAPName propertyName = envelope.createName(name, "p", nameSpace);
  xoap::SOAPElement propertyElement = propertiesElement.addChildElement(propertyName);
  xoap::SOAPName propertyTypeName = envelope.createName("type",
                                                          "xsi",
                                                          "http://www.w3.org/2001/XMLSchema-instance");
  propertyElement.addAttribute(propertyTypeName, "xsd:"+type);

  msg_request_ = new xoap::MessageReference ( msg );
}
