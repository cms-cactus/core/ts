#include "ts/framework/CellOpReset.h"

#include "ts/toolbox/CellToolbox.h"

#include "ts/framework/CellOperation.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellOperationFactory.h"

#include "ts/exception/CellException.h"

#include "xcept/tools.h"

using namespace std; 

namespace tsframework
{

CellOpReset::CellOpReset ( log4cplus::Logger& log,CellAbstractContext* context )
:
CellCommand ( log,context )
{
    logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellOpReset" );
}

void 
CellOpReset::code()
{
    string opid = tstoolbox::getOpid ( getMsg() );
    CellOperationFactory* f = getContext()->getOperationFactory();

    tstoolbox::MutexHandler handler ( f->getOperation ( opid ).getLock() );

    CellOperation& op = f->getOperation ( opid );
    op.setParameterMap ( this->getParamList() );
    op.setResult ( "" );
    op.getWarning().clear();
    LOG4CPLUS_INFO ( getLogger(),opid << ".reset..." );
    op.reset();

    string str = "Reset has been executed. Current State: '" + op.getFSM().getState() + "'";
    payload_->fromString ( str );
    getWarning().setLevel ( op.getWarning().getLevel() );
    getWarning().setMessage ( op.getWarning().getMessage() );
}
} // end ns tsframework

