/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril                                    *
 *************************************************************************/

#include "ts/framework/CellXhannelRequestMonitor.h"

#include "ts/exception/CellException.h"

#include "xdaq/ApplicationDescriptor.h"

#include "xdata/Table.h"
#include "xdata/TableIterator.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"

#include <string>

using namespace std; 

namespace tsframework
{

CellXhannelRequestMonitor::CellXhannelRequestMonitor ( log4cplus::Logger& log, CellAbstractContext* context, const xdaq::ApplicationDescriptor* target )
:
CellXhannelRequest(),
buffer_ ( 0 ), 
size_ ( 0 ), 
target_ ( target )
{
    setContext ( context );
    isRetrieveExdr_ = false;
    logger_ = log4cplus::Logger::getInstance ( log.getName() + ".CellXhannelRequestMonitor" );
}


CellXhannelRequestMonitor::~CellXhannelRequestMonitor()
{
    if ( buffer_ && size_ )
    {
        free ( buffer_ );
    }
}

void 
CellXhannelRequestMonitor::doRetrieveCatalog()
{
    isRetrieveExdr_ = false;
    curlQuery_ = getTargetUrl();
    curlQuery_ += "/" + getTargetUrn();
    curlQuery_ += "/retrieveCatalog?fmt=plain";
}


vector<string>
CellXhannelRequestMonitor::retrieveCatalogReply()
{

    if ( isRetrieveExdr_ )
    {
        ostringstream msg;
        msg << "Trying to parse the reply for retrieving the Monitor Catalog without creating the apropiate XhannelRequest initialization";
        XCEPT_RAISE ( tsexception::XhannelUsageError, msg.str() );
    }

    vector<string> substrings;
    getSubstrings ( getCurlReply(), "\n", substrings );

    if ( !substrings.empty() )
    {
        substrings.erase ( substrings.begin() );
    }

    return substrings;
}


void
CellXhannelRequestMonitor::doRetrieveCollection ( const string& flashlist )
{
    isRetrieveExdr_ = true;
    curlQuery_ = getTargetUrl();
    curlQuery_ += "/" + getTargetUrn();
    curlQuery_ += "/retrieveCollection";
    curlQuery_ += "?flash=";
    curlQuery_ += flashlist + "&fmt=exdr";
}


xdata::Table*
CellXhannelRequestMonitor::retrieveCollectionReply()
{
    if ( !isRetrieveExdr_ )
    {
        ostringstream msg;
        msg << "Trying to parse the reply for retrieving the Collection in EXDR format of Monitorables without creating the apropiate XhannelRequest initialization";
        XCEPT_RAISE ( tsexception::MonitoringError, msg.str() );
    }

    if ( ( buffer_ == 0 ) || ( size_ == 0 ) )
    {
        ostringstream msg;
        msg << "The EXDR reply is empty. Impossible to parse";
        XCEPT_RAISE ( tsexception::MonitoringError, msg.str() );
    }

    xdata::Table* result;

    try
    {
        xdata::exdr::FixedSizeInputStreamBuffer inBuffer ( buffer_,size_ );
        result = new xdata::Table();
        xdata::exdr::Serializer serializer;
        serializer.import ( result, &inBuffer );
    }
    catch ( xcept::Exception& e )
    {
        XCEPT_RAISE ( tsexception::MonitoringError,"Can not parse received buffer from xmas::las" );
    }

  return result;
}


void
CellXhannelRequestMonitor::doRetrieveExdr ( const string& flashlist )
{
    doRetrieveCollection ( flashlist );
}


map<string,xdata::Table*>
CellXhannelRequestMonitor::retrieveExdrReply()
{
    xdata::Table* t = retrieveCollectionReply();
    map<string,xdata::Table*> result;

    if ( t->getRowCount() ==0 )
    {
        delete t;
        return result;
    }

    vector<string> columns = t->getColumns();

    if ( columns.empty() )
    {
        delete t;
        return result;
    }

    unsigned int count = t->getRowCount();

    for ( xdata::TableIterator i ( t->begin() ); t->end() != i; ++i,--count )
    {
        xdata::Table* intable = new xdata::Table();

        for ( vector<string>::iterator j ( columns.begin() ); columns.end() != j; ++j )
        {
            intable->addColumn ( *j,t->getColumnType ( *j ) );
            intable->setValueAt ( 0,*j,* ( i->getField ( *j ) ) );
        }

        ostringstream msg;
        msg << count;
        result.insert ( make_pair ( msg.str(), intable ) );
    }

    delete t;
    return result;
}


void
CellXhannelRequestMonitor::doRetrieveDefinition ( const string& flashlist )
{
    doRetrieveCollection ( flashlist );
}


map<string,string>
CellXhannelRequestMonitor::retrieveDefinitionReply()
{
    xdata::Table* t=retrieveCollectionReply();
    map<string,string> result;

    if ( t->getRowCount() ==0 )
    {
        delete t;
        return result;
    }

    vector<string> columns = t->getColumns();

    if ( columns.empty() )
    {
        delete t;
        return result;
    }

    for ( vector<string>::iterator i ( columns.begin() ); columns.end() != i; ++i )
    {
        result.insert ( make_pair ( *i,t->getColumnType ( *i ) ) );
    }

    delete t;
    return result;
}

void
CellXhannelRequestMonitor::setCurlReply ( const string& curlReply )
{
    curlReply_ = curlReply;
}

string
CellXhannelRequestMonitor::getCurlReply()
{
    return curlReply_;
}

string
CellXhannelRequestMonitor::getCurlQuery()
{
    return curlQuery_;
}

void
CellXhannelRequestMonitor::getSubstrings ( const string& str,const string& separators,vector<string>& substrings )
{
    string actual ( "" );
    substrings.clear();

    for ( size_t i=0; i < str.size(); ++i )
    {
     
        if ( isSeparator ( str[i],separators ) )
        {
          
            if ( !actual.empty() )
            {
                substrings.push_back ( actual );
                actual ="";
            }

            for ( ; ( i<str.size() ) && isSeparator ( str[i],separators ); ++i )
            {
                ;
            }

            if ( ( i <str.size() ) && !isSeparator ( str[i],separators ) )
            {
                actual += str[i];
            }
        }
        else
        {
            actual += str[i];
        }
    }

    if ( !actual.empty() )
    {
        substrings.push_back ( actual );
    }
}


bool
CellXhannelRequestMonitor::isSeparator ( char c, const string& separators )
{
    for ( string::const_iterator i ( separators.begin() ); i!=separators.end(); ++i )
    {
        if ( *i == c )
        {
            return true;
        }
    }

    return false;
}

string CellXhannelRequestMonitor::getTargetUrl()
{
    return target_->getContextDescriptor()->getURL();
}

string CellXhannelRequestMonitor::getTargetUrn()
{
    return target_->getURN();
}

} // end ns tsframework
