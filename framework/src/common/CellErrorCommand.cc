/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/
#include "ts/framework/CellErrorCommand.h"

#include "ts/framework/CellWarning.h"

using namespace std;


namespace tsframework
{


CellErrorCommand::CellErrorCommand ( log4cplus::Logger& log,CellAbstractContext* context )
:
CellCommand ( log,context )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellErrorCommand" );
}


void CellErrorCommand::code()
{
	getWarning().setLevel ( CellWarning::ERROR );
	getWarning().setMessage ( errorMessage_ );
}


void CellErrorCommand::setErrorMessage ( const string& error )
{
	errorMessage_= error;
}

} // end ns tsframework
