#include "ts/framework/Level1Source.h"

#include "ts/framework/CellOperation.h"
#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellAbstractContext.h"

#include "ts/toolbox/Mutex.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "xdata/Table.h"
#include "xdata/UnsignedInteger32.h"

#include "xcept/tools.h"

#include "toolbox/ProcessInfo.h"
#include "toolbox/Runtime.h"



using namespace std; 

namespace tsframework
{


Level1Source::Level1Source ( CellAbstractContext* context, log4cplus::Logger& logger )
: 
PeriodicSource ( "l1ts-cell", context, logger, REFRESH_INTERVAL_SEC ),
callback_ ( 0 ),
subsystem_ ( "Unknown" ),
autoclear_ ( true )
{
    logger_ = log4cplus::Logger::getInstance ( logger.getName() +".Level1Source" );

    add<xdata::String> ( "urn" );
    put ( "urn", xdata::String ( jsonEscape ( context->getLocalUrn() ) ) );

    add<xdata::String> ( "subsystem" );
    put ( "subsystem", xdata::String ( jsonEscape ( getSubsystem() ) ) );

    add<xdata::String> ( "name" );
    put ( "name", xdata::String ( jsonEscape ( context->getCell()->getName() ) ) );

    add<xdata::Integer> ( "PID" );
    add<xdata::Double> ( "CPU" );
    add<xdata::UnsignedInteger32> ( "VSZ" );
    add<xdata::UnsignedInteger32> ( "RSS" );

    this->processInfo_ = toolbox::getRuntime()->getProcessInfo ( toolbox::getRuntime()->getPid() );
    add<xdata::Table> ( "Operations" );
    add<xdata::Table> ( "Alarms" );
    add<xdata::Table> ( "Rates" );
}


void 
Level1Source::periodicAction()
{
    LOG4CPLUS_DEBUG ( getLogger(),"Updating information of the 'l1ts-cell' infospace" );
    put ( "name", xdata::String ( jsonEscape ( getContext()->getCell()->getName() ) ) );
    put ( "subsystem", xdata::String ( jsonEscape ( getSubsystem() ) ) );
    updateProcessInfo();
    updateOperationStatus();

    try
    {
        if ( callback_ )
        {
            callback_->invoke();
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getLogger(), xcept::stdformat_exception_history ( e ) );
    }
    catch ( exception& e )
    { 
        LOG4CPLUS_ERROR ( getLogger(), "exception caught while executing Level1Source::periodicAction(): " << e.what() );
    }
    catch ( ... )
    {
        LOG4CPLUS_ERROR ( getLogger(), "Unknown exception caught while executing Level1Source::periodicAction()" );
    }

    updateAlarms();
    updateRates();
    push();

    if ( autoclear_ )
    {
        clear();
    }
}

void 
Level1Source::updateProcessInfo()
{
    processInfo_->sample();
    put ( "PID", xdata::Integer ( processInfo_->pid() ) );
    put ( "CPU",xdata::Double ( processInfo_->cpuUsage() ) );
    put ( "VSZ",xdata::UnsignedInteger32 ( processInfo_->vsize() ) );
    put ( "RSS",xdata::UnsignedInteger32 ( processInfo_->rsize() ) );
    ostringstream msg;
    msg << "pid = "<< processInfo_->pid() << ", vsz = " << processInfo_->vsize() << ", rss = " << processInfo_->rsize() <<", cpu = " << processInfo_->cpuUsage();
    LOG4CPLUS_DEBUG ( getLogger(),msg.str() );
}


void 
Level1Source::updateOperationStatus()
{
    xdata::Table opStatus;
    opStatus.addColumn ( "className","string" );
    opStatus.addColumn ( "id","string" );
    opStatus.addColumn ( "state","string" );
    opStatus.addColumn ( "result","string" );
    opStatus.addColumn ( "warningLevel","int" );
    opStatus.addColumn ( "warningMessage","string" );
    vector<string> ops ( getContext()->getOperationFactory()->getOperations() );
    size_t col ( 0 );

    for ( vector<string>::const_iterator i ( ops.begin() ); i != ops.end(); )
    {
        string opid ( *i );
        string className ( "" );
        string state ( "" );
        string wngMsg ( "" );
        int wngLevel ( 0 );
        string result ( "" );

        try
        {
            CellOperation& op ( getContext()->getOperationFactory()->getOperation ( opid ) );
            tstoolbox::MutexHandler handler ( op.getLock() );
            className = typeid ( op ).name();
            result = op.getResult();
            wngMsg = op.getWarning().getMessage();
            wngLevel = op.getWarning().getLevel();
            CellFSM::State s = op.getFSM().getCurrentState();
            state = op.getFSM().getStateName ( s );
        }
        catch ( xcept::Exception& e )
        {
            ostringstream msg;
            msg << "Error trying to retrieve status information from operation '" << opid << "'. ";
            msg << xcept::stdformat_exception_history ( e );
            LOG4CPLUS_WARN ( getLogger(),msg.str() );
            //!If some operation was deleted during the loop, we start again
            opStatus.clear();
            ops =getContext()->getOperationFactory()->getOperations();
            i = ops.begin();
        }

        xdata::String x_class ( jsonEscape ( className ) );
        opStatus.setValueAt ( col,"className",x_class );
        xdata::String x_id ( jsonEscape ( opid ) );
        opStatus.setValueAt ( col,"id",x_id );
        xdata::String x_state ( jsonEscape ( state ) );
        opStatus.setValueAt ( col,"state",x_state );
        xdata::String x_result ( jsonEscape ( result ) );
        opStatus.setValueAt ( col,"result",x_result );
        xdata::Integer x_level ( wngLevel );
        opStatus.setValueAt ( col,"warningLevel",x_level );
        xdata::String x_wmsg ( jsonEscape ( wngMsg ) );
        opStatus.setValueAt ( col,"warningMessage",x_wmsg );
        i++;
        col++;
    }

    put ( "Operations",opStatus );
}


void 
Level1Source::clear()
{
    tstoolbox::MutexHandler handler ( getMutex() );
    alarms_.clear();
    rates_.clear();
}


void 
Level1Source::setAlarm ( const string& name, SeverityLevel severity, const string& details )
{
    Alarm a;
    a.details = details;
    a.severity = severity;
    tstoolbox::MutexHandler handler ( getMutex() );
    alarms_[name] = a;
}

void 
Level1Source::setRate ( const string& name, double value, SeverityLevel severity, const string& details )
{
    Rate s;
    s.value = value;
    s.severity = severity;
    s.details = details;
    tstoolbox::MutexHandler handler ( getMutex() );
    rates_[name] = s;
}

void 
Level1Source::updateAlarms()
{
    map<string,Alarm> als;
    {
        tstoolbox::MutexHandler handler ( getMutex() );
        als = alarms_;
    }

    xdata::Table alarms;
    alarms.addColumn ( "name","string" );
    alarms.addColumn ( "severity","string" );
    alarms.addColumn ( "details","string" );
    size_t col ( 0 );

    for ( map<string,Alarm>::const_iterator i ( als.begin() ); i != als.end(); i++,col++ )
    {
        xdata::String name ( jsonEscape ( i->first ) );
        xdata::String details ( jsonEscape ( i->second.details ) );
        xdata::String severity ( jsonEscape ( severityLevel2String ( i->second.severity ) ) );
        alarms.setValueAt ( col,"name",name );
        alarms.setValueAt ( col,"severity",severity );
        alarms.setValueAt ( col,"details",details );
    }

    put ( "Alarms",alarms );
}


void 
Level1Source::updateRates()
{
    map<string,Rate> r;
    {
        tstoolbox::MutexHandler handler ( getMutex() );
        r = rates_;
    }

    xdata::Table rates;
    rates.addColumn ( "name","string" );
    rates.addColumn ( "value","double" );
    rates.addColumn ( "severity","string" );
    rates.addColumn ( "details","string" );
    size_t col ( 0 );

    for ( map<string,Rate>::const_iterator i ( r.begin() ); i != r.end(); i++,col++ )
    {
        xdata::String name ( jsonEscape ( i->first ) );
        xdata::Double value ( i->second.value );
        xdata::String severity ( jsonEscape ( severityLevel2String ( i->second.severity ) ) );
        xdata::String details ( jsonEscape ( i->second.details ) );
        rates.setValueAt ( col,"name",name );
        rates.setValueAt ( col,"value",value );
        rates.setValueAt ( col,"severity",severity );
        rates.setValueAt ( col,"details",details );
    }

    put ( "Rates",rates );
}


string
Level1Source::severityLevel2String ( SeverityLevel l )
{
    if ( l <= INFO )
    {
        return "INFO";
    }
    else if ( l <= WARNING )
    {
        return "WARNING";
    }
    else
    {
        return "ERROR";
    }
}


string 
Level1Source::getSubsystem()
{
    return subsystem_;
}


void 
Level1Source::setSubsystem ( const string& subsystem )
{
    subsystem_ = subsystem;
}

void 
Level1Source::replace ( string& str,const string& from, const string& to )
{
    if ( from.empty() )
    {
        return;
    }

    size_t pos ( 0 );
    pos=str.find ( from,pos );

    while ( pos != string::npos )
    {
        str.erase ( pos,from.size() );
        str.insert ( pos,to );
        pos += to.size();
        pos = str.find ( from,pos+to.size() );
    }
}


string 
Level1Source::jsonEscape ( const string& result )
{
    string str ( result );
    replace ( str,"\\","\\\\" );
    replace ( str,"\"","\\\"" );
    replace ( str,"\n","\\n" );
    replace ( str,"\r","\\r" );
    replace ( str,"\b","\\b" );
    replace ( str,"\f","\\f" );
    replace ( str,"\t","\\t" );
    return str;
}


void 
Level1Source::setAutoClear ( bool autoclear )
{
    autoclear_ = autoclear;
}

} // end ns tsframework
