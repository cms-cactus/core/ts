#include "ts/framework/CellMonitorWidget.h"
#include "ajax/PolymerElement.h"
#include "ajax/toolbox.h"

#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestHandler.h"
#include "ts/framework/CellXhannelRequestMonitor.h"

#include "xdata/Table.h"
#include "xdata/TableIterator.h"
#include <jsoncpp/json/json.h>

#include <sstream>

using namespace std;
namespace tsframework {
	CellMonitorWidget::CellMonitorWidget ( CellAbstractContext* context, log4cplus::Logger& logger )
	: CellPanel ( context,logger ) {}
	CellMonitorWidget::~CellMonitorWidget() {}

	void CellMonitorWidget::init ( const string& flashlist ) {
		flashlistName_ = flashlist;
	}
	string CellMonitorWidget::getFlashlist() {
		return flashlistName_;
	}

	void CellMonitorWidget::layout ( cgicc::Cgicc& cgi ) {
		remove();
		setEvent("getTable", ajax::Eventable::OnTime, this, &CellMonitorWidget::getTable);
		setEvent("getSubTable", ajax::Eventable::OnClick, this, &CellMonitorWidget::getSubTable);
		add(new ajax::PolymerElement("ts-flashlist"));
	}

	xdata::Table* CellMonitorWidget::getData() {
		CellXhannelMonitor* mXhannel = dynamic_cast<CellXhannelMonitor*> ( getContext()->getXhannel ( "MON" ) );
		if ( !mXhannel ) {
			XCEPT_RAISE ( tsexception::XhannelDoesNotExist,"Xhannel MON is not of type CellXhannelMonitor" );
		}

		//Looking for history
		CellXhannelRequestHandler<CellXhannelMonitor> requestHandler ( mXhannel );
		requestHandler.getRequest()->doRetrieveCollection ( getFlashlist() );
		mXhannel->send ( requestHandler.getRequest() );
		return requestHandler.getRequest()->retrieveCollectionReply();
	}

	void CellMonitorWidget::getTable(cgicc::Cgicc& cgi,std::ostream& out) {
		out << xdataTableToJSON(*getData());
	}

	void CellMonitorWidget::getSubTable(cgicc::Cgicc& cgi,std::ostream& out) {
		std::string srow = ajax::toolbox::getSubmittedValue(cgi,"row");
		std::string column = ajax::toolbox::getSubmittedValue(cgi,"column");

		if (srow != "" && column != "") {
			istringstream ssrow;ssrow.str(srow);
			int row;ssrow >> row;
			xdata::Table* subdata = dynamic_cast<xdata::Table*> ( getData()->getValueAt ( row,column ) );
			out << xdataTableToJSON(*subdata);
		}
	}

	Json::Value CellMonitorWidget::xdataTableToJSON(xdata::Table& data) {
		Json::Value result;
		vector<string> xcolumns ( data.getColumns() );
		Json::Value items(Json::arrayValue);
		Json::Value columns(Json::arrayValue);
		for ( vector<string>::iterator j ( xcolumns.begin() ); j!=xcolumns.end(); ++j ) {
			Json::Value column;
			column["name"] = *j;
			column["type"] = data.getColumnType(*j);
			columns.append(column);
		}
		result["columns"] = columns;
		for(xdata::TableIterator i ( data.begin() ); i != data.end(); ++i) {
			Json::Value item;
			for ( vector<string>::iterator j ( xcolumns.begin() ); j!=xcolumns.end(); ++j ) {
				xdata::Serializable* xitem = i->getField ( *j );
				if ( xitem->type() == "table" ) {
					item[*j] = "";
				} else {
					item[*j] = xitem->toString();
				}
			}
			items.append(item);
		}
		result["items"] = items;
		return result;
	}
}
