/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun.
 *************************************************************************/


#include "ts/framework/CellOperationTypesAndParams.h"

#include "ts/framework/CellOperationParamList.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellOperationFactory.h"

#include "xdata/String.h"

#include <sstream>

using namespace std;


namespace tsframework
{


CellOperationTypesAndParams::CellOperationTypesAndParams ( log4cplus::Logger& log, CellAbstractContext* context )
:
CellCommand ( log, context )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellOperationTypesAndParams" );
}


void
CellOperationTypesAndParams::code()
{
	ostringstream result;
	vector<string> types = getContext()->getOperationFactory()->getOperationTypes();
	result << "[";

	for ( vector<string>::iterator i = types.begin(); i != types.end(); ++i )
	{
		if ( i != types.begin() )
		{
			result << ",";
		}

		result << "{\"" << *i << "\":";
		CellOperationParamList paramList ( getLogger(), getContext() );
		paramList.getParamList() ["OperationType"]->fromString ( *i );
		paramList.code();
		result << paramList.getResult().toString() << "}";
	}

	result << "]";
	delete payload_;
	this->payload_ = new xdata::String ( result.str() );
}


} // end ns tsframework
