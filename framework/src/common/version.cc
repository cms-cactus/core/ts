#include "ts/framework/version.h"

#include "config/version.h"

GETPACKAGEINFO ( tsframework )

void 
tsframework::checkPackageDependencies()
{
  	CHECKDEPENDENCY ( config );
}


std::set<std::string, std::less<std::string> > 
tsframework::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY ( dependencies,config );
	return dependencies;
}
