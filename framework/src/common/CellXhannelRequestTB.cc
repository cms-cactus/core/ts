/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/

#include "ts/framework/CellXhannelRequestTB.h"

#include "ts/toolbox/CellNamespaceURI.h"

#include "ts/exception/CellException.h"

#include "xdata/Table.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"

#include "tstore/client/Client.h"
#include "tstore/client/AttachmentUtils.h"


using namespace std; 

namespace tsframework
{


CellXhannelRequestTB::CellXhannelRequestTB ( log4cplus::Logger& log, CellAbstractContext* context,const string& view, const string& id )
:
view_ ( view ), 
id_ ( id )
{
    setContext ( context );
    logger_ =log4cplus::Logger::getInstance ( log.getName() +".CellXhannelRequestTB" );
}


CellXhannelRequestTB::~CellXhannelRequestTB()
{
    ;
}


void 
CellXhannelRequestTB::doGetRegister ( const string& table,  const string& primaryColumn, const string& primaryValue )
{
    xoap::MessageReference msg = xoap::createMessage();

    try
    {
        xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
        xoap::SOAPName msgName = envelope.createName ( "query", "tstore", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
        xoap::SOAPElement queryElement = envelope.getBody().addBodyElement ( msgName );
        //connectionID was obtained from a previous connect message
        xoap::SOAPName id = envelope.createName ( "connectionID", "tstore", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
        queryElement.addAttribute ( id, getId() );
        //add the parameters to the message (in this example we are using an SQLView, so use the appropriate namespace)
        queryElement.addNamespaceDeclaration ( "sql","urn:tstore-view-SQL" );
        xoap::SOAPName property = envelope.createName ( "name", "sql","urn:tstore-view-SQL" );
        queryElement.addAttribute ( property, "getRegister" );
        property = envelope.createName ( "table", "sql","urn:tstore-view-SQL" );
        queryElement.addAttribute ( property, table );
        property = envelope.createName ( "primaryColumn", "sql","urn:tstore-view-SQL" );
        queryElement.addAttribute ( property, primaryColumn );
        property = envelope.createName ( "primaryValue", "sql","urn:tstore-view-SQL" );
        queryElement.addAttribute ( property, primaryValue );
    }
    catch ( xoap::exception::Exception& e )
    {
        ostringstream err;
        err << "Can not create the SOAP message in doGetRegister";
        XCEPT_RETHROW ( tsexception::SoapEncodingError,err.str(),e );
    }

    msg_request_ = new xoap::MessageReference ( msg );
  }


map<string, string>
CellXhannelRequestTB::getRegisterReply()
{
    if ( !msg_reply_ )
    {
        XCEPT_RAISE ( tsexception::XhannelUsageError,"Can not parse non-received reply" );
    }

    map<string,string> result;

    try
    {
        xdata::Table t;

        if ( tstoreclient::getFirstAttachmentOfType ( *msg_reply_, t ) )
        {
            vector<string> columns ( t.getColumns() );

            for ( vector<string>::iterator i ( columns.begin() ); i!=columns.end(); ++i )
            {
                result[*i] = t.getValueAt ( 0,*i )->toString();
            }
        }
    }
    catch ( xcept::Exception& e )
    {
        ostringstream err;
        err << "Failed to import table in CellXhannelRequestTB::getRegisterReply";
        XCEPT_RETHROW ( tsexception::SoapParsingError,err.str(),e );
    }

    return result;
}

void
CellXhannelRequestTB::doGetQuery ( const string& query, map<string,string>& parameters )
{
    xoap::MessageReference msg = xoap::createMessage();

    try
    {
        xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
        xoap::SOAPName msgName = envelope.createName ( "query", "tstore", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
        xoap::SOAPElement queryElement = envelope.getBody().addBodyElement ( msgName );
        //connectionID was obtained from a previous connect message
        xoap::SOAPName id = envelope.createName ( "connectionID", "tstore", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
        queryElement.addAttribute ( id, getId() );
        //xoap::SOAPName property = envelope.createName("name", "sql","urn:tstore-view-SQL");
        //queryElement.addAttribute(property, query);
        queryElement.addNamespaceDeclaration ( "sql",  "urn:tstore-view-SQL" );
        xoap::SOAPName property = envelope.createName ( "name", "sql","urn:tstore-view-SQL" );
        queryElement.addAttribute ( property, query );

        for ( map<string,string>::iterator i ( parameters.begin() ); i != parameters.end(); ++i )
        {
            property = envelope.createName ( i->first, "sql","urn:tstore-view-SQL" );
            queryElement.addAttribute ( property, i->second );
        }
    }
    catch ( xoap::exception::Exception& e )
    {
        ostringstream msg;
        msg << "Can not create the SOAP message in query " << query;
        XCEPT_RETHROW ( tsexception::SoapEncodingError,msg.str(),e );
    }

   
    msg_request_ = new xoap::MessageReference ( msg );
}


xdata::Table* 
CellXhannelRequestTB::getQueryReply()
{
    if ( !msg_reply_ )
    {
        XCEPT_RAISE ( tsexception::XhannelUsageError,"Can not parse non-received reply" );
    }

    xdata::Table* t = new xdata::Table();

    try
    {
        tstoreclient::getFirstAttachmentOfType ( *msg_reply_,*t );
    }
    catch ( xcept::Exception& e )
    {
        ostringstream err;
        err << "Failed to import table in CellXhannelRequestTB::getQueryReply";
        XCEPT_RETHROW ( tsexception::SoapParsingError,err.str(),e );
    }

    return t;
}

void
CellXhannelRequestTB::doInsert ( const string& insertQuery, xdata::Table* table )
{
    xoap::MessageReference msg = xoap::createMessage();

    try
    {
        xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
        xoap::SOAPName msgName = envelope.createName ( "insert", "tstore", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
        xoap::SOAPElement element = envelope.getBody().addBodyElement ( msgName );
        //connectionID was obtained from a previous connect message
        xoap::SOAPName id = envelope.createName ( "connectionID", "tstore", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
        element.addAttribute ( id, getId() );
        element.addNamespaceDeclaration ( "sql", "urn:tstore-view-SQL" );
        xoap::SOAPName property = envelope.createName ( "name","sql","urn:tstore-view-SQL" );
        element.addAttribute ( property,insertQuery );
        xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
        xdata::exdr::Serializer serializer;
        serializer.exportAll ( table,&outBuffer );
        xoap::AttachmentPart* attachment = msg->createAttachmentPart ( outBuffer.getBuffer(), outBuffer.tellp(), "application/xdata+table" );
        attachment->setContentEncoding ( "exdr" );
        attachment->setContentId ( insertQuery );
        msg->addAttachmentPart ( attachment );
    }
    catch ( xdata::exception::Exception& e )
    {
        ostringstream err;
        err << "Failed to export table while creating SOAP message";
        XCEPT_RETHROW ( tsexception::SoapEncodingError,err.str(),e );
    }
    catch ( xoap::exception::Exception& e )
    {
        ostringstream err;
        err << "Failed in constructing insert message "<< insertQuery <<" in CellXhannelTB::doInsertRow()";
        XCEPT_RETHROW ( tsexception::SoapEncodingError,err.str(),e );
    }

  
    msg_request_ = new xoap::MessageReference ( msg );
}


void
CellXhannelRequestTB::doUpdate ( const string& updateQuery, xdata::Table* table )
{
    xoap::MessageReference msg = xoap::createMessage();

    try
    {
        xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
        xoap::SOAPName msgName = envelope.createName ( "update","tstore","http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
        xoap::SOAPElement element = envelope.getBody().addBodyElement ( msgName );
        xoap::SOAPName id = envelope.createName ( "connectionID", "tstore", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
        element.addAttribute ( id, getId() );
        element.addNamespaceDeclaration ( "sql", "urn:tstore-view-SQL" );
        xoap::SOAPName property = envelope.createName ( "name","sql","urn:tstore-view-SQL" );
        element.addAttribute ( property,updateQuery );
        xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
        xdata::exdr::Serializer serializer;
        serializer.exportAll ( table,&outBuffer );
        xoap::AttachmentPart* attachment = msg->createAttachmentPart ( outBuffer.getBuffer(), outBuffer.tellp(), "application/xdata+table" );
        attachment->setContentEncoding ( "exdr" );
        attachment->setContentId ( updateQuery );
        msg->addAttachmentPart ( attachment );
    }
    catch ( xdata::exception::Exception& e )
    {
        ostringstream err;
        err << "Failed to export table while creating SOAP message";
        XCEPT_RETHROW ( tsexception::SoapEncodingError,err.str(),e );
    }
    catch ( xoap::exception::Exception& e )
    {
        ostringstream err;
        err << "Failed in constructing update message "<< updateQuery <<" in CellXhannelTB::doInsertRow()";
        XCEPT_RETHROW ( tsexception::SoapEncodingError,err.str(),e );
    }

 
    msg_request_ = new xoap::MessageReference ( msg );
}


void
CellXhannelRequestTB::doInsertForTable (const string& tableName, xdata::Table* data )
{
    doWithNestedView ( "insert", tableName, data );
}


void
CellXhannelRequestTB::doDeleteForTable (const string& tableName, xdata::Table* data )
{
    if ( tableName.find ( '.' ) ==string::npos ) //< NOT qualified: OK with TStore
    {
        doWithNestedView ( "delete", tableName, data );
    }
    else //< qualified! TStore bug (https://svnweb.cern.ch/trac/cmsos/ticket/742)
    {
        XCEPT_RAISE (
          tsexception::TStoreUnsupportedQualifiedTableName,
          "The table name "+tableName+" is qualified, "
          "which is not supported by TStore in delete actions." );
    }
}


void
CellXhannelRequestTB::doWithNestedView (const string& action, const string& tableName, xdata::Table* data )
{
    xoap::MessageReference msg= xoap::createMessage();
    xoap::SOAPEnvelope envelope= msg->getSOAPPart().getEnvelope();
    xoap::SOAPName msgName = envelope.createName ( action, "tstoresoap", TSTORE_NS_URI );
    xoap::SOAPElement element = envelope.getBody().addBodyElement ( msgName );

    // set connection ID property
    string propConnectionIDNamespaceURI=    TSTORE_NS_URI;
    string propConnectionIDNamespacePrefix= "tstoresoap";
    string propConnectionIDName=            "connectionID";
    string propConnectionIDValue= getId();

    element.addNamespaceDeclaration (
        propConnectionIDNamespacePrefix,
        propConnectionIDNamespaceURI );

    xoap::SOAPName propConnectionID=
        envelope.createName (
            propConnectionIDName,
            propConnectionIDNamespacePrefix,
           propConnectionIDNamespaceURI );

    element.addAttribute (
        propConnectionID,
        propConnectionIDValue );

      // set table property (viewspecific for NestedView)
    string propTableNamespaceURI=    "urn:tstore-view-Nested";
    string propTableNamespacePrefix= "viewspecific";
    string propTableName=            "table";

    element.addNamespaceDeclaration (
        propTableNamespacePrefix,
        propTableNamespaceURI );

    xoap::SOAPName propTable=
        envelope.createName (
            propTableName,
            propTableNamespacePrefix,
            propTableNamespaceURI );

    element.addAttribute (
        propTable,
        tableName );

    tstoreclient::addAttachment ( msg, *data, tableName );
    msg_request_ = new xoap::MessageReference ( msg );
}


string 
CellXhannelRequestTB::getView()
{
    return view_;
}


string 
CellXhannelRequestTB::getId()
{
    return id_;
}

} // end ns tsframework
