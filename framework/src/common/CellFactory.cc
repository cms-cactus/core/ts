#include "ts/framework/CellFactory.h"

#include "ts/framework/CellCommand.h"

#include <thread>
#include <chrono>

using namespace std;

namespace tsframework
{

CellFactory::CellFactory ( log4cplus::Logger& log, CellAbstractContext* context )
{
	context_ = context;
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellFactory" );

	// garbage collection initialization
	gcjob_ = toolbox::task::bind ( this, &CellFactory::gcjob, "gcCommandJob" );
	gcwl_ = toolbox::task::getWorkLoopFactory()->getWorkLoop ( "gcCommandWorkLoop", "waiting" );
	gcwl_->submit ( gcjob_ );

	if ( !gcwl_->isActive() )
	{
		gcwl_->activate();
	}
}


CellFactory::~CellFactory()
{
	if ( gcwl_->isActive() )
	{
		gcwl_->cancel();
	}

	toolbox::task::getWorkLoopFactory()->removeWorkLoop ( "gcCommandWorkLoop", "waiting" );
	delete gcwl_;
	delete gcjob_;
}


void
CellFactory::dispose ( CellCommand* object )
{
	tstoolbox::ReadWriteLockHandler handle ( gclock_, tstoolbox::ReadWriteLockHandler::WRITE );
	garbage_.push_back ( object );
}


bool
CellFactory::gcjob ( toolbox::task::WorkLoop* wl )
{
	{
	tstoolbox::ReadWriteLockHandler handle ( gclock_,tstoolbox::ReadWriteLockHandler::WRITE );

	if ( !garbage_.empty() )
	{
		for ( CommandDisposalIterator i ( garbage_.begin() ); i != garbage_.end(); )
		{
			if ( ( *i )->isOver() )
			{
				delete *i;
				i = garbage_.erase ( i );
			}
			else
			{
				++i;
			}
		}
	}

	}

	std::this_thread::sleep_for(std::chrono::milliseconds(50));

	return true;
}


vector<string>
CellFactory::getGroups()
{
	tstoolbox::ReadWriteLockHandler handle ( gclock_,tstoolbox::ReadWriteLockHandler::READ );
	vector<string> result;

	for ( CommandGroupIterator i ( groups2commands_.begin() ); i != groups2commands_.end(); )
	{
		string group = i->first;

		if ( group != "System" )
		{
			result.push_back ( group );
		}

		i = groups2commands_.upper_bound ( group );
	}

	return result;
}


vector<string>
CellFactory::getCommands ( const string& group )
{
	tstoolbox::ReadWriteLockHandler handle ( gclock_,tstoolbox::ReadWriteLockHandler::READ );
	vector<string> result;
	CommandGroupIterator i = groups2commands_.find ( group );

	if ( i == groups2commands_.end() )
	{
		XCEPT_RAISE ( tsexception::GroupDoesNotExist,"The group '" + group + "' does not exist in the CellFactory" );
	}

	for ( ; i !=  groups2commands_.upper_bound ( group ); ++i )
	{
		result.push_back ( i->second );
	}

	return result;
}


CellCommand*
CellFactory::create ( const string& command )
{
	tstoolbox::ReadWriteLockHandler handle ( gclock_,tstoolbox::ReadWriteLockHandler::WRITE );
	CreatorIterator i = creators_.find ( command );

	if ( i == creators_.end() )
	{
		XCEPT_RAISE ( tsexception::CommandDoesNotExist,"The command '" + command + "' does not exist in the CellFactory. Check that has been added in Cel::init" );
	}

	CreatorSignature* c = i->second;

	CellCommand* cmd = c->create ( getLogger(),getContext() );

	return cmd;
}


bool
CellFactory::isNameCompliant ( const string& name )
{
	for ( string::const_iterator i = name.begin(); i!=name.end(); ++i )
	{
		char c ( *i );

		if ( !isalnum ( c ) && c!='.' && c!='-' && c!='_' )
		{
			return false;
		}
	}

	return true;
}

} // end ns tsframework
