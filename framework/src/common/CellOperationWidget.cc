#include "ts/framework/CellOperationWidget.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellLocalSession.h"
#include "ts/framework/CellOperation.h"
#include "ts/framework/CellOperationGraph.h"
#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellCommandPort.h"

#include "ts/toolbox/CellNamespaceURI.h"
#include "ts/toolbox/CellToolbox.h"

#include "log4cplus/loggingmacros.h"

#include "xoap/MessageReference.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPFault.h"
#include "xoap/SOAPEnvelope.h"

#include "xcept/tools.h"

#include <jsoncpp/json/json.h>

#include <iomanip>
#include <set>


using namespace std;

namespace tsframework
{
CellOperationWidget::CellOperationWidget ( CellAbstractContext* context, log4cplus::Logger& logger )
:
CellPanel ( context,logger )
{
	logger_ = log4cplus::Logger::getInstance ( logger.getName() +".CellOperationWidget" );
}

CellOperationWidget::~CellOperationWidget()
{
}


void
CellOperationWidget::layout ( cgicc::Cgicc& cgi )
{
	remove();

	ajax::PolymerElement* operation = new ajax::PolymerElement("ts-operations");
	// operation->set("id", getOperationId() );
	add( operation );

	setEvent("getCommands", ajax::Eventable::OnClick, this, &CellOperationWidget::getCommands);
	setEvent("getGraph", ajax::Eventable::OnClick, this, &CellOperationWidget::fillOperationGraph);
	setEvent("getResult", ajax::Eventable::OnTime, this, &CellOperationWidget::fillResult);
	setEvent("getInputs", ajax::Eventable::OnClick, this, &CellOperationWidget::getInputs);
	setEvent("getStatus", ajax::Eventable::OnClick, this, &CellOperationWidget::getStatus);
	setEvent("execute", ajax::Eventable::OnSubmit, this, &CellOperationWidget::formExecute);
}

void
CellOperationWidget::fillOperationGraph ( cgicc::Cgicc& cgi,std::ostream& out )
{
	CellOperationGraph* opGraph = new CellOperationGraph ( getContext(),getLogger(),getOperationId() );
	opGraph->html(cgi, out);
}

void
CellOperationWidget::getStatus ( cgicc::Cgicc& cgi,std::ostream& out ) {
	out << getOperation().getFSM().getStateName(getOperation().getFSM().getCurrentState());
}

void
CellOperationWidget::getCommands ( cgicc::Cgicc& cgi,std::ostream& out )
{
	vector<string> transitions = getPossibleCommands();
	Json::Value root(Json::arrayValue);
	if ( !transitions.empty() ) {
		for ( vector<string>::iterator i ( transitions.begin() ); i!=transitions.end(); ++i ) {
			root.append(*i);
		}
	}
	out << root;
}

void
CellOperationWidget::fillResult ( cgicc::Cgicc& cgi,std::ostream& out )
{
	Json::Value root(Json::arrayValue);
	if ( !getOperation().getResult().empty() ) {
		Json::Value someresult;
		someresult["type"] = "result";
		someresult["value"] = getOperation().getResult();
		root.append(someresult);
	} else {
		Json::Value someresult;
		someresult["type"] = "busy";
		someresult["value"] = "no results yet...";
		root.append(someresult);
	}

	//warning
	if ( !getOperation().getWarning().getMessage().empty() ) {
		Json::Value someresult;
		int level = getOperation().getWarning().getLevel();
		if ( level <= CellWarning::INFO ) {
			someresult["type"] = "info";
		}
		else if ( level <= CellWarning::WARNING ) {
			someresult["type"] = "warning";
		}
		else {
			someresult["type"] = "error";
		}
		someresult["value"] = getOperation().getWarning().getMessage();
		root.append(someresult);
	}
	out << root;
}

void
CellOperationWidget::getInputs ( cgicc::Cgicc& cgi,std::ostream& out ) {
	map<string, xdata::Serializable*> dummyParams = getOperation().getParamList();

	Json::Value root(Json::arrayValue);
	for ( map<string,xdata::Serializable*>::iterator i = dummyParams.begin(); i != dummyParams.end(); ++i ) {
		Json::Value input;
		input["name"] = i->first;
		input["type"] = i->second->type();
		input["value"] = dummyParams [i->first]->toString();
		root.append(input);
	}
	out << root;
}

void
CellOperationWidget::init ( const string& operationId )
{
	operationId_ = operationId;
}


string CellOperationWidget::getOperationId()
{
  	return operationId_;
}


void
CellOperationWidget::formExecute ( cgicc::Cgicc& cgi,ostream& out )
{
	map<string,string> values(ajax::toolbox::getSubmittedValues(cgi));
	map<string,string>::iterator valuesIt;

	vector<string> commands = getPossibleCommands();
	vector<string>::iterator commandsIt;

	map<string, xdata::Serializable*> params = getOperation().getParamList();
	map<string,xdata::Serializable*>::iterator paramsIt;

	valuesIt = values.find("command");
	string requestedCommand;
	if ( valuesIt == values.end() ) {
		LOG4CPLUS_ERROR ( getLogger(), "submitted data contains no command" );
		return;
	} else {
		requestedCommand = valuesIt->second;
		if ( strcmp(requestedCommand.c_str(), "reset") == 0 ) {
			this->CellOperationWidget::reset(cgi,out);
			return;
		}
	}

	commandsIt = std::find( commands.begin(), commands.end(), requestedCommand);
	if ( commandsIt == commands.end() ) {
		LOG4CPLUS_ERROR ( getLogger(), "command " + requestedCommand + " is not in the list of available commands" );
		return;
	}

	for(map<string,xdata::Serializable*>::iterator param(params.begin()); param != params.end(); ++param) {
		string paramName = param->first;
		valuesIt = values.find(paramName);
		if ( valuesIt != values.end() ) {
			try {
				LOG4CPLUS_WARN ( getLogger(),"setting " + valuesIt->first + " = " + valuesIt->second );
				getOperation().setParameterFromString( valuesIt->first, valuesIt->second );
			}
			catch ( tsexception::OperationDoesNotExist& e ) {
				Json::Value root;
				root["type"] = "error";
				root["value"] = "Can't find operation " + getOperationId() + "... \n"
												"Is the operation still alive? \n"
												"Exception: " + xcept::htmlformat_exception_history ( e );
				out << root;
			}
			catch ( xcept::Exception& e ) {
				out << "Type mismatch: Can not convert '" << valuesIt->first << "' for value '" << valuesIt->second << "'";
				return;
			}
		}
	}

	srand ( time ( NULL ) );
	ostringstream cid;
	cid << getOperationId() << "_" << fixed << setprecision ( 0 ) << rand();
	commandId_ = cid.str();

	LOG4CPLUS_DEBUG ( getLogger(),"Sending command " + requestedCommand + " to operation " + getOperationId() );

	try
	{
		bool async = true;
		map<string, xdata::Serializable*> paramToExec;
		xoap::MessageReference msg = tstoolbox::doSoapOpSendComand (
																			TS_NS_URI,
																			commandId_,
																			getSessionId ( cgi ),
																			async,
																			getOperationId(),
																			requestedCommand,
																			paramToExec,
																			"guiResponse",
																			getContext()->getLocalUrl(),
																			getContext()->getLocalUrn() );

		Json::Value root;
		root["type"] = "busy";
		root["value"] = "Aknowledge received. Waiting for response...";
		Json::FastWriter fastWriter;
		getSession ( cgi )->addReply ( commandId_, fastWriter.write(root) );

		xoap::MessageReference reply = getContext()->getCommandPort()->run ( msg );
		xoap::SOAPBody replyBody = reply->getSOAPPart().getEnvelope().getBody();

		if ( replyBody.hasFault() ) {
			Json::Value operationReplyJSON;
			operationReplyJSON["type"] = "error";

			string operationReply;
			xoap::SOAPFault fault = replyBody.getFault();
			operationReply = getOperationId() + " does error. SOAP Fault received from server: " + fault.getFaultString();
			LOG4CPLUS_ERROR ( getLogger(),operationReply );

			operationReplyJSON["value"] = operationReply;
			getSession ( cgi )->addReply ( commandId_,fastWriter.write(operationReplyJSON) );
		}
	} catch ( xcept::Exception& e ) {
		Json::Value root;
		root["type"] = "error";

		ostringstream msg;
		msg << "Can not Send SOAP. Exception caught: " << xcept::stdformat_exception_history ( e );
		LOG4CPLUS_ERROR ( getLogger(),msg.str() );

		root["value"] = msg.str();
		Json::FastWriter fastWriter;
		getSession ( cgi )->addReply ( commandId_,fastWriter.write(root) );
		return;
	}
}


void
CellOperationWidget::reset ( cgicc::Cgicc& cgi,ostream& out )
{
	srand ( time ( NULL ) );

	ostringstream cid;
	cid << getOperationId() << "_";
	cid << fixed;
	cid << setprecision ( 0 );
	cid << rand();

	commandId_ = cid.str();
	map<string,xdata::Serializable*> param;

	bool async = true;
	xoap::MessageReference msg = tstoolbox::doSoapOpReset (
																		TS_NS_URI,
																		commandId_,
																		getSessionId ( cgi ),
																		async,
																		getOperationId(),
																		"guiResponse",
																		getContext()->getLocalUrl(),
																		getContext()->getLocalUrn() );

	try
	{
		Json::Value root;
		root["type"] = "busy";
		root["value"] = "Aknowledge received. Waiting for response...";
		Json::FastWriter fastWriter;
		getSession ( cgi )->addReply ( commandId_, fastWriter.write(root) );

		xoap::MessageReference reply = getContext()->getCommandPort()->run ( msg );
		xoap::SOAPBody replyBody = reply->getSOAPPart().getEnvelope().getBody();
		string operationReply;

		if ( replyBody.hasFault() ) {
			Json::Value operationReplyJSON;
			operationReplyJSON["type"] = "error";

			string operationReply;
			xoap::SOAPFault fault = replyBody.getFault();
			operationReply = getOperationId() + " does error. SOAP Fault received from server: " + fault.getFaultString();
			LOG4CPLUS_ERROR ( getLogger(),operationReply );

			operationReplyJSON["value"] = operationReply;
			getSession ( cgi )->addReply ( commandId_,fastWriter.write(operationReplyJSON) );
		}
	}
	catch ( xcept::Exception& e )
	{
		ostringstream msg;
		msg << "Can not Reset Operation with id '" << getOperationId() << "'." << endl;
		msg << "Exception caught: " << xcept::stdformat_exception_history ( e );
		LOG4CPLUS_ERROR ( getLogger(), msg.str() );
		getSession ( cgi )->addReply ( commandId_,msg.str() );
	}
}


CellLocalSession*
CellOperationWidget::getSession ( cgicc::Cgicc& cgi )
{
	map<string,CellLocalSession*>::iterator i ( getContext()->getSessionMap().find ( getSessionId ( cgi ) ) );

  	if ( i == getContext()->getSessionMap().end() )
  	{
  		XCEPT_RAISE ( tsexception::SessionError,"Can not find sessionId " + getSessionId ( cgi ) + " in the session map" );
  	}

  	return i->second;
}


CellOperation&
CellOperationWidget::getOperation()
{
	CellOperation& operation = getContext()->getOperationFactory()->getOperation ( getOperationId() );
  	return operation;
}


vector<string>
CellOperationWidget::getPossibleCommands()
{
	std::set<string> transitions = getOperation().getFSM().getInputs ( getOperation().getFSM().getCurrentState() );
	vector<string> result;

	for ( std::set<string>::iterator i=transitions.begin(); i != transitions.end(); ++i )
	{
		if ( getOperation().getFSM().isFirable ( *i ) )
		{
			result.push_back ( *i );
		}
	}

	return result;
}

} // end ns tsframework
