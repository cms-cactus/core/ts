/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Project Manager: Ildfons Magrans de Abril      				         *
 * Authors: Marc Magrans de Abril               				         *
 *************************************************************************/
#include "ts/framework/CellOperationGraph.h"

#include "ts/framework/CellWarning.h"
#include "ts/framework/CellOperation.h"
#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellAbstractContext.h"

#include "ts/exception/CellException.h"

#include "toolbox/string.h"
#include "toolbox/fsm/FiniteStateMachine.h"

#include "log4cplus/loggingmacros.h"

#include "xcept/tools.h"

#include <ctime>

#include <vector>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>


using namespace std;

namespace tsframework
{

CellOperationGraph::CellOperationGraph ( CellAbstractContext* context, log4cplus::Logger& logger, const string& operationid )
:
ajax::Widget(),
context_ ( context ),
operationid_ ( operationid ),
logger_ ( log4cplus::Logger::getInstance ( logger.getName() +".CellOperationGraph" ) )
{
	format_ = "svg";
}


void
CellOperationGraph::html ( cgicc::Cgicc& cgi,ostream& out )
{
	Agraph_t* aGraph;
	GVC_t* aGvc;

	// set up a graphviz context
	aGvc = gvContext();

	// parse commands to the layout engine the first command is the kind of layoyt engine
	// char* path_cstr = string2cstr ( tmpAbsolutePath_ + fileName_ );
	char* g_fmt = string2cstr( "-T" + format_ );

	// char* gvcArguments[4] = {(char*)("dot"), g_fmt, (char*)("-o"), path_cstr};
	// gvParseArgs ( aGvc, 4, gvcArguments );
	char* gvcArguments[2] = {(char*)("dot"), g_fmt};
	gvParseArgs ( aGvc, 2, gvcArguments );

	//create directed graph with the name of the CellOperation
	char* gr_cstr=string2cstr ( getOperation().getId() );
	aGraph = agopen ( gr_cstr , Agdirected, NULL);

	//select color
	string color="green";

	if ( getOperation().getWarning().getLevel() >= CellWarning::ERROR )
	{
		color = "red";
	}
	else if ( getOperation().getWarning().getLevel() >= CellWarning::WARNING )
	{
		color = "yellow";
	}

	// Create nodes
	vector<toolbox::fsm::State> states = getOperation().getFSM().getStates();//fsm_.getStates();
	Agnode_t* node;
	ostringstream ostNodeName, ostFrom, ostTo;

	for ( vector<toolbox::fsm::State>::const_iterator i = states.begin(); i!=states.end(); ++i )
	{
		if ( *i != 'F' )
		{
			ostNodeName.str ( "" );
			ostNodeName << getOperation().getFSM().getStateName ( *i ); //fsm_.getStateName(*i);
			char* state_cstr = string2cstr ( toUpper ( ostNodeName.str() ) );

			node = agnode ( aGraph, state_cstr, TRUE );
			delete [] state_cstr;

			setNodeProperty ( aGraph, node, "label", toUpper ( ostNodeName.str() ) );
			setNodeProperty ( aGraph, node, "fontname", "Lucida Grande" );
			setNodeProperty ( aGraph, node, "fontsize", "8" );

			//Draw current State diferent
			if ( getOperation().getFSM().getCurrentState() == *i ) //fsm_.getCurrentState() == *i)
			{
				setNodeProperty ( aGraph, node, "color", color.c_str() );
				setNodeProperty ( aGraph, node, "style", "filled" );
			}

			if ( getOperation().getFSM().getInitialState() == *i ) //fsm_.getInitialState() == *i)
			{
				setNodeProperty ( aGraph, node, "shape", "box" );
			}
			else
			{
				setNodeProperty ( aGraph, node, "shape", "ellipse" );
			}
		}
	}

	//Create edges
	 Agedge_t* edge;

	for ( vector<toolbox::fsm::State>::const_iterator i = states.begin(); i!=states.end(); ++i )
	{
		map<string, toolbox::fsm::State> transitionMap = getOperation().getFSM().getTransitions ( *i ); //fsm_.getTransitions(*i);

		for ( map<string, toolbox::fsm::State>::const_iterator j=transitionMap.begin(); j != transitionMap.end(); ++j )
		{
			if ( isFirable ( j->first ) )
			{
				ostFrom.str( "");
				ostFrom << getOperation().getFSM().getStateName ( *i );
				ostTo.str( "" );
				ostTo << getOperation().getFSM().getStateName( j->second );

				char* from_cstr = string2cstr( toUpper ( ostFrom.str() ) );
				char* to_cstr = string2cstr( toUpper ( ostTo.str() ) );
				Agnode_t* from = agnode ( aGraph, from_cstr, TRUE );
				Agnode_t* to = agnode ( aGraph, to_cstr, TRUE );
				delete [] from_cstr;
				delete [] to_cstr;

				edge = agedge ( aGraph, from, to, NULL, 1 );
				setEdgeProperty ( aGraph, edge, "fontname", "Lucida Grande" );
				setEdgeProperty ( aGraph, edge, "fontsize", "8" );
				setEdgeProperty ( aGraph, edge, "style", "bold" );
				setEdgeProperty ( aGraph, edge, "label", j->first );
				setEdgeProperty ( aGraph, edge, "color", "black" );

				if ( getOperation().getFSM().isCurrentTransition ( *i,j->first ) )
				{
					setEdgeProperty ( aGraph, edge, "color", color.c_str() );
				}

				//Arrows from current State are green and wider
				if ( getOperation().getFSM().getCurrentState() == *i )
				{
						setEdgeProperty ( aGraph, edge, "color", color.c_str() );
				}
			}
		}
	}

	//Compute a layout using layout engine with gvcArguments
	gvLayoutJobs ( aGvc, aGraph );

	//Write the graph according to -T and -o options
	// gvRenderJobs ( aGvc,aGraph );
	unsigned int length = 0;
	char* buf = NULL;
	gvRenderData( aGvc, aGraph, "dot", &buf, &length);
	for (unsigned int i = 0; i < length; i++) {
		out << buf[i];
	}
	delete [] buf;

	//Free layout data
	gvFreeLayout ( aGvc, aGraph );

	//Free graph structures
	agclose ( aGraph );
	gvFreeContext ( aGvc );
	delete [] gr_cstr;
	// delete [] path_cstr;
	delete [] g_fmt;

}

void
CellOperationGraph::setWidth ( const string& width )
{
	width_ = width;
}

string
CellOperationGraph::getWidth()
{
	return width_;
}

void
CellOperationGraph::setHeight ( const string& height )
{
  	height_ = height;
}

string
CellOperationGraph::getHeight()
{
  	return height_;
}

char*
CellOperationGraph::string2cstr ( const string& s )
{
	char* cstr = new char[s.length() +1];
	s.copy ( cstr, string::npos );
	cstr[s.length() ] = 0;
	return cstr;
}

void
CellOperationGraph::setNodeProperty ( Agraph_t* g, Agnode_t* node,const string& property, const string& value )
{
	Agsym_t* a;
	char* prop_cstr = string2cstr ( property );

	if ( ! ( a = agattrsym ( g, prop_cstr ) ) )
	{
		a = agattr ( g, AGNODE, prop_cstr, (char *)("") );
	}

	char* val_cstr=string2cstr ( value );
	agxset ( node, a, val_cstr );
	delete [] prop_cstr;
	delete [] val_cstr;
}

void
CellOperationGraph::setEdgeProperty ( Agraph_t* g, Agedge_t* edge,const string& property, const string& value )
{
	Agsym_t* a;
	char* prop_cstr = string2cstr ( property );

	if ( ! ( a = agattrsym ( g, prop_cstr ) ) )
	{
		a = agattr ( g, AGEDGE, prop_cstr, (char *)("") );
	}

	char* val_cstr=string2cstr ( value );
	agxset ( edge, a, val_cstr );
	delete [] prop_cstr;
	delete [] val_cstr;
}

string
CellOperationGraph::toUpper ( const string& str )
{
	string result ( str );

	for ( unsigned int i = 0; i < str.length(); i++ )
	{
		if ( isalpha ( str[i] ) )
		{
			result[i] = toupper ( str[i] );
		}
		else
		{
	  		result[i] = str[i];
		}
	}

	return result;
}


bool
CellOperationGraph::isFirable ( const string& transition )
{
	return getOperation().getFSM().isFirable ( transition );
}


CellOperation& CellOperationGraph::getOperation()
{
	CellOperation& operation = getContext()->getOperationFactory()->getOperation ( operationid_ );
  	return operation;
}


CellAbstractContext*
CellOperationGraph::getContext()
{
	return context_;
}


log4cplus::Logger&
CellOperationGraph::getLogger()
{
	return  logger_;
}

} // end ts framework
