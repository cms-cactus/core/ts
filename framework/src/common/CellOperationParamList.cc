#include "ts/framework/CellOperationParamList.h"

#include "ts/framework/CellOperation.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellOperationFactory.h"

#include "xdata/Serializable.h"
#include "xdata/String.h"


using namespace std;

namespace tsframework
{

CellOperationParamList::CellOperationParamList ( log4cplus::Logger& log, CellAbstractContext* context )
:
CellCommand ( log,context )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellOperationParamList" );
	getParamList().insert ( make_pair ( "OperationType",new xdata::String ( "Configuration" ) ) );
}


void CellOperationParamList::code()
{
	string opType = getParamList()["OperationType"]->toString();

	CellOperationFactory* factory = getContext()->getOperationFactory();
	CellOperation& op = factory->getOperation ( opType );

	map<string, xdata::Serializable*>& m = op.getParamList();
	string json ( map2json ( m ) );

	delete payload_;
	this->payload_ = new xdata::String ( map2json ( m ) );
}

string
CellOperationParamList::map2json ( const map<string,xdata::Serializable*>& m )
{
	string result ( "{" );

	for ( map<string,xdata::Serializable*>::const_iterator i ( m.begin() ); i != m.end(); ++i )
	{
		if ( result != "{" )
		{
			result += ",";
		}

		result += "\"" + i->first + "\":\"" + i->second->type() + "\"";
	}

	result += "}";
	return result;
}

string
CellOperationParamList::createId()
{
	ostringstream id;
	id << "auto_operation_ids" << rand() << rand();
	return id.str();
}

} // end ns tsframework
