/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#include "ts/framework/CellWarning.h"

#include <string>

#include <sstream>

using namespace std; 

namespace tsframework
{

CellWarning::CellWarning()
: 
message_ ( "" ), 
level_ ( 0 )
{
    ;
}


CellWarning::~CellWarning()
{
    ;
}


string 
CellWarning::getMessage() const
{
    return message_;
}


void 
CellWarning::setMessage ( const string& msg )
{
    message_ = msg;
}


int 
CellWarning::getLevel() const
{
    return level_;
}


void 
CellWarning::setLevel ( int level )
{
    level_ = level;
}


string 
CellWarning::level2string ( int level )
{
    switch ( level )
    {
        case CellWarning::NO_WARNING:
            return "NO_WARNING";

        case CellWarning::INFO:
            return "INFO";

        case CellWarning::WARNING:
            return "WARNING";

        case CellWarning::ERROR:
            return "ERROR";
    
        case CellWarning::FATAL:
            return "FATAL";
    
        default:
            ;
  }

  ostringstream msg;
  msg << level << endl;
  return msg.str();
}


void 
CellWarning::clear()
{
    setLevel ( 0 );
    setMessage ( "" );
}


void 
CellWarning::append ( const string& msg, int level )
{
    //Do not show info and warning messages if there are errors
    if ( level < getLevel() && level < ERROR )
    {
        return;
    }

    if ( level > getLevel() && getLevel() < ERROR )
    {
        setMessage ( "" );
    }

    setLevel ( max ( getLevel(),level ) );
    setMessage ( getMessage() + msg );
}

} // end ns tsframework
