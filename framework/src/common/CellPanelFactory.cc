/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/
#include "ts/framework/CellPanelFactory.h"

#include "ts/exception/CellException.h"

#include "log4cplus/logger.h"
#include "log4cplus/clogger.h"

#include "xcept/Exception.h"

using namespace std;

namespace tsframework
{

CellPanelFactory::CellPanelFactory ( log4cplus::Logger& logger )
{
    logger_ = new log4cplus::Logger ( log4cplus::Logger::getInstance ( logger.getName() +".CellFactory" ) );
    logger_->setLogLevel ( L4CP_INFO_LOG_LEVEL );
}


CellPanelFactory::~CellPanelFactory()
{
    delete logger_;

    for ( map<string,CreatorSignature*>::iterator i ( creators_.begin() ); i != creators_.end(); ++i )
    {
        delete i->second;
    }
}


log4cplus::Logger&
CellPanelFactory::getLogger()
{
    return *logger_;
}


vector<string>
CellPanelFactory::getClassList()
{
    vector<string> result;

    for ( map<string,CreatorSignature*>::iterator i ( creators_.begin() ); i != creators_.end(); ++i )
    {
        result.push_back ( i->first );
    }

    return result;
}


CellPanel*
CellPanelFactory::create ( const string& className, CellAbstractContext* context, log4cplus::Logger& logger )
{
    map<string,CreatorSignature*>::iterator j ( creators_.find ( className ) );

    if ( j == creators_.end() )
    {
        XCEPT_RAISE ( tsexception::CellPanelClassDoesNotExist, "Trying to create a CellPanel of a non-existing class " + className + " " );
    }

    CellPanel* p = j->second->create ( context,logger );
    return p;
}

} // end ns tsframework
