#include "ts/framework/CellOperationFactory.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellOperation.h"


using namespace std;


namespace tsframework
{

CellOperationFactory::CellOperationFactory ( log4cplus::Logger& log )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellOperationFactory" );
}


void
CellOperationFactory::add ( const string& type, CellOperation* op )
{
	tstoolbox::ReadWriteLockHandler handler ( rwlock_,tstoolbox::ReadWriteLockHandler::WRITE );
	OperationIterator i = operations_.find ( type );

	if ( i != operations_.end() )
	{
		delete op;
    	XCEPT_RAISE ( tsexception::OperationAlreadyExists, "The operation '" + type + "' already exists in the factory" );
 	}

	op->setId ( type );
  	operations_[type]=op;
}


bool
CellOperationFactory::isOperation ( const string& id )
{
  tstoolbox::ReadWriteLockHandler handler ( rwlock_,tstoolbox::ReadWriteLockHandler::READ );
  OperationIterator i = operations_.find ( id );

  if ( i == operations_.end() )
  {
	  return false;
  }
  else
  {
	  return true;
  }
}


vector<string>
CellOperationFactory::getOperationTypes()
{
	return getOperations();
}


vector<string>
CellOperationFactory::getOperations()
{
	tstoolbox::ReadWriteLockHandler handler ( rwlock_,tstoolbox::ReadWriteLockHandler::READ );
	vector<string> v;

	for ( OperationIterator i = operations_.begin(); i != operations_.end(); ++i )
	{
		v.push_back ( i->first );
	}

	return v;
}

CellOperation&
CellOperationFactory::getOperation ( const string& id )
{
	tstoolbox::ReadWriteLockHandler handler ( rwlock_,tstoolbox::ReadWriteLockHandler::READ );
  	OperationIterator i = operations_.find ( id );

  	if ( i == operations_.end() )
  	{
  		XCEPT_RAISE ( tsexception::OperationDoesNotExist,"There is not an instance of operation with id '" + id + "'" );
  	}
  	else
  	{
  		return * ( i->second );
  	}
}

} // end ns tsframework
