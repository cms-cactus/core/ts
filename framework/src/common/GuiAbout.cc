#include "ts/framework/GuiAbout.h"
#include "ts/framework/version.h"
#include "ts/framework/CellAbstract.h"

#include "ajax/PolymerElement.h"

#include "ts/toolbox/ReadWriteLock.h"
#include "toolbox/net/URL.h"

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include "log4cplus/logger.h"

#include "ts/toolbox/FsTools.h"
#include "ts/toolbox/Tools.h"
#include "ts/toolbox/Timer.h"
// required to read the core dumps
#include <sys/stat.h>

#include <stdio.h>
#include <stdlib.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <map>
#include <jsoncpp/json/json.h>
#include "ajax/toolbox.h"


std::string FormatDateTime( std::string date );

frameworkpanels::GuiAbout::GuiAbout( tsframework::CellAbstractContext* context, log4cplus::Logger& logger )
 : tsframework::CellPanel( context, logger ) {
	logger_ = log4cplus::Logger::getInstance ( logger.getName() + ".About" );
	_cellStartTimeAsString = context->getCellStartTime();
}

frameworkpanels::GuiAbout::~GuiAbout() {
  ;
}

void frameworkpanels::GuiAbout::layout(cgicc::Cgicc& cgi) {
  remove();
  setEvent("getCoreDump", ajax::Eventable::OnClick, this, &frameworkpanels::GuiAbout::getCoreDump);
  setEvent("getCoreDumps", ajax::Eventable::OnClick, this, &frameworkpanels::GuiAbout::getCoreDumps);
  setEvent("getLogs", ajax::Eventable::OnClick, this, &frameworkpanels::GuiAbout::getLogs);
  setEvent("getCPU", ajax::Eventable::OnTime, this, &frameworkpanels::GuiAbout::getCPU);
  setEvent("getMemory", ajax::Eventable::OnTime, this, &frameworkpanels::GuiAbout::getMemory);
  ajax::PolymerElement* about = new ajax::PolymerElement("ts-about");
  Json::Value root;
  root["version"] = frameworkpanels::GuiAbout::getVersion();
  root["compiled"] = frameworkpanels::GuiAbout::getCompiledDate();
  root["started"] = frameworkpanels::GuiAbout::getStartedDate();
  Json::StreamWriterBuilder wbuilder;
  about->set("data", ajax::toolbox::escapeHTML(Json::writeString(wbuilder, root)));
  add(about);
}

std::string frameworkpanels::GuiAbout::getVersion() {
  std::ostringstream version_text;
  version_text <<
            boost::lexical_cast<std::string>(TS_TSFRAMEWORK_VERSION_MAJOR) << "."
         << boost::lexical_cast<std::string>(TS_TSFRAMEWORK_VERSION_MINOR) << "."
         << boost::lexical_cast<std::string>(TS_TSFRAMEWORK_VERSION_PATCH);
  return version_text.str();
}

std::string frameworkpanels::GuiAbout::getCompiledDate() {
  std::ostringstream compiled_text;
  compiled_text << __DATE__ << " " << __TIME__;
  return compiled_text.str();
}

void frameworkpanels::GuiAbout::getCPU( cgicc::Cgicc& cgi, std::ostream& out ) {
  out << tstoolbox::Tools::execWithPipe("mpstat 1 1", false);
}
void frameworkpanels::GuiAbout::getMemory( cgicc::Cgicc& cgi, std::ostream& out ) {
  out << tstoolbox::Tools::execWithPipe("free", false);
}

std::string frameworkpanels::GuiAbout::getStartedDate() {
  std::map<std::string,std::string> months;
  months["01"] = "Jan";
  months["02"] = "Feb";
  months["03"] = "Mar";
  months["04"] = "Apr";
  months["05"] = "May";
  months["06"] = "Jun";
  months["07"] = "Jul";
  months["08"] = "Aug";
  months["09"] = "Sep";
  months["10"] = "Oct";
  months["11"] = "Nov";
  months["12"] = "Dec";

  //extract values from date
  std::string year, month, day, time;
  year = _cellStartTimeAsString.substr(0,4);
  month= _cellStartTimeAsString.substr(5,2);
  day = _cellStartTimeAsString.substr(8,2);
  day.erase(0, day.find_first_not_of('0'));
  time = _cellStartTimeAsString.substr(11,8);

  std::ostringstream final_date;
  final_date << months[month] << " ";
  if ( std::atoi(day.c_str()) < 10 )
    final_date << "0" << day << " " << year << "  " << time;
  else
    final_date << day << " " << year << "  " << time;

 return final_date.str();
}

void frameworkpanels::GuiAbout::getCoreDumps( cgicc::Cgicc& cgi, std::ostream& out ) {
  Json::Value result(Json::arrayValue);

  std::vector<std::string> core_dumps = tstoolbox::FsTools::listFiles("/tmp/core*");
  for ( std::vector<std::string>::const_iterator i = core_dumps.begin(); i != core_dumps.end(); ++i ) {
    result.append(i->c_str());
  }

  out << result;
}

void frameworkpanels::GuiAbout::getCoreDump( cgicc::Cgicc& cgi, std::ostream& out ) {
  Json::Value result;
  std::string filename = "";

  std::map<std::string,std::string> values(ajax::toolbox::getSubmittedValues(cgi));
  for(std::map<std::string,std::string>::iterator i(values.begin()); i != values.end(); ++i) {
    if (strcmp(i->first.c_str(),"filename") == 0) {
      filename = i->second;
    }
  }

  if (filename == "") {
    LOG4CPLUS_WARN ( getLogger(),"frameworkpanels::GuiAbout::getCoreDump no filename: " + filename );
    result["error"] = "No filename specified";
    // out << "Error: No filename specified in request";
  } else {
    std::string command = "sudo gdb -batch -n -ex 'set pagination off' -ex 'thread apply all bt' /opt/xdaq/bin/xdaq.exe " + filename;
    std::string output = tstoolbox::Tools::execWithPipe(command, false);
    if (output == "") {
      result["output"] = filename + " is not a core dump: File format not recognized";
    } else {
      result["output"] = output;
    }
  }

  out << result;
}

void frameworkpanels::GuiAbout::getLogs( cgicc::Cgicc& cgi, std::ostream& out ) {
  Json::Value result(Json::arrayValue);
  log4cplus::SharedAppenderPtrList appenders = log4cplus::Logger::getRoot().getAllAppenders();
  for (log4cplus::SharedAppenderPtrList::iterator appender = appenders.begin(); appender != appenders.end(); ++appender) {
    // outputs `file.append:/var/log/l1test.subsystem-supervisor.log`
    std::string s_appender = (*appender)->getName();
    std::string path = s_appender.substr(s_appender.find(":") + 1);
    Json::Value log;
    log["file"] = path;
    std::ostringstream le_command;
    le_command << "cat " << path << " | grep -E \"FATAL|ERROR|WARN\" | tail -n 5000";
    log["content"] = tstoolbox::Tools::execWithPipe(le_command.str(), false);

    result.append(log);
  }
  out << result;
}
