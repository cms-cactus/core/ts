#include "ts/framework/CellGetOperationPayload.h"

#include "ts/framework/CellAbstractContext.h"

/*
#include "ts/toolbox/CellToolbox.h"
#include "ts/framework/CellOperation.h"
#include "ts/exception/CellException.h"
#include "ts/framework/CellOperationFactory.h"
*/
#include "log4cplus/logger.h"

namespace tsframework
{

CellGetOperationPayload::CellGetOperationPayload ( log4cplus::Logger& log, CellAbstractContext* context )
:
CellCommand ( log,context )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellGetOperationPayload" );
	getParamList() ["OperationType"] = new xdata::String ( "Configuration" );
}


void
CellGetOperationPayload::code()
{
	std::string opid = getParamList() ["OperationType"]->toString();
	CellOperation& op = context_->getOperationFactory()->getOperation ( opid );
  	payload_->fromString ( op.getResult() );

  	getWarning().setMessage ( op.getWarning().getMessage() );
  	getWarning().setLevel ( op.getWarning().getLevel() );
}

} // end ns tsframework
