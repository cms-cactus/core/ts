/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons & Marc Magrans de Abril               		 *
 *************************************************************************/

#include "ts/framework/MonitorableItem.h"

namespace tsframework
{

MonitorableItem::MonitorableItem ( const std::string& name, xdata::Serializable* serializable )
:
name_ ( name ), 
serializable_ ( serializable ), 
refreshFunctional_ ( 0 )
{
	  ;
}

MonitorableItem::~MonitorableItem()
{
  	delete serializable_;
  	delete refreshFunctional_;
}


xdata::Serializable*
MonitorableItem::get()
{
	  return serializable_;
}

bool 
MonitorableItem::isRefreshable()
{
	  return refreshFunctional_;
}

} // end ns tsframework
