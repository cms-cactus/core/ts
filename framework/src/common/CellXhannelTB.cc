#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequest.h"
#include "ts/framework/CellXhannelRequestTB.h"
#include "ts/toolbox/CellNamespaceURI.h"
#include "ts/toolbox/CellToolbox.h"
#include "ts/exception/CellException.h"
#include "ts/framework/CellAbstractContext.h"

#include <string>
#include <sstream>

#include "log4cplus/logger.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/ApplicationGroup.h"

#include "xdaq/XceptSerializer.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/Method.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"

#include "xcept/tools.h"

#include "tstore/client/Client.h"
#include <thread>
#include <chrono>

//
// provides factory method for instantion of ControlCell application
//

namespace tsframework {
  XDAQ_INSTANTIATOR_IMPL ( CellXhannelTB )


CellXhannelTB::CellXhannelTB ( xdaq::ApplicationStub* s ) : tsframework::CellXhannel ( s ),id_ ( "" ),view_ ( "" )
{
  ;
}

CellXhannelTB::~CellXhannelTB()
{
  ;
}

bool
CellXhannelTB::isConnected()
{
  return ( ( !getView().empty() ) && ( !getId().empty() ) );
}

void CellXhannelTB::connect ( const std::string& view, const std::string& username, const std::string& password, bool retry )
{
  tstoolbox::MutexHandler h ( mutex_ );
  xoap::MessageReference msg = xoap::createMessage();

  try
  {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPName msgName = envelope.createName ( "connect", "tstoresoap", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
    xoap::SOAPElement commandElement = envelope.getBody().addBodyElement ( msgName );
    xoap::SOAPName id = envelope.createName ( "id", "tstoresoap", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
    commandElement.addAttribute ( id, view );
    //the attribute authentication="basic" is mandatory
    xoap::SOAPName authentication = envelope.createName ( "authentication", "tstoresoap", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
    commandElement.addAttribute ( authentication, "basic" );
    //with authentication mode "basic" the credentials should be in the form username/password
    xoap::SOAPName credentials = envelope.createName ( "credentials", "tstoresoap", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
    commandElement.addAttribute ( credentials, username + "/" + password );
    xoap::SOAPName timeoutName = envelope.createName ( "timeout", "tstoresoap", TSTORE_NS_URI );
    std::string timeout ( "PT1M" );
    commandElement.addAttribute ( timeoutName, timeout );
  }
  catch ( xoap::exception::Exception& e )
  {
    std::ostringstream err;
    err << "Can not create SOAP message for connecting to view '" << view << "' and username '" << username << "'.";
    XCEPT_RETHROW ( tsexception::SoapEncodingError, err.str(),e );
  }

  xoap::MessageReference reply;
  int count ( 0 );
  timeval start;
  gettimeofday ( &start,0 );

  while ( true )
  {
    try
    {
      ++count;
      reply = getApplicationContext()->postSOAP ( msg, *getApplicationDescriptor(), *getTargetDescriptor() );
      break;
    }
    catch ( xcept::Exception& e )
    {
      timeval end;
      gettimeofday ( &end,0 );

      if ( count >= MAX_ATTEMPTS || tstoolbox::timeval_diff ( end,start ) >MAX_TIMEOUT_MS )
      {
        XCEPT_RETHROW ( tsexception::CommunicationError,"Several communication attempts to TStore have failed. It is possible that the server is down",e );
      }
      else
      {
        std::ostringstream msg;
        msg << "A Connection attempt to TStore has failed (trying again). ";
        msg << xcept::stdformat_exception_history ( e );
        LOG4CPLUS_WARN ( getLogger(),msg.str() );
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      }
    }
  }

  xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();

  if ( body.hasFault() )
  {
    std::ostringstream err;
    err << "Connection SOAP reply contains SOAPFault. ";

    if ( body.getFault().hasDetail() )
    {
      xoap::SOAPElement detail = body.getFault().getDetail();
      xcept::Exception rae;
      xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
      XCEPT_RETHROW ( tsexception::SoapFault, err.str(), rae );
    }
    else
    {
      err << body.getFault().getFaultString();
      XCEPT_RAISE ( tsexception::SoapFault,err.str() );
    }
  }
  else
  {
    std::string id = tstoreclient::connectionID ( reply );
    setConnection ( view,id );
  }
}

void
CellXhannelTB::disconnect()
{
  tstoolbox::MutexHandler h ( mutex_ );

  if ( getView().empty() || getId().empty() )
  {
    setConnection ( "","" );
    return;
  }

  xoap::MessageReference msg = xoap::createMessage();

  try
  {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPName msgName = envelope.createName ( "disconnect", "tstore", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
    xoap::SOAPElement queryElement = envelope.getBody().addBodyElement ( msgName );
    xoap::SOAPName id = envelope.createName ( "connectionID", "tstore", "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd" );
    queryElement.addAttribute ( id, getId() );
  }
  catch ( xoap::exception::Exception& e )
  {
    std::ostringstream err;
    err << "Can not create SOAP message for disconnecting from view '" << getView() << "' and connectioID '" << getId() << "'.";
    XCEPT_RETHROW ( tsexception::SoapEncodingError, err.str(),e );
  }

  xoap::MessageReference reply;

  try
  {
    reply = getApplicationContext()->postSOAP ( msg, *getApplicationDescriptor(), *getTargetDescriptor() );
  }
  catch ( xcept::Exception& e )
  {
    std::ostringstream msg;
    msg << "Disconnect to TStore failed. ";
    msg << xcept::stdformat_exception_history ( e );
    LOG4CPLUS_WARN ( getLogger(),msg.str() );
    setConnection ( "","" );
    return;
  }

  xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();

  if ( body.hasFault() )
  {
    std::ostringstream err;
    err << "Disconnect SOAP reply contains SOAPFault. ";

    if ( body.getFault().hasDetail() )
    {
      xoap::SOAPElement detail = body.getFault().getDetail();
      xcept::Exception rae;
      xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
      err << xcept::stdformat_exception_history ( rae );
    }
    else
    {
      err << body.getFault().getFaultString();
    }

    LOG4CPLUS_WARN ( getLogger(), err.str() );
  }

  setConnection ( "","" );
}


void
CellXhannelTB::send ( tsframework::CellXhannelRequest* req )
{
  tstoolbox::MutexHandler h ( mutex_ );
  CellXhannelRequestTB* tbReq=dynamic_cast<CellXhannelRequestTB*> ( req );

  if ( ( tbReq->getId() != getId() ) ||
       ( tbReq->getView() != getView() ) )
  {
    std::ostringstream err;
    err << "Can not execute request. The request view or id (" << tbReq->getView() << "," << tbReq->getId() << ") does not match the xhannel view or id (";
    err << getView() << "," << getId() << ").";
    XCEPT_RAISE ( tsexception::XhannelUsageError,err.str() );
  }

  xoap::MessageReference* msg = tbReq->getRequest();
  xoap::MessageReference request = xoap::createMessage ( *msg );
  xoap::MessageReference reply;
  int count ( 0 );
  timeval start;
  gettimeofday ( &start,0 );

  while ( true )
  {
    try
    {
      ++count;
      reply = getContext()->getApContext()->postSOAP ( request, *getApplicationDescriptor(), *getTargetDescriptor() );
      break;
    }
    catch ( xcept::Exception& e )
    {
      timeval end;
      gettimeofday ( &end,0 );

      if ( count >= 10 || tstoolbox::timeval_diff ( end,start ) >MAX_TIMEOUT_MS )
      {
        XCEPT_RETHROW ( tsexception::CommunicationError,"Several communication attempts to TStore have failed. It is possible that the server is down",e );
      }
      else
      {
        std::ostringstream msg;
        msg << "Communication attempt to TStore has failed (trying again). ";
        msg << xcept::stdformat_exception_history ( e );
        LOG4CPLUS_WARN ( getLogger(),msg.str() );
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      }
    }
  }

  xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();

  if ( rb.hasFault() )
  {
    std::ostringstream err;
    err << "SOAP reply contains SOAPFault. ";

    if ( rb.getFault().hasDetail() )
    {
      xoap::SOAPElement detail = rb.getFault().getDetail();
      xcept::Exception rae;
      xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
      XCEPT_RETHROW ( tsexception::SoapFault, err.str(), rae );
    }
    else
    {
      err << rb.getFault().getFaultString();
      XCEPT_RAISE ( tsexception::SoapFault,err.str() );
    }
  }

  tbReq->setReply ( reply );
}

tsframework::CellXhannelRequest* CellXhannelTB::createRequestVirtual()
{
  if ( getView().empty() || getId().empty() )
  {
    std::ostringstream err;
    err << "Trying to create a request before connecting to the database.";
    XCEPT_RAISE ( tsexception::XhannelUsageError,err.str() );
  }

  tsframework::CellXhannelRequestTB* req = new tsframework::CellXhannelRequestTB ( getLogger(),getContext(),getView(),getId() );
  return req;
}

void CellXhannelTB::setConnection ( const std::string& view, const std::string& id )
{
  view_ = view;
  id_ = id;
}

std::string CellXhannelTB::getView()
{
  return view_;
}

std::string CellXhannelTB::getId()
{
  return id_;
}

}