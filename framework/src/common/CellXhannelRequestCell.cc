/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#include "ts/framework/CellXhannelRequestCell.h"

#include "ts/toolbox/CellToolbox.h"

#include "ts/exception/CellException.h"

#include "xdaq/ApplicationDescriptor.h"

#include "xdata/Integer.h"
#include "xdata/String.h"


using namespace std; 

namespace tsframework
{


CellXhannelRequestCell::CellXhannelRequestCell ( log4cplus::Logger& log, CellAbstractContext* context, const xdaq::ApplicationDescriptor* sender, const string& ns )
:
CellXhannelRequest(),
sender_ ( sender ),
targetNs_ ( ns )
{
    setContext ( context );
    logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellXhannelRequestCell" );
}


CellXhannelRequestCell::~CellXhannelRequestCell()
{
    ;
}


void CellXhannelRequestCell::doOpGetState ( const string& ns, const string& cid, const string& sid, bool async, const string& opid, const string& url, const string& urn )
{
    string cbcom = "ResponseCell";
    xoap::MessageReference request = tstoolbox::doSoapOpGetState ( ns, cid, sid, async, opid, cbcom, url, urn );
    msg_request_ = new xoap::MessageReference ( request );
}


void
CellXhannelRequestCell::doOpGetState ( const string& sid, bool async, const string& opid )
{
    this->doOpGetState ( targetNs_, requestId_, sid, async, opid, getUrl(), getUrn() );
}


void CellXhannelRequestCell::doOpReset ( const string& ns, const string& cid, const string& sid, bool async, const string& op, const string& url, const string& urn )
{
    string cbcom = "ResponseCell";
    xoap::MessageReference request = tstoolbox::doSoapOpReset ( ns, cid, sid, async, op, cbcom, url, urn );
    msg_request_ = new xoap::MessageReference ( request );
}


void
CellXhannelRequestCell::doOpReset ( const string& sid, bool async, const string& op )
{
    this->doOpReset ( targetNs_, requestId_, sid, async, op, getUrl(), getUrn() );;
}


void
CellXhannelRequestCell::doOpSendCommand ( const string& ns, const string& cid, 
                                          const string& sid, bool async, const string& opid, 
                                          const string& command, map<string, xdata::Serializable*> param, 
                                          const string& url, const string& urn )
{
    string cbcom = "ResponseCell";
    xoap::MessageReference request = tstoolbox::doSoapOpSendComand ( ns, cid, sid, async, opid, command, param, cbcom, url, urn );
    msg_request_ = new xoap::MessageReference ( request );
}


void
CellXhannelRequestCell::doOpSendCommand ( const string& sid, bool async, const string& opid, const string& command, map<string, xdata::Serializable*> param )
{
    this->doOpSendCommand ( targetNs_, requestId_, sid, async, opid, command, param,getUrl(), getUrn() );
}


void
CellXhannelRequestCell::doCommand ( const string& ns, const string& cid, const string& sid, 
                                    bool async, const string& command, map<string,xdata::Serializable*> param, 
                                    const string& url, const string& urn )
{
    string cbcom = "ResponseCell";
    xoap::MessageReference request = tstoolbox::doSoapCommand ( ns, cid, sid, async, command, param, cbcom, url, urn );
    msg_request_ = new xoap::MessageReference ( request );
}


void
CellXhannelRequestCell::doCommand ( const string& sid, bool async, const string& command, map<string,xdata::Serializable*> param )
{
    this->doCommand ( targetNs_, requestId_, sid, async, command, param, getUrl(), getUrn() );
}


string
CellXhannelRequestCell::opGetStateReply()
{
    if ( !msg_reply_ )
    {
        XCEPT_RAISE ( tsexception::XhannelUsageError,"Can not parse non-received reply" );
    }

    xdata::Serializable* serial = tstoolbox::getPayload ( *msg_reply_ );
    string sresult = serial->toString();
    return sresult;
}


string
CellXhannelRequestCell::opResetReply()
{
    if ( !msg_reply_ )
    {
        XCEPT_RAISE ( tsexception::XhannelUsageError,"Can not parse non-received reply" );
    }

    xdata::Serializable* serial = tstoolbox::getPayload ( *msg_reply_ );
    string sresult = serial->toString();
    return sresult;
}


xdata::Serializable*
CellXhannelRequestCell::opSendCommandReply()
{
    if ( !msg_reply_ )
    {
        XCEPT_RAISE ( tsexception::XhannelUsageError,"Can not parse non-received reply" );
    }

    xdata::Serializable* serial = tstoolbox::getPayload ( *msg_reply_ );
    return serial;
}


xdata::Serializable* 
CellXhannelRequestCell::commandReply()
{
    if ( !msg_reply_ )
    {
        XCEPT_RAISE ( tsexception::XhannelUsageError,"Can not parse non-received reply" );
    }

    xdata::Serializable* serial = tstoolbox::getPayload ( *msg_reply_ );
    return serial;
}


CellWarning& 
CellXhannelRequestCell::getWarning()
{
    if ( !msg_reply_ )
    {
          XCEPT_RAISE ( tsexception::XhannelUsageError,"Can not parse non-received reply" );
    }

    xdata::Serializable* slevel = tstoolbox::getWarningLevel ( *msg_reply_ );

    if ( slevel )
    {
        xdata::Integer* level ( 0 );
        level = dynamic_cast<xdata::Integer*> ( slevel );

        if ( level )
        {
            warning_.setLevel ( level->value_ );
        }

        delete slevel;
    }

    xdata::Serializable* smessage = tstoolbox::getWarningMessage ( *msg_reply_ );

    if ( smessage )
    {
        xdata::String* message ( 0 );
        message = dynamic_cast<xdata::String*> ( smessage );

        if ( message )
        {
            warning_.setMessage ( message->value_ );
        }

        delete smessage;
    }

    return warning_;
}


string 
CellXhannelRequestCell:: getUrl()
{
    return sender_->getContextDescriptor()->getURL();
}


string 
CellXhannelRequestCell:: getUrn()
{
    return sender_->getURN();
}

} // end ns tsframework