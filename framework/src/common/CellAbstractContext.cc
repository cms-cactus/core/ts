#include "ts/framework/CellAbstractContext.h"

#include "ts/framework/CellFactory.h"
#include "ts/framework/CellPanelFactory.h"
#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellCommandPort.h"
#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellLocalSession.h"
#include "ts/framework/CellXhannel.h"
#include "ts/framework/Level1Source.h"

#include "toolbox/task/TimerFactory.h"

#include "ts/toolbox/Timer.h"

using namespace std;

namespace tsframework
{


CellAbstractContext::CellAbstractContext ( log4cplus::Logger& log, CellAbstract* cell )
:
logger_ ( log4cplus::Logger::getInstance ( log.getName() + ".CellAbstractContext" ) ),
cell_ ( cell ),
lock_ ( true )
{
	primaryColor_ = ajax::RGB(0, 103, 26);
	secondaryColor_ = ajax::RGB(68, 138, 255);
	factory_ = new CellFactory ( log,this );

	operationFactory_ = new CellOperationFactory ( log );
	operationFactory_->setContext ( this );

	panelFactory_ = new CellPanelFactory ( log );

	commandPort_ = new CellCommandPort ( log );
	commandPort_->setContext ( this );

	timerName_ =  "CellAbstractContext_timer";

	toolbox::task::Timer* timer = toolbox::task::getTimerFactory()->createTimer ( timerName_ );

      //start time
	_cellStartTimeAsString = tstoolbox::Timer().startTimeAsString();

	toolbox::TimeInterval interval ( 600, 0 );
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
	timer->scheduleAtFixedRate ( start, this, interval, 0, "" );

	l1Source_ = new Level1Source ( this,log );
}

std::string CellAbstractContext::getCellStartTime()
{
  return _cellStartTimeAsString;
}

CellAbstractContext::~CellAbstractContext()
{
	delete factory_;
	delete operationFactory_;
	delete panelFactory_;
	toolbox::task::getTimerFactory()->removeTimer ( timerName_ );

	// why there aren't the following lines:
	//delete commandPort_;
	//delete l1Source_;
	// ?
}


CellFactory*
CellAbstractContext::getCommandFactory()
{
	return factory_;
}


CellOperationFactory*
CellAbstractContext::getOperationFactory()
{
	return operationFactory_;
}


string
CellAbstractContext::getLocalUrl()
{
	CellAbstract* cell = getCell();
	xdaq::ApplicationContext* context = cell->getApplicationContext();
	const xdaq::ContextDescriptor* desc = context->getContextDescriptor();
	string url = desc->getURL();
	return url;
}


string
CellAbstractContext::getLocalUrn()
{
	return getCell()->getApplicationDescriptor()->getURN();
}


string
CellAbstractContext::getMyNs()
{
	return getCell()->getNamespace();
}


xdaq::ApplicationContext*
CellAbstractContext::getApContext()
{
  	return getCell()->getApplicationContext();
}


void
CellAbstractContext::setXhannel ( string name, CellXhannel* xhn )
{
	xhannels_[name] = xhn;
}


CellXhannel*
CellAbstractContext::getXhannel ( string name )
{
	map< string, CellXhannel*>::iterator im = xhannels_.find(name);

	if (im == xhannels_.end())
	{
		XCEPT_RAISE ( tsexception::XhannelDoesNotExist,toolbox::toString ( "The xhannel: %s does not exist",name.c_str() ) );
	}

	return im->second;

}


map<string, CellXhannel*>
CellAbstractContext::getXhannelList()
{
	return xhannels_;
}


void
CellAbstractContext::removeXhannel ( string name )
{
	map<string, CellXhannel*>::iterator i;
	i = xhannels_.find ( name );

	if ( i == xhannels_.end() )
	{
		XCEPT_RAISE ( tsexception::XhannelDoesNotExist,toolbox::toString ( "The xhannel: %s does not exist",name.c_str() ) );
	}

	CellXhannel* chn = i->second;
	delete chn;
	xhannels_.erase ( i );
}


CellCommandPort*
CellAbstractContext::getCommandPort()
{
	return commandPort_;
}


CellAbstract*
CellAbstractContext::getCell()
{
  	return cell_;
}


std::string
CellAbstractContext::getImports()
{
	if ( imports_.empty() ) {
		return "";
	} else {
		return imports_;
	}
}

void
CellAbstractContext::addImport(const std::string& importurl)
{
	std::size_t found = imports_.find(importurl);
	if (found==std::string::npos) {
		std::ostringstream oss;
		oss << "    <link rel='import' href='" << importurl << "'>\n";
		imports_.append(oss.str());
	}
}

void
CellAbstractContext::addImports(const std::vector<std::string>& importurls)
{
	std::size_t found;
	std::ostringstream oss;
	for(vector<string>::const_iterator i = importurls.begin(); i != importurls.end(); ++i) {
		found = imports_.find(*i);
		if (found==std::string::npos) {
	    oss << "    <link rel='import' href='" << *i << "'>\n";
		}
	}
	imports_.append(oss.str());
}

void CellAbstractContext::setPrimaryColor(ajax::RGB color) {
  primaryColor_ = color;
}
void CellAbstractContext::setPrimaryColor(int red, int green, int blue) {
  ajax::RGB color(red, green, blue);
  primaryColor_ = color;
}
void CellAbstractContext::setSecondaryColor(ajax::RGB color) {
  secondaryColor_ = color;
}
void CellAbstractContext::setSecondaryColor(int red, int green, int blue) {
  ajax::RGB color(red, green, blue);
  secondaryColor_ = color;
}
ajax::RGB CellAbstractContext::getPrimaryColor() {
  return primaryColor_;
}
ajax::RGB CellAbstractContext::getSecondaryColor() {
  return secondaryColor_;
}


CellPanelFactory*
CellAbstractContext::getPanelFactory()
{
 	return panelFactory_;
}


bool
CellAbstractContext::hasSession ( const string& sessionId )
{
	tstoolbox::MutexHandler handler ( lock_ );
	map<string,CellLocalSession*>::iterator i ( sessions_.find ( sessionId ) );

	if ( i != sessions_.end() )
	{
		return true;
	}
	else
	{
		return false;
	}
}


map<string,CellLocalSession*>&
CellAbstractContext::getSessionMap()
{
  return sessions_;
}


log4cplus::Logger&
CellAbstractContext::getLogger()
{
	return logger_;
}


void
CellAbstractContext::timeExpired ( toolbox::task::TimerEvent& )
{
	tstoolbox::MutexHandler handler ( lock_ );
	LOG4CPLUS_DEBUG ( getLogger(), "Checking for old sessions" );
	map<string,CellLocalSession*>::iterator i;

	for ( i = getSessionMap().begin(); i != getSessionMap().end(); )
	{
		string sname = i->first;
		time_t start = i->second->getLastCalled();
		time_t end = time ( NULL );
		double d = difftime ( end,start );

		if ( d >= CellLocalSession::CELL_TIMEOUT_SEC )
		{
			CellLocalSession* s = i->second;
			getSessionMap().erase ( i );
			delete s;
			i = getSessionMap().begin();
			LOG4CPLUS_DEBUG ( getLogger(), "Session '" << sname << "' removed. " << d << " seconds passed without using the session" );
		}
		else
		{
			++i;
		}
	}
}


Level1Source&
CellAbstractContext::getLevel1Source()
{
	return *l1Source_;
}


tstoolbox::Mutex&
CellAbstractContext::getLock()
{
  	return lock_;
}

} // end ns tsframework
