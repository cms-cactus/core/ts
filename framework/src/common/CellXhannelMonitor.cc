/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril                                    *
 *************************************************************************/

#include "ts/framework/CellXhannelMonitor.h"

#include "ts/framework/CellXhannelRequestMonitor.h"

#include "ts/toolbox/HttpMessenger.h"

#include "toolbox/regex.h"



using namespace std; 

namespace tsframework
{

XDAQ_INSTANTIATOR_IMPL ( CellXhannelMonitor )


CellXhannelMonitor::CellXhannelMonitor ( xdaq::ApplicationStub* s ) 
: 
CellXhannelXdaqSimple ( s )
{
    ;
}


CellXhannelMonitor::~CellXhannelMonitor()
{
    ;
}


void
CellXhannelMonitor::send ( CellXhannelRequest* req )
{
    tstoolbox::MutexHandler h ( mutex_ );
    CellXhannelRequestMonitor* monReq = dynamic_cast<CellXhannelRequestMonitor*> ( req );

    if ( !monReq->isRetrieveExdr_ )
    {
        try
        {
            tstoolbox::HttpMessenger msn;
            string reply;
            msn.requestHtml ( monReq->getCurlQuery(),reply );
            monReq->setCurlReply ( reply );

            if ( toolbox::regx_match ( reply, "HTTP Request Error" ) )
            {
                XCEPT_RAISE ( tsexception::CommunicationError, "HTTP Request Error: '" + reply + "'." );
            }
        }
        catch ( xcept::Exception& e )
        {
            ostringstream msg;
            msg << "Error trying to send HTTP query '" << monReq->getCurlQuery();
            XCEPT_RETHROW ( tsexception::CommunicationError,msg.str(),e );
        }
    }
    else
    {
        try
        {
            tstoolbox::HttpMessenger msn;
            char* buffer;
            size_t size;
            msn.requestFile ( monReq->getCurlQuery(),buffer,size );
            monReq->buffer_ = buffer;
            monReq->size_ = size;
        }
        catch ( xcept::Exception& e )
        {
            ostringstream msg;
            msg << "Error trying to request a file through an HTTP query '" << monReq->getCurlQuery();
            XCEPT_RETHROW ( tsexception::CommunicationError,msg.str(),e );
        }
    }
}


CellXhannelRequest*
CellXhannelMonitor::createRequestVirtual()
{
    CellXhannelRequestMonitor* req = new CellXhannelRequestMonitor ( getLogger(),getContext(),getTargetDescriptor() );
    return req;
}

} // end ns tsframework
