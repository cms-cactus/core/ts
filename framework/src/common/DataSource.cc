#include "ts/framework/DataSource.h"

#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellAbstractContext.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/ItemGroupEvent.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "toolbox/net/URN.h"


using namespace std; 

namespace tsframework
{

DataSource::DataSource ( const string& infospaceName, CellAbstractContext* context, log4cplus::Logger& logger ) 
:
mutex_ ( true )
{
    logger_ = log4cplus::Logger::getInstance ( logger.getName() +".DataSource" );
    setContext ( context );
    toolbox::net::URN urn = getContext()->getCell()->createQualifiedInfoSpace ( infospaceName );
    infospaceName_ = urn.toString();
    infospace_ = xdata::InfoSpaceFactory::getInstance()->get ( infospaceName_ );
    infospace_->addGroupRetrieveListener ( this );
}


DataSource::~DataSource()
{
    tstoolbox::MutexHandler handler ( getMutex() );
    map<string,MonitorableItem*>::iterator i ( monitorables_.begin() );

    for ( ; i != monitorables_.end(); ++i )
    {
        delete i->second;
    }
}


void 
DataSource::actionPerformed ( xdata::Event& received )
{
  
    tstoolbox::MutexHandler handler ( getMutex() );

    if ( received.type() == "urn:xdata-event:ItemGroupRetrieveEvent" )
    {
        xdata::ItemGroupRetrieveEvent& event = dynamic_cast<xdata::ItemGroupRetrieveEvent&> ( received );

        if ( event.infoSpace()->name().find ( infospaceName_ ) != string::npos )
        { 
            InfoSpaceHandler ih ( *infospace_ );
            map<string, xdata::Serializable*> items = event.getItems();

            for ( map<string, xdata::Serializable*>::iterator i ( items.begin() ); i != items.end(); ++i )
            {
                string name ( i->first );
                map<string,MonitorableItem*>::iterator j ( monitorables_.find ( name ) );

                if ( j != monitorables_.end() )
                {
                    try
                    {
                        j->second->refresh();
                    }
                    catch ( xcept::Exception& e )
                    {
                        ostringstream msg;
                        msg << "Error refreshing monitorable '" << name << "' from infospace '" << infospaceName_ << "'. ";
                        msg << xcept::stdformat_exception_history ( e );
                        LOG4CPLUS_ERROR ( getLogger(),msg.str() );
                    }
                    catch ( bad_alloc& e )
                    {
                        ostringstream msg;
                        msg << "Error refreshing monitorable '" << name << "', infospace '" << infospaceName_ << "', class '" << typeid ( this ).name() << ". ";
                        msg << "Can not allocate more memory for that process (bad_alloc caught). This is symptom of a possible memory leak somewhere. ";
                        msg << "RESTART THE PROCESS AS SOON AS POSSIBLE!";
                        LOG4CPLUS_FATAL ( getLogger(),msg.str() );
                    }
                    catch ( exception& e )
                    {
                        ostringstream msg;
                        msg << "Error refreshing monitorable '" << name << "' from infospace '" << infospaceName_ << "': ";
                        msg << e.what();
                        LOG4CPLUS_ERROR ( getLogger(),msg.str() );
                    }
                    catch ( ... )
                    {
                        ostringstream msg;
                        msg << "Error refreshing monitorable '" << name << "' from infospace '" << infospaceName_ << "'. ";
                        msg << "Unknown exception caught";
                        LOG4CPLUS_ERROR ( getLogger(),msg.str() );
                    }
                }
                else
                {
                    ostringstream msg;
                    msg << "Monitorable item '" << name << "' does not exist in infospace '" << infospaceName_ << "'";
                    LOG4CPLUS_ERROR ( getLogger(),msg.str() );
                }
            }
        }
    }
}


void 
DataSource::remove ( const string& name )
{
    tstoolbox::MutexHandler handler ( getMutex() );

    if ( monitorables_.find ( name ) != monitorables_.end() )
    {
        if ( monitorables_.find ( name )->second->isRefreshable() )
        {
            infospace_->fireItemRevoked ( name, this );
        }

        delete monitorables_[name];
        monitorables_.erase ( name );
    }
    else
    {
        XCEPT_RAISE ( xcept::Exception,"Trying to erase a Monitorable item " + name + " that does not exist " );
    }
}


xdata::Serializable* 
DataSource::get ( const string& name )
{
    tstoolbox::MutexHandler handler ( getMutex() );

    if ( monitorables_.find ( name ) != monitorables_.end() )
    {
        return monitorables_.find ( name )->second->get();
    }
    else
    {
        XCEPT_RAISE ( xcept::Exception,"Trying to get a Monitorable item " + name + " that does not exist " );
    }
}

vector<string> 
DataSource::getNames()
{
    tstoolbox::MutexHandler handler ( getMutex() );
    vector<string> result;
    map<string,MonitorableItem*>::iterator i ( monitorables_.begin() );

    for ( ; i != monitorables_.end(); ++i )
    {
        result.push_back ( i->first );
    }

    return result;
}

//!fire item changed for one item only
void
DataSource::push ( const string& name )
{
    tstoolbox::MutexHandler handler ( getMutex() );

    if ( monitorables_.find ( name ) == monitorables_.end() )
    {
        string msg ( "Trying to push value in a Monitorable item " + name + " that does not exist " );
        XCEPT_RAISE ( xcept::Exception,msg );
    }

    list<string> names;
    names.push_back ( name );
    infospace_->fireItemGroupChanged ( names, this );
}

//!fire item changed for all items
void
DataSource::push()
{
    tstoolbox::MutexHandler handler ( getMutex() );
    list<string> names;

    for ( map<string, MonitorableItem*>::iterator itr = monitorables_.begin();
          itr != monitorables_.end();
          itr++ )
    {
        names.push_back ( itr->first );
    }

    if ( names.size() == 0 )
    {
        string msg ( "Trying to push an empty list of Monitorable items" );
        XCEPT_RAISE ( xcept::Exception,msg );
    }

    infospace_->fireItemGroupChanged ( names, this );
}


tstoolbox::Mutex& 
DataSource::getMutex()
{
    return mutex_;
}

} // end ns tsframework
