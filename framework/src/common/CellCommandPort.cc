/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#include "ts/framework/CellCommandPort.h"

#include "ts/framework/CellCommand.h"
#include "ts/framework/CellFactory.h"

#include "ts/toolbox/CellToolbox.h"

#include "log4cplus/logger.h"

#include <string>



using namespace std;

namespace tsframework
{


CellCommandPort::CellCommandPort ( log4cplus::Logger& log )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellCommandPort" );
}



xoap::MessageReference
CellCommandPort::run ( xoap::MessageReference msg )
{
	string com = tstoolbox::getCommand ( msg );
	tsframework::CellFactory* factory = context_->getCommandFactory();
	tsframework::CellCommand* command = factory->create ( com );
	command->setMsg ( msg );
	xoap::MessageReference reply = command->run();
	factory->dispose ( command );
	return reply;
}

} // end ns tsframework
