/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/
#include "ts/framework/CellOpGetState.h"

#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellOperation.h"

#include "log4cplus/logger.h" 
#include "log4cplus/loggingmacros.h" 

#include "ts/toolbox/CellToolbox.h"

#include <string>

using namespace std; 

namespace tsframework
{

CellOpGetState::CellOpGetState ( log4cplus::Logger& log, CellAbstractContext* context )
:
CellCommand ( log,context )
{
  logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellOpGetState" );
}


void CellOpGetState::code()
{
  string opid = tstoolbox::getOpid ( getMsg() );
  CellOperation& op = context_->getOperationFactory()->getOperation ( opid );
  delete payload_;
  
  LOG4CPLUS_DEBUG ( getLogger(), "CellOpGetState requested; current state: " << op.getFSM().getState() ); 
    
  this->payload_ = new xdata::String ( op.getFSM().getState() );
}
} // end ns tsframework
