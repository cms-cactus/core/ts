#include "ts/framework/CellXhannelCell.h"

#include "ts/framework/CellXhannelRequest.h"
#include "ts/framework/CellXhannelRequestCell.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/toolbox/CellNamespaceURI.h"

#include "xcept/tools.h"

#include "xdaq/XceptSerializer.h"

#include "xoap/MessageFactory.h"
#include "xoap/Method.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"
#include "log4cplus/helpers/timehelper.h"
#include <thread>
#include <chrono>

using namespace std; 

namespace tsframework
{

XDAQ_INSTANTIATOR_IMPL ( CellXhannelCell )

CellXhannelCell::CellXhannelCell ( xdaq::ApplicationStub* s )
: 
CellXhannel ( s )
{
    //binding channel listener
    xoap::bind ( this, &CellXhannelCell::response, "ResponseCell", TS_NS_URI );
}


CellXhannelCell::~CellXhannelCell()
{
    ;
}


void CellXhannelCell::send ( CellXhannelRequest* req )
{
    //arxive request
    this->setRequest ( req->getRequestId(), req );

    //get Soap message from request object
    xoap::MessageReference* msg = req->getRequest();
    xoap::MessageReference request = xoap::createMessage ( *msg );
    bool async = tstoolbox::isAsync ( request );
  
    //send message
    //synchonous or asynchronous communication
    xoap::MessageReference reply;
    int count ( 0 );
    timeval start;
    gettimeofday ( &start,0 );
    
    while ( true ) {
      
      try {
        
        ++count;
        req->setSendTimestamp();
        reply = getContext()->getApContext()->postSOAP ( request, *getApplicationDescriptor(),*getTargetDescriptor() );
        req->setReplyTimestamp();
        break;
      }
      catch ( xdaq::exception::Exception& e ) { 
        
        timeval end; 

        if ( count >= MAX_ATTEMPTS || tstoolbox::timeval_diff ( end,start ) > MAX_TIMEOUT_MS ) {
          
          XCEPT_RETHROW ( tsexception::CommunicationError, "Several communication attempts have failed. " 
                               "Before retrying please check if the cell is still operational.",e ); 
        } 
        else { 
          
          ostringstream msg; 
          msg << "Communication attempt to Trigger Supervisor cell failed (trying again)... count : " << count; 
          msg << xcept::stdformat_exception_history ( e ); 
          LOG4CPLUS_WARN ( getLogger(),msg.str() ); 
          std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
      } 
      catch ( xcept::Exception& e ) {
        
        XCEPT_RETHROW ( tsexception::CommunicationError,"Unexpected exception while trying to communicate with the " 
                                   "Trigger Supervisor cell. Before retrying please check its operational status.",e ); 
      } 
    }

    xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();

    if ( rb.hasFault() ) {
      
        ostringstream err;
        err << "SOAP reply contains SOAPFault. ";

        if ( rb.getFault().hasDetail() ) {
          
            xoap::SOAPElement detail = rb.getFault().getDetail();
            xcept::Exception rae;
            xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
            XCEPT_RETHROW ( tsexception::SoapFault, err.str(), rae );
        }
        else {
          
            err << rb.getFault().getFaultString();
            XCEPT_RAISE ( tsexception::SoapFault,err.str() );
        }
    }

    if ( async == false ){
      
        req->setReply ( reply );
    }
}


xoap::MessageReference CellXhannelCell::response ( xoap::MessageReference msg )
{
    tstoolbox::MutexHandler h ( mutex_ );

    try
    {
        string cid = tstoolbox::getCid ( msg );
        CellXhannelRequest* req = this->getRequest ( cid );

        if ( !req )
        {
            XCEPT_RAISE ( tsexception::CommunicationError,"The requested object can not be found in given response" );
        }

        req->setReply ( msg );
        req->setReplyTimestamp();
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), xcept::stdformat_exception_history ( e ) );
    }
    catch ( exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), 
                          "exception caught while executing CellXhannelCell::response(): " << e.what() );
    }
    catch ( ... )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), 
                          "Unknown exception caught while executing CellXhannelCell::response()" );
    }

    //return acknowledge
    xoap::MessageReference reply =  xoap::createMessage();
    return reply;
}


CellXhannelRequest* CellXhannelCell::createRequestVirtual()
{
    CellXhannelRequestCell* req = new CellXhannelRequestCell ( getApplicationLogger(),getContext(),getApplicationDescriptor(),TS_NS_URI );
                
    return req;
}
} // end ns tsframework
