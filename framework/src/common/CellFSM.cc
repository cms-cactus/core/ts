/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#include "ts/framework/CellFSM.h"

#include "toolbox/fsm/exception/Exception.h"


using namespace std;

namespace tsframework
{



CellFSM::CellFSM()
{
	higestState_ = 0;
	fsm_ = new toolbox::fsm::FiniteStateMachine();
}


CellFSM::~CellFSM()
{
	if ( !stateMethods_.empty() )
    	for ( map<string,StateMethodSignature*>::iterator i=stateMethods_.begin(); i != stateMethods_.end(); ++i )
    	{
    		delete i->second;
    	}

  	delete fsm_;
}


void
CellFSM::addState ( const string& state )
{
	if ( ( unsigned ) higestState_ >= 254 )
	{
		XCEPT_RAISE ( tsexception::FsmError,"It is not possible to create more than 255 states" );
	}

	//check that the state name is unique
	if ( states_.find ( state ) != states_.end() )
	{
		string msg = "State name: " + state + " is already chosen";
		XCEPT_RAISE ( tsexception::FsmError, msg );
	}

	if ( higestState_ == ( 'F' - 1 ) )
	{
		higestState_ += 2;
	}
	else
	{
		higestState_ += 1;
	}


	try
	{
		fsm_->addState ( higestState_, state );
		states_[state] = higestState_;
	}
	catch ( toolbox::fsm::exception::Exception& e )
	{
		XCEPT_RETHROW ( tsexception::FsmError, "The state " + state + " could not be added to CellFSM object", e );
	}
}


void
CellFSM::emptyTransitionMethod ( toolbox::Event::Reference e )
{
  ;
}


void
CellFSM::interStateMethod ( toolbox::fsm::FiniteStateMachine& fsm )
{
	try
	{
		string state = fsm_->getStateName ( fsm_->getCurrentState() );

		if ( stateMethods_[state]->condition() )
		{
			toolbox::Event::Reference e ( new toolbox::Event ( "move_forward", NULL ) );
			stateMethods_[state]->method();
			fsm.fireEvent ( e );
		}
		else
		{
			toolbox::Event::Reference e ( new toolbox::Event ( "move_back", NULL ) );
	  	fsm.fireEvent ( e );
		}
	}
	catch ( toolbox::fsm::exception::Exception& except )
	{
		toolbox::Event::Reference e ( new toolbox::Event ( "move_back", NULL ) );
		fsm.fireEvent ( e );
		XCEPT_RETHROW ( toolbox::fsm::exception::Exception, "Exception caught in intermediate state method of CellFSM", except );
	}
	catch ( xcept::Exception& except )
	{
		toolbox::Event::Reference e ( new toolbox::Event ( "move_back", NULL ) );
		fsm.fireEvent ( e );
		XCEPT_RETHROW ( toolbox::fsm::exception::Exception, "Exception caught in intermediate state method of CellFSM", except );
	}
	catch ( exception& except )
	{
		toolbox::Event::Reference e ( new toolbox::Event ( "move_back", NULL ) );
		fsm.fireEvent ( e );
		XCEPT_RAISE ( toolbox::fsm::exception::Exception, "exception caught in intermediate state method of CellFSM, exception caught: "+ ( string ) ( except.what() ) );
	}
	catch ( ... )
	{
		toolbox::Event::Reference e ( new toolbox::Event ( "move_back", NULL ) );
		fsm.fireEvent ( e );
		XCEPT_RAISE ( toolbox::fsm::exception::Exception, "Unexpected exception caught in intermediate state method of CellFSM, unknown caught exception" );
	}
}


void
CellFSM::reset()
{
	try
	{
		fsm_->reset();
  	}
	catch ( toolbox::fsm::exception::Exception e )
  	{
		XCEPT_RETHROW ( tsexception::FsmError, "CellFSM not initialized", e );
  	}
}


string
CellFSM::getState()
{
	CellFSM::State s = fsm_->getCurrentState();
	string state = fsm_->getStateName ( s );
  	return state;
}


void
CellFSM::fireEvent ( toolbox::Event::Reference e )
{
	fsm_->fireEvent ( e );
}


vector<CellFSM::State>
CellFSM::getAllStates()
{
	return fsm_->getStates();
}


vector<CellFSM::State>
CellFSM::getStates()
{
	vector<CellFSM::State> allStates = fsm_->getStates();
  	vector<CellFSM::State> visibleStates;
  	vector<CellFSM::State>::iterator i;

	for ( i = allStates.begin(); i != allStates.end(); i++ )
	{
		if ( isVisible ( *i ) )
		{
			visibleStates.push_back ( *i );
		}
	}

	return visibleStates;
}


bool
CellFSM::isVisible ( const CellFSM::State& state )
{
	map<string, CellFSM::State>::iterator i;

	for ( i = states_.begin(); i != states_.end(); i++ )
	{
		if ( i->second == state )
		{
			return true;
		}
	}

	return false;
}

map<string, CellFSM::State>
CellFSM::getTransitions ( const CellFSM::State& state )
{
	map<string, CellFSM::State> allTransitions = fsm_->getTransitions ( state );
	map<string, CellFSM::State> visibleTransitions;

	for ( map<string, CellFSM::State>::iterator i ( allTransitions.begin() ); i != allTransitions.end(); i++ )
	{
		map<string, CellFSM::State> auxTransitions = fsm_->getTransitions ( i->second );
		CellFSM::State finalState = auxTransitions["move_forward"];
		visibleTransitions[i->first] = finalState;
	}

	return visibleTransitions;
}


string
CellFSM::getStateName ( const CellFSM::State& state )
{
	return fsm_->getStateName ( state );
}


CellFSM::State
CellFSM::getCurrentState()
{
	return fsm_->getCurrentState();
}


CellFSM::State
CellFSM::getInitialState()
{
	return fsm_->getInitialState();
}


map<string, CellFSM::State>
CellFSM::getAllTransitions ( const CellFSM::State& state )
{
  	return fsm_->getTransitions ( state );
}


set<string>
CellFSM::getInputs ( const CellFSM::State& state )
{
  	return fsm_->getInputs ( state );
}


void
CellFSM::setInitialState ( const string& st )
{
	if ( states_.find ( st ) == states_.end() )
	{
		string msg = "Can not define state '" + st + "' as initial. State name: " + st + " does not exist";
		XCEPT_RAISE ( tsexception::FsmError, msg );
	}

	CellFSM::State state = states_[st];
	fsm_->setInitialState ( state );
}


bool
CellFSM::isCurrentTransition ( const CellFSM::State& state, const string& transition )
{
	map<CellFSM::State, map<string,CellFSM::State> >::iterator i ( transition2state_.find ( state ) );

	if ( i == transition2state_.end() )
	{
		return false;
	}

	map<string,CellFSM::State>::iterator j ( i->second.find ( transition ) );

	if ( j != i->second.end() )
	{
		if ( j->second == fsm_->getCurrentState() )
		{
			return true;
		}

	}

	return false;

}


void
CellFSM::executeTransition ( const string& transition )
{
	toolbox::Event::Reference e ( new toolbox::Event ( transition, NULL ) );
	fsm_->fireEvent ( e );
}


bool CellFSM::isFirable ( const string& transition )
{
	if ( transition.find ( "move_" ) != string::npos )
	{
		return false;
	}
	else
	{
		return true;
	}
}


string
CellFSM::getRollbackTransition ( const string& from, const string& transition )
{
	string rollback;
	rollback = "move_back_";
	rollback += from + "_";
	rollback += transition;
	return rollback;
}


void
CellFSM::clear()
{
	if ( !stateMethods_.empty() )
	{
		for ( map<string,StateMethodSignature*>::iterator i=stateMethods_.begin(); i != stateMethods_.end(); ++i )
		{
			delete i->second;
		}

		stateMethods_.clear();
		states_.clear();
		transition2state_.clear();
		higestState_ = 0;
		delete fsm_;

		fsm_ = new toolbox::fsm::FiniteStateMachine();
	}
}

} // end ns tsframework
