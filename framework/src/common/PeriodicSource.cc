#include "ts/framework/PeriodicSource.h"

#include "ts/framework/CellAbstractContext.h"

#include "toolbox/task/TimerFactory.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "xcept/tools.h"


using namespace std; 

namespace tsframework
{

PeriodicSource::PeriodicSource ( const string& infospaceName,
    CellAbstractContext* context,
    log4cplus::Logger& logger,
    int seconds )
: 
DataSource ( infospaceName, context, logger ),
period_(seconds)
{
    logger_ = log4cplus::Logger::getInstance ( logger.getName() +".PeriodicSource" );
    this->setContext ( context );

    ostringstream timerNameStream;
    timerNameStream << "_tsframework_periodicsource_" << infospaceName << "_" << rand();
    timerName_ = timerNameStream.str();

    timer_ = toolbox::task::getTimerFactory()->createTimer ( timerName_ );
    toolbox::TimeInterval interval ( period_, 0 );
    toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
    timer_->scheduleAtFixedRate ( start, this, interval, 0, "" );

    LOG4CPLUS_INFO ( getLogger(), "Created PeriodicSource \'" + timerName_ + "\'" );
}


PeriodicSource::~PeriodicSource()
{
    toolbox::task::getTimerFactory()->removeTimer ( timerName_ );
}


void 
PeriodicSource::timeExpired ( toolbox::task::TimerEvent& )
{
  
    try
    {
        periodicAction();
    }
    catch ( xcept::Exception& e )
    {
        ostringstream msg;
        msg << "Error executing PeriodicSource::periodicAction \'" << timerName_ << "\'.";
        msg << xcept::stdformat_exception_history ( e );
        LOG4CPLUS_ERROR ( getLogger(),msg.str() );
    }
    catch ( bad_alloc& e )
    {
        ostringstream msg;
        msg << "bad_alloc caught while executing executing PeriodicSource::periodicAction() \'" << timerName_ << "\'.";
        msg << "Can not allocate more memory for that process (bad_alloc caught). This is symptom of a possible memory leak somewhere.";
        msg << "RESTART THE PROCESS AS SOON AS POSSIBLE!";
        LOG4CPLUS_FATAL ( getLogger(),msg.str() );
    }
    catch ( exception& e )
    {
        ostringstream msg;
        msg << "exception caught while executing PeriodicSource::periodicAction() \'" << timerName_ << "\': ";
        msg << e.what() ;
        LOG4CPLUS_ERROR ( getLogger(),msg.str() );
    }
    catch ( ... )
    {
        ostringstream msg;
        msg << "Unknown exception caught while executing PeriodicSource::periodicAction() \'" << timerName_ << "\'";
        LOG4CPLUS_ERROR ( getLogger(), msg.str() );
    }
}

void
PeriodicSource::start()
{
    tstoolbox::MutexHandler handler ( getMutex() );

    if ( !timer_->isActive() )
    {
        timer_->start();
        toolbox::TimeInterval interval ( period_, 0 );
        toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
        timer_->scheduleAtFixedRate ( start, this, interval, 0, "" );
    }
}

void
PeriodicSource::stop()
{
    tstoolbox::MutexHandler handler ( getMutex() );

    if ( timer_->isActive() )
    {
        timer_->stop();
    }
}

void
PeriodicSource::setPeriod ( int seconds )
{
    tstoolbox::MutexHandler handler ( getMutex() );
    period_ = seconds;
}

} // end ns tsframework

