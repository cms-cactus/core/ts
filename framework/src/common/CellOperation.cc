/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/
#include "ts/framework/CellOperation.h"

#include "ts/framework/CellAbstractContext.h"

#include "xdata/Serializable.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "xcept/tools.h"


using namespace std;

namespace tsframework
{


CellOperation::CellOperation ( log4cplus::Logger& log, CellAbstractContext* context )
:lock_ ( true )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellOperation" );
	context_ = context;
}


CellOperation::~CellOperation()
{
	if ( !paramList_.empty() )
	{
		for ( map<string,xdata::Serializable*>::iterator i ( paramList_.begin() ); i != paramList_.end(); ++i )
		{
			delete i->second;
		}
	}
}


string
CellOperation::getId()
{
	return operationId_;
}


void
CellOperation::setId ( const string& opId )
{
	operationId_ = opId;
}


map<string, xdata::Serializable*>&
CellOperation::getParamList()
{
	return paramList_;
}


void
CellOperation::setParameterMap ( map<string, xdata::Serializable*> param )
{
	map<string, xdata::Serializable*>::iterator i;

	for ( i = param.begin(); i != param.end(); i++ )
  	{
		if ( paramList_.find ( i->first ) == paramList_.end() )
    	{
			string msg = "The parameter " + i->first + " is not a parameter of the operation '" + getId() + "'. The existing parameters are: ";

			for ( map<string, xdata::Serializable*>::iterator j = paramList_.begin(); j != paramList_.end(); j++ )
			{
				msg += " " + j->first + " ";
      		}

			msg += ".";
      		LOG4CPLUS_ERROR ( getLogger(), msg );
    	}
    	else
    	{
    		setParameterFromString ( i->first, ( i->second )->toString() );
    	}
  	}
}


void
CellOperation::setParameterFromString ( const string& name, const string& value )
{
	map<string, xdata::Serializable*>::iterator i = paramList_.find ( name );

	if ( i != paramList_.end() )
	{
		( i->second )->fromString ( value );
	}
	else
	{
		XCEPT_RAISE ( tsexception::ParameterError,"Parameter '" + name + "' not found in CellOperation parameter list" );
	}
}


void
CellOperation::setSessionId ( const string& sid )
{
  	currentSid_ = sid;
}


string
CellOperation::getSessionId()
{
	return currentSid_;
}

CellWarning&
CellOperation::getWarning()
{
	return warning_;
}


CellFSM&
CellOperation::getFSM()
{
	return fsm_;
}


tstoolbox::Mutex&
CellOperation::getLock()
{
	return lock_;
}


string
CellOperation::getResult()
{
	string result = result_.toString();
	return result;
}


void
CellOperation::setResult ( const string& result )
{
	result_ = result;
}


void
CellOperation::reset()
{
	try
	{
		resetting();
	}
	catch ( xcept::Exception& e )
  	{
		ostringstream msg;
		msg << "xcept::Exception during reset of operation '" << getId() << "'.";
		msg << xcept::stdformat_exception_history ( e );
		getWarning().setLevel ( CellWarning::WARNING );
		getWarning().setMessage ( msg.str() );
	}
	catch ( exception& e )
	{
		ostringstream msg;
		msg << "exception during reset of operation '" << getId() << "'.";
		msg << e.what();
		getWarning().setLevel ( CellWarning::WARNING );
		getWarning().setMessage ( msg.str() );
	}
	catch ( ... )
	{
		ostringstream msg;
		msg << "Unknown exception during reset of operation '" << getId() << "'.";
		getWarning().setLevel ( CellWarning::WARNING );
		getWarning().setMessage ( msg.str() );
	}

	if ( getWarning().getLevel() >= CellWarning::ERROR )
	{
		getWarning().append ( "\n\nSeverity level downgraded from ERROR to WARNING...\n",CellWarning::WARNING );
		getWarning().setLevel ( CellWarning::WARNING );
	}

	getFSM().reset();
}

} // end ns tsframework
