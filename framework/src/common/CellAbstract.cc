#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellPanelFactory.h"
#include "ts/framework/CellCommandPort.h"
#include "ts/framework/CellWarning.h"
#include "ts/framework/CellLocalSession.h"
#include "ts/framework/CellFactory.h"
#include "ts/framework/CellOpSendCommand.h"
#include "ts/framework/CellErrorCommand.h"
#include "ts/framework/CellOpGetState.h"
#include "ts/framework/CellOpReset.h"
#include "ts/framework/CellPingMe.h"
#include "ts/framework/CellOperationTypes.h"
#include "ts/framework/CellOperationParamList.h"
#include "ts/framework/CellOperationTypesAndParams.h"
#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelXdaqSimple.h"
#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellPage.h"

#include "ts/toolbox/CellXhannelListDeclaration.h"
#include "ts/toolbox/CellNamespaceURI.h"
#include "ts/toolbox/CellToolbox.h"
#include "ts/toolbox/Gzip.h"

#include "ts/framework/GuiAbout.h"

#include "xoap/Method.h"
#include "xoap/SOAPBody.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/MessageReference.h"

#include "xgi/Method.h"

#include "xcept/tools.h"

#include "ajax/toolbox.h"

#include "xdaq/XceptSerializer.h"

#include <boost/smart_ptr/scoped_ptr.hpp>


using namespace std;

namespace tsframework
{

CellAbstract::CellAbstract ( xdaq::ApplicationStub* s )
                              :
                              xdaq::Application ( s ),
                              cellContext_ ( 0 )
{
  srand ( time ( NULL ) );
  getApplicationInfoSpace()->fireItemAvailable ( "xhannelListUrl",&xhannelListUrl_ );
  getApplicationInfoSpace()->fireItemAvailable ( "name",&name_ );
  stateName_ = "Running";
  getApplicationInfoSpace()->fireItemAvailable ( "stateName",&stateName_ );

  //bind parameters
  getApplicationInfoSpace()->addListener ( this, "urn:xdaq-event:setDefaultValues" );
  xgi::bind ( this,&CellAbstract::Default, "Default" );
  xoap::bind ( this, &CellAbstract::guiResponse, "guiResponse", TS_NS_URI );
}


xoap::MessageReference CellAbstract::command ( xoap::MessageReference msg )
{
  xoap::MessageReference reply;

  try
  {
    reply =  getContext()->getCommandPort()->run ( msg );
  }
  catch ( xcept::Exception& e )
  {
    LOG4CPLUS_ERROR ( getLogger(),xcept::stdformat_exception_history ( e ) );
    return createSoapFault ( e );
  }
  catch ( bad_alloc& e )
  {
    ostringstream msg;
    msg << "bad_alloc while executing CellAbstract::command(). ";
    msg << "Can not allocate more memory for that process (bad_alloc caught). This is a symptom of a possible memory leak. ";
    msg << "RESTART THE PROCESS AS SOON AS POSSIBLE!";
    LOG4CPLUS_FATAL ( getLogger(),msg.str() );
    XCEPT_DECLARE ( tsexception::CommandExecutionError, q , msg.str() );
    return createSoapFault ( q );
  }
  catch ( exception& e )
  {
    ostringstream msg;
    msg << "exception caught while executing CellAbstract::command(): ";
    msg << e.what() ;
    LOG4CPLUS_ERROR ( getLogger(),msg.str() );
    XCEPT_DECLARE ( tsexception::CommandExecutionError, q , msg.str() );
    return createSoapFault ( q );
  }
  catch ( ... )
  {
    ostringstream msg;
    msg << "Unknown exception caught while executing CellAbstract::command()";
    LOG4CPLUS_ERROR ( getLogger(),msg.str() );
    XCEPT_DECLARE ( tsexception::CommandExecutionError, q , msg.str() );
    return createSoapFault ( q );
  }

  return reply;
}


xoap::MessageReference CellAbstract::guiResponse ( xoap::MessageReference msg )
{
  try
  {
    string sessionId = tstoolbox::getSid ( msg );
    string commandId = tstoolbox::getCid ( msg );
    string replyString ( "" );
    string warningMessage ( "" );
    int warningLevel ( 0 );
    xoap::SOAPBody body = msg->getSOAPPart().getEnvelope().getBody();

    if ( body.hasFault() && getContext()->hasSession ( sessionId ) )
    {
      ostringstream err;
      err <<  body.getFault().getFaultString();
      warningLevel = CellWarning::ERROR;
      warningMessage = err.str();
      LOG4CPLUS_ERROR ( getLogger(),"GUI Reply error. " + err.str() );
      getContext()->getSessionMap() [sessionId]->addReply ( commandId,replyString,warningMessage,warningLevel );
    }
    else if ( getContext()->hasSession ( sessionId ) )
    {
      boost::scoped_ptr<xdata::Serializable> r ( tstoolbox::getPayload ( msg ) );

      if ( r.get() )
      {
        replyString += r->toString();
      }

      boost::scoped_ptr<xdata::Serializable> wmsg ( tstoolbox::getWarningMessage ( msg ) );

      if ( wmsg.get() )
      {
        warningMessage = wmsg->toString();
      }

      boost::scoped_ptr<xdata::Serializable> wlvl ( tstoolbox::getWarningLevel ( msg ) );

      if ( wlvl.get() )
      {
        if ( xdata::Integer* q = dynamic_cast<xdata::Integer*> ( wlvl.get() ) )
        {
          warningLevel = *q;
        }
      }

      getContext()->getSessionMap() [sessionId]->addReply ( commandId,replyString,warningMessage,warningLevel );
    }
    else
    {
      XCEPT_RAISE ( tsexception::SessionError,"SessionId '"+ sessionId + "' of an incoming message doesn't exist" );
    }
  }
  catch ( xcept::Exception& e )
  {
    LOG4CPLUS_ERROR ( getLogger(), xcept::stdformat_exception_history ( e ) );
  }
  catch ( bad_alloc& e )
  {
    ostringstream msg;
    msg << "bad_alloc while executing CellAbstract::guiResponse(). ";
    msg << "Can not allocate more memory for that process (bad_alloc caught). This is a symptom of a possible memory leak. ";
    msg << "RESTART THE PROCESS AS SOON AS POSSIBLE!";
    LOG4CPLUS_FATAL ( getLogger(), msg.str() );
  }
  catch ( exception& e )
  {
    ostringstream msg;
    msg << "exception caught while executing CellAbstract::guiResponse(): ";
    msg << e.what() ;
    LOG4CPLUS_ERROR ( getLogger(), msg.str() );
  }
  catch ( ... )
  {
    ostringstream msg;
    msg << "Unknown exception caught  while executing CellAbstract::guiResponse(). ";
    LOG4CPLUS_ERROR ( getLogger(), msg.str() );
  }

  //acknowledge
  return xoap::createMessage();
}


void CellAbstract::actionPerformed ( xdata::Event& e )
{
  if ( e.type() == "urn:xdaq-event:setDefaultValues" )
  {
    tstoolbox::CellXhannelListDeclaration xDeclaration;
    vector<string> xList;

    try
    {
      xDeclaration.load ( xhannelListUrl_.toString() );
      xList = xDeclaration.getXhannelNames();
    }
    catch ( xcept::Exception& e )
    {
      LOG4CPLUS_FATAL ( getLogger(),xcept::stdformat_exception_history ( e ) );
    }

    for ( vector<string>::iterator iter = xList.begin(); iter != xList.end(); iter++ )
    {
      try
      {
        string name = *iter;
        string type = xDeclaration.getTargetAttribute ( name,"type" );
        string app = xDeclaration.getTargetAttribute ( name,"application" );
        string instance = xDeclaration.getTargetAttribute ( name,"instance" );
        unsigned long targetInstance = str2uint ( instance );
        createXhannel ( name,type, app, targetInstance );
      }
      catch ( xcept::Exception& e )
      {
        ostringstream msg;
        msg << "Error during the creation of a xhannel. The initialization of the Cell is not consistent. Solve the bug before continuing. ";
        msg << xcept::stdformat_exception_history ( e );
        LOG4CPLUS_FATAL ( getLogger(),msg.str() );
      }
    }

    try
    {
      CellFactory* cmdF = getContext()->getCommandFactory();
      cmdF->add<CellOpGetState> ( "System","OpGetState" );
      cmdF->add<CellOpSendCommand> ( "System","OpSendCommand" );
      cmdF->add<CellOpReset> ( "System","OpReset" );
      cmdF->add<CellPingMe> ( "Default","PingMe" );
      cmdF->add<CellErrorCommand> ( "System","ErrorCommand" );
      cmdF->add<CellOperationTypes> ( "Default","GetOperationTypes" );
      cmdF->add<CellOperationParamList> ( "Default","GetOperationParamList" );
      cmdF->add<CellOperationTypesAndParams> ( "Default","GetOperationTypesAndParams" );
      tsframework::CellPanelFactory* panelF = getContext()->getPanelFactory();
      panelF->add<frameworkpanels::GuiAbout>("About");
    }
    catch ( xcept::Exception& e )
    {
      ostringstream msg;
      msg << "Error adding a command, operation or contorl panel to a factory. The initialization of the Cell is not consistent. Solve the bug before continuing.";
      msg << xcept::stdformat_exception_history ( e );
      LOG4CPLUS_FATAL ( getLogger(),msg.str() );
    }

    try
    {
      init();
      LOG4CPLUS_INFO ( getLogger(),"Cell '" + getName() + "' has been initialized succesfully" );
    }
    catch ( xcept::Exception& e )
    {
      ostringstream msg;
      msg << "Error initializing the Cell: " << xcept::stdformat_exception_history ( e );
      LOG4CPLUS_FATAL ( getLogger(),msg.str() );
    }
    catch ( exception& e )
    {
      ostringstream msg;
      msg << "Error initializing the Cell: " << e.what();
      LOG4CPLUS_FATAL ( getLogger(),msg.str() );
    }
    catch ( ... )
    {
      ostringstream msg;
      msg << "Error initializing the Cell: Unknown exception type";
      LOG4CPLUS_FATAL ( getLogger(),msg.str() );
    }
  }
}

void CellAbstract::createXhannel ( const string& name, const string& type,
                                   const string& app, unsigned long targetInstance )
{
  try
  {
    CellXhannel* x;

    if ( type == "cell" )
    {
      x = createXhannel<CellXhannelCell> ( "tsframework::CellXhannelCell",app,targetInstance );
    }
    else if ( type == "tstore" )
    {
        x = createXhannel<CellXhannelTB> ( "tsframework::CellXhannelTB",app,targetInstance );
    }
    else if ( type == "xdaq" )
    {
      x = createXhannel<CellXhannelXdaqSimple> ( "tsframework::CellXhannelXdaqSimple",app,targetInstance );
    }
    else if ( type == "monitor" )
    {
        x = createXhannel<CellXhannelMonitor> ( "tsframework::CellXhannelMonitor",app,targetInstance );
    }
    else
    {
        XCEPT_RAISE ( tsexception::XhannelCreationError, "Unsupported xhannel type '" + type + "'" );
    }

    getContext()->setXhannel ( name, x );
  }
  catch ( xcept::Exception& e )
  {
    ostringstream msg;
    msg << "Cannot create xhannel (name='" << name << "', type='" << type << "', application='" << app << "', ";
    msg << "instance='" << targetInstance << "'). Exception caught: ";
    XCEPT_RETHROW ( tsexception::XhannelCreationError,msg.str(),e );
  }
}


string CellAbstract::getNamespace()
{
  return TS_NS_URI;
}


unsigned long CellAbstract::getMaxLocalId()
{
  unsigned long int max = 1;
  set<const xdaq::ApplicationDescriptor*> vAppDesc = this->getApplicationContext()->getDefaultZone()->getApplicationDescriptors ( getApplicationContext()->getContextDescriptor() );
  set<const xdaq::ApplicationDescriptor* >::iterator i;

  for ( i = vAppDesc.begin(); i != vAppDesc.end(); ++i )
  {
    const xdaq::ApplicationDescriptor* appd= *i;
    unsigned long aux = appd->getLocalId();

    if ( max < aux )
    {
      max = aux;
    }
  }

  return max;
}


string CellAbstract::getXhannelListUrl()
{
  return xhannelListUrl_.toString();
}


void CellAbstract::Default ( xgi::Input* in, xgi::Output* out )
{
  LOG4CPLUS_DEBUG ( getLogger(),"Start of the 'Default' callback" );
  tstoolbox::MutexHandler handler ( getContext()->getLock() );
  cgicc::Cgicc cgi ( in );
  string id ( ajax::toolbox::getSubmittedValue ( cgi,"_id_" ) );
  string eventName ( ajax::toolbox::getSubmittedValue ( cgi,"_eventType_" ) );
  string sessionId ( ajax::toolbox::getSubmittedValue ( cgi,"_sessionid_" ) );

  std::string const acceptEncoding = in->getenv("ACCEPT_ENCODING");
  bool gzip = (!acceptEncoding.empty() && (acceptEncoding.find("gzip") != std::string::npos));
  ostringstream gzipOut;

  try
  {
    timeval start;
    gettimeofday ( &start,0 );
    LOG4CPLUS_DEBUG ( getLogger(),"HTTP request received: id = " + id + ", eventType= " + eventName + ", session id = " + sessionId );
    out->getHTTPResponseHeader().addHeader ( "Content-Type", "text/html; charset=utf-8" );
    // out->getHTTPResponseHeader().addHeader ( "Cache-Control", "post-check=0, pre-check=0,no-store, no-cache, must-revalidate, max-age=0" );
    out->getHTTPResponseHeader().addHeader ( "Cache-Control", "private, max-age=3600" );
    // out->getHTTPResponseHeader().addHeader ( "Pragma", "no-cache" );
    out->getHTTPResponseHeader().addHeader ( "X-Clacks-Overhead", "GNU Terry Pratchett, also ↑↑↓↓←→←→ba" );
    if (gzip) {
      out->getHTTPResponseHeader().addHeader ( "Content-Encoding", "gzip" );
    }
    map<string,CellLocalSession*>::iterator i ( getContext()->getSessionMap().find ( sessionId ) );

    LOG4CPLUS_DEBUG ( getLogger(),"HTTP request: " << cgi.getEnvironment().getPathTranslated());
    std::size_t found = cgi.getEnvironment().getPathTranslated().find("/",1);
    LOG4CPLUS_DEBUG ( getLogger(),"HTTP request / pos: " << found);

    if ( found==std::string::npos )
    {
      //requests to /urn:xdaq-application:lid=13 should be /urn:xdaq-application:lid=13/ (otherwise you get 2 session cookies)
      LOG4CPLUS_DEBUG ( getLogger(),"redirecting HTTP request to main page");
      ostringstream redirectUrl;
      redirectUrl <<
          ((cgi.getEnvironment().usingHTTPS()) ? "https://" : "http://")
          << cgi.getEnvironment().getServerName() <<
          ":"
          << cgi.getEnvironment().getServerPort() <<
          "/"
          << getContext()->getLocalUrn() <<
          "/#!/";

      out->getHTTPResponseHeader().getStatusCode ( 301 );
      out->getHTTPResponseHeader().addHeader ( "Location", redirectUrl.str() );
      LOG4CPLUS_DEBUG ( getLogger(),"Redirecting to " << redirectUrl.str() );
    }
    else
    {
      if ( i == getContext()->getSessionMap().end() )
      {
        //create a new session if absent or expired
        CellLocalSession* session = new CellLocalSession ( getContext(),getLogger() );
        ostringstream newid;
        newid << & ( session->getPage() );
        sessionId=newid.str();
        session->setSessionId ( sessionId );
        getContext()->getSessionMap().insert ( make_pair ( sessionId,session ) );
        session->getPage().setAppPath ( getContext()->getCell()->getApplicationDescriptor()->getAttribute("appPath") );
        session->getPage().setImports ( getContext()->getImports() );
        session->getPage().setPrimaryColor( getContext()->getPrimaryColor() );
        session->getPage().setSecondaryColor( getContext()->getSecondaryColor() );
        out->getHTTPResponseHeader().addHeader("_sessionid_", sessionId);

        LOG4CPLUS_DEBUG ( getLogger(),"New session created\n"
                          "sessionid: " << sessionId << "\n"
                          "Host: " << cgi.getEnvironment().getRemoteHost() << "\n"
                          "Address: " << cgi.getEnvironment().getRemoteAddr() << "\n"
                          "Request: " << cgi.getEnvironment().getScriptName() << "\n"
                          "Path info: " << cgi.getEnvironment().getPathInfo() << "\n"
                          "Path translated: " << cgi.getEnvironment().getPathTranslated() << "\n"
                          "Request method: " << cgi.getEnvironment().getRequestMethod() << "\n"
                          "Query: " << cgi.getEnvironment().getQueryString()
                        );
        if ( !id.empty() && !eventName.empty() )
        {
          //fake a default page load to re-link event handlers
          CellPage& page = getContext()->getSessionMap().find ( sessionId )->second->getPage();
          page.html ( cgi,gzipOut );
        }
      }

      if ( id.empty() && eventName.empty() )
      {
        CellPage& page = getContext()->getSessionMap().find ( sessionId )->second->getPage();
        page.html ( cgi,gzipOut );
      }
      else if ( !id.empty() && !eventName.empty() )
      {
        CellPage& page = getContext()->getSessionMap().find ( sessionId )->second->getPage();
        page.callHandler ( cgi,gzipOut );
      }
      else
      {
        out->getHTTPResponseHeader().getStatusCode ( 400 );
        gzipOut << "<p>Client Error</p>";
        gzipOut << "<p>Can not retrieve callback for sessionId = '" << sessionId << "', id = '" << id << "', eventType = '" << eventName << "'</p>";
      }
    }

    timeval end;
    gettimeofday ( &end,0 );
    ostringstream msg;
    msg << "HTTP/CGI Callback has taken " << tstoolbox::timeval_diff ( end,start ) << " ms";
    LOG4CPLUS_DEBUG ( getLogger(), msg.str() );
  }
  catch ( xcept::Exception& e )
  {
    ostringstream msg;
    msg << "xcept::Exception caught while creating the Trigger Supervisor GUI response.";
    msg << "Error happended for sessionId='" << sessionId << "', id='" << id << "', and eventType='" << eventName <<"'." << endl;
    XCEPT_DECLARE_NESTED ( tsexception::GuiError, q , "xcept::Exception caught while creating the Trigger Supervisor GUI response",e );
    LOG4CPLUS_ERROR ( getLogger(),xcept::stdformat_exception_history ( q ) );
    out->getHTTPResponseHeader().getStatusCode ( 500 );
    gzipOut << xcept::htmlformat_exception_history ( q );
  }
  catch ( ajax::CallbackNotFound& e )
  {
    ostringstream msg;
    msg << "There is no handler for sessionId='" << sessionId << "', id='" << id << "', and eventType='" << eventName <<"'." << endl;
    msg << "Server is not Synchronized with the AJAX application. Try again or refresh to continue";
    LOG4CPLUS_WARN ( getLogger(),msg.str() );
  }
  catch ( bad_alloc& e )
  {
    ostringstream msg;
    msg << "bad_alloc caught while creating the Trigger Supervisor web page." << endl;
    msg << "Can not allocate more memory for that process (bad_alloc caught). This is a symptom of a possible memory leak." << endl;
    msg << "RESTART THE PROCESS AS SOON AS POSSIBLE!" <<endl;
    XCEPT_DECLARE ( tsexception::PossibleMemoryLeak, q , msg.str() );
    LOG4CPLUS_ERROR ( getLogger(),xcept::stdformat_exception_history ( q ) );
    out->getHTTPResponseHeader().getStatusCode ( 500 );
    gzipOut << xcept::htmlformat_exception_history ( q );
  }
  catch ( exception& e )
  {
    ostringstream msg;
    msg << "exception caught while creating the Trigger Supervisor web page: " << e.what();
    msg << "Error happended for sessionId='" << sessionId << "', id='" << id << "', and eventType='" << eventName <<"'." << endl;
    XCEPT_DECLARE ( tsexception::GuiError, q , msg.str() );
    LOG4CPLUS_ERROR ( getLogger(),xcept::stdformat_exception_history ( q ) );
    out->getHTTPResponseHeader().getStatusCode ( 500 );
    gzipOut << xcept::htmlformat_exception_history ( q );
  }
  catch ( ... )
  {
    ostringstream msg;
    msg << "Unexpected exception caught while creating the Trigger Supervisor web page";
    msg << "Error happended for sessionId='" << sessionId << "', id='" << id << "', and eventType='" << eventName <<"'." << endl;
    XCEPT_DECLARE ( tsexception::GuiError, q , msg.str() );
    LOG4CPLUS_ERROR ( getLogger(),xcept::stdformat_exception_history ( q ) );
    out->getHTTPResponseHeader().getStatusCode ( 500 );
    gzipOut << xcept::htmlformat_exception_history ( q );
  }

  if (gzip) {
    *out << tstoolbox::Gzip::encode(gzipOut.str());
  } else {
    *out << gzipOut.str();
  }

  LOG4CPLUS_DEBUG ( getLogger(),"End of the 'Default' callback" );
}


string CellAbstract::newline2br ( const string& str )
{
  string result ( str );
  size_t pos=0;
  pos = result.find ( '\n', pos );

  while ( pos < result.npos )
  {
    result.erase ( pos, 1 );
    result.insert ( pos, "<br />", 6 );
    pos = result.find ( '\n', pos );
  }

  return result;
}


unsigned long CellAbstract::str2uint ( const string& str )
{
  char* pEnd;
  errno=0;
  long number = strtol ( str.c_str(), &pEnd, 10 );

  if ( errno != 0 )
  {
    XCEPT_RAISE ( tsexception::ConversionError,"String '" + str + "' is not a number" );
  }

  if ( number < 0 )
  {
    XCEPT_RAISE ( tsexception::ConversionError,"Number '" + str + "' must be positive" );
  }

  return ( unsigned long ) number;
}


xoap::MessageReference CellAbstract::createSoapFault ( xcept::Exception& e )
{
  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
  xoap::SOAPFault f = b.addFault();
  f.setFaultCode ( "Server" );
  f.setFaultString ( tstoolbox::escape ( e.what() ) );
  xoap::SOAPElement detail = f.addDetail();
  xdaq::XceptSerializer::writeTo ( e,detail.getDOM() );
  return reply;
}


log4cplus::Logger& CellAbstract::getLogger()
{
  return getApplicationLogger();
}

} // end ns tsframework
