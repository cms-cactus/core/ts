/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/
#include "ts/framework/CellOpSendCommand.h"

#include "ts/framework/CellOperation.h"
#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellAbstractContext.h"

#include "ts/exception/CellException.h"

#include "ts/toolbox/CellToolbox.h"

#include "xcept/tools.h"


using namespace std; 

namespace tsframework
{


CellOpSendCommand::CellOpSendCommand ( log4cplus::Logger& log, CellAbstractContext* context )
:
CellCommand ( log,context )
{
    logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellOpSendCommand" );
}


void 
CellOpSendCommand::code()
{
    string opid = tstoolbox::getOpid ( getMsg() );
    CellOperation& op = getContext()->getOperationFactory()->getOperation ( opid );

    tstoolbox::MutexHandler handler ( op.getLock() );

    string transition = tstoolbox::getOpComName ( getMsg() );
    CellFSM::State state ( op.getFSM().getCurrentState() );
    map<string, CellFSM::State> transitions ( op.getFSM().getTransitions ( state ) );


    if ( transitions.find ( transition ) == transitions.end() || not op.getFSM().isFirable ( transition ) ) {

      ostringstream msg;
      msg << "Transition '" << transition << "' is not allowed from " << opid 
          << "@" << op.getFSM().getStateName ( state ) << ". The allowed transitions are: [";

      for ( map<string, toolbox::fsm::State>::iterator i ( transitions.begin() ); i != transitions.end(); ++i ) {
          if ( op.getFSM().isFirable ( i->first ) ) {
            
              msg << i->first << ", ";
          }
      }

      msg << "].";
      XCEPT_RAISE ( tsexception::OperationExecutionError,msg.str() );
    }

    op.setParameterMap ( this->getParamList() );
    CellFSM::State previousState = op.getFSM().getCurrentState();

    try {
      
        UnexpectedHandler uhandler();
        op.setResult ( "" );
        op.getWarning().clear();
        LOG4CPLUS_INFO ( getLogger(),"Executing transition: " << opid << "." << transition << "..." ); 
        op.getFSM().executeTransition ( transition );
    }
    catch ( xcept::Exception& e ) {
      
        ostringstream msg;
        msg << "xcept::Exception caught while executing transition '" << transition << "' on operation '" << opid << "'.";
        msg << xcept::stdformat_exception_history ( e );
        op.getWarning().setLevel ( CellWarning::ERROR );
        op.getWarning().setMessage ( msg.str() );
        ostringstream err;
        err << "xcept::Exception caught while executing transition '" << transition << "' on operation '" << opid << "'.";
        XCEPT_RETHROW ( tsexception::CommandExecutionError,err.str(),e );
    }

    if ( previousState != op.getFSM().getCurrentState() &&
        op.getWarning().getLevel() >= CellWarning::ERROR )
    {
        UnexpectedHandler uhandler;
        string from = op.getFSM().getStateName ( previousState );
        string rollback = op.getFSM().getRollbackTransition ( from,transition );
        op.getFSM().executeTransition ( rollback );
    }

    delete payload_;
    this->payload_ = new xdata::String ( op.getResult() );
    getWarning().setLevel ( op.getWarning().getLevel() );
    getWarning().setMessage ( op.getWarning().getMessage() );
}
} // end ns tsframework
