#include "ts/framework/CellCommandWidget.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellCommand.h"
#include "ts/framework/CellFactory.h"
#include "ts/framework/CellCommandPort.h"
#include "ts/framework/CellWarning.h"
#include "ts/framework/CellLocalSession.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/toolbox/CellNamespaceURI.h"

#include "ajax/toolbox.h"
#include "ajax/PolymerElement.h"
#include "ajax/JSONTree.h"
#include "ajax/JSONTreeNode.h"
#include <ctime>

#include "xoap/SOAPBody.h"
#include "xoap/SOAPEnvelope.h"

#include "log4cplus/logger.h"

#include "xcept/tools.h"

#include "cgicc/Cgicc.h"

#include <sstream>
#include <iomanip>


using namespace std;

namespace tsframework
{

CellCommandWidget::CellCommandWidget ( CellAbstractContext* context, log4cplus::Logger& logger )
                                          :
                                          CellPanel ( context,logger ),
                                          command_ ( 0 ),
                                          justExecuted_ ( false ),
                                          autoChkValue_ ( false )
{
	logger_ = log4cplus::Logger::getInstance ( logger.getName() +".CellCommandWidget" );
}


CellCommandWidget::~CellCommandWidget()
{
	if ( command_ )
	{
		getContext()->getCommandFactory()->dispose ( command_ );
	}
}


void CellCommandWidget::fillResult ( cgicc::Cgicc& cgi, ostream& out )
{

	if ( !getSession ( cgi )->getPayload ( getCommandId() ).empty() )
	{
		out << "<pre>" << ajax::toolbox::escapeHTML(getSession ( cgi )->getPayload (getCommandId()) ) << "</pre>" << endl;
		justExecuted_ = false;
	} else {
		out << "<p>No results yet...</p>";
	}

	if ( getSession ( cgi )->hasReply ( getCommandId() ) && !getSession ( cgi )->getWarningMessage (getCommandId()).empty() )
  	{
			out << "<h5>Warning</h5>";
			out << "<pre warning>" << ajax::toolbox::escapeHTML(getSession ( cgi )->getWarningMessage (getCommandId()) ) << "</pre>" << endl;
			justExecuted_ = false;
  	}
}


void CellCommandWidget::refresh ( cgicc::Cgicc& cgi,ostream& out )
{
	fillResult ( cgi, out );
}


void CellCommandWidget::execute ( cgicc::Cgicc& cgi,ostream& out )
{
	ostringstream id;
	id << getCommandName();
	id << fixed << setprecision ( 0 );
	id << rand();
	commandid_ = id.str();

    for ( map<string,xdata::Serializable*>::iterator i = command_->getParamList().begin(); i != command_->getParamList().end(); ++i) {
		string paramName = i->first;
		string value = ajax::toolbox::getSubmittedValue ( cgi, paramName );
		LOG4CPLUS_DEBUG ( getLogger(), "executing command: paramName: '" + paramName + "', value: '" + value + "'" );

		try
		{
			command_->setParameterFromString ( paramName,value );
		}
		catch ( xcept::Exception& e )
		{
			ostringstream msg;
			msg << "Can not convert the input " << paramName << " to type " << command_->getParamList() [paramName]->type();
			LOG4CPLUS_ERROR ( getLogger(),msg.str() );
			showAlert ( out,msg.str() );
			fillResult ( cgi, out );
			return;
		}
	}

	try
	{
		//SOAP message preparation
		justExecuted_ = true;
		string commandName = getCommandName();
		bool async ( true );
		string cb ( "guiResponse" );
		string url ( getContext()->getLocalUrl() );
		string urn ( getContext()->getLocalUrn() );

		map<string, xdata::Serializable*> param = command_->getParamList();
		xoap::MessageReference msg = tstoolbox::doSoapCommand ( TS_NS_URI, getCommandId(), getSessionId ( cgi ), async, getCommandName(), param,cb,url,urn );
		LOG4CPLUS_DEBUG ( getLogger(), "Running command '" + getCommandName() + "' locally" );
		xoap::MessageReference reply = getContext()->getCommandPort()->run ( msg );
		xoap::SOAPBody replyBody = reply->getSOAPPart().getEnvelope().getBody();
		string commandReply;

		if ( replyBody.hasFault() )
		{
			xoap::SOAPFault fault = replyBody.getFault();
		  	ostringstream error;
		  	error << "SOAP Fault received instead of ACKNOWLEDGE. ";
		  	error << fault.getFaultString();
		  	XCEPT_RAISE ( tsexception::SoapFault,error.str() );
		}
	}
	catch ( xcept::Exception& e )
  	{
		ostringstream msg;
		msg << "Can not send SOAP. Exception caught: " << xcept::stdformat_exception_history ( e );
		LOG4CPLUS_ERROR ( getLogger(),msg.str() );
		showAlert ( out,msg.str() );
		fillResult ( cgi,out );
		return;
  	}

  	fillResult ( cgi, out );
}


void CellCommandWidget::handleSubmit ( cgicc::Cgicc& cgi,ostream& out ) {
    std::map<std::string,std::string> values(ajax::toolbox::getSubmittedValues(cgi));
    for(std::map<std::string,std::string>::iterator i(values.begin()); i != values.end(); ++i) {
        out << "<key-value-pair key='" << i->first << "' value='" << i->second << "'></key-value-pair>";
    }
}


void CellCommandWidget::layout ( cgicc::Cgicc& cgi )
{
    remove();
    ajax::PolymerElement* command = new ajax::PolymerElement("ts-command");
    command->set( "name", getCommandName() );

    ajax::JSONTreeNode* parameters = new ajax::JSONTreeNode();
    for ( map<string,xdata::Serializable*>::iterator i = command_->getParamList().begin(); i != command_->getParamList().end(); ++i) {
        ajax::JSONTreeNode* parameter = new ajax::JSONTreeNode();
        parameter->set ( "variable", i->first );
        parameter->set ( "type", i->second->type() );
        parameter->set ( "default", command_->getParamList() [i->first]->toString() );
        parameters->add(parameter);
    }
    std::ostringstream json;
    parameters->html(cgi, json);
    command->set( "parameters", ajax::toolbox::escapeHTML(json.str()));

    setEvent("ts-command-submit", ajax::Eventable::OnSubmit, this, &CellCommandWidget::execute);
		setEvent("command-refresh", ajax::Eventable::OnTime, this, &CellCommandWidget::refresh);
    add( command );
}


void CellCommandWidget::init ( const string& commandName )
{
	commandName_ = commandName;
	command_ = getContext()->getCommandFactory()->create ( commandName );

	ostringstream id;
	id << getCommandName();
	id << fixed << setprecision ( 0 );
	id << rand();

	commandid_ = id.str();
}


string CellCommandWidget::getCommandName()
{
	return commandName_;
}


CellLocalSession* CellCommandWidget::getSession ( cgicc::Cgicc& cgi )
{
	map<string,CellLocalSession*>::iterator i ( getContext()->getSessionMap().find ( getSessionId ( cgi ) ) );

	if ( i == getContext()->getSessionMap().end() )
	{
		XCEPT_RAISE ( tsexception::SessionError,"Can not find sessionId " + getSessionId ( cgi ) + " in the session map" );
	}

	return i->second;
}


string CellCommandWidget::getCommandId()
{
	return commandid_;
}


void CellCommandWidget::showAlert ( ostream& out, const string& msg )
{
    //TODO: this can be done with the paper-dialog element
	out << "<script>";
	out << "alert(\"" << string2alert ( msg ) << "\");";
	out << "</script>" << endl;
}


string CellCommandWidget::string2alert ( const string& in )
{
	string ret = replace ( in,"\n","" );
	ret = replace ( ret,"\"","\'" );
	return ret;
}


string CellCommandWidget::replace ( const string& in,const string& from,const string& to )
{
	string result ( in );

	if ( from.empty() )
	{
		return result;
	}

	size_t pos ( 0 );
	pos = result.find ( from,pos );

	while ( pos != result.npos )
	{
		result.erase ( pos,from.size() );
		result.insert ( pos,to );
		pos = result.find ( from,pos+to.size() );
	}

	return result;
}


string CellCommandWidget::getBaseId()
{
	return "tsgui_cmwdgt_" + getCommandId() + "_";
}


string CellCommandWidget::htmlEncoding (const string& html) {
    std::string buffer;
    buffer.reserve(html.size());
    for(size_t pos = 0; pos != html.size(); ++pos) {
        switch(html[pos]) {
            case '&':  buffer.append("&amp;");       break;
            case '\"': buffer.append("&quot;");      break;
            case '\'': buffer.append("&apos;");      break;
            case '<':  buffer.append("&lt;");        break;
            case '>':  buffer.append("&gt;");        break;
            default:   buffer.append(&html[pos], 1); break;
        }
    }
    return buffer;
}

} // end ns tsframework
