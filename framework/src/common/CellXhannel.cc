/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons Magrans          				         *
 *************************************************************************/
#include "ts/framework/CellXhannel.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannelRequest.h"

#include "xdaq/ApplicationContext.h"

#include "log4cplus/logger.h"

#include "xcept/tools.h"

using namespace std; 

namespace tsframework
{

CellXhannel::CellXhannel ( xdaq::ApplicationStub* s )
: 
xdaq::Application ( s ),
mutex_ ( true )

{
    ;
}


CellXhannel::~CellXhannel()
{
    for ( map<string,CellXhannelRequest*>::iterator i ( reqMap_.begin() ); i!=reqMap_.end(); ++i )
    {
        delete i->second;
    }

    reqMap_.clear();
}


void 
CellXhannel::send ( CellXhannelRequest* req )
{
    ;
}


void
CellXhannel::setRequest ( const string& name, CellXhannelRequest* req )
{
    tstoolbox::MutexHandler h ( mutex_ );
    reqMap_.insert ( make_pair ( name, req ) );
}


CellXhannelRequest*
CellXhannel::getRequest ( const string& name )
{
    tstoolbox::MutexHandler h ( mutex_ );
    map<string, CellXhannelRequest*>::iterator i;
    i = reqMap_.find ( name );

    if ( i == reqMap_.end() )
    {
        XCEPT_RAISE ( tsexception::XhannelDoesNotExist,toolbox::toString ( "The xhannel request: %s does not exist",name.c_str() ) );
    }

    CellXhannelRequest* req = i->second;
    return req;
}


void
CellXhannel::removeRequest ( const string& name )
{
    tstoolbox::MutexHandler h ( mutex_ );
    map<string, CellXhannelRequest*>::iterator i;
    i = reqMap_.find ( name );

    if ( i == reqMap_.end() )
    {
        XCEPT_RAISE ( tsexception::XhannelDoesNotExist,toolbox::toString ( "The xhannel request: %s does not exist",name.c_str() ) );
    }

    CellXhannelRequest* req = i->second;
    delete req;
    reqMap_.erase ( i );
}

void
CellXhannel::removeRequest ( CellXhannelRequest* req )
{
    string id = req->getRequestId();
    removeRequest ( id );
}


CellXhannelRequest*
CellXhannel::createRequest()
{
    CellXhannelRequest* req = this->createRequestVirtual();
    setRequest ( req->getRequestId(), req );
    return req;
}


const xdaq::ApplicationDescriptor*
CellXhannel::getTargetDescriptor()
{
    const xdaq::ApplicationDescriptor* d;

    try
    {
        d = getApplicationContext()->getDefaultZone()->getApplicationDescriptor ( targetApplication_, targetInstance_ );
    }
    catch ( xcept::Exception& e )
    {
        ostringstream msg;
        msg << "Cannot find application " + targetApplication_ + "  with instance " << targetInstance_;
        XCEPT_RETHROW ( tsexception::XhannelCreationError, msg.str(), e );
    }

    return d;
}


void
CellXhannel::setTarget ( const string& group, const string& application, unsigned long instance )
{
    targetGroup_ = group;
    targetInstance_ = instance;
    targetApplication_ = application;
}


void
CellXhannel::setContext ( CellAbstractContext* context )
{
    context_ = context;
}


CellAbstractContext*
CellXhannel::getContext()
{
    return context_;
}

log4cplus::Logger&
CellXhannel::getLogger()
{
    return getApplicationLogger();
}

} // end ns tsframework
