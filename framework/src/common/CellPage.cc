#include "ts/framework/CellPage.h"

#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellFactory.h"
#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellPanel.h"
#include "ts/framework/CellPanelFactory.h"
#include "ts/framework/CellXhannelMonitor.h"
#include "ts/framework/CellXhannelRequestMonitor.h"
#include "ts/framework/CellMonitorWidget.h"
#include "ts/framework/CellCommandWidget.h"
#include "ts/framework/CellOperationWidget.h"
#include "ts/framework/version.h"

#include "xcept/tools.h"


#include "ajax/PlainHtml.h"

#include "ajax/JSONTree.h"
#include "ajax/JSONTreeNode.h"
#include "ajax/toolbox.h"

#include "log4cplus/loggingmacros.h"

#include <iostream>
#include <iomanip>

using namespace std;

namespace tsframework
{

CellPage::CellPage ( CellAbstractContext* context, log4cplus::Logger& logger ) :
    ajax::Page ( "/" + context->getLocalUrn() + "/Default" ),
    context_ ( context ),
    logger_ ( log4cplus::Logger::getInstance ( logger.getName() +".CellPage" ) )
{
    setCaption ( getContext()->getCell()->getName() );

    main_ = new ajax::ResultBox();
    main_->set ("id", "tsgui_content_" );
    add ( main_ );
    getContext()->addImport("/ts/framework/html/elements/framework-elements.html");
}


CellPage::~CellPage()
{
    remove();
}


void
CellPage::refreshTree ( ajax::JSONTree* tree )
{
    tree->set( "name", "main tree" );

    ajax::JSONTreeNode* mainNode = new ajax::JSONTreeNode();
    mainNode->set( "name", "Trigger Supervisor" );
    mainNode->setSubMenuName("menu");
    tree->add ( mainNode );

    ajax::JSONTreeNode* commandsNode = new ajax::JSONTreeNode();
    commandsNode->set( "name", "Commands" );
    commandsNode->setSubMenuName("menu");
    mainNode->add ( commandsNode );

    ajax::JSONTreeNode* operationsNode = new ajax::JSONTreeNode();
    operationsNode->set( "name", "Operations" );
    operationsNode->setSubMenuName("menu");
    mainNode->add ( operationsNode );

    ajax::JSONTreeNode* panelsNode = new ajax::JSONTreeNode();
    panelsNode->set( "name", "Control Panels" );
    panelsNode->setSubMenuName("menu");
    mainNode->add ( panelsNode );

    ajax::JSONTreeNode* monitorNode = new ajax::JSONTreeNode();
    monitorNode->set( "name", "Monitoring" );
    monitorNode->setSubMenuName("menu");
    mainNode->add ( monitorNode );

    ajax::JSONTreeNode* subCellNode = new ajax::JSONTreeNode();
    subCellNode->set( "name", "Peers" );
    subCellNode->setSubMenuName("menu");
    mainNode->add ( subCellNode );

    vector<string> stringGroups = getContext()->getCommandFactory()->getGroups();

    for ( vector<string>::iterator i ( stringGroups.begin() ); i != stringGroups.end(); ++i )
    {
        ajax::JSONTreeNode* cmdGrupNode = new ajax::JSONTreeNode();
        cmdGrupNode->set( "name", *i );
        cmdGrupNode->setSubMenuName("menu");
        commandsNode->add ( cmdGrupNode );
        vector<string> stringCommands = getContext()->getCommandFactory()->getCommands ( *i );

        for ( vector<string>::iterator j ( stringCommands.begin() ); j != stringCommands.end(); ++j )
        {
            ajax::JSONTreeNode* cmdNode = new ajax::JSONTreeNode();
            cmdNode->set( "name", *j );
            cmdNode->set ("id", "tsgui_tree_Commands_" + *j );
            cmdNode->setDefaultValue ( *j );
            cmdGrupNode->add ( cmdNode );
            setEvent("tsgui_tree_Commands_" + *j, ajax::Eventable::OnClick, this, &CellPage::onTreeCreateCommand);
        }
    }


    vector<string> opInstances = getContext()->getOperationFactory()->getOperations();

    for ( vector<string>::iterator i ( opInstances.begin() ); i != opInstances.end(); ++i )
    {
        ajax::JSONTreeNode* operationNode = new ajax::JSONTreeNode();
        operationNode->set( "name", *i );
        operationNode->setDefaultValue ( *i );
        operationsNode->add ( operationNode );
        operationNode->set ("id", "tsgui_tree_Operations_" + *i );
        setEvent("tsgui_tree_Operations_" + *i, ajax::Eventable::OnClick, this, &CellPage::onTreeControlOperation);
    }

    vector<string> panelClasses = getContext()->getPanelFactory()->getClassList();

    for ( vector<string>::iterator i ( panelClasses.begin() ); i != panelClasses.end(); ++i )
    {
        ajax::JSONTreeNode* panelNode = new ajax::JSONTreeNode();
        panelNode->set( "name", *i );
        panelNode->setDefaultValue ( *i );
        panelsNode->add ( panelNode );
        panelNode->set ("id", "tsgui_tree_Control Panels_" + *i );
        setEvent("tsgui_tree_Control Panels_" + *i, ajax::Eventable::OnClick, this, &CellPage::onTreeCreatePanel);
    }

    //Monitoring flashlists
    if ( hasMonitor() )
    {
        try
        {
            vector<string> flashlists = getFlashlists();

            for ( vector<string>::iterator i ( flashlists.begin() ); i!=flashlists.end(); ++i )
            {
                ajax::JSONTreeNode* flashlistNode = new ajax::JSONTreeNode();
                flashlistNode->setId ( "tsgui_tree_Monitoring_" + *i );
                flashlistNode->set( "name", *i );
                flashlistNode->setDefaultValue ( *i );
                setEvent("tsgui_tree_Monitoring_" + *i, ajax::Eventable::OnClick, this, &CellPage::onTreeMonitor);
                monitorNode->add ( flashlistNode );
            }
        }
        catch ( xcept::Exception& e )
        {
            ostringstream msg;
            msg << "Can not create Monitor Tree. The 'MON' xhannel is not available";
            msg << xcept::stdformat_exception_history ( e );
            LOG4CPLUS_WARN ( getLogger(),msg.str() );
        }
    }

    try
    {
        map<string, CellXhannel*> xList = getContext()->getXhannelList();

        for ( map<string, CellXhannel*>::iterator i ( xList.begin() ); i != xList.end(); ++i )
        {
            ajax::JSONTreeNode* peer = new ajax::JSONTreeNode();
            CellXhannel* x =i->second;
            string url = x->getTargetDescriptor()->getContextDescriptor()->getURL() + "/" + x->getTargetDescriptor()->getURN();
            peer->set( "name", i->first );
            peer->set("url", url);
            subCellNode->add(peer);
        }
    }
    catch ( tsexception::CellException& e )
    {
        ostringstream msg;
        msg << "Any URL can not be retrieved from the Xhannels. Exception found: " << endl;
        msg <<  xcept::stdformat_exception_history ( e ) << endl;
        LOG4CPLUS_WARN ( getLogger(),msg.str() );
    }
}


void
CellPage::onTreeCreateCommand ( cgicc::Cgicc& cgi,ostream& out )
{
    string id ( ajax::toolbox::getSubmittedValue ( cgi, "_id_" ) );
    string commandName ( ajax::toolbox::getSubmittedValue ( cgi,id ) );
    CellCommandWidget* cmdWidget ( 0 );

    try
    {
        main_->remove();
        cmdWidget = new CellCommandWidget ( getContext(), getLogger() );
        cmdWidget->init ( commandName );
        main_->add ( cmdWidget );
        main_->innerHtml ( cgi,out );
    }
    catch ( xcept::Exception& e )
    {
        delete cmdWidget;
        main_->remove();
        ostringstream msg;
        msg << "Can not create CellCommand '" + commandName + "'. Exception caught: ";
        msg << xcept::stdformat_exception_history ( e );

        LOG4CPLUS_ERROR ( getLogger(), msg.str() );
        out << "<p>Can not create CellCommand '" + commandName + "'</p>";
        out << "<p/>" << xcept::htmlformat_exception_history ( e );
    }
}

void
CellPage::onTreeControlOperation ( cgicc::Cgicc& cgi, ostream& out )
{
    string id ( ajax::toolbox::getSubmittedValue ( cgi, "_id_" ) );
    string operationId ( ajax::toolbox::getSubmittedValue ( cgi, id ) );

    try
    {
        main_->remove();
        CellOperationWidget* coWidget = new CellOperationWidget ( getContext(), getLogger() );
        coWidget->init ( operationId );
        main_->add ( coWidget );
        main_->innerHtml ( cgi,out );
    }
    catch ( xcept::Exception& e )
    {
        ostringstream err;
        err << "Can not open operation '" + operationId + "'.";
        err << xcept::stdformat_exception_history ( e );
        LOG4CPLUS_WARN ( getLogger(), err.str() );
        main_->remove();
        string url = "/" + getContext()->getLocalUrn() + "/Default";
        out << "<script type=\"text/javascript\">" << endl;
        out << "<!--" << endl;
        out << "window.location = \"" << url << "\" " << endl;
        out << "//-->" << endl;
        out << "</script>" << endl;
    }
}

void
CellPage::onTreeCreatePanel ( cgicc::Cgicc& cgi,ostream& out )
{
    //tstoolbox::MutexHandler handler(contentMtx_);
    string id ( ajax::toolbox::getSubmittedValue ( cgi, "_id_" ) );
    string className ( ajax::toolbox::getSubmittedValue ( cgi,id ) );

    try
    {
        main_->remove();
        CellPanel* panel = getContext()->getPanelFactory()->create ( className,getContext(),getLogger() );
        main_->add ( panel );
        main_->innerHtml ( cgi,out );
    }
    catch ( xcept::Exception& e )
    {
        main_->remove();
        out << "<p>Can not create CellPanel widget of class '" << className << "'<p>";
        out << "<p/>" << xcept::htmlformat_exception_history ( e );
        ostringstream msg;
        msg << "Can not create CellPanel widget of class '" << className << "'.";
        msg << xcept::stdformat_exception_history ( e );
        LOG4CPLUS_ERROR ( getLogger(),msg.str() );
    }
}


void
CellPage::onTreeMonitor ( cgicc::Cgicc& cgi,ostream& out )
{
    string id ( ajax::toolbox::getSubmittedValue ( cgi, "_id_" ) );
    string flash ( ajax::toolbox::getSubmittedValue ( cgi, id ) );

    try
    {
        main_->remove();
        CellMonitorWidget* monWidget = new CellMonitorWidget ( getContext(), getLogger() );
        monWidget->init ( flash );
        main_->add ( monWidget );
        main_->innerHtml ( cgi,out );
    }
    catch ( xcept::Exception& e )
    {
        main_->remove();
        out << "<p>Can not create CellMonitorWidget for flashlist '" << flash << "'</p>";
        out << "<p/>" << xcept::htmlformat_exception_history ( e );
        ostringstream msg;
        msg << "Can not create CellMonitorWidget for flashlist '" << flash << "'.";
        msg << xcept::stdformat_exception_history ( e );
        LOG4CPLUS_ERROR ( getLogger(), msg.str() );
    }
}


CellAbstractContext*
CellPage::getContext()
{
    return context_;
}


log4cplus::Logger&
CellPage::getLogger()
{
    return logger_;
}


bool
CellPage::hasMonitor()
{
    try
    {
        CellXhannelMonitor* p = dynamic_cast<CellXhannelMonitor*> ( getContext()->getXhannel ( "MON" ) );

        if ( !p )
        {
            LOG4CPLUS_WARN ( getLogger(), "Monitor Xhannel not available" );
            return false;
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_WARN ( getLogger(), "Monitor Xhannel not available" );
        return false;
    }

    return true;
}

vector<string>
CellPage::getFlashlists()
{
    vector<string> result;
    CellXhannelMonitor* mXhannel;

    try
    {
        mXhannel = dynamic_cast<CellXhannelMonitor*> ( getContext()->getXhannel ( "MON" ) );

        if ( !mXhannel )
        {
            XCEPT_RAISE ( tsexception::XhannelDoesNotExist,"Xhannel MON is not of type CellXhannelMonitor" );
        }
    }
    catch ( xcept::Exception& e )
    {
        ostringstream msg;
        msg << "Monitor Xhannel 'MON' not found. Is it defined in the Xhannel List?";
        XCEPT_RETHROW ( tsexception::XhannelDoesNotExist,msg.str(),e );
    }

    string type;
    CellXhannelRequestMonitor* typeReq = dynamic_cast<CellXhannelRequestMonitor*> ( mXhannel->createRequest() );
    typeReq->doRetrieveCatalog();

    try
    {
        mXhannel->send ( typeReq );
        result = typeReq->retrieveCatalogReply();
        mXhannel->removeRequest ( typeReq );
    }
    catch ( xcept::Exception& e )
    {
        mXhannel->removeRequest ( typeReq );
        ostringstream msg;
        msg << "Exception caught while requesting Catalog of flashlists. ";
        XCEPT_RETHROW ( tsexception::CommunicationError,msg.str(),e );
    }

    return result;
}


vector<string>
CellPage::getMonitorableItems ( const string& flashlist )
{
    vector<string> result;
    CellXhannelMonitor* mXhannel;

    try
    {
        mXhannel = dynamic_cast<CellXhannelMonitor*> ( getContext()->getXhannel ( "MON" ) );

        if ( !mXhannel )
        {
            XCEPT_RAISE ( tsexception::XhannelDoesNotExist,"Xhannel MON is not of type CellXhannelMonitor" );
        }
    }
    catch ( xcept::Exception& e )
    {
        ostringstream msg;
        msg << "Monitor Xhannel 'MON' not found. Is it defined in the Xhannel List?";
        XCEPT_RETHROW ( tsexception::XhannelDoesNotExist,msg.str(),e );
    }

    //Looking for type
    string type;
    CellXhannelRequestMonitor* typeReq = dynamic_cast<CellXhannelRequestMonitor*> ( mXhannel->createRequest() );
    typeReq->doRetrieveDefinition ( flashlist );

    try
    {
        mXhannel->send ( typeReq );
        map<string,string> reply = typeReq->retrieveDefinitionReply();
        mXhannel->removeRequest ( typeReq );

        for ( map<string,string>::iterator i ( reply.begin() ); i!=reply.end(); ++i )
        {
            result.push_back ( i->first );
        }
    }
    catch ( xcept::Exception& e )
    {
        mXhannel->removeRequest ( typeReq );
        ostringstream msg;
        msg << "Exception caught while requesting flashlist '" << flashlist << "' definition to the Monitor collector.";
        XCEPT_RETHROW ( tsexception::MonitoringError,msg.str(),e );
    }

    return result;
}


void
CellPage::layout ( cgicc::Cgicc& cgi )
{

    main_->remove();
    main_->add(new ajax::PlainHtml("<script>var tree = "));
    ajax::JSONTree* tree = new ajax::JSONTree();
    refreshTree ( tree );
    main_->add ( tree );
    main_->add(new ajax::PlainHtml("</script>"));
}


void
CellPage::html ( cgicc::Cgicc& cgi,ostream& out )
{
    tstoolbox::MutexHandler handler ( mutex_ );
    layout ( cgi );
    Page::html ( cgi,out );
}


bool
CellPage::isOperationInUse ( const string& id )
{
    if ( main_->getWidgets().empty() )
    {
        return false;
    }

    CellOperationWidget* w = dynamic_cast<CellOperationWidget*> ( * ( main_->getWidgets().begin() ) );

    if ( !w )
    {
        return false;
    }

    if ( w->getOperationId() == id )
    {
        return true;
    }

    return false;
}


string
CellPage::string2alert ( const string& in )
{
    string ret = replace ( in,"\n","" );
    ret = replace ( ret,"\"","\'" );
    return ret;
}


string CellPage::replace ( const string& in,const string& from,const string& to )
{
    string result ( in );
    size_t pos ( 0 );

    if ( from.empty() )
    {
        return result;
    }

    pos = result.find ( from,pos );

    while ( pos != result.npos )
    {
        result.erase ( pos,from.size() );
        result.insert ( pos,to );
        pos = result.find ( from,pos+to.size() );
    }

    return result;
}


CellLocalSession*
CellPage::getSession ( cgicc::Cgicc& cgi )
{
    map<string,CellLocalSession*>::iterator i ( getContext()->getSessionMap().find ( getSessionId() ) );

    if ( i == getContext()->getSessionMap().end() )
    {
        XCEPT_RAISE ( tsexception::SessionError,"Can not find sessionId '" + getSessionId() + "' in the session map" );
    }

    return i->second;
}


ajax::Widget*
CellPage::showAlert ( const string& msg )
{
    ajax::PlainHtml* alert = new ajax::PlainHtml();
    alert->getStream() << "<script type=\"text/javascript\">";
    alert->getStream() << "<!--" << endl;
    alert->getStream() << "alert(\"" << string2alert ( msg ) << "\");";
    alert->getStream() << "// -->" << endl;
    alert->getStream() << "</script>" << endl;
    return alert;
}


string
CellPage::removeNamespace ( const string& flashlist )
{
    size_t pos ( 0 );
    string name = flashlist;
    pos = name.find ( "urn:xdaq-flashlist:", pos );

    if ( pos != string::npos )
    {
        name.erase ( pos,pos+19 );
    }

    return name;
}


string
CellPage::createOperationId ( const string& operationName )
{
    static map<string,unsigned long> names;
    map<string,unsigned long>::iterator i ( names.find ( operationName ) );
    unsigned long count;

    if ( names.empty() || ( i == names.end() ) )
    {
        names.insert ( make_pair ( operationName,1 ) );
        count =1;
    }
    else
    {
        i->second = i->second + 1;
        count = i->second;
    }

    ostringstream cid;
    cid << operationName;
    cid << "_" <<count;
    cid << "_from_gui";
    return cid.str();

}

} // end ns tsframework
