/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril                                *
 *************************************************************************/

#include "ts/framework/CellXhannelXdaqSimple.h"

#include "ts/framework/CellXhannelRequestXdaqSimple.h"

#include "ts/toolbox/CellNamespaceURI.h"
#include "ts/toolbox/CellToolbox.h"

#include "ts/exception/CellException.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"

#include "xdaq/XceptSerializer.h"

#include "xcept/tools.h"
#include <thread>
#include <chrono>



using namespace std;

namespace tsframework
{

XDAQ_INSTANTIATOR_IMPL ( CellXhannelXdaqSimple )


CellXhannelXdaqSimple::CellXhannelXdaqSimple ( xdaq::ApplicationStub* s ) 
: 
CellXhannel ( s )
{
    string myNs = TS_NS_URI;
}


CellXhannelXdaqSimple::~CellXhannelXdaqSimple()
{
    ;
}

void
CellXhannelXdaqSimple::send ( CellXhannelRequest* req )
{

    tstoolbox::MutexHandler h ( mutex_ );
    xoap::MessageReference* msg = req->getRequest();
    xoap::MessageReference reply;

    int count ( 0 );
    timeval start;
    gettimeofday ( &start,0 );

    while ( true )
    {
        try
        {
            ++count;
            xoap::MessageReference request = xoap::createMessage ( *msg );
            reply = getContext()->getApContext()->postSOAP ( request, *getApplicationDescriptor(),*getTargetDescriptor() );
            break;
        }
        catch ( xdaq::exception::Exception& e )
        {
            timeval end;
            gettimeofday ( &end,0 );

            if ( count >= MAX_ATTEMPTS || tstoolbox::timeval_diff ( end,start ) >MAX_TIMEOUT_MS )
            {
                XCEPT_RETHROW ( tsexception::CommunicationError,"Several communication attempts to XDAQ application have failed. Before retrying please check if the cell in the SOAP request is still operational.",e );
            }
            else
            {
                ostringstream msg;
                msg << "Communication attempt to XDAQ application failed (trying again)... count : " << count;
                msg << xcept::stdformat_exception_history ( e );
                LOG4CPLUS_WARN ( getLogger(),msg.str() );
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        }
        catch ( xcept::Exception& e ) 
        { 
          timeval end; 
          gettimeofday ( &end,0 ); 

          XCEPT_RETHROW ( tsexception::CommunicationError,"Unexpected exception while trying to communicate with XDAQ application. Before retrying please check the operational status of trigger cells and XDAQ applications.",e ); 
        } 
    }

    xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();

    if ( body.hasFault() )
    {
        ostringstream err;
        err << "SOAP reply contains SOAPFault. ";

        if ( body.getFault().hasDetail() )
        {
            xoap::SOAPElement detail = body.getFault().getDetail();
            xcept::Exception rae;
            xdaq::XceptSerializer::importFrom ( detail.getDOM(), rae );
            XCEPT_RETHROW ( tsexception::SoapFault, err.str(), rae );
        }
        else
        {
            err << body.getFault().getFaultString();
            XCEPT_RAISE ( tsexception::SoapFault,err.str() );
        }
    }

    req->setReply ( reply );
}


CellXhannelRequest* CellXhannelXdaqSimple::createRequestVirtual()
{
    CellXhannelRequestXdaqSimple* req = new CellXhannelRequestXdaqSimple ( getLogger(),getContext() );
    return req;
}

} // end ns tsframework
