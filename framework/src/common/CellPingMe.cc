/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril                                *
 *************************************************************************/
#include "ts/framework/CellPingMe.h"

#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellAbstractContext.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "xdata/Boolean.h"

using namespace std; 

namespace tsframework
{

CellPingMe::CellPingMe ( log4cplus::Logger& log, CellAbstractContext* context )
:
CellCommand ( log,context )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".pingCell" );
}


void 
CellPingMe::code()
{
	LOG4CPLUS_INFO ( getLogger(),"Cell '" + getContext()->getCell()->getName() + "' responding to ping command" );
  	
  	delete payload_;
  	payload_ = new xdata::Boolean ( true );
}

} // end ns tsframework






