/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Marc Magrans de Abril      				         *
 *************************************************************************/
#include "ts/framework/CellLocalSession.h"

#include "ts/exception/CellException.h"
#include "ts/framework/CellPage.h"

#include "xdaq/exception/Exception.h"

#include "toolbox/BSem.h"

#include "log4cplus/logger.h"

#include <cassert>
#include <utility>
#include <vector>
#include <iostream>
#include <map>
#include <sstream>
#include <stdlib.h>

// Static API
const double tsframework::CellLocalSession::CELL_TIMEOUT_SEC = 3600;

const int 	 tsframework::CellLocalSession::HISTORY_SIZE = 50;


using namespace std;

namespace tsframework
{


CellLocalSession::CellLocalSession ( CellAbstractContext* context,log4cplus::Logger& log )
:
replies_ ( HISTORY_SIZE ),
page_ ( 0 ),
lazyLoad_ ( true )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellSessionInfo" );
	setContext ( context );
	setLastCalled();
	page_ = new CellPage ( getContext(), getLogger() );
	page_->setSessionId ( getSessionId() );
}


CellLocalSession::~CellLocalSession()
{
	delete page_;
}

string
CellLocalSession::getSessionId()
{
	return sessionId_;
}

void
CellLocalSession::setSessionId ( const string& sessionId )
{
	sessionId_ = sessionId;
	page_->setSessionId ( sessionId );
}


CellPage&
CellLocalSession::getPage()
{
	setLastCalled();
	return *page_;
}


void
CellLocalSession::addReply ( const string& id, const string& payload, const string& warningMessage, int warningLevel )
{
	setLastCalled();

	Reply r;
	r.payload = payload;
	r.warningMessage = warningMessage;
	r.warningLevel = warningLevel;
	replies_.add ( make_pair ( id,r ) );
}


bool
CellLocalSession::hasReply ( const string& id )
{
	setLastCalled();

	for ( unsigned int i=0; i < replies_.size(); ++i )
	{
		if ( replies_[i].first == id )
		{
			return true;
		}
	}

	return false;
}


string
CellLocalSession::getPayload ( const string& id )
{
	setLastCalled();

	for ( unsigned int i=0; i < replies_.size(); ++i )
	{
		if ( replies_[i].first == id )
		{
			return replies_[i].second.payload;
		}
	}

	return "";
}


string
CellLocalSession::getWarningMessage ( const string& id )
{
	setLastCalled();

	for ( unsigned int i=0; i < replies_.size(); ++i )
	{
		if ( replies_[i].first == id )
		{
			return replies_[i].second.warningMessage;
		}
	}

	return "";
}


int
CellLocalSession::getWarningLevel ( const string& id )
{
	setLastCalled();

	for ( unsigned int i=0; i < replies_.size(); ++i )
	{
		if ( replies_[i].first == id )
		{
			return replies_[i].second.warningLevel;
		}
	}

	return 0;
}


void
CellLocalSession::setUsername ( const string& username )
{
	username_ = username;
}

string
CellLocalSession::getUsername()
{
	return username_;
}

time_t
CellLocalSession::getLastCalled()
{
	return lastCalled_;
}

void
CellLocalSession::setLastCalled()
{
	lastCalled_ = time ( NULL );
}

} // end ns tsframework
