#include "ts/framework/CellCommand.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellWarning.h"
#include "ts/framework/CellOpSendCommand.h"
#include "ts/framework/CellOpReset.h"

#include "ts/toolbox/CellNamespaceURI.h"
#include "ts/toolbox/CellToolbox.h"
#include "ts/toolbox/SoapMessenger.h"

#include "log4cplus/loggingmacros.h"

#include "toolbox/task/WorkLoopFactory.h"

#include "xcept/tools.h"

#include "xoap/SOAPElement.h"
#include "xoap/SOAPBody.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"

#include "xdata/Boolean.h"
#include "xdata/String.h"


using namespace std;

namespace tsframework
{

CellCommand::CellCommand ( log4cplus::Logger& log, CellAbstractContext* context )
{
	logger_		= log4cplus::Logger::getInstance ( log.getName() +".CellCommand" );
	context_ 	= context;
	isover_ 	= true;
	async_ 		= false;

	double timeus = tstoolbox::getTimeus();
	int r		= rand();
	pipeName_ 	= toolbox::toString ( "commandWorkloop.%d%d",int ( timeus ),r );
	pipe_ 		= toolbox::task::getWorkLoopFactory()->getWorkLoop ( pipeName_, "waiting" );

	job_ 		= toolbox::task::bind ( ( ( CellCommand* ) this ), &CellCommand::job, "job" );

	payload_	= new xdata::String ( "" );
	warning_ 	= new CellWarning();
}


CellCommand::~CellCommand()
{

	if ( pipe_->isActive() )
	{
		pipe_->cancel();
	}

	toolbox::task::getWorkLoopFactory()->removeWorkLoop ( pipeName_, "waiting" );
	delete pipe_;
	delete job_;
	delete payload_;
	delete warning_;

	if ( !paramList_.empty() )
		for ( map<string,xdata::Serializable*>::iterator i ( paramList_.begin() ); i != paramList_.end(); ++i )
		{
			delete i->second;
		}
}


xoap::MessageReference
CellCommand::run()
{
	isover_ = false;

	xoap::MessageReference reply;
	string com = tstoolbox::getCommand ( msg_ );
	async_ = tstoolbox::isAsync ( msg_ );

	if ( dynamic_cast<CellOpSendCommand*> ( this ) || dynamic_cast<CellOpReset*> ( this ) )
	{
		paramList_ = tstoolbox::getOpComParamList ( msg_ );
	}
	else
	{
		map<string,xdata::Serializable*> params = tstoolbox::getOpComParamList ( msg_ );
		setParameterMap ( params );

		if ( !params.empty() )
		{
			for ( map<string,xdata::Serializable*>::iterator i ( params.begin() ); i != params.end(); ++i )
			{
				delete i->second;
			}
		}
	}

	cid_ = tstoolbox::getCid ( msg_ );
	sid_ = tstoolbox::getSid ( msg_ );
	fun_ = tstoolbox::getCallbackFun ( msg_ );

	if ( fun_.empty() )
	{
		fun_ = "NULL";
	}

	if ( async_ )
	{
		localUrl_ = context_->getLocalUrl();
		url_ = tstoolbox::getCallbackUrl ( msg_ );
		urn_ = tstoolbox::getCallbackUrn ( msg_ );

	if ( url_.empty() || url_ == "NULL" )
	{
		XCEPT_RAISE ( tsexception::SoapParsingError,"Trying to execute an asynchronous CellCommand without having callback URL" );
	}

	if ( urn_.empty() || urn_ == "NULL" )
	{
		XCEPT_RAISE ( tsexception::SoapParsingError,"Trying to execute an asynchronous CellCommand without having callback URN" );
	}

	if ( fun_.empty() || fun_ == "NULL" )
	{
	  	XCEPT_RAISE ( tsexception::SoapParsingError,"Trying to execute an asynchronous CellCommand without having callback name" );
	}

	reply = runAsync();

	}
	else
	{
		reply = runSync();
		isover_ = true;
	}

	return reply;
}


xoap::MessageReference
CellCommand::runAsync()
{
	xoap::MessageReference reply;
	reply =  xoap::createMessage();
	pipe_->submit ( job_ );
	pipe_->activate();
	return reply;
}


xoap::MessageReference
CellCommand::runSync()
{
	pipe_->submit ( job_ );
	pipe_->activate();
	xoap::MessageReference* refp = reply_.pop();
	xoap::MessageReference reply = *refp;
	delete refp;
	return reply;
}


string
CellCommand::getNS()
{
	return TS_NS_URI;
}


bool
CellCommand::job ( toolbox::task::WorkLoop* wl )
{
	try
	{
		UnexpectedHandler uhandler();
		this->commandCode();

		if ( getWarning().getLevel() >= CellWarning::ERROR )
		{
			 LOG4CPLUS_ERROR ( getLogger(),getWarning().getMessage() );
		}
		else if ( getWarning().getLevel() >= CellWarning::WARNING )
		{
			LOG4CPLUS_WARN ( getLogger(),getWarning().getMessage() );
		}
	}
	catch ( xcept::Exception& e )
	{
		LOG4CPLUS_ERROR ( getLogger(),xcept::stdformat_exception_history ( e ) );
		getWarning().setLevel ( CellWarning::ERROR );
		getWarning().setMessage ( xcept::stdformat_exception_history ( e ) );
	}
	catch ( bad_alloc& e )
  	{
		ostringstream msg;
		msg << "bad_alloc while executing a CellCommand. ";
		msg << "Can not allocate more memory for that process (bad_alloc caught). This is symptom of a possible memory leak somewhere. ";
		msg << "RESTART THE PROCESS AS SOON AS POSSIBLE!";
		LOG4CPLUS_FATAL ( getLogger(), msg.str() );
		//Can not allocate memory reply can not be generated
		return false;
  	}
  	catch ( exception& e )
  	{
		ostringstream msg;
		msg << "exception caught while executing a CellCommand '" << typeid ( this ).name() <<"': ";
		msg << e.what();
		LOG4CPLUS_ERROR ( getLogger(), msg.str() );
		getWarning().setLevel ( CellWarning::ERROR );
		getWarning().setMessage ( msg.str() );
  	}
  	catch ( ... )
  	{
		ostringstream msg;
		msg << "Unknown exception caught while executing a CellCommand '" << typeid ( this ).name() <<"'. ";
		LOG4CPLUS_ERROR ( getLogger(), msg.str() );
		getWarning().setLevel ( CellWarning::ERROR );
		getWarning().setMessage ( msg.str() );
  	}

  	xoap::MessageReference reply;

	try
	{
		reply = insertPayload();
	}
	catch ( xcept::Exception& e )
	{
		ostringstream msg;
		msg << "Can not create SOAP reply. ";
		msg << xcept::stdformat_exception_history ( e );
		LOG4CPLUS_ERROR ( getLogger(),msg.str() );
		return false;
	}
	catch ( exception& e )
	{
		ostringstream msg;
		msg << "Can not create SOAP reply: " << e.what();
		LOG4CPLUS_ERROR ( getLogger(),msg.str() );
		return false;
	}
	catch ( ... )
	{
		ostringstream msg;
		msg << "Can not create SOAP reply. Unknown exception caught.";
		LOG4CPLUS_ERROR ( getLogger(),msg.str() );
		return false;
	}

	if ( async_ == true )
	{
		tstoolbox::SoapMessenger sm ( getLogger() );

		try
		{
			xoap::MessageReference reply_ack = sm.send ( reply, localUrl_, ( string ) url_, ( string ) urn_ );
		}
		catch ( xcept::Exception& e )
		{
			ostringstream msg;
			msg << "Can not sent acknowledge to asynchronous CellCommand. ";
			msg << xcept::stdformat_exception_history ( e );
			LOG4CPLUS_ERROR ( getLogger(), msg.str() );
		}

		isover_ = true;
	}
	else
	{
		xoap::MessageReference* refp = new xoap::MessageReference ( reply );
		reply_.push ( refp );
	}

	return false; // end
}


void
CellCommand::commandCode()
{
	if ( this->preCondition() )
	{
		this->code();
	}
	else
	{
		getWarning().append ( "Invalid precondition, the command code has not been executed", CellWarning::ERROR );
	}
}


bool
CellCommand::preCondition()
{
	return true;
}


xoap::MessageReference
CellCommand::insertPayload()
{

	xoap::MessageReference msg = xoap::createMessage();

	try
	{
		xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
		envelope.addNamespaceDeclaration ( "soap-enc","http://schemas.xmlsoap.org/soap/encoding/" );
		envelope.addNamespaceDeclaration ( "xsi","http://www.w3.org/2001/XMLSchema-instance" );
		envelope.addNamespaceDeclaration ( "xsd","http://www.w3.org/2001/XMLSchema" );
		xoap::SOAPBody body = envelope.getBody();
		xoap::SOAPName commandName = envelope.createName ( fun_, "cell", getNS() );
		xoap::SOAPElement element = body.addBodyElement ( commandName );
		element.addNamespaceDeclaration ( "cell",getNS() );
		xoap::SOAPName commandIdName = envelope.createName ( "cid" );
		element.addAttribute ( commandIdName, cid_ );
		xoap::SOAPName sesionIdName = envelope.createName ( "sid" );
		element.addAttribute ( sesionIdName, sid_ );

		if ( getWarning().getLevel() != CellWarning::NO_WARNING )
		{
			xoap::SOAPName levelName = envelope.createName ( "warningLevel","cell",getNS() );
			xoap::SOAPElement levelElement = element.addChildElement ( levelName );
			xoap::SOAPName typeName = envelope.createName ( "type","xsi","http://www.w3.org/2001/XMLSchema-instance" );
			levelElement.addAttribute ( typeName, "xsd:integer" );
			ostringstream level;
			level << getWarning().getLevel();
			levelElement.addTextNode ( level.str() );
		}

		if ( !getWarning().getMessage().empty() )
		{
			xoap::SOAPName msgName = envelope.createName ( "warningMessage","cell",getNS() );
			xoap::SOAPElement msgElement = element.addChildElement ( msgName );
			xoap::SOAPName typeName = envelope.createName ( "type","xsi","http://www.w3.org/2001/XMLSchema-instance" );
			msgElement.addAttribute ( typeName, "xsd:string" );
			msgElement.addTextNode ( tstoolbox::escape ( getWarning().getMessage() ) );
		}

		//payload
		string mytype="";

		if ( payload_->type() =="int" )
		{
			mytype="integer";
		}
		else if ( payload_->type() =="string" )
		{
			mytype="string";
		}
		else if ( payload_->type() =="bool" )
		{
			mytype="boolean";
		}
		else if ( payload_->type() =="unsigned long" )
		{
			mytype="unsignedLong";
		}
		else if ( payload_->type() =="unsigned short" )
		{
			mytype="unsignedShort";
		}
		else
		{
			mytype=payload_->type();
		}

		xoap::SOAPName payloadName = envelope.createName ( "payload", "cell", getNS() );
		xoap::SOAPElement payloadElement = element.addChildElement ( payloadName );
		xoap::SOAPName typeName = envelope.createName ( "type","xsi","http://www.w3.org/2001/XMLSchema-instance" );
		payloadElement.addAttribute ( typeName, "xsd:" + mytype );
		payloadElement.addTextNode ( tstoolbox::escape ( payload_->toString() ) );

		//newopId
		if ( !newopid_.empty() )
		{
			xoap::SOAPName opIdName = envelope.createName ( "operation", "cell", getNS() );
			xoap::SOAPElement opIdElement = element.addChildElement ( opIdName );
			xoap::SOAPName typeName = envelope.createName ( "type","xsi","http://www.w3.org/2001/XMLSchema-instance" );
			opIdElement.addAttribute ( typeName, "xsd:string" );
			opIdElement.addTextNode ( newopid_ );
		}
	}
	catch ( xcept::Exception& e )
	{
		ostringstream str;
		str << "Can not create the SOAP message in doOpSendCommand";
		XCEPT_RETHROW ( tsexception::SoapEncodingError,str.str(),e );
	}

	return msg;
}


bool
CellCommand::isOver()
{
	return isover_;
}


xdata::Serializable&
CellCommand::getResult()
{
	return *payload_;
}


void
CellCommand::setMsg ( xoap::MessageReference msg )
{
	msg_ = msg;
}


xoap::MessageReference
CellCommand::getMsg()
{
	return msg_;
}


map<string,xdata::Serializable*>&
CellCommand::getParamList()
{
	return paramList_;
}


void
CellCommand::setParameterMap ( map<string, xdata::Serializable*>& param )
{
	map<string, xdata::Serializable*>::iterator i;

	for ( i = param.begin(); i != param.end(); i++ )
	{
		if ( paramList_.find ( i->first ) == paramList_.end() )
		{
			string msg = "The parameter " + i->first + " is not a parameter of " + typeid ( *this ).name() + ". The existing parameters are: ";

			for ( map<string, xdata::Serializable*>::iterator j = paramList_.begin(); j != paramList_.end(); j++ )
			{
				msg += " " + j->first + " ";
			}

			msg += ".";
	 		LOG4CPLUS_ERROR ( getLogger(),msg );
		}
		else
		{
			setParameterFromString ( i->first, ( i->second )->toString() );
		}
	}
}


void
CellCommand::setParameterFromString ( const string& name, const string& value )
{
	map<string, xdata::Serializable*>::iterator i = paramList_.find ( name );

	if ( i != paramList_.end() )
	{
		xdata::Serializable* x = i->second;

		if ( dynamic_cast<xdata::Boolean*> ( x ) )
		{
			x->fromString ( value=="true"?"true":"false" );
		}
		else
		{
			x->fromString ( value );
		}
	}
	else
	{
		XCEPT_RAISE ( tsexception::ParameterError,"Parameter '" + name + "' not found in Cellcommand parameter list" );
	}
}

string
CellCommand::getSessionId()
{
	return sid_;
}

void
CellCommand::setSessionId ( const string& sid )
{
	sid_ = sid;
}

CellWarning&
CellCommand::getWarning()
{
  	return *warning_;
}

} // end ns tsframework
