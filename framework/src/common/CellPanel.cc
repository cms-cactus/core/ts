/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/
#include "ts/framework/CellPanel.h"

#include "ts/framework/CellAbstractContext.h"


using namespace std;

namespace tsframework
{


CellPanel::CellPanel ( CellAbstractContext* context,log4cplus::Logger& logger )
:
ajax::AutoHandledWidget ( "/" + context->getLocalUrn() + "/Default" ),
logger_ ( log4cplus::Logger::getInstance ( logger.getName() ) ),
context_ ( context )
{
  	;
}


CellPanel::~CellPanel()
{
  	;
}


CellAbstractContext*
CellPanel::getContext()
{
	return context_;
}


log4cplus::Logger&
CellPanel::getLogger()
{
	return logger_;
}


void
CellPanel::html ( cgicc::Cgicc& cgi, ostream& out )
{
	tstoolbox::MutexHandler handler ( mutex_ );
	layout ( cgi );
	AutoHandledWidget::html ( cgi, out );
}


} // end ns tsframework
