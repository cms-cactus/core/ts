#include "ts/framework/CellOperationTypes.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellOperationFactory.h"

#include "xdata/Serializable.h"
#include "xdata/String.h"

using namespace std;

namespace tsframework
{


CellOperationTypes::CellOperationTypes ( log4cplus::Logger& log, CellAbstractContext* context )
:
CellCommand ( log,context )
{
	logger_ = log4cplus::Logger::getInstance ( log.getName() +".CellOperationTypes" );
}


void
CellOperationTypes::code()
{
	vector<string> v = getContext()->getOperationFactory()->getOperationTypes();
	delete payload_;
	this->payload_ = new xdata::String ( vector2json ( v ) );
}


string
CellOperationTypes::vector2json ( const vector<string>& v )
{
	string result ( "[" );

	for ( vector<string>::const_iterator i ( v.begin() ); i != v.end(); ++i )
	{
		if ( result != "[" )
		{
			result += ",";
		}

		result += "\"" + *i + "\"";
	}

	result += "]";
	return result;
}

} // end ns tsframework
