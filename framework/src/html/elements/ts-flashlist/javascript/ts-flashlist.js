Polymer({
    is: 'ts-flashlist',
    properties: {
      /**
       * The main table
       */
      table: {
        type: Object,
        observer: "makeRealTable"
      },
      /**
       * A copy, to prevent update flashes
       */
       realtable: {
         type: Object,
         value: function() {return {items: [],columns: []};}
       },
       row: Number,
       column: String
    },
    makeRealTable: function(table) {
      if(this.realtable.columns.length == 0) {
        this.set('realtable.columns', table.columns);
      }
      this.set('realtable.items', table.items);
      this.refreshSubTable();
    },
    refreshSubTable: function() {
      var ajax = this.$.ajax_subtable;
      if (ajax.row != undefined) {
        ajax.generateRequest();
      }
    },
    parseColumns: function(columns) {
      var newColumns = [];
      for (var i = 0; i < columns.length; i++) {
        var column = columns[i];
        var newColumn = {};
        newColumn.name = column.name;

        switch (column.type) {
          case "table":
            newColumn.renderer = this.subTableRenderer.bind(this);
            break;
          case "time":
            newColumn.renderer = this.timeRenderer.bind(this);
            break;
        }
        newColumns.push(newColumn);
      }
      return newColumns;
    },

    subTableRenderer: function(cell) {
      if (cell.element.innerHTML == "") {
        var button = document.createElement('paper-button');
        var icon = document.createElement('iron-icon');
        icon.icon="open-in-new";
        button.toggles = true;
        button.appendChild(icon);
        button.row = cell.row.index;
        button.column = cell.columnName;
        button.ajax = this.$.ajax_subtable;
        button.addEventListener("click", this.openSubTable);
        cell.element.appendChild(button);
      }
    },
    timeRenderer: function(cell) {
      if (cell.element.innerHTML == "") {
        var time = document.createElement('relative-time');
        time.date = cell.data;
        cell.element.appendChild(time);
      }
    },

    openSubTable: function(event) {
      var ajax = this.ajax;
      ajax.row = this.row;
      ajax.column = this.column;
      ajax.fire('row-changed', this.row);
      ajax.fire('column-changed', this.column);
      ajax.generateRequest();
    }
});
