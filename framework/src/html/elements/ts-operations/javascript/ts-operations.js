Polymer({
    is: 'ts-operations',
    properties: {
      _svg: {
        type: String,
        observer: "_setSVG"
      },
      _result: {
        type: Object,
        observer: "_resultChanged"
      },
      _inputs: {
        type: Array
      },
      _status: {
        type: String,
        observer: "_statusChanged"
      },
      _displayStatus: String,
      _userInterval: {
        type: Number,
        value: 7000
      },
      _inTransition: {
        type: Boolean,
        value: false,
        observer: "_inTransitionChanged"
      }
    },
    _sortInput: function(a, b) {
      var ba = this._isBoolean(a);
      var bb = this._isBoolean(b);

      if (ba && !bb) {
        return -1;
      }
      if (!ba && bb) {
        return 1;
      }
      return 0;
    },
    _getSVG: function() {
      this.$.svg.open();
      this.$.ajax_svg.generateRequest();
    },
    _setSVG: function() {
      Polymer.dom(this.$.svg).innerHTML = this._svg;
    },
    _isBoolean: function(item) {
      return item.type.toLowerCase() == "boolean" || item.type.toLowerCase() == "bool";
    },
    _execute: function(e) {
      var ajax = this.$.execute;
      this._doingReset = !(e.model);
      if (this._doingReset) {
        this._displayStatus = "reset";
      }

      //clear parameters
      ajax.splice('parameters', 0, ajax.parameters.length);

      ajax.push('parameters', "_eventType_", "command");
      ajax._eventType_ = "OnSubmit";
      ajax.command = this._doingReset ? "reset" : e.model.command;
      if (this._inputs) {
        for (var i = 0; i < this._inputs.length; i++) {
          var currentParameter = this._inputs[i];
          ajax.push('parameters', currentParameter.name);
          ajax[currentParameter.name] = currentParameter.value;
        }
      }
      ajax.generateRequest();

      this._inTransition = true;
    },
    _inTransitionChanged: function(inTransition) {
      if (!this._inTransition || this._doingReset) {
        this.$.update_result.set('interval', this._userInterval);
      } else {
        this._userInterval = this.$.update_result.interval;
        this.$.update_result.set('interval', 1000);
      }
    },
    _newStatus: function(event, request) {
      var status = request.response;
      this._inTransition = status.indexOf("_") > -1;
      if (this._inTransition) {
        this._displayStatus = status.split("_")[1];
      } else {
        this._displayStatus = status;
      }
    },
    _statusChanged: function(status) {
      var inTransition = status.indexOf("_") > -1;
      if (!inTransition) {
        this.$.ajax_inputs.generateRequest();
      }
    },
    _encodeParams: function(params) {
      var result = "";
      for (var key in params) {
          if (result != "") {
              result += "&";
          }
          result += key + "=" + encodeURIComponent(params[key]);
      }
      return result;
    },
    _resultChanged: function(newval, oldval) {
      this.$.ajax_commands.generateRequest();
      this.$.ajax_status.generateRequest();

      if (typeof oldval != "undefined") {
        var container = Polymer.dom(this.$.result);
        container.innerHTML = "";
        for (var i = 0; i < newval.length; i++) {
          var response = newval[i];
          var resultbox = document.createElement('div');
          resultbox.setAttribute('result-type', response.type);
          resultbox.innerHTML = response.value;
          container.appendChild(resultbox);
        }
      }


    }
});
