Polymer({
    is: "ts-command",
    properties: {
        name: {
            type: String,
            value: ""
        },
        parameters: {
          type: Array,
          value: function() {return [];}
        },
        _target: {
          type: Object,
          value: function() {return this;}
        }
    },
    _onEnter: function() {
      this._submit();
    },
    _getLabel: function(name, variabletype) {
        return name + " (" + variabletype +")";
    },
    _getValidationPattern: function(variabletype) {
        switch(variabletype) {
            case "bool":
                return "^(0|1|true|false|True|False)$";
                break;
            case "int":
                return "[0-9]*";
                break;
            case "unsigned int":
                return "[0-9]*";
                break;
            case "long":
                return "[0-9]*";
                break;
            case "unsigned long":
                return "[0-9]*";
                break;
            case "double":
                return "^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$";
                break;
            case "string":
                return ".*";
                break;
            default:
                console.warn("<ts-command>: variable type '" + variabletype + "' is unknown, consider developing a validation pattern for it");
        }
    },
    _submit: function(event, detail, sender) {
      var ajax = this.$.ajax;
      for (i=0; i<this.parameters.length;i++) {
        var paramName = this.parameters[i].variable;
        ajax.push('parameters', paramName);
        ajax[paramName] = this.$$("[id='" + this._toId(this.parameters[i].variable + "']")).value;
      }
      ajax.push('parameters', '_eventType_');
      ajax._eventType_ = "OnSubmit";
      this.$.ajax.generateRequest();
    },
    _isJSON: function(data) {
      var json = false;
      try {
        json = JSON.parse(this._unescapeHTML(data.replace('<pre>', '').replace('</pre>', '')));
      } catch (e) {
        return false;
      }
      return json;
    },
    _handleResponse: function(e) {
      this._setResult(e.detail.response);
      this.$.autoupdate.set('noRefresh', false);
    },
    _handleAutoupdate: function(e) {
      var data = this.$.autoupdate.data;
      if (data && data != "") {
        this._setResult(data);
      }
    },
    _setResult: function(data) {
      var resultbox = Polymer.dom(this.$.result);
      var json = this._isJSON(data);

      if (json) {
        var curreneditor = resultbox.querySelector('juicy-jsoneditor');
        if ( !curreneditor || JSON.stringify(json) != JSON.stringify(curreneditor.get()) ) {
          resultbox.innerHTML = "";
          var juicyjson = document.createElement('juicy-jsoneditor');
          juicyjson.set('json', json);
          resultbox.appendChild(juicyjson);
          if (juicyjson.editor) {
            juicyjson.editor.expandAll();
          }
        }
      } else {
        resultbox.innerHTML = data;
      }
    },
    _toId: function(name) {
        return name.replace(" ", "_");
    },

    _unescapeHTML: function(escapedHTML) {
      var div = document.createElement('div');
      div.innerHTML = escapedHTML;
      return div.childNodes.length === 0 ? "" : div.childNodes[0].nodeValue;
    }
});
