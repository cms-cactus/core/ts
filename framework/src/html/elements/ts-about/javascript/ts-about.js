Polymer({
    is: 'ts-about',

    properties: {
        data: Object,

        selectedMode: {
          type: String,
          value: "info"
        },

        rawLogs: Array,
        selectedLog: {
          type: Number,
          value: 0
        },
        processedLogs: {
          type: Array,
          computed: "processLogs(rawLogs)"
        },
        filteredLogs: {
          type: Array,
          computed: "filterLogs(processedLogs, filter_message, filter_location, show_warnings, show_errors, show_fatals)"
        },

        filter_message: {
          type: String,
          value: ""
        },
        filter_location: {
          type: String,
          value: ""
        },
        show_warnings: {
          type: Boolean,
          value: true
        },
        show_errors: {
          type: Boolean,
          value: true
        },
        show_fatals: {
          type: Boolean,
          value: true
        },

        coreDumps: {
          type: Array
        },
        selectedCoreDump: {
          type: Number,
          observer: "selectedCoreDumpChanged"
        },
        coreDump: {
          type: Object,
          value: function() {
            return {
              output: "Select a core dump file"
            }
          }
        },

        cpudata: {
          type: Array,
          value: function() {
            return [
              {
                key: "usr",
                values: []
              },
              {
                key: "nice",
                values: []
              },
              {
                key: "sys",
                values: []
              },
              {
                key: "iowait",
                values: []
              },
              {
                key: "irq",
                values: []
              },
              {
                key: "soft",
                values: []
              },
              {
                key: "steal",
                values: []
              },
              {
                key: "guest",
                values: []
              },
              {
                key: "idle",
                values: []
              }
            ]
          }
        },
        cpuconfig: {
          type: Object,
          value: function() {

            return {
              x: function(d) { return d[0] },
              y: function(d) { return d[1] },
              clipEdge: true,
              useInteractiveGuideline: true,
              color: [
                "#42A5F5",  //usr
                "#D4E157",  //nice
                "#EF5350",  //sys
                "#26A69A",  //iowait
                "#EC407A",  //irq
                "#78909C",  //soft
                "#8D6E63",  //steal
                "#AB47BC",  //guest
                "white"     //idle
              ],
              showControls: false,
              duration: 0
            };
          }
        },
        cpuconfigjs: {
          type: Function,
          value: function() {
            return function() {
              this._chart.style("expand");
              this._chart.xAxis
                .tickFormat(function(d) { return d3.time.format('%X')(new Date(d)) });
              this._chart.yAxis
                .tickFormat(d3.format(',.2f'));
            }
          }
        },

        memdata: {
          type: Array,
          value: function() {
            return [
              {
                "x": "used",
                "y": 0
              }, {
                "x": "free",
                "y": 0
              }
            ];
          }
        },

        memconfig: {
          type: Object,
          value: function() {
            return {
              showLabels : true
            }
          }
        },

        memconfigjs: {
          type: Function,
          value: function() {
            return function() {
              this._chart.valueFormat(function(kbytes) {
                var format = d3.format('.2f');
                if (kbytes < 1024) {
                    return format(kbytes) + 'kB';
                } else if (kbytes < 1024 * 1024) {
                    return format(kbytes / 1024) + 'MB';
                } else if (kbytes < 1024 * 1024 * 1024) {
                    return format(kbytes / 1024 / 1024) + 'GB';
                } else {
                    return format(kbytes / 1024 / 1024 / 1024) + 'TB';
                }
              });
            }
          }
        },
    },

    gotoCoredumps: function() {
      this.selectedMode = "coredumps";
    },
    gotoInfo: function() {
      this.selectedMode = "info";
    },

    selectedCoreDumpChanged: function(i) {
      if (this.coreDumps[i]) {
        this.$.getCoreDump.filename = this.coreDumps[i];
        this.$.getCoreDump.generateRequest();
        this.set("coreDump.output", "Loading " + this.coreDumps[i]);
        Polymer.dom(this.root).querySelector("#download").disabled = false;
      }
    },

    downloadCoreDump: function() {
      var blob = new Blob([this.coreDump.output], {type: "text/plain;charset=utf-8"});
      saveAs(blob, this.coreDumps[this.selectedCoreDump].substring(5) + ".txt");
    },

    addMemoryData: function() {
      var line = this.$.getMemory.data.split("\n")[1].split(" ").filter(Boolean);
      //used
      this.memdata[0].y = Number(line[2]);
      //free
      this.memdata[1].y = Number(line[3]);

      this.$.memchart._dataChanged();
    },

    thereAreCoreDumps: function(coreDumps) {
      return coreDumps.length != 0;
    },

    addCPUData: function() {
      var line = this.$.getCPU.data.split("\n")[4].split(" ").filter(Boolean);
      var timestamp = new Date();
      //usr
      this.cpudata[0].values.push([timestamp, Number(line[2])]);
      //nice
      this.cpudata[1].values.push([timestamp, Number(line[3])]);
      //sys
      this.cpudata[2].values.push([timestamp, Number(line[4])]);
      //iowait
      this.cpudata[3].values.push([timestamp, Number(line[5])]);
      //irq
      this.cpudata[4].values.push([timestamp, Number(line[6])]);
      //soft
      this.cpudata[5].values.push([timestamp, Number(line[7])]);
      //steal
      this.cpudata[6].values.push([timestamp, Number(line[8])]);
      //guest
      this.cpudata[7].values.push([timestamp, Number(line[9])]);
      //idle
      this.cpudata[8].values.push([timestamp, Number(line[10])]);

      this.$.cpuchart._dataChanged();
    },

    getLogsColumns: function() {
      return [{
          name: "time",
          renderer: this.timeRenderer,
          hidable: true
      }, {
          name: "type",
          hidable: true
      }, {
          name: "message",
          hidable: true
      }, {
          name: "location",
          hidable: true
      }];
    },

    processLogs: function(rawLogs) {
      var processedlogs = [];
      if (rawLogs) {
        for (var i = 0; i < rawLogs.length; i++) {
          var rawLog = rawLogs[i];
          var processedLog = {};
          processedLog.file = rawLog.file;
          processedLog.content = [];

          var lines = rawLog.content.split("\n");
          lines.pop(); // last line is always empty
          for (var j = 0; j < lines.length; j++) {
            var record = {};
            var line = lines[j].split(" ");
            record.time = line[1] + " " + line[0] + " " + line[2] + " " + line[3].substring(0, line[3].length - 4);
            switch (line[5]) {
              case "WARN":
                record.type = "warning";
                break;
              case "ERROR":
                record.type = "error";
                break;
              case "FATAL":
                record.type = "fatal";
                break;
              default:
                line[5];
            }
            line[6] == "" ? record.location = line[7] : record.location = line[6];
            line[6] == "" ? record.message = line.splice(10).join(" ") : record.message = line.splice(9).join(" ");

            processedLog.content.unshift(record);
          }
          processedlogs.push(processedLog);
        }
      }
      return processedlogs;
    },

    filterLogs: function(processedLogs, filter_message, filter_location, show_warnings, show_errors, show_fatals) {
      var filteredLogs = [];
      if (processedLogs) {
        for (var i = 0; i < processedLogs.length; i++) {
          var processedLog = processedLogs[i];
          var filteredLog = {};
          filteredLog.file = processedLog.file;
          filteredLog.content = [];

          for (var j = 0; j < processedLog.content.length; j++) {
            var record = processedLog.content[j];

            if (record.message.toLowerCase().indexOf(filter_message) != -1) {
              if (record.location.toLowerCase().indexOf(filter_location) != -1) {
                if (record.type == "warning" && show_warnings ||
                    record.type == "error" && show_errors ||
                    record.type == "fatal" && show_fatals) {
                  filteredLog.content.push(record);
                }
              }
            }
          }

          filteredLogs.push(filteredLog);
        }
      }
      return filteredLogs;
    },

    timeRenderer: function(cell) {
      cell.element.innerHTML = "";
      var relativetime = document.createElement('relative-time');
      relativetime.date = cell.data;
      cell.element.appendChild(relativetime);
    },

    onlyOneLogfile: function(logs) {
      return logs.length == 1;
    },

    insertFilters: function(event) {
      var tables = Polymer.dom(this.root).querySelectorAll('vaadin-grid');
      this.warningboxes = [];
      this.errorboxes = [];
      this.fatalboxes = [];
      this.messagefilters = [];
      this.locationfilters = [];

      for (var i = 0; i < tables.length; i++) {
        var target = tables[i];
        if (target.header.rowCount == 1) {
          var typeFilter = document.createElement('div');

          var warningbox = document.createElement('label');
          var warningcheckbox = document.createElement('input');
          warningcheckbox.setAttribute("type", "checkbox");
          warningcheckbox.style.position = "static";
          warningcheckbox.style.opacity = "1";
          warningcheckbox.checked = true;
          warningcheckbox.addEventListener("change", function(e) {
            console.log(e);
            var target = e.target || e.path[0];
            this.show_warnings = target.checked;
            for (var i = 0; i < this.warningboxes.length; i++) {
              this.warningboxes[i].checked = target.checked;
            }
          }.bind(this));
          this.warningboxes.push(warningcheckbox);
          warningbox.appendChild(warningcheckbox);
          warningbox.appendChild(document.createTextNode("show warnings"));
          typeFilter.appendChild(warningbox);
          var br2 = document.createElement('br');
          typeFilter.appendChild(br2);

          var errorbox = document.createElement('label');
          var errorcheckbox = document.createElement('input');
          errorcheckbox.setAttribute("type", "checkbox");
          errorcheckbox.style.position = "static";
          errorcheckbox.style.opacity = "1";
          errorcheckbox.checked = true;
          errorcheckbox.addEventListener("change", function(e) {
            console.log(e);
            var target = e.target || e.path[0];
            this.show_errors = target.checked;
            for (var i = 0; i < this.errorboxes.length; i++) {
              this.errorboxes[i].checked = target.checked;
            }
          }.bind(this));
          this.errorboxes.push(errorcheckbox);
          errorbox.appendChild(errorcheckbox);
          errorbox.appendChild(document.createTextNode("show errors"));
          typeFilter.appendChild(errorbox);
          var br2 = document.createElement('br');
          typeFilter.appendChild(br2);

          var fatalbox = document.createElement('label');
          var fatalcheckbox = document.createElement('input');
          fatalcheckbox.setAttribute("type", "checkbox");
          fatalcheckbox.style.position = "static";
          fatalcheckbox.style.opacity = "1";
          fatalcheckbox.checked = true;
          fatalcheckbox.addEventListener("change", function(e) {
            console.log(e);
            var target = e.target || e.path[0];
            this.show_fatals = target.checked;
            for (var i = 0; i < this.fatalboxes.length; i++) {
              this.fatalboxes[i].checked = target.checked;
            }
          }.bind(this));
          this.fatalboxes.push(fatalcheckbox);
          fatalbox.appendChild(fatalcheckbox);
          fatalbox.appendChild(document.createTextNode("show fatals"));
          typeFilter.appendChild(fatalbox);
          var br2 = document.createElement('br');
          typeFilter.appendChild(br2);

          var messageFilter = document.createElement('paper-input');
          messageFilter.noLabelFloat = true;
          messageFilter.label = "filter...";
          messageFilter.style.width = "100%";
          messageFilter.addEventListener("value-changed", function(e) {
            this.filter_message = e.detail.value;
            for (var i = 0; i < this.messagefilters.length; i++) {
              this.messagefilters[i].value = e.detail.value;
            }
          }.bind(this));
          this.messagefilters.push(messageFilter);

          var locationFilter = document.createElement('paper-input');
          locationFilter.noLabelFloat = true;
          locationFilter.label = "filter...";
          locationFilter.style.width = "100%";
          locationFilter.addEventListener("value-changed", function(e) {
            this.filter_location = e.detail.value;
            for (var i = 0; i < this.locationfilters.length; i++) {
              this.locationfilters[i].value = e.detail.value;
            }
          }.bind(this));
          this.locationfilters.push(locationFilter);

          target.header.addRow(1, ["", typeFilter, messageFilter, locationFilter]);
        }
      }
    }
});
