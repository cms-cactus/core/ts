#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/lang/Class.h"
#include "toolbox/net/URN.h"

#include <iostream>
#include <sstream>
#include <unistd.h>

class Test: public toolbox::lang::Class
{
  public:
    Test()
    {
      static unsigned long int count=0;
      count++;
      std::ostringstream name;
      name << "wl" << count;
      name_= name.str();
      wl_ = toolbox::task::getWorkLoopFactory()->getWorkLoop ( name_, "waiting" );
      std::cout << name_ << ".Test(): WorkLoop created" << std::endl;
      j1_ = toolbox::task::bind ( this, &Test::job1, "job1" );
      std::cout << name_ << ".Test(): Job created" << std::endl;
    }

    void start()
    {
      wl_->submit ( j1_ );
      std::cout << name_ << ".Test(): Job submitted to WorkLoop" << std::endl;
      wl_->activate();
    }


    ~Test()
    {
      std::cout << "~Test(): Removing from factory" << std::endl;
      toolbox::task::getWorkLoopFactory()->removeWorkLoop ( name_, "waiting" );
      std::cout << "~Test(): deleting workloop" << std::endl;
      delete wl_;
      std::cout << "~Test(): deleting job" << std::endl;
      delete j1_;
    }

    bool job1 ( toolbox::task::WorkLoop* wl )
    {
      std::cout << name_ << ".job1(): Executing Job " << std::endl;
      return false;
    }



  private:

    toolbox::task::ActionSignature* j1_;
    toolbox::task::WorkLoop* wl_;
    std::string name_;

};

int main ()
{
  for ( unsigned long i=1; i< 1000000; ++i )
  {
    std::cout << "main(): Creating WorkLoop number " << i << std::endl;
    Test* t=new Test();
    std::cout << "main(): starting " << i << std::endl;
    t->start();
    std::cout << "main(): deleting t " << i << std::endl;
    delete t;
    std::cout << "main(): Workloop number " << i << " Destroyed" << std::endl;
  }

  return 0;
}

