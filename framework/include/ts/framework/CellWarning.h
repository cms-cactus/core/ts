/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#ifndef _ts_framework_CellWarning_h_
#define _ts_framework_CellWarning_h_

#include <string>

namespace tsframework
{
class CellWarning
{
public:

    enum WarningLevel {NO_WARNING=0,INFO=1000,WARNING=2000,ERROR=3000,FATAL=4000};

    CellWarning();
    
    ~CellWarning();

    void setMessage ( const std::string& msg );
  
    std::string getMessage() const;
  
    void setLevel ( int level );
  
    int getLevel() const;

    //!Sets the message to "" and the level to o (NO_WARNING)
    void clear();
  
    //!Append the message and sets the level to the maximum between the current and the updated
    void append ( const std::string& msg, int level );

    //! Convert a numeric level to string
    static std::string level2string ( int level );

private:
  
    std::string message_;
  
    int level_;
};
}//ns tsframework
#endif
