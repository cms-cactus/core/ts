/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#ifndef _ts_framework_CellXhannelRequestCell_h_
#define _ts_framework_CellXhannelRequestCell_h_


#include "ts/framework/CellXhannelRequest.h"

#include "ts/framework/CellWarning.h"

#include <string>

namespace xdaq
{
class ApplicationDescriptor; 
}


namespace log4cplus
{
class Logger; 
}

namespace xdata
{
class Serializable; 
}

namespace tsframework
{

class CellXhannelRequestCell : public CellXhannelRequest
{
public:

    CellXhannelRequestCell ( log4cplus::Logger& log, CellAbstractContext* context, const xdaq::ApplicationDescriptor* sender, const std::string& ns );

    virtual ~CellXhannelRequestCell();

    void doOpGetState ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& opid, const std::string& url, const std::string& urn );

    void doOpGetState ( const std::string& sid, bool async, const std::string& opid );

    std::string opGetStateReply();

    void doOpReset ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& op, const std::string& url, const std::string& urn );

    void doOpReset ( const std::string& sid, bool async, const std::string& op );

    std::string opResetReply();

    void doOpSendCommand ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& opid, const std::string& command, std::map<std::string, xdata::Serializable*> param, const std::string& url, const std::string& urn );
    
    void doOpSendCommand ( const std::string& sid, bool async, const std::string& opid, const std::string& command, std::map<std::string, xdata::Serializable*> param );
    
    xdata::Serializable* opSendCommandReply();

    void doCommand ( const std::string& ns, const std::string& cid, const std::string& sid, bool async, const std::string& command, std::map<std::string,xdata::Serializable*> param, const std::string& url, const std::string& urn );
    
    void doCommand ( const std::string& sid, bool async, const std::string& command, std::map<std::string,xdata::Serializable*> param );
    
    xdata::Serializable* commandReply();

    CellWarning& getWarning();


  private:

    const xdaq::ApplicationDescriptor* sender_;
    
    std::string getUrl();
    
    std::string getUrn();

    std::string getTargetNs();
    
    void setTargetNs ( const std::string& ns );
    
    std::string targetNs_;
    
    CellWarning warning_;
};
}
#endif
