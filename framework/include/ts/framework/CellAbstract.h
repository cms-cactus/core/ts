#ifndef _ts_framework_CellAbstract_h_
#define _ts_framework_CellAbstract_h_

#include "ts/framework/CellAbstractContext.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptorFactory.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/ApplicationContextImpl.h"

#include "xdata/xdata.h"

#include <string>
#include <set>
#include <sstream>


namespace xgi
{
  class Input; 
  class Output; 
}


namespace tsframework
{

class CellCommandPort;

class CellAbstract : public xdaq::Application , public xdata::ActionListener
{
public:

	CellAbstract ( xdaq::ApplicationStub* s );


	virtual ~CellAbstract()
	{
		delete cellContext_;
	}


	virtual void init() = 0;


	//! Callback for listening SOAP interface
	xoap::MessageReference command ( xoap::MessageReference msg );


	//!SOAP callback for the asynchronous command calls from the GUI
	xoap::MessageReference guiResponse ( xoap::MessageReference msg );


	//! Callback for listening interface
	void actionPerformed ( xdata::Event& e );


	//!Gets Shared Object space for a common xdaq context
	virtual CellAbstractContext* getContext() = 0;


	//!Gets the name of the cell node. Comes froms configuration file "name" parameter
	virtual std::string getName()
	{
		return name_.toString();
	}

	//!Look at CellNamespaceURI.h for your namespace. Owerwrite this method
	virtual std::string getNamespace();

	//!get the xhannel list Url
	std::string getXhannelListUrl();

	void Default ( xgi::Input* in, xgi::Output* out );

	//!Retrieves the logger reference. To be used with LOGCPLUS macros
	log4cplus::Logger& getLogger();


	void createXhannel ( const std::string& name, const std::string& type, const std::string& app, unsigned long targetInstance );


private:

	//!Creates a Xhannel from the cureent cell
	template <class XHANNEL> XHANNEL* createXhannel ( const std::string& xhannelClass, const std::string& targetClass, unsigned long targetInstance )
	{
		const xdaq::ApplicationDescriptor* d;

		try
		{
			d = getApplicationContext()->getDefaultZone()->getApplicationDescriptor ( targetClass, targetInstance );
		}
		catch ( xcept::Exception& e )
		{
			std::ostringstream msg;
			msg << "Cannot find application " + targetClass + "  with instance " << targetInstance;
			XCEPT_RETHROW ( tsexception::XhannelCreationError, msg.str(), e );
		}

		XHANNEL* pXhannel;

		try
		{
			std::set<std::string> allowedZones = xdaq::ApplicationDescriptorFactory::getInstance()->getZoneNames();
			xdata::UnsignedInteger localId ( getMaxLocalId() + 1 );
			const xdaq::ContextDescriptor* c = getApplicationContext()->getContextDescriptor(); 
			std::set<std::string> groups ( getApplicationContext()->getDefaultZone()->getGroupNames() );
      
			xdaq::ApplicationDescriptor* dsub = xdaq::ApplicationDescriptorFactory::getInstance()->createApplicationDescriptor ( c, xhannelClass,static_cast<xdata::UnsignedIntegerT> ( localId ), allowedZones, groups );
      
      dsub->setAttribute("class",xhannelClass);
      dsub->setAttribute("network","local");
      dsub->setAttribute("name",targetClass);

      // better way to instantiate apps needed
      // https://svnweb.cern.ch/trac/cmsos/ticket/4348
      xdaq::Application* aci = dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->getApplicationRegistry()->instantiateApplication ( dsub ) ;
      pXhannel =  dynamic_cast<XHANNEL*>(aci);
      
		}
		catch ( xcept::Exception& e )
		{
			XCEPT_RETHROW ( tsexception::XhannelCreationError, "Cannot create " + xhannelClass,e );
		}

			pXhannel->setTarget ( "subsystem",targetClass,targetInstance );
			pXhannel->setContext ( getContext() );
			return pXhannel;
	}


      unsigned long getMaxLocalId();


protected:

	xdata::String name_;

	CellAbstractContext* cellContext_;

	std::string newline2br ( const std::string& str );

	unsigned long str2uint ( const std::string& str );

	xoap::MessageReference createSoapFault ( xcept::Exception& e );

	xdata::String xhannelListUrl_;

	//!Required by the function manager
	xdata::String stateName_;

};

}	//ns tsframework
#endif
