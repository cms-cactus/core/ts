#ifndef _ts_framework_CellOperationFactory_h_
#define _ts_framework_CellOperationFactory_h_

#include "ts/framework/CellObject.h"

#include "ts/toolbox/ReadWriteLock.h"

#include "log4cplus/loggingmacros.h"

#include "toolbox/lang/Class.h"

#include <map>
#include <string>

namespace tsframework
{

class CellOperation;
class CellAbstractContext;

class CellOperationFactory : public toolbox::lang::Class, public CellObject
{

private:

    class CreatorSignature;
    typedef std::map<std::string,CellOperation*> OperationMap;
    typedef OperationMap::iterator OperationIterator;

public:

    CellOperationFactory ( log4cplus::Logger& log );

    //!Adds a new CellOperation descendant to the factory
    template <class OPERATION>
    void add ( const std::string& type )
    {
        LOG4CPLUS_INFO ( getLogger(), "add :" << type );
        add ( type,new OPERATION ( getLogger(),getContext() ) );
    }

private:
    //!Deletes the CreatorSignature c
    void add ( const std::string& type, CellOperation* cellOperation );

public:

    //!Returns a vector of string with the available operations types
    std::vector<std::string> getOperationTypes();

    //!Returns a vector of strings with the current operation id's
    std::vector<std::string> getOperations();

    //CellOperation* getOperation(const std::string& name);
    CellOperation& getOperation ( const std::string& id );

    // Checks operation instance map to check that the opid does not exist yet
    bool isOperation ( const std::string& id );

private:

    //register of operation instances
    //("opid", CellOperation*)
    std::map<std::string, CellOperation*> operations_;

    // Garbage collection actors
    tstoolbox::ReadWriteLock rwlock_;
  };
}//ns tsframework
#endif
