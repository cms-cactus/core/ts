/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Marc Magrans de Abril                        *
 *************************************************************************/

#ifndef _ts_framework_CellLocalSession_h_
#define _ts_framework_CellLocalSession_h_

#include "ts/framework/CellObject.h"

#include "ts/toolbox/ReadWriteLock.h"

#include <deque>
#include <string>
#include <ctime>


namespace log4cplus
{
    class Logger;
}


namespace tsframework
{
    class CellPage;

//!Describes the local information about the Session used in the GUI
class CellLocalSession: public CellObject
{
public:

    //!Period equal to a shift duration
    static const double CELL_TIMEOUT_SEC;

    static const int HISTORY_SIZE;

    CellLocalSession ( CellAbstractContext* context, log4cplus::Logger& logger );

    ~CellLocalSession();

    std::string getSessionId();

    void setSessionId ( const std::string& sessionId );

    CellPage& getPage();

    void addReply ( const std::string& id, const std::string& payload, const std::string& warningMessage="", int warningLevel=0 );

    bool hasReply ( const std::string& id );

    std::string getPayload ( const std::string& id );

    std::string getWarningMessage ( const std::string& id );

    int getWarningLevel ( const std::string& id );

    void setUsername ( const std::string& username );

    std::string getUsername();

    time_t getLastCalled();

    void setLastCalled();


private:

    time_t lastCalled_;

    struct Reply
    {
        std::string payload;
        std::string warningMessage;
        int warningLevel;
    };

    template <class T>
    class History
    {
    public:

        History ( unsigned int capacity )
        :
        nofItems_ ( 0 ),
        capacity_ ( capacity )
        {
            ;
        }

        History ( const History<T>& history )
        :
        nofItems_ ( 0 ),
        capacity_ ( history.capacity() )
        {
            for ( unsigned int i=history.size()-1; i >= 0 ; --i )
            {
                this->add ( history[i] );
            }
        }

        //! returns capacity
        unsigned int
        capacity() const
        {
            tstoolbox::ReadWriteLockHandler ( rwlock_,tstoolbox::ReadWriteLockHandler::READ );
            return capacity_;
        }

        //! returns # of items in buffer
        unsigned int
        size() const
        {
            tstoolbox::ReadWriteLockHandler ( rwlock_,tstoolbox::ReadWriteLockHandler::READ );
            return nofItems_;
        }


        const T&
        operator[] ( unsigned int i ) const
        {
            tstoolbox::ReadWriteLockHandler ( rwlock_,tstoolbox::ReadWriteLockHandler::READ );
            assert ( i < size() );

            // we are adding items.size to the left op of % to avoid
            // negative operands (effect not defined by ISO C++)
            const T& element ( items_[i] );
            return element;
        }

        void
        add ( const T& item )
        {
            tstoolbox::ReadWriteLockHandler ( rwlock_,tstoolbox::ReadWriteLockHandler::WRITE );

            if ( size() == capacity() )
            {
                items_.erase ( items_.begin() );
            }

            items_.push_front ( item );

            if ( size() < capacity() )
            {
                ++nofItems_;
            }
        }

    private:

        std::deque<T> items_;

        unsigned int nofItems_;

        unsigned int capacity_;

        mutable tstoolbox::ReadWriteLock rwlock_;
    };


    std::string sessionId_;

    //!record of the las replies
    History<std::pair<std::string,Reply> > replies_;

    CellPage* page_;

    //!If true the page is created the first time getPage is called
    bool lazyLoad_;

    std::string username_;
};

}//ns tsframework

#endif
