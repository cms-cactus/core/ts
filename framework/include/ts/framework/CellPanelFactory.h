/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/

#ifndef _ts_framework_CellPanelFactory_h_
#define _ts_framework_CellPanelFactory_h_

#include "ts/framework/CellPanel.h"
#include "ts/framework/CellAbstractContext.h"

#include <string>
#include <vector>
#include <map>


namespace log4cplus
{
	class Logger;
}

//!Abstract Factory class to store and create control panels. This clas is the owner of the control panels
namespace tsframework
{
class CellPanelFactory
{
public:

    CellPanelFactory ( log4cplus::Logger& logger );
  
    ~CellPanelFactory();

    //!Adds a new CellPanel descendant to the factory. After this a control panel can be created/destroyed through the factory
    template <class T> void add ( const std::string& name )
    {
        std::map<std::string,CreatorSignature*>::iterator i ( creators_.find ( name ) );

        if ( i != creators_.end() )
        {
            delete i->second;
        }

        creators_[name] = new Creator<T>();
    }

    //!gets the list of CellPanel classes
    std::vector<std::string> getClassList();

    CellPanel* create ( const std::string& className, CellAbstractContext* context, log4cplus::Logger& logger );

private:
  
    log4cplus::Logger& getLogger();

    class CreatorSignature
    {
    public:
    
        virtual ~CreatorSignature()
        {
            ;
        };

        virtual CellPanel* create ( CellAbstractContext* context, log4cplus::Logger& logger ) = 0;
    };

    template <class T>
    class Creator : public CreatorSignature
    {
    public:

        Creator() {};
     
        CellPanel* create ( CellAbstractContext* context, log4cplus::Logger& logger )
        {
            CellPanel* p = new T ( context,logger );
            return p;
        }
  };

  log4cplus::Logger* logger_;

  std::map<std::string,CreatorSignature*> creators_;
};
}//ns tsframework
#endif
