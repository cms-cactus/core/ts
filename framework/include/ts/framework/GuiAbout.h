#ifndef _ts_GuiAbout_h_
#define _ts_GuiAbout_h_


#include "ts/framework/CellPanel.h"
#include "ts/framework/CellAbstractContext.h"

#include <string>
#include <map>
#include <sstream>

namespace frameworkpanels
{


  class GuiAbout : public tsframework::CellPanel
  {
    private:

      ajax::ResultBox* CoreDumpsResultBox;
      ajax::ResultBox* LatestErrorsResultBox;
      std::string getStartedDate();
      std::string getCompiledDate();
      std::string getVersion();

    public:
      GuiAbout(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);


      ~GuiAbout();

      void layout ( cgicc::Cgicc& cgi );

    protected:

       void getCoreDump ( cgicc::Cgicc& cgi, std::ostream& out );
       void getCoreDumps ( cgicc::Cgicc& cgi, std::ostream& out );
       void getLogs ( cgicc::Cgicc& cgi, std::ostream& out );
       void getCPU ( cgicc::Cgicc& cgi, std::ostream& out );
       void getMemory ( cgicc::Cgicc& cgi, std::ostream& out );
       std::string _cellStartTimeAsString;

  };
}

#endif
