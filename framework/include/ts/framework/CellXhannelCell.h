/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons Magrans          				         *
 *************************************************************************/

#ifndef _ts_framework_CellXhannelCell_h_
#define _ts_framework_CellXhannelCell_h_

#include "ts/framework/CellXhannel.h"

#include "xdaq/Application.h"

#include "xoap/MessageReference.h"

namespace tsframework
{

class CellXhannelRequest;
class CellXhannelRequestCell;

class CellXhannelCell : public CellXhannel
{
public:

    typedef CellXhannelRequestCell RequestHandler;

    XDAQ_INSTANTIATOR();
    
    CellXhannelCell ( xdaq::ApplicationStub* s );
    
    ~CellXhannelCell();

    void send ( CellXhannelRequest* req );

    CellXhannelRequest* createRequestVirtual();

    xoap::MessageReference response ( xoap::MessageReference msg );


};
}//ns tsframework
#endif
