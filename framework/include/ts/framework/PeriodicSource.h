/*************************************************************************
* Trigger Supervisor Component                                          *
*                                                                       *
* Coordinator: Ildefons Magrans de Abril                                *
*                                                                       *
* Responsible: Marc Magrans de Abril                                    *
*                                                                       *
* Author: Christos Lazaridis                                            *
*************************************************************************/

#ifndef _ts_framework_PeriodicSource_h_
#define _ts_framework_PeriodicSource_h_

#include "ts/framework/DataSource.h"

#include "toolbox/task/Timer.h"

#include <string>

namespace log4cplus
{
    class Logger; 
}


namespace tsframework
{

class CellAbstractContext; 

class PeriodicSource : private toolbox::task::TimerListener, public tsframework::DataSource
{
public:

  PeriodicSource ( const std::string& infospaceName,
                   tsframework::CellAbstractContext* context,
                   log4cplus::Logger& logger,
                   int seconds = 10 );

  virtual ~PeriodicSource();

  void setPeriod ( int seconds );

  int getPeriod()
  {
      return period_;
  }

  void start();

  void stop();

protected:

    virtual void periodicAction() = 0;

private:

    PeriodicSource();

    virtual void timeExpired ( toolbox::task::TimerEvent& );

    toolbox::task::Timer* timer_;

    int period_;

    std::string timerName_;
};
}
#endif
