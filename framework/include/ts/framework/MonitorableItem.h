/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons & Marc Magrans de Abril          				         *
 *************************************************************************/

#ifndef _MonitorableItem_h_
#define _MonitorableItem_h_


#include "ts/framework/RefreshFunctionalSignature.h"
#include "ts/framework/RefreshFunctional.h"

#include "ts/exception/CellException.h"

#include "xdata/Serializable.h"

#include "xcept/Exception.h"

#include <typeinfo>

namespace tsframework
{
class MonitorableItem
{
public:

    MonitorableItem ( const std::string& name,xdata::Serializable* serializable );
    
    ~MonitorableItem();

    xdata::Serializable* get();

    template <class ITEM_CLASS>
    void put ( const ITEM_CLASS& value )
    {
        if ( typeid ( ITEM_CLASS ) == typeid ( *serializable_ ) )
        {
            serializable_->setValue ( value );
        }
        else
        {
            std::string msg = "Autorefresh ITEM_CLASS type ";
            msg += typeid ( ITEM_CLASS ).name();
            msg += " does not correspond with Monitorable type ";
            msg += typeid ( serializable_ ).name();
            XCEPT_RAISE ( xcept::Exception,msg );
        }
    }

    void refresh()
    {
        if ( isRefreshable() )
        {
            this->refreshFunctional_->refresh ( name_,serializable_ );
        }
    }

    bool isRefreshable();

    template <class OBJECT,class ITEM_CLASS>
    void autorefresh ( OBJECT* object, void ( OBJECT::*refreshMethod ) ( const std::string&,ITEM_CLASS& ) )
    {
        if ( typeid ( ITEM_CLASS ) == typeid ( *serializable_ ) )
        {
            this->refreshFunctional_ = new RefreshFunctional<OBJECT,ITEM_CLASS> ( object,refreshMethod );
        }
        else
        {
            std::string msg = "Autorefresh ITEM_CLASS type ";
            msg += typeid ( ITEM_CLASS ).name();
            msg += " does not correspond with Monitorable type ";
            msg += typeid ( serializable_ ).name();
            XCEPT_RAISE ( tsexception::NullPointer,msg );
        }
    }

private:

    std::string name_;

    xdata::Serializable* serializable_;

    RefreshFunctionalSignature* refreshFunctional_;
};
}//ns tsframework
#endif
