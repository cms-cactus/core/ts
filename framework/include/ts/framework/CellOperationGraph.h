/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Project Manager: Ildfons Magrans de Abril                             *
 * Authors: Marc Magrans de Abril                                        *
 *************************************************************************/

#ifndef _ts_framework_CellOperationGraph_h_
#define _ts_framework_CellOperationGraph_h_

#include "ajax/Widget.h"

#include "log4cplus/logger.h"

#include <string>
#include <ostream>
#include <graphviz/gvc.h>

namespace tsframework
{

class CellAbstractContext;
class CellOperation;

class CellOperationGraph : public ajax::Widget
{
public:

    CellOperationGraph ( CellAbstractContext* context, log4cplus::Logger& logger, const std::string& operationid );

    // doesn't do anything
    void setWidth ( const std::string& width );
    std::string getWidth();
    void setHeight ( const std::string& height );
    std::string getHeight();

    // renders SVG code
    void html ( cgicc::Cgicc& cgi,std::ostream& out );


private:

    bool isFirable ( const std::string& transition );

    char* string2cstr ( const std::string& s );

    std::string toUpper ( const std::string& str );

    void setNodeProperty ( Agraph_t* g, Agnode_t* node,const std::string& property, const std::string& value );

    void setEdgeProperty ( Agraph_t* g, Agedge_t* edge,const std::string& property, const std::string& value );

    CellOperation& getOperation();

    CellAbstractContext* getContext();

    CellAbstractContext* context_;

    std::string operationid_;

    // doesn't do anything
    std::string width_;
    std::string height_;

    std::string format_;

    log4cplus::Logger& getLogger();

    log4cplus::Logger logger_;

  };
}//ns tsframework
#endif
