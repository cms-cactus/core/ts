/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _ts_framework_CellCommandPort_h_
#define _ts_framework_CellCommandPort_h_

#include "ts/framework/CellObject.h"

#include "xoap/MessageReference.h"


namespace log4cplus
{
	class Logger;
}


namespace tsframework
{

class CellCommandPort: public CellObject
{
public:

	CellCommandPort ( log4cplus::Logger& log );

	//!Used to execute commands from external SOAP requests
	xoap::MessageReference run ( xoap::MessageReference msg );


};
}//ns tsframework
#endif
