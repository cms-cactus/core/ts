/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril                                    *
 *************************************************************************/

#ifndef _ts_framework_CellXhannelMonitor_h_
#define _ts_framework_CellXhannelMonitor_h_

#include "ts/framework/CellXhannelXdaqSimple.h"

//Implements the Xhannel to CGI interface of the XDAQ Monitor
namespace tsframework
{
class CellXhannelRequestMonitor;
class CellXhannelRequest; 

class CellXhannelMonitor : public CellXhannelXdaqSimple
{
  
public:

    typedef CellXhannelRequestMonitor RequestHandler;

    XDAQ_INSTANTIATOR();

    CellXhannelMonitor ( xdaq::ApplicationStub* s );

    ~CellXhannelMonitor();

    //Creates a synchronous call to curl. This implies that it is a blocking call
    void send ( CellXhannelRequest* req );

    CellXhannelRequest* createRequestVirtual();

};
}//ns tsframework
#endif
