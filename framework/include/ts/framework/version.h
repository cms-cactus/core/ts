#ifndef _ts_framework_version_h_
#define _ts_framework_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define TS_TSFRAMEWORK_VERSION_MAJOR 5
#define TS_TSFRAMEWORK_VERSION_MINOR 2
#define TS_TSFRAMEWORK_VERSION_PATCH 4

//
// Template macros
//
#define TS_TSFRAMEWORK_VERSION_CODE PACKAGE_VERSION_CODE(TS_TSFRAMEWORK_VERSION_MAJOR,TS_TSFRAMEWORK_VERSION_MINOR,TS_TSFRAMEWORK_VERSION_PATCH)
#ifndef TS_TSFRAMEWORK_PREVIOUS_VERSIONS
#define TS_TSFRAMEWORK_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TS_TSFRAMEWORK_VERSION_MAJOR,TS_TSFRAMEWORK_VERSION_MINOR,TS_TSFRAMEWORK_VERSION_PATCH)
#else
#define TS_TSFRAMEWORK_FULL_VERSION_LIST  TS_TSFRAMEWORK_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TS_TSFRAMEWORK_VERSION_MAJOR,TS_TSFRAMEWORK_VERSION_MINOR,TS_TSFRAMEWORK_VERSION_PATCH)
#endif

namespace tsframework
{
  const std::string project = "ts";
  const std::string package  =  "tsframework";
  const std::string versions =  TS_TSFRAMEWORK_FULL_VERSION_LIST;
  const std::string description = "Trigger Supervisor Framework library";
  const std::string authors = "Ildefons Magrans de Abril, Marc Magrans de Abril";
  const std::string summary = "Contains the core components of the Trigger Supervisor Framework (https://savannah.cern.ch/projects/l1ts/)";
  const std::string link = "https://savannah.cern.ch/projects/l1ts/";

  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
