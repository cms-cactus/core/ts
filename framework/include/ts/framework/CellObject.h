/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _ts_framework_CellObject_h_
#define _ts_framework_CellObject_h_

#include "log4cplus/logger.h"


namespace tsframework
{
	class CellAbstractContext;

class CellObject
{

public:

	CellObject()
	:
	logger_ ( log4cplus::Logger::getInstance ( "NoSource.CellObject" ) )
	{
		;
	}


	virtual ~CellObject()
	{
		;
	}


	void setContext ( CellAbstractContext* context )
	{
		context_ = context;
	}


	CellAbstractContext* getContext()
	{
		return context_;
	}


	log4cplus::Logger& getLogger()
	{
		return logger_;
	}


protected:

	CellAbstractContext* context_;

	log4cplus::Logger logger_;
};

}//ns tsframework
#endif
