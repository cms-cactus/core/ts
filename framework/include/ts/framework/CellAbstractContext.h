	/*************************************************************************
* Trigger Supervisor Component                                          *
*                                                                       *
* Coordinator: Ildefons Magrans de Abril                                *
*                                                                       *
* Responsible: Marc Magrans de Abril	                                 *
*************************************************************************/

#ifndef _ts_framework_CellAbstractContext_h_
#define _ts_framework_CellAbstractContext_h_

#include "toolbox/task/TimerListener.h"

#include "ts/toolbox/Mutex.h"

#include "log4cplus/logger.h"

#include <string>
#include <map>
#include "ajax/RGB.h"


namespace xdaq
{
	class ApplicationContext;
}

namespace tsframework
{
	class CellAbstract;
	class CellFactory;
	class CellOperationFactory;
	class CellPanelFactory;
	class CellXhannel;
	class CellCommandPort;
	class Level1Source;
	class CellLocalSession;


//!This class contains shared objects accessible from CellCommands, CellOperations and CellPanels
class CellAbstractContext: private toolbox::task::TimerListener
{

	friend class CellAbstract;

public:

	CellAbstractContext ( log4cplus::Logger& log , CellAbstract* cell);

	virtual ~CellAbstractContext();

	CellFactory* getCommandFactory();

	CellOperationFactory* getOperationFactory();

	CellPanelFactory* getPanelFactory();

	std::string getLocalUrl();

	std::string getLocalUrn();

	std::string getMyNs();

	CellXhannel* getXhannel ( std::string name );

	std::map<std::string, CellXhannel*> getXhannelList();

	void setXhannel ( std::string name, CellXhannel* xhn );

	void removeXhannel ( std::string name );

	xdaq::ApplicationContext* getApContext();

	bool hasSession ( const std::string& sessionId );

	std::map<std::string,CellLocalSession*>& getSessionMap();

	CellCommandPort* getCommandPort();

	CellAbstract* getCell();

	std::string getImports();

	void addImport(const std::string& importurl);

	void addImports(const std::vector<std::string>& importurls);

	void setPrimaryColor(ajax::RGB color);
	void setPrimaryColor(int red, int green, int blue);
	void setSecondaryColor(ajax::RGB color);
	void setSecondaryColor(int red, int green, int blue);

	ajax::RGB getPrimaryColor();
	ajax::RGB getSecondaryColor();

	//std::string getSessionCookieName();

	//!Retrieves a reference to the Level 1 monitoring source
	Level1Source& getLevel1Source();

	std::string getCellStartTime();

protected:

	log4cplus::Logger& getLogger();

	log4cplus::Logger logger_;

	std::string _cellStartTimeAsString;

private:

	CellFactory* factory_;

	CellOperationFactory* operationFactory_;

	CellPanelFactory* panelFactory_;

	std::map<std::string, CellXhannel*> xhannels_;

	std::map<std::string,CellLocalSession*> sessions_;

	CellCommandPort* commandPort_;

	CellAbstract* cell_;

	std::string imports_;
	ajax::RGB primaryColor_;
	ajax::RGB secondaryColor_;

	tstoolbox::Mutex lock_;

	Level1Source* l1Source_;

	std::string timerName_;

private:

	virtual void timeExpired ( toolbox::task::TimerEvent& );

	tstoolbox::Mutex& getLock();

};
}//ns tsframework
#endif
