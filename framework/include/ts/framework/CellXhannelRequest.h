/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons Magrans                                             *
 *************************************************************************/

#ifndef _ts_framework_CellXhannelRequest_h_
#define _ts_framework_CellXhannelRequest_h_

#include "ts/framework/CellObject.h"

#include "xoap/MessageReference.h"

#include <string>

#include "log4cplus/helpers/timehelper.h"

namespace tsframework
{
class CellXhannelRequest : public CellObject
{

public:

  CellXhannelRequest();

  void Init();

  virtual ~CellXhannelRequest();

  std::string getRequestId();

  xoap::MessageReference* getRequest();
  
  void setReply ( xoap::MessageReference msg );
  
  xoap::MessageReference* getReply();
  
  bool hasResponse();
  
  void   setSendTimestamp();
  struct timeval getSendTimestamp();
  
  void setReplyTimestamp();
  struct timeval getReplyTimestamp();
  
protected:

  std::string requestId_;

  xoap::MessageReference* msg_request_;

  xoap::MessageReference* msg_reply_;
  
  struct timeval mSendTimestamp;
  struct timeval mReplyTimestamp;

};
}//ns tsframework
#endif
