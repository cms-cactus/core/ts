/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#ifndef _ts_framework_CellFactory_h_
#define _ts_framework_CellFactory_h_

#include "ts/framework/CellObject.h"

#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellAbstractContext.h"

#include "ts/toolbox/ReadWriteLock.h"

#include "toolbox/lang/Class.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xoap/Method.h"

#include <map>
#include <string>
#include <vector>
#include <deque>


namespace log4cplus
{
class Logger;
}

namespace tsframework
{

class CellCommand;

class CellFactory: public toolbox::lang::Class, CellObject
{

public:

	CellFactory ( log4cplus::Logger& log, CellAbstractContext* context );

    ~CellFactory();

	void dispose ( CellCommand* object );

	bool gcjob ( toolbox::task::WorkLoop* wl );

	std::vector<std::string> getGroups();

	std::vector<std::string> getCommands ( const std::string& group );

	CellCommand* create ( const std::string& command );

	//!Adds a new CellCommand descendant to the factory
	template <class COMMAND> void add ( const std::string& group, const std::string& name )
	{
		tstoolbox::ReadWriteLockHandler handle ( gclock_, tstoolbox::ReadWriteLockHandler::WRITE );

		if ( ! isNameCompliant ( name ) )
		{
			XCEPT_RAISE ( tsexception::CommandNameError,"Command name '" + name + "' should be XML tag compliant" );
		}

		CreatorIterator i ( creators_.find ( name ) );

		if ( i != creators_.end() )
		{
			XCEPT_RAISE ( tsexception::CommandAlreadyExist, "The command '" + name + "' already exists in the factory" );
		}

		groups2commands_.insert ( std::make_pair ( group,name ) );
		creators_[name] = new Creator<COMMAND>();
		xoap::bind ( getContext()->getCell(), &CellAbstract::command, name, getContext()->getCell()->getNamespace() );
	}

private:

	class CreatorSignature;

	typedef std::map<std::string,CreatorSignature*> CreatorMap;

	typedef CreatorMap::iterator CreatorIterator;

	typedef std::multimap<std::string,std::string> CommandGroupMap;

	typedef CommandGroupMap::iterator CommandGroupIterator;

	typedef std::deque<CellCommand*> CommandDisposalList;

	typedef CommandDisposalList::iterator CommandDisposalIterator;

	// Garbage collection actors
	toolbox::task::ActionSignature* gcjob_;

	toolbox::task::WorkLoop* gcwl_;

	CommandDisposalList garbage_;

	tstoolbox::ReadWriteLock gclock_;

	static bool isNameCompliant ( const std::string& name );

private:

	class CreatorSignature
	{
	public:

		virtual ~CreatorSignature()
		{
			;
		};

		virtual CellCommand* create ( log4cplus::Logger& logger,CellAbstractContext* context ) = 0;
	};

	template <class T> class Creator: public CreatorSignature
	{
	public:

		Creator() {};

		CellCommand* create ( log4cplus::Logger& logger, CellAbstractContext* context )
		{
			CellCommand* p = new T ( logger,context );
			return p;
		}
	};

private:

      //!multimaps with <group name, command name>
      CommandGroupMap groups2commands_;

      CreatorMap creators_;
  };

}//ns tsframework
#endif
