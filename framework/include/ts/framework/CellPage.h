/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Project Manager: Ildfons Magrans de Abril      				         *
 * Authors: Marc Magrans de Abril               				         *
 *************************************************************************/

#ifndef _ts_framework_CellPage_h_
#define _ts_framework_CellPage_h_

#include "ajax/Page.h"

#include "ts/framework/CellAbstractContext.h"

#include "log4cplus/logger.h"

#include <vector>
#include <string>
#include <iostream>



namespace ajax
{
  class JSONTree;
  class ResultBox;
  class TabContainer;
}


namespace tsframework
{
class CellLocalSession;

class CellPage : public ajax::Page
{

public:

    CellPage ( CellAbstractContext* context, log4cplus::Logger& logger );

    ~CellPage();

    CellAbstractContext* getContext();

    log4cplus::Logger& getLogger();

    void layout ( cgicc::Cgicc& cgi );

    void html ( cgicc::Cgicc& cgi,std::ostream& out );


private:

    bool isOperationInUse ( const std::string& id );

    void onTreeCreateCommand ( cgicc::Cgicc& cgi,std::ostream& out );

    void onTreeCreatePanel ( cgicc::Cgicc& cgi,std::ostream& out );

    void onTreeMonitor ( cgicc::Cgicc& cgi,std::ostream& out );

    void onTreeControlOperation ( cgicc::Cgicc& cgi, std::ostream& out );

    void refreshTree ( ajax::JSONTree* tree );


    ajax::Widget* showAlert ( const std::string& msg );

    bool hasMonitor();

    std::string removeNamespace ( const std::string& flashlist );

    std::vector<std::string> getFlashlists();

    std::vector<std::string> getMonitorableItems ( const std::string& flashlists );

    std::vector<std::string> getOperationNames();

    std::string string2alert ( const std::string& in );

    std::string replace ( const std::string& in,const std::string& from,const std::string& to );

    CellLocalSession* getSession ( cgicc::Cgicc& cgi );

    std::string createOperationId ( const std::string& operationName );

    CellAbstractContext* context_;

    log4cplus::Logger logger_;

    ajax::ResultBox* main_;

    ajax::ResultBox* content_;

    ajax::ResultBox* treeBox_;

    ajax::ResultBox* dummyResult_;

};
}
#endif
