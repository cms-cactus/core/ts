#ifndef _ts_framework_CellOperationTypes_h_
#define _ts_framework_CellOperationTypes_h_

#include "ts/framework/CellCommand.h"

#include <vector>
#include <string>

namespace log4cplus
{
	class Logger;
}

namespace tsframework
{

class CellAbstractContext;

//!Creates a JSON vector with the operation types defined in the CellOperationFactory.
class CellOperationTypes : public CellCommand
{
public:

	CellOperationTypes ( log4cplus::Logger& log, CellAbstractContext* context );

	virtual void code();

private:

	std::string vector2json ( const std::vector<std::string>& v );
};
}//ns tsframework
#endif
