/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril                                *
 *************************************************************************/
#ifndef _ts_framework_CellPingMe_h_
#define _ts_framework_CellPingMe_h_

#include "ts/framework/CellCommand.h"

namespace tsframework
{
class CellPingMe : public CellCommand
{
public:

	CellPingMe ( log4cplus::Logger& log, CellAbstractContext* context );

    virtual void code();

};
}//ns tsframework
#endif
