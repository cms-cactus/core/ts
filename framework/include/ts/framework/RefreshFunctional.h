/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons & Marc Magrans de Abril          				         *
 *************************************************************************/

#ifndef _RefreshFunctional_h_
#define _RefreshFunctional_h_

#include "ts/framework/RefreshFunctionalSignature.h"

namespace xdata
{
    class Serializable; 
}

namespace tsframework
{
template <class OBJECT, class ITEM_CLASS>
class RefreshFunctional : public RefreshFunctionalSignature
{
public:

    RefreshFunctional ( OBJECT* object, void ( OBJECT::*refreshMethod ) ( const std::string&, ITEM_CLASS& ) )
    :
    object_ ( object ),
    refreshMethod_ ( refreshMethod )
    {
        ;
    }

    ~RefreshFunctional()
    {
        ;
    }

    void refresh ( const std::string& name, xdata::Serializable* value )
    {
        ( object_->*refreshMethod_ ) ( name,dynamic_cast<ITEM_CLASS&> ( *value ) );
    }

private:

  OBJECT* object_;
  
  void ( OBJECT::*refreshMethod_ ) ( const std::string&,ITEM_CLASS& );

};
}//ns tsframework
#endif
