#ifndef _ts_framework_CellXhannelTB_h_
#define _ts_framework_CellXhannelTB_h_

#include "ts/framework/CellXhannel.h"
#include "ts/framework/CellXhannelRequest.h"

#include "xdaq/Application.h"

#include "xoap/MessageReference.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"

#include "cgicc/HTMLClasses.h"

#include "xdata/Table.h"

#include <string>
namespace tsframework
{

  class CellXhannelRequestTB;

  class CellXhannelTB: public CellXhannel
  {
    public:
      typedef CellXhannelRequestTB RequestHandler;

    public:

      XDAQ_INSTANTIATOR();
      CellXhannelTB ( xdaq::ApplicationStub* s );
      ~CellXhannelTB();

      bool isConnected();
      void connect ( const std::string& view, const std::string& username, const std::string& password, bool retry=true );
      void disconnect();

      void send ( CellXhannelRequest* req );

      CellXhannelRequest* createRequestVirtual();

    private:
      DOMNode* getNodeNamed ( xoap::MessageReference msg,const std::string& nodeName );

    private:
      void setConnection ( const std::string& view, const std::string& id );
      std::string getView();
      std::string getId();

    private:
      std::string id_;
      std::string view_;

  };
}
#endif
