
#ifndef _ts_framework_CellOpReset_h_
#define _ts_framework_CellOpReset_h_

#include "ts/framework/CellCommand.h"

namespace tsframework
{

class CellOpReset: public CellCommand
{

public:

    CellOpReset ( log4cplus::Logger& log, CellAbstractContext* context );

    void code();

};

}//ns tsframework
#endif
