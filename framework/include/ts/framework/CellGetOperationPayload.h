#ifndef _ts_framework_CellGetOperationPayload_h_
#define _ts_framework_CellGetOperationPayload_h_

#include "ts/framework/CellCommand.h"

namespace log4cplus
{
	class Logger;
}

namespace tsframework
{

class CellAbstractContext;

class CellGetOperationPayload : public CellCommand
{
public:

	CellGetOperationPayload ( log4cplus::Logger& log, CellAbstractContext* context );

  	virtual void code();
};

}//ns tsframework
#endif
