/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons Magrans          				         *
 *************************************************************************/

#ifndef _ts_framework_CellXhannel_h_
#define _ts_framework_CellXhannel_h_

#include "ts/toolbox/Mutex.h"

#include "ts/framework/CellXhannelRequest.h"
#include "ts/framework/CellAbstractContext.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"

#include <string>


namespace log4cplus
{
    class Logger;
}

namespace tsframework
{

class CellAbstractContext; 
class CellXhannelRequest;

class CellXhannel : public xdaq::Application
{

protected:

    static const int MAX_ATTEMPTS = 2;

    static const int MAX_TIMEOUT_MS = 5000;

public:

    CellXhannel ( xdaq::ApplicationStub* s );

    virtual ~CellXhannel();

    virtual void send ( CellXhannelRequest* req ) = 0;

    void setRequest ( const std::string& name, CellXhannelRequest* req );

    CellXhannelRequest* getRequest ( const std::string& name );

    void removeRequest ( const std::string& name );

    CellXhannelRequest* createRequest();

    virtual CellXhannelRequest* createRequestVirtual() = 0;

    void removeRequest ( CellXhannelRequest* req );

    void setTarget ( const std::string& group, const std::string& application, unsigned long instance );

    const xdaq::ApplicationDescriptor* getTargetDescriptor();

    void setContext ( CellAbstractContext* context );

    CellAbstractContext* getContext();

    log4cplus::Logger& getLogger();

protected:

    std::map<std::string, CellXhannelRequest*> reqMap_;

    std::string targetGroup_;

    std::string targetApplication_;

    unsigned long targetInstance_;

private:

    CellAbstractContext* context_;

protected:
    
    tstoolbox::Mutex mutex_;
};
}//ns tsframework
#endif
