/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _ts_framework_CellOpGetState_h_
#define _ts_framework_CellOpGetState_h_

#include "ts/framework/CellCommand.h"

namespace tsframework
{
  
class CellOpGetState : public CellCommand
{

public:

	CellOpGetState ( log4cplus::Logger& log, CellAbstractContext* context );

    virtual void code();

};

}//ns tsframework
#endif
