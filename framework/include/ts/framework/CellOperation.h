/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#ifndef _ts_framework_CellOperation_h_
#define _ts_framework_CellOperation_h_

#include "ts/framework/CellObject.h"
#include "ts/framework/CellWarning.h"
#include "ts/framework/CellFSM.h"

#include "ts/toolbox/Mutex.h"

#include "xdata/String.h"

#include "toolbox/lang/Class.h"

#include <string>
#include <map>

namespace toolbox
{
namespace task
{
	class WorkLoop;
}
}


namespace xdata
{
	class Serializable;
}

namespace tsframework
{

	class CellAbstractContext;

class CellOperation: public toolbox::lang::Class, public CellObject
{

protected:

	CellOperation ( log4cplus::Logger& log, tsframework::CellAbstractContext* context );

private:

	CellOperation();

	CellOperation ( const CellOperation& );

	CellOperation& operator= ( const CellOperation& );

public:

	virtual ~CellOperation();

	bool job ( toolbox::task::WorkLoop* wl );

	std::string getId();

	void setId ( const std::string& opId );

	//!access to the parameters
	std::map<std::string, xdata::Serializable*>& getParamList();

	//!To set the parameter values
	void setParameterFromString ( const std::string& name, const std::string& value );

	//!To set the parameter map
	void setParameterMap ( std::map<std::string, xdata::Serializable*> param );

	//!To set the current Sesion id that controls this operation
	void setSessionId ( const std::string& );

	//!To get the current sesion Id that is using the oepration
	std::string getSessionId();

	//!Get the warning object from the operation
	CellWarning& getWarning();

	tstoolbox::Mutex& getLock();

	CellFSM& getFSM();

	//!Calls reseting and always reset without tthrowing. Errors added in the getWarning object
	void reset();

	//!Operation Reset method. Does all necessary action before reseting the opertion. If returns false the operation is not reset
	virtual void resetting() = 0;

	//!retrieves the current payload/result of the operation
	std::string getResult();

	//!sets the current [ayload/string of the operation
	void setResult ( const std::string& result );

private:

	CellFSM fsm_;

	std::string operationId_;

	std::string currentSid_;

	std::map<std::string,xdata::Serializable*> paramList_;

	CellWarning  warning_;

	xdata::String result_;

	tstoolbox::Mutex lock_;

  };
}//ns tsframework
#endif
