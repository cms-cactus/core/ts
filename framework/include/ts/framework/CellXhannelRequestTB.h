/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/

#ifndef _ts_framework_CellXhannelRequestTB_h_
#define _ts_framework_CellXhannelRequestTB_h_

#include "ts/framework/CellXhannelRequest.h"

#include "xdata/Table.h"

#include <string>

namespace log4cplus
{
class Logger; 
}

namespace tsframework
{
class CellXhannelTB;

class CellXhannelRequestTB: public CellXhannelRequest
{
    friend class CellXhannelTB;

public:

    CellXhannelRequestTB ( log4cplus::Logger& log, CellAbstractContext* context,const std::string& view, const std::string& id );

    virtual ~CellXhannelRequestTB();

    //!Uses a "getRegister" query as defined in the configuration file. For a generic query use doGetQuery/getQueryReply
    void doGetRegister ( const std::string& table, const std::string& primaryColumn, const std::string& primaryValue );

    //!Returns the Request reply for a single row SQL query
    std::map<std::string,std::string> getRegisterReply();

    //!Generic Query interface for TStore. The map contains pairs (name of the parameter, parameter value)
    void doGetQuery ( const std::string& query, std::map<std::string,std::string>& parameters );

    //!The xdata::Table is not deleted with the request.
    xdata::Table* getQueryReply();

    //!Using the subview @insertQuery inserts the rows in the table @table
    void doInsert ( const std::string& insertQuery, xdata::Table* table );

    //!Using the subview @insertQuery inserts the rows in the table @table
    void doUpdate ( const std::string& insertQuery, xdata::Table* table );

    //! inserts the rows in @data into table @tableName using NestedView
    void doInsertForTable ( const std::string& tableName, xdata::Table* data );

    //! deletes the rows in @data from table @tableName using NestedView
    void doDeleteForTable ( const std::string& tableName, xdata::Table* data );

private:

  std::string getView();

  std::string getId();

  void doWithNestedView ( const std::string& action, const std::string& tableName, xdata::Table* data );

  std::string view_;

  std::string id_;

};

}
#endif
