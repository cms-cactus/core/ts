/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#ifndef _ts_framework_CellFSM_h_
#define _ts_framework_CellFSM_h_

#include "ts/exception/CellException.h"

#include "toolbox/fsm/FiniteStateMachine.h"

#include <string>
#include <vector>
#include <map>
#include <set>


namespace tsframework
{

class CellFSM
{
public:

	typedef toolbox::fsm::State State;

private:

	CellFSM ( const CellFSM& );

	CellFSM& operator = ( const CellFSM& );

public:

	CellFSM();

	~CellFSM();

	//!Execute transition with a given name
	void executeTransition ( const std::string& transition );

	//!Resets the finite State Machine.
	void reset();

	//!Remove the FSM definition. This is the existing transitions and states are removed and the methods addState amd addTransition should be called again
	void clear();

	//!Gets all the sates of the FSM
	std::vector<State> getStates();

	//!Gets the name for a given State
	std::string getStateName ( const State& s );

	//!Gets the current State
	State getCurrentState();

	//!Gets the initial State
	State getInitialState();

	//!Sets the initial State of the FSM
	void setInitialState ( const std::string& );

	//!Gets the transitions leaving a given State. A map with transition name and destination state is returned
	std::map<std::string,State> getTransitions ( const State& s );

	//!For a given State gives the name of the incoming transitions
	std::set<std::string> getInputs ( const State& s );

	//!Is true if the transition is being executed
	bool isCurrentTransition ( const State& state,const std::string& transtion );

	//!Adds an State to the FSM
	void addState ( const std::string& state );

	//!Adds a transition to the FSM
	template <class OBJECT>
	void addTransition ( const std::string& from, const std::string& to, const std::string& event, OBJECT* object, bool ( OBJECT::*condition ) (), void ( OBJECT::*function ) () )
	{
		if ( states_.find ( from ) == states_.end() )
		{
			std::string msg = "State name: " + from + " does not exist";
			XCEPT_RAISE ( tsexception::CellException, msg );
		}

		State fromState = states_[from];

		if ( states_.find ( to ) == states_.end() )
		{
			std::string msg = "State name: " + to + " does not exist";
			XCEPT_RAISE ( tsexception::CellException, msg );
		}

		State toState = states_[to];

		//add intermediate state
		if ( ( unsigned ) higestState_ >= 254 )
		{
			XCEPT_RAISE ( tsexception::CellException,"It is not possible to create more than 255 states" );
		}

		if ( higestState_ == ( 'F' - 1 ) )
		{
			higestState_ += 2;
		}
		else
		{
			higestState_ += 1;
		}

		State interState = higestState_;
		std::string interStateName = from + "_" + event + "_" + to;
		fsm_->addState ( interState, interStateName, this, &CellFSM::interStateMethod );
		StateMethodSignature* stateMethod = new StateMethod<OBJECT> ( object, condition, function );

		stateMethods_[interStateName] 		= stateMethod;
		transition2state_[fromState][event] = interState;

		//add event transition
		fsm_->addStateTransition ( fromState, interState, event, this, &CellFSM::emptyTransitionMethod );

		//add move_forward transition
		fsm_->addStateTransition ( interState, toState, "move_forward", this, &CellFSM::emptyTransitionMethod );

		//add move_back transition
		fsm_->addStateTransition ( interState, fromState, "move_back", this, &CellFSM::emptyTransitionMethod );

		//add rollback transition
		std::string rollback = getRollbackTransition ( from,event );
		fsm_->addStateTransition ( toState, fromState, rollback, this, &CellFSM::emptyTransitionMethod );
	}

private:

	//!Sents an event to the FSM
	void fireEvent ( toolbox::Event::Reference e );

	//generic method in the intermediate state
	void interStateMethod ( toolbox::fsm::FiniteStateMachine& fsm );

	//method that does nothing in move and move_back transitions
	void emptyTransitionMethod ( toolbox::Event::Reference e );

	class StateMethodSignature
	{
	public:

		virtual ~StateMethodSignature()
		{
		;
		}

		virtual bool condition() = 0;

		virtual void method() = 0;
	};

	template <class OBJECT> class StateMethod: public StateMethodSignature
	{
	public:

		StateMethod ( OBJECT* object, bool ( OBJECT::*condition ) (), void ( OBJECT::*method ) () )
		:
		obj_ ( object ),
		condition_ ( condition ),
		method_ ( method )
		{
		}

		bool
		condition()
		{
			if ( condition_ != 0 )
			{
			return ( obj_->*condition_ ) ();
			}

			return true;
		}

		void
		method()
		{
			if ( method_ != 0 )
			{
				return ( obj_->*method_ ) ();
			}
		}

	private:

		OBJECT* obj_;

		bool ( OBJECT::*condition_ ) ();

		void ( OBJECT::*method_ ) ();
	};

public:

	//!get the name of the transition that rollbacks a given transition
	std::string getRollbackTransition ( const std::string& from, const std::string& transition );

	std::vector<State> getAllStates();

	bool isVisible ( const State& state );

	std::map<std::string, State> getAllTransitions ( const State& state );

	std::string getState();

	bool isFirable ( const std::string& transition );

private:
	toolbox::fsm::FiniteStateMachine* fsm_;

	State higestState_;

	std::map<std::string, State> states_;

	std::map<std::string,StateMethodSignature*> stateMethods_;

	std::map<toolbox::fsm::State,std::map<std::string,State> > transition2state_;

};
}//ns tsframework
#endif
