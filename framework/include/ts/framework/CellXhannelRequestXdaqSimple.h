/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril                                *
 *************************************************************************/

#ifndef _ts_framework_CellXhannelRequestXdaqSimple_h_
#define _ts_framework_CellXhannelRequestXdaqSimple_h_

#include "ts/framework/CellObject.h"
#include "ts/framework/CellXhannelRequest.h"

#include <string>

#include "xoap/MessageReference.h"

#include "log4cplus/logger.h"

namespace xdata
{
class Serializable; 
}


namespace tsframework
{
  //!Should be used with CellXhannelXdaqSimple. Allows the communication between Cells and XDAQ applications
  class CellXhannelRequestXdaqSimple: public CellXhannelRequest
  {
    public:

      CellXhannelRequestXdaqSimple ( log4cplus::Logger& log, CellAbstractContext* context );

      virtual ~CellXhannelRequestXdaqSimple();

      //!Send a SOAP command to a XDAQ aplication without parameters
      void doCommand ( const std::string& command, const std::string& nameSpace );
      //!Send a SOAP command with a custom syntax
      void doCommand ( xoap::MessageReference msg );
      void doCommand ( const std::string& msg );
      
      xoap::MessageReference commandReply();

      xdata::Serializable* parameterGetReply();
      //!Get a parameter from the application infospace of the target XDAQ application
      void doParameterGet ( const std::string& name, const std::string& nameSpace, const std::string& type );
      
      //!Set a parameter from the application infospace of the target XDAQ application
      void buildCommand( const std::string& command, const std::string& nameSpace );
      void doParameterSet ( const std::string& name, const std::string& nameSpace, const std::string& type, const std::string& value );

  };
}
#endif
