/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _ts_framework_CellOpSendCommand_h_
#define _ts_framework_CellOpSendCommand_h_

#include "ts/framework/CellCommand.h"

#include "ts/exception/CellException.h"

namespace tsframework
{
class CellOpSendCommand : public CellCommand
{
public:

    CellOpSendCommand ( log4cplus::Logger& log,CellAbstractContext* context );

    virtual void code();

private:

    //!class to handle unexpected exceptions
    class UnexpectedHandler
    {
    public:

        UnexpectedHandler()
        {
            old = std::set_unexpected ( handler );
        }

        ~UnexpectedHandler()
        {
            std::set_unexpected ( old );
        }

        static void handler()
        {
            std::string msg;
            msg = "Unexpected exception while executing a transition. Try to catch the exception and rethrow to toolbox::fsm::exception::Exception";
            XCEPT_RAISE ( tsexception::OperationExecutionError,msg );
        }

    private:

        std::unexpected_handler old;
    };
};


}//ns tsframework
#endif
