/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: I. Magrans          				         *
 *************************************************************************/

#ifndef _ts_framework_CellErrorCommand_h_
#define _ts_framework_CellErrorCommand_h_

#include "ts/framework/CellCommand.h"

#include <string>

namespace log4cplus
{
	class Logger;
}


namespace tsframework
{

class CellAbstractContext;

class CellErrorCommand: public CellCommand
{
public:

	CellErrorCommand ( log4cplus::Logger& log,CellAbstractContext* context );

	virtual void code();

	void setErrorMessage ( const std::string& error );

private:

	std::string errorMessage_;

};

}//ns tsframework
#endif
