/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons & Marc Magrans de Abril,                            *
 *                                                                       *
 * Modified by: Christos Lazaridis                                       *
 *************************************************************************/

#ifndef _ts_framework_DataSource_h_
#define _ts_framework_DataSource_h_

#include "ts/framework/MonitorableItem.h"
#include "ts/framework/CellObject.h"
#include "ts/toolbox/Mutex.h"

#include "xdata/xdata.h"

#include <vector>
#include <map>
#include <string>
#include <typeinfo>

namespace log4cplus
{
    class Logger;
}

namespace tsframework
{

class CellAbstractContext;
//!Contains monitorable items. Always use the same name for the infospace.

class DataSource : public xdata::ActionListener, public CellObject
{
public:

    DataSource ( const std::string& infospaceName, CellAbstractContext* context, log4cplus::Logger& logger );
    
    virtual ~DataSource();

    //!refreshes the monitorable items
    void actionPerformed ( xdata::Event& received );

    //!adds a new Monitorable item that should be xdata::Serializable descendant
    template <class ITEM_CLASS> 
    void add ( const std::string& name )
    {
        MonitorableItem* mon = new MonitorableItem ( name, new ITEM_CLASS() );
        monitorables_[name] = mon;
        infospace_->fireItemAvailable ( name, mon->get() );
    }

    //!remove a monitorable item
    void remove ( const std::string& name );

    //!Gets the xdata::Serializable of a Monitorable item
    xdata::Serializable* get ( const std::string& name );

    //!fire item changed for one item only
    void push ( const std::string& name );

    //!fire item changed for all items
    void push();

    //!Makes a copy of the monitorable item
    template <class ITEM_CLASS>
    ITEM_CLASS clone ( const std::string& name )
    {
        tstoolbox::MutexHandler h ( getMutex() );
        xdata::Serializable* x = get ( name );

        if ( ITEM_CLASS* xx = dynamic_cast<ITEM_CLASS*> ( x ) )
        {
            return ITEM_CLASS ( *xx );
        }
        else
        {
            std::ostringstream msg;
            msg << "Can not cast '" << name << "' to '" << typeid ( ITEM_CLASS ).name() << "'";
            XCEPT_RAISE ( tsexception::CellException, msg.str() );
        }
    }

    //!Sets the monitored item to another value * Does NOT fire item changed! Need to push() for that
    template <class ITEM_CLASS> void put ( const std::string& name, const ITEM_CLASS& value )
    {
        tstoolbox::MutexHandler h ( mutex_ );

        if ( monitorables_.find ( name ) != monitorables_.end() )
        {
            MonitorableItem* mon = monitorables_.find ( name )->second;
            mon->put ( value );
        }
        else
        {
            std::string msg ( "Trying to put value in a Monitorable item " + name + " that does not exist " );
            XCEPT_RAISE ( xcept::Exception,msg );
        }
    }


    //!adds a new Monitorable item to be collected. pull mode
    template <class OBJECT, class ITEM_CLASS> void autorefresh ( const std::string& name, OBJECT* object, void ( OBJECT::*refreshMethod ) ( const std::string&,ITEM_CLASS& ) )
    {
        if ( monitorables_.find ( name ) != monitorables_.end() )
        {
            MonitorableItem* mon = monitorables_.find ( name )->second;
            mon->autorefresh ( object,refreshMethod );
        }
        else
        {
            std::string msg ( "Trying to define autorefresh method for a Monitorable item " + name + " that does not exist " );
            XCEPT_RAISE ( xcept::Exception,msg );
        }
    }

    //!gets the names of all the monitorable items of the present source
    std::vector<std::string> getNames();

private:

    std::map<std::string, MonitorableItem*> monitorables_;

    std::string infospaceName_;

    xdata::InfoSpace* infospace_;

    tstoolbox::Mutex mutex_;

protected:

    //!Passes a refernce to the DataSource recursive mutex
    tstoolbox::Mutex& getMutex();

private:

    //!Manges the lock/unlock of the XDAQ infospace
    class InfoSpaceHandler
    {
    public:
      
        InfoSpaceHandler ( xdata::InfoSpace& info ) :info_ ( info )
        {
            info_.lock();
        };

        ~InfoSpaceHandler()
        {
            info_.unlock();
        };

    private:

        xdata::InfoSpace& info_;
      
        InfoSpaceHandler ( const InfoSpaceHandler& );
      
        InfoSpaceHandler& operator= ( const InfoSpaceHandler& );
    };
};
}//ns tsframework
#endif
