/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril	                                 *
 *************************************************************************/

#ifndef _ts_framework_CellPanel_h_
#define _ts_framework_CellPanel_h_

#include "ajax/AutoHandledWidget.h"

#include "log4cplus/logger.h"
 
#include <iostream>

namespace cgicc
{
	class Cgicc;
}
#include "log4cplus/loggingmacros.h"

//!Base class for all the complex widgets of the TS framework: CellCommandWidget, CellPage, CellControlPanel,etc.
namespace tsframework
{
class CellAbstractContext;


class CellPanel: public ajax::AutoHandledWidget
{
public:

	CellPanel ( CellAbstractContext* context,log4cplus::Logger& logger );

	virtual ~CellPanel();

	CellAbstractContext* getContext();

	log4cplus::Logger& getLogger();

	void html ( cgicc::Cgicc& cgi, std::ostream& out );

protected:

  	log4cplus::Logger logger_;

	virtual void layout ( cgicc::Cgicc& cgi ) = 0;

  	CellAbstractContext* context_;
};
}//ns tsframework
#endif
