#ifndef _ts_framework_CellMonitorWidget_h_
#define _ts_framework_CellMonitorWidget_h_

#include "ts/framework/CellPanel.h"
#include "ajax/PolymerElement.h"
#include <jsoncpp/json/json.h>

namespace cgicc {
	class Cgicc;
}
namespace log4cplus {
	class Logger;
}
namespace xdata {
	class Serializable;
	class Table;
}
namespace tsframework {
	class CellAbstractContext;
	class CellMonitorWidget : public CellPanel {
		public:
			CellMonitorWidget ( CellAbstractContext* context, log4cplus::Logger& logger );
			~CellMonitorWidget();
			//!flashlist is passed with namespace, item is passed without
			void init ( const std::string& flashlist );
			std::string getFlashlist();

			void getTable(cgicc::Cgicc& cgi,std::ostream& out);
			void getSubTable(cgicc::Cgicc& cgi,std::ostream& out);

		private:
			std::string flashlistName_;
			void layout ( cgicc::Cgicc& cgi );
			xdata::Table* getData();
			Json::Value xdataTableToJSON(xdata::Table& data);
	};
}
#endif
