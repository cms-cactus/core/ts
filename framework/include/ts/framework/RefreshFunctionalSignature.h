/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Authors: Ildefons & Marc Magrans de Abril          				         *
 *************************************************************************/

#ifndef _RefreshFunctionalSignature_h_
#define _RefreshFunctionalSignature_h_

#include <string>
 
namespace xdata
{
	class Serializable; 
}


namespace tsframework
{
//!Base class for all subscription items
class RefreshFunctionalSignature
{
public:
	
  virtual ~RefreshFunctionalSignature();

  virtual void refresh ( const std::string& name, xdata::Serializable* value ) = 0;

};
}//ns tsframework
#endif
