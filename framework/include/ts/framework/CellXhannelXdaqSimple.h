/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril                                *
 *************************************************************************/

#ifndef _ts_framework_CellXhannelXdaqSimple_h_
#define _ts_framework_CellXhannelXdaqSimple_h_

#include "ts/framework/CellXhannel.h"

#include "xdaq/Application.h"


namespace xdaq
{
class ApplicationStub; 
}

//!Allows communication from the Cell to other XDAQ applications
namespace tsframework
{

class CellXhannelRequest; 
class CellXhannelRequestXdaqSimple;

class CellXhannelXdaqSimple : public CellXhannel
{
public:
  
    typedef CellXhannelRequestXdaqSimple RequestHandler;
  
    XDAQ_INSTANTIATOR();
  
    CellXhannelXdaqSimple ( xdaq::ApplicationStub* s );
  
    ~CellXhannelXdaqSimple();

    void send ( CellXhannelRequest* req );

    CellXhannelRequest* createRequestVirtual();

};
}//ns tsframework
#endif
