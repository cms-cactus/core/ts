/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun.
 *************************************************************************/

#ifndef _ts_framework_CellOperationTypesAndParams_h_
#define _ts_framework_CellOperationTypesAndParams_h_

#include "ts/framework/CellCommand.h"


namespace tsframework
{

//! Returns all operations along with their parameters as a JSON string
//!
//! E.g.
//! [{"Configuration":{"L1_KEY":"string","MI_KEY":"string"}},{"InterconnectionTest":{"Testcase_KEY":"string"}}]
//!
class CellOperationTypesAndParams : public CellCommand
{

public:

	CellOperationTypesAndParams ( log4cplus::Logger& log, CellAbstractContext* context );

	virtual void code();

};

} // end ns tsframework

#endif
