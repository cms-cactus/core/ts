#ifndef _ts_framework_CellOperationParamList_h_
#define _ts_framework_CellOperationParamList_h_

#include "ts/framework/CellCommand.h"

#include <map>
#include <string>

namespace xdata
{
	class Serializable;
}

namespace tsframework
{

//!Creates a JSON object (or map) with the operation parameters names and types.
//!The type identifiers are the ones appearing in the xdata::Serialize::type

class CellOperationParamList : public CellCommand
{
public:

	CellOperationParamList ( log4cplus::Logger& log, CellAbstractContext* context );

    virtual void code();

private:

    std::string map2json ( const std::map<std::string, xdata::Serializable*>& m );

    std::string createId();
};

}//ns tsframework
#endif
