#ifndef _ts_framework_CellXhannelRequestHandler_h_
#define _ts_framework_CellXhannelRequestHandler_h_

#include <typeinfo>
#include <string>

#include "ts/exception/CellException.h"

namespace tsexception
{
    DEFINE_TS_EXCEPTION ( RequestCreationFailed );
}


namespace tsframework
{

template <typename T>
class CellXhannelRequestHandler
{
public:

    CellXhannelRequestHandler ( T* x )
    :
    x_ ( x )
    {
        r_ = dynamic_cast<typename T::RequestHandler*> ( x_->createRequest() );

        if ( !r_ )
        {
            std::string err ( "Failed to create request of type \'" );
            err += typeid ( typename T::RequestHandler ).name();
            err += "\'";
            XCEPT_RAISE ( tsexception::RequestCreationFailed,err );
        }
    };


    ~CellXhannelRequestHandler()
    {
        x_->removeRequest ( r_ );
    };

    typename T::RequestHandler* getRequest()
    {
        return r_;
    };

private:

    CellXhannelRequestHandler ( const CellXhannelRequestHandler& );

    CellXhannelRequestHandler& operator = ( const CellXhannelRequestHandler& );

    T* x_;
    
    typename T::RequestHandler* r_;
};

}//ns tsframework
#endif
