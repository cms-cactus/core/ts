/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Ildefons Magrans de Abril	                         *
 *************************************************************************/

#ifndef _ts_framework_CellCommand_h_
#define _ts_framework_CellCommand_h_

#include "ts/framework/CellObject.h"

#include "ts/exception/CellException.h"

#include "toolbox/lang/Class.h"
#include "toolbox/squeue.h"
#include "toolbox/task/Action.h"

#include "xoap/MessageReference.h"

#include <string>
#include <exception>
#include <map>


namespace toolbox
{
namespace task
{
	class WorkLoop;
}
}

namespace xdata
{
	class Serializable;
}


namespace tsframework
{

	class CellWarning;

class CellCommand: public toolbox::lang::Class, public CellObject
{


protected:

	CellCommand ( log4cplus::Logger& log, CellAbstractContext* context );

private:

	CellCommand();

	CellCommand ( const CellCommand& );

	CellCommand& operator= ( const CellCommand& );

public:

	virtual ~CellCommand();

	//!runs the CellCommand used from CellCommandPort
	xoap::MessageReference run();

	//!get the result of the CellCommand
	xdata::Serializable& getResult();

	//!Returns the namespace that will be used for the corresponding SOAP message
	static std::string getNS();

	void setMsg ( xoap::MessageReference msg );

	xoap::MessageReference getMsg();

	//!access to the parameters
	std::map<std::string, xdata::Serializable*>& getParamList();

	//!Converts the value parameter to the apropiate xdata::SimpleType
	virtual void setParameterFromString ( const std::string& name, const std::string& value );

	//!this method tells whether or not the command is over, so the command object can be removed. Is used by CellFactory
	bool isOver();

	std::string getSessionId();

	void setSessionId ( const std::string& sid );

	// get warning
	CellWarning& getWarning();

	//!Code that will be executed while executing the command. to be filled during the implementation of a new command class
	virtual void code() =0;

	//!If false is returned an error is returned by the command and the code() is not executed. to be filled during the implementation of a new command class
	virtual bool preCondition();


protected:
	xdata::Serializable* payload_;

	std::string newopid_;

	std::string cid_;

private:

  std::map<std::string,xdata::Serializable*> paramList_;

  CellWarning* warning_;

private:

	bool job ( toolbox::task::WorkLoop* wl );

	void commandCode();

	xoap::MessageReference runAsync();

	xoap::MessageReference runSync();

	xoap::MessageReference insertPayload();

	void setParameterMap ( std::map<std::string, xdata::Serializable*>& param );


private:

	std::string sid_;

	xoap::MessageReference msg_;

	toolbox::task::WorkLoop* pipe_;

	toolbox::task::ActionSignature* job_;

	bool async_;

	std::string url_;

	std::string urn_;

	std::string fun_;

	std::string localUrl_;

	volatile bool isover_;

	toolbox::squeue<xoap::MessageReference*> reply_;

	toolbox::squeue<xdata::Serializable*> result_;

	std::string pipeName_;


private:
  //!class to handle unexpected exceptions
	class UnexpectedHandler
	{
	public:
		UnexpectedHandler()
		{
			old = std::set_unexpected ( handler );
		}

		~UnexpectedHandler()
		{
			std::set_unexpected ( old );
		}

		static void handler()
		{
			std::string msg;
			msg = "Unexpected exception while executing a command. The exception stack is lost";
			XCEPT_RAISE ( tsexception::CommandExecutionError,msg );
		}

	private:
		std::unexpected_handler old;
  };


};
}//ns tsframework
#endif
