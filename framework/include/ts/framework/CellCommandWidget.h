/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Project Manager: Ildfons Magrans de Abril      				         *
 * Authors: Marc Magrans de Abril               				         *
 *************************************************************************/

#ifndef _ts_framework_CellCommandWidget_h_
#define _ts_framework_CellCommandWidget_h_

#include "CellPanel.h"

#include <string>
#include <iostream>
#include <map>

namespace cgicc
{
	class Cgicc;
}


namespace ajax
{
	class ResultBox;
	class Timer;
}


namespace log4cplus
{
	class Logger;
}

namespace tsframework
{

class CellAbstractContext;
class CellCommand;
class CellLocalSession;

class CellCommandWidget : public CellPanel
{
public:

	CellCommandWidget ( CellAbstractContext* context, log4cplus::Logger& logger );

	virtual ~CellCommandWidget();

	void init ( const std::string& commandName );

	std::string getCommandId();

  	std::string getCommandName();

private:

	void defaultPage ( cgicc::Cgicc& cgi,std::ostream& out );

	void execute ( cgicc::Cgicc& cgi,std::ostream& out );

	void refresh ( cgicc::Cgicc& cgi,std::ostream& out );

    void handleSubmit ( cgicc::Cgicc& cgi,std::ostream& out );
	void layout ( cgicc::Cgicc& cgi );

	void fillResult ( cgicc::Cgicc& cgi,std::ostream& out );

	CellLocalSession* getSession ( cgicc::Cgicc& cgi );

	void showAlert ( std::ostream& out, const std::string& msg );

	std::string string2alert ( const std::string& in );

	std::string replace ( const std::string& in,const std::string& from,const std::string& to );

	std::string htmlEncoding ( const std::string& str );

	std::string createCommandId();

	std::string getBaseId();

private:

	std::map<std::string, std::string> parameterMap_;

	CellCommand* command_;

	std::string commandid_;

	std::string commandName_;

	bool justExecuted_;

	bool autoChkValue_;

	ajax::ResultBox* result_;

	ajax::ResultBox* resultTimer_;


};
}
#endif
