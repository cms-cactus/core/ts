/*************************************************************************
 * Trigger Supervisor Component                                          *
 *                                                                       *
 * Coordinator: Ildefons Magrans de Abril                                *
 *                                                                       *
 * Responsible: Marc Magrans de Abril                                    *
 *************************************************************************/

#ifndef _ts_framework_CellXhannelRequestMonitor_h_
#define _ts_framework_CellXhannelRequestMonitor_h_

#include "ts/framework/CellXhannelRequest.h"


namespace xdaq
{
class ApplicationDescriptor;
}


namespace xdata
{
class Table; 
}


namespace tsframework
{

class CellXhannelMonitor;

class CellXhannelRequestMonitor : public CellXhannelRequest
{
    friend class CellXhannelMonitor;

public:

    CellXhannelRequestMonitor ( log4cplus::Logger& log, CellAbstractContext* context, const xdaq::ApplicationDescriptor* target );

    virtual ~CellXhannelRequestMonitor();

    void doRetrieveCatalog();

    std::vector<std::string> retrieveCatalogReply();

    void doRetrieveCollection ( const std::string& flashlist );

    xdata::Table* retrieveCollectionReply();

    void doRetrieveDefinition ( const std::string& flashlist );

    std::map<std::string,std::string> retrieveDefinitionReply();

    void doRetrieveExdr ( const std::string& flashlist );

    std::map<std::string,xdata::Table*> retrieveExdrReply();

private:

    std::string getCurlQuery();

    void setCurlReply ( const std::string& curlReply_ );

    std::string getCurlReply();

    void getSubstrings ( const std::string& str,const std::string& separators,std::vector<std::string>& substrings );

    bool isSeparator ( char c, const std::string& separators );

    std::string curlQuery_;

    std::string curlReply_;

    bool isRetrieveExdr_;

    char* buffer_;

    size_t size_;

    const xdaq::ApplicationDescriptor* target_;

    std::string getTargetUrl();
    
    std::string getTargetUrn();
};
}
#endif
