#ifndef _ts_framework_CellOperationWidget_h_
#define _ts_framework_CellOperationWidget_h_

#include "ts/framework/CellPanel.h"
#include "ajax/PolymerElement.h"
#include "ajax/toolbox.h"

#include <vector>
#include <iostream>
#include <map>

namespace ajax
{
	class ResultBox;
}

namespace cgicc
{
	class Cgicc;
}

namespace log4cplus
{
	class Logger;
}

namespace tsframework
{

class CellAbstractContext;
class CellOperation;
class CellLocalSession;


class CellOperationWidget : public CellPanel
{
public:

	CellOperationWidget ( CellAbstractContext* context, log4cplus::Logger& logger );

	~CellOperationWidget();

  void layout ( cgicc::Cgicc& cgi );

  void init ( const std::string& operationId );

  std::string getOperationId();

  CellOperation& getOperation();

private:

	void formExecute ( cgicc::Cgicc& cgi,std::ostream& out );

	void reset ( cgicc::Cgicc& cgi,std::ostream& out );

	void fillResult ( cgicc::Cgicc& cgi,std::ostream& out );

	void fillOperationGraph ( cgicc::Cgicc& cgi,std::ostream& out );

	void getStatus ( cgicc::Cgicc& cgi,std::ostream& out );

	void getCommands ( cgicc::Cgicc& cgi,std::ostream& out );

	void getInputs ( cgicc::Cgicc& cgi,std::ostream& out );

	std::vector<std::string> getPossibleCommands();

	CellLocalSession* getSession ( cgicc::Cgicc& cgi );

private:

	std::map<std::string, std::string> parameterMap_;

	std::string operationId_;

	std::string commandId_;

	typedef void ( CellOperationWidget::*Method ) ( cgicc::Cgicc&,std::ostream& );

};
}
#endif
