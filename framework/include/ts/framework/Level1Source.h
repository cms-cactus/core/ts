#ifndef _ts_framework_Level1Source_h_
#define _ts_framework_Level1Source_h_

#include "ts/framework/PeriodicSource.h"

#include "ts/exception/CellException.h"

#include "toolbox/ProcessInfo.h"

namespace log4cplus
{
    class Logger;
}

//!Contains the defintion of monitorable items to monitor all the nodes of the L1 cluster.
namespace tsframework
{
class CellAbstractContext;

class Level1Source : private tsframework::PeriodicSource
{
private:

    //!Interval between consecutive refreshes of the monitoring information
    static const int REFRESH_INTERVAL_SEC = 10;

public:

    //!The different possible states of the hardware status
    enum SeverityLevel {ERROR=3000, WARNING=2000, INFO=1000};


private:

    struct Alarm
    {
        SeverityLevel severity;
        std::string details;
    };

    struct Rate
    {
        double value;
        SeverityLevel severity;
        std::string details;
    };



    class CallbackSignature
    {
    public:

        virtual ~CallbackSignature()
        {
            ;
        };

        virtual void invoke() = 0;
    };


    template <class OBJECT> 
    class Callback : public CallbackSignature
    {
    public:

      Callback ( OBJECT* object, void ( OBJECT::*func ) () ) 
      : 
      obj_ ( object ), 
      func_ ( func ) 
      {};

      void invoke()
      {
          if ( func_ != 0 )
          {
              ( obj_->*func_ ) ();
          }
          else
          { 
              XCEPT_RAISE ( tsexception::NullPointer, "Null pointer passed to Callback::invoke" );
          }
      }

    private:

        OBJECT* obj_;
      
        void ( OBJECT::*func_ ) ();
    };

  private:

    CallbackSignature* callback_;

  public:

    Level1Source ( tsframework::CellAbstractContext* context, log4cplus::Logger& logger );

  public:

    //!If true the alarms are cleared after publishing, else they are kept and should be cleared manually callint clear()
    void setAutoClear ( bool autoclear );

    //!Sets the subsystem name
    void setSubsystem ( const std::string& subsystem );

    //!Gets the subsystem name
    std::string getSubsystem();

    //!clears hardware status and rate counters data
    void clear();

    //Sets the hardware status for a given module
    void setAlarm ( const std::string& name, SeverityLevel severity, const std::string& details="" );

    //!Sets the rate (Hz) for a given input or output module
    void setRate ( const std::string& name, double value, SeverityLevel severity = INFO, const std::string& details="" );

    //!Add a callback that will be executed periodically
    template <class T> 
    void setPeriodicAction ( T* object,void ( T::*func ) () )
    {
        if ( object )
        {
            callback_ = new Callback<T> ( object,func );
        }
        else
        {
            XCEPT_RAISE ( tsexception::NullPointer, "Trying to use a NULL pointer in Level1Source::setCallback" );
        }
    }

private:

    virtual void periodicAction();

    void updateProcessInfo();

    void updateOperationStatus();

    void updateAlarms();

    void updateRates();

private:

    static std::string severityLevel2String ( SeverityLevel l );

    static std::string jsonEscape ( const std::string& result );

    static void replace ( std::string& str,const std::string& from, const std::string& to );

private:

    toolbox::ProcessInfo::Reference processInfo_;

    std::map<std::string,Alarm> alarms_;

    std::map<std::string,Rate> rates_;

    std::string subsystem_;

    bool autoclear_;

};
}
#endif
