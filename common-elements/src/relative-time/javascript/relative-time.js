Polymer({
    is: 'relative-time',
    properties: {
        /**
        * The date string of the format ["YYYY/MM/D-HH:mm::ss", "MMM D YYYY HH:mm::ss", "ddd MMM DD YYYY HH:mm:ss ZZ"]
        */
        date: {
            type: Object,
            value: "unknown",
            observer: '_dateChanged'
        },
        _dateMoment: Object,
        _dateRelative: {
            type: String,
            value: ""
        },
        /**
        * if set, overrides moment.js for the first 60s and gives a more accurate result (moment.js would say "a few seconds ago")
        */
        precise: {
          type: Boolean,
          value: false
        },
        _timer: Object
    },
    _dateChanged: function() {
      window.clearTimeout(this._timer);
      this.set('_dateRelative', this._getRelative(this.date));
      this._setInterval();
    },
    _getRelative: function(date) {
      if (date == "unknown") {
        this._dateMoment = null;
        return date;
      } else {
          this._dateMoment = moment(date, ["YYYY/MM/D-HH:mm::ss", "MMM D YYYY HH:mm::ss", "ddd MMM DD YYYY HH:mm:ss ZZ"]);
          if (this._dateMoment.isValid()) {
            if (this.precise) {
              var now = moment();
              var diff = now.diff(this._dateMoment, 'seconds');
              if (diff < 60) {
                return diff + "s ago";
              } else {
                return this._dateMoment.fromNow();
              }
            } else {
              return this._dateMoment.fromNow();
            }
          } else {
            this._dateMoment = null;
            return date;
          }
      }
    },
    _setInterval: function() {
      var interval = this._getInterval(this._dateMoment);
      if (interval > 0) {
        this._timer = window.setTimeout(this._dateChanged.bind(this), interval);
      }
    },
    _getInterval: function() {
      if (!(this._dateMoment)) {
        return 0;
      } else {
        var now = moment();
        if (now.diff(this._dateMoment, 'seconds') < 60) {
          return 1000;
        } else {
          return 10000;
        }
      }
    },
    detached: function() {
      window.clearTimeout(this._timer);
    }
});
