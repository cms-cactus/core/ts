Polymer({
    is: 'ts-tree',
    properties: {
      tree: {
        type: Object,
        observer: '_treeChanged'
      },
      _recursionLevel: {
        type: Number,
        value: 0
      },
      /**
       * This will hide this tree, used to collapse submenus
       */
      closed: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },
      /**
       * Contains the currently selected value
       */
      selected: {
        type: String,
        value: "",
        readOnly: true,
        notify: false,
        reflectToAttribute: true
      }
    },

    _treeChanged: function() {
      var root = Polymer.dom(this.$.container);
      root.innerHTML = "";

      for (var i = 0; i < this.tree.length; i++) {
        var currenttree = this.tree[i];
        var hasSubtree = currenttree.tree && true;

        var title = document.createElement('p');

        if (this._recursionLevel > 0) {
          var svg = document.createElement('tree-empty');
          title.appendChild(svg);
        }

        for (var y = 1; y < this._recursionLevel; y++) {
          var svg = document.createElement('tree-l-line');
          title.appendChild(svg);
        }

        if (hasSubtree) {
          var button = document.createElement('tree-button');
          button.addEventListener('click', this._submenuTitleClicked);
          title.appendChild(button);
        } else {
            if (i < this.tree.length - 1) {
              var svg = document.createElement('tree-t-line');
              title.appendChild(svg);
            } else {
              var svg = document.createElement('tree-corner');
              title.appendChild(svg);
            }
        }

        var titleText = document.createElement('span');
        titleText.appendChild(document.createTextNode(currenttree.name));
        title.addEventListener('click', this._itemClicked.bind(this));
        title.appendChild(titleText);
        root.appendChild(title);

        if (hasSubtree) {
          // title.addEventListener('click', this._submenuTitleClicked);
          var subtree = document.createElement('ts-tree');
          subtree.addEventListener('selected-changed', function(e) {
            this._setSelected(e.detail.value);
            this.fire('selected-changed', e.detail.value);
          }.bind(this))
          subtree._recursionLevel = this._recursionLevel + 1;
          subtree.tree = currenttree.tree;
          root.appendChild(subtree);
        }
      }
    },

    _submenuTitleClicked: function() {
      var submenu = this.parentElement.nextSibling;
      submenu.toggle();
      this.closed = submenu.closed;
    },
    toggle: function() {
      this.closed = !this.closed;
    },
    _itemClicked: function(e) {
      this._setSelected(e.target.textContent);
      this.fire('selected-changed', {value: e.target.textContent});
    }
});
