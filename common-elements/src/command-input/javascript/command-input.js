Polymer({
  is: "command-input",
  properties: {
    name: String,
    value: {
      type: Object,
      notify: true
    },
    type: String,
    invalid: {
      type: Boolean,
      value: false,
      notify: true
    },
    alwaysFloatLabel: {
      type: Boolean,
      value: false
    }
  },
  _isBoolean: function(type) {
    return type.toLowerCase() == "boolean" || type.toLowerCase() == "bool";
  },
  _toBool: function(input) {
    if (typeof input === "string") {
      return input.toLowerCase() == "true" || input == "1";
    } else {
      return !!(input);
    }
  },
  _toLabel: function() {
    return this.name + " (" + this.type +")";
  },
  _toType: function() {
    switch (this.type) {
      case "number":
      case "int":
      case "long":
      case "unsigned int":
      case "unsigned long":
      case "short":
      case "unsigned short":
        return "number";
        break;
      case "string":
      case "double":
      case "float":
        return "text";
      default:
        console.warn("command-input: unknown input type '" +  this.type + "'. reverting to text input");
        return "text";
    }
  },
  _getValidationPattern: function() {
      switch(this.type) {
          // case "bool":
          //     return "^(0|1|true|false|True|False)$";
          //     break;
          case "unsigned int":
          case "unsigned long":
          case "unsigned short":
            return "^\d+$";
            break;
          case "int":
          case "long":
          case "short":
            return /^[-+]?[0-9]*$/.toString().slice(0,-1).substring(1);
            break;
          case "double":
          case "float":
            return "^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$";
            break;
          case "string":
            return ".*";
            break;
          default:
            console.warn("<ts-command>: variable type '" + this.type + "' is unknown, consider developing a validation pattern for it");
      }
  },
  _getInputPattern: function() {
      switch(this.type) {
        case "unsigned int":
        case "unsigned long":
        case "unsigned short":
          return "[0-9]";
          break;
        case "int":
        case "long":
        case "short":
          return "[0-9-]";
          break;
        case "double":
        case "float":
          return "[-+0-9eE\.]";
          break;
        case "string":
          return ".";
          break;
        default:
          console.warn("<ts-command>: variable type '" + this.type + "' is unknown, consider developing an input pattern for it");
      }
  },
  _getMin: function() {
    if (this.type.indexOf("unsigned") > -1) {
      return 0;
    } else {
      return undefined;
    }
  },
  _bleh: function() {
    this.fire('change');
  },
  _updateBool: function(e) {
    var checkbox = this.$$('paper-checkbox');
    this.value = checkbox.checked;
  }
});
