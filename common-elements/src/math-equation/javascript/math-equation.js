Polymer({
    is: 'math-equation',
    properties: {

      /**
       * If true the math will be rendered in display mode,
       * which will put the math in display style (so \int and \sum are large,
       * for example), and will center the math on the page on its own line.
       * If false the math will be rendered in inline mode.
       */
      big: {
        type: Boolean,
        value: false
      },

      /**
       * Use this to make the element inline
       */
      inline: {
        type: Boolean,
        value: false
      }

    },

    observers: ['render(inline)'],

    render: function() {
      Polymer.dom(this.$.result).innerHTML = katex.renderToString(Polymer.dom(this).textContent, { displayMode: this.big });
    },

    attached: function() {
      this.render();
      var observer = new MutationObserver(function(mutations) {
        // thanks firefox
        if (mutations[0].target.nodeName != "SPAN") {
          this.render();
        }
      }.bind(this));
      observer.observe(this, {
	      childList: true,
      	characterData: true,
        subtree: true
      });
    },
});
