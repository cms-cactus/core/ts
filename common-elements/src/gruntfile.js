require('es6-shim');
module.exports = function(grunt) {
    grunt.initConfig({
        /*
          Compile SASS code to CSS
         */
        sass: {
            options: {
                sourcemap: 'none',
                // style: 'expanded',
                outputStyle: 'compressed',
                noCache: true
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '',
                    src: [
                        './**/*.scss',
                        '!./**/bower_components/**/*.scss',
                        '!./**/node_modules/**/*.scss'
                    ],
                    dest: '',
                    ext: '-min.css'
                }]
            }
        },
        /*
          Add css prefixes for compatibility (mainly for Firefox ESL)
         */
        postcss: {
            options: {
                map: false,
                processors: [
                    require('autoprefixer')({
                        browsers: ['firefox 24', 'IE 10', 'last 2 versions']
                    })
                ]
            },
            dist: {
                src: [
                  './**/*-min.css',
                  '!./**/bower_components/**/*.scss',
                  '!./**/node_modules/**/*.scss'
                ]
            }
        },
        /*
          Process JavaScript
         */
        uglify: {
            options: {
                preserveComments: true,
                srewIE8: true,
                sourceMap: true
            },
            /*
              Compile JavaScript on Polymer Elements
             */
            polymerjs: {
                options: {
                    mangle: false,
                    sourceMap: false
                },
                files: [{
                    expand: true,
                    src: [
                      './**/*.js',
                      '!./**/bower_components/**/*.js',
                      '!./**/node_modules/**/*.js'
                    ],
                    ext: '-min.js'
                }]
            }
        },

        inline: {

          dist: {
            files: [{
              expand: true,
              src: [
                  './**/*.html',
                  '!./**/bower_components/**/*.html',
                  '!./**/node_modules/**/*.html'
              ],
              dest: '../common-elements/',
              ext: '.html'
            }]
          }
      	},

        clean: {
            cssfiles: {
                options: {
                    'no-write': false
                },
                src: [
                  "./**/*-min.css",
                  '!./**/bower_components/**/*-min.css',
                  '!./**/node_modules/**/*-min.css'
                ]
            },
            jsfiles: {
                options: {
                    'no-write': false
                },
                src: [
                  "./**/*-min.js",
                  '!./**/bower_components/**/*-min.js',
                  '!./**/node_modules/**/*-min.js'
                ]
            }
        },

        execute: {
          main: {
            src: ['makeIndex.js'],
            options: {
              cwd: "./"
            }
          },
          charts: {
            src: ['charts/makeIndex.js'],
            options: {
              cwd: "charts/"
            }
          }
        }
    });

    grunt.loadNpmTasks('grunt-execute');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-inline');

    grunt.registerTask('default', ['execute', 'sass', 'postcss', 'uglify', 'inline', 'clean']);
};
