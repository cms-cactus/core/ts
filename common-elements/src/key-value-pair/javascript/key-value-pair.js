Polymer({
    is: "key-value-pair",

    properties: {
      /**
      * The key string
      */
      key: String,
      /**
      * The value string
      */
      value: String,
      /**
      * Set this to true if the value is a date string, it will render it as a relative time
      */
      isdate: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      }
    }
});
