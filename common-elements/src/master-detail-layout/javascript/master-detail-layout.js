Polymer({
    is: 'master-detail-layout',
    behaviors: [responsiveBehavior],
    properties: {
      /**
       * Which view is active in case of small device. 0 for master, 1 for detail.
       */
      smallSelected: {
        type: Number,
        value: 0,
        observer: '_smallSelectedChanged'
      },

      masterSelector: {
        type: String,
        value: '[master]'
      },
      detailSelector: {
        type: String,
        value: '[detail]'
      },

      largestSmall: {
        type: String,
        value: 'small'
      },

      // for shady dom patch
      masterElement: Object,
      detailElement: Object
    },
    observers: ['_responsiveChanged(responsiveMode)'],

    _responsiveChanged: function(responsiveMode) {
      if (this['lte_' + this.largestSmall]) {
        this._moveStuff(this.$.smallcontainer);
        this.$.largecontainer.setAttribute('hide', 'hide');
        this.$.smallcontainer.removeAttribute('hide');
      } else {
        this._moveStuff(this.$.largecontainer);
        this.$.smallcontainer.setAttribute('hide', 'hide');
        this.$.largecontainer.removeAttribute('hide');
      }
    },
    /**
     * Move the master and detail view to the specified container
     */
    _moveStuff: function(container) {
      Polymer.dom(container).appendChild(this.$.mastercontainer);
      Polymer.dom(container).appendChild(this.$.detailcontainer);
      // if shady dom, manually reinsert master and detail in its containers
      // sometimes they get lost, and they need to be shown the right path in order to step into the light once again
      if ( !(Polymer.Settings.useNativeShadow) && this.masterElement ) {
        this.$.mastercontainer.appendChild(this.masterElement);
        this.$.detailcontainer.appendChild(this.detailElement);
      }
    },

    attached: function() {
      // if shady dom, manually insert master and detail element
      if ( !(Polymer.Settings.useNativeShadow) ) {
        this.masterElement = Polymer.dom(this).querySelector(this.masterSelector);
        this.$.mastercontainer.appendChild(this.masterElement);
        this.detailElement = Polymer.dom(this).querySelector(this.detailSelector);
        this.$.detailcontainer.appendChild(this.detailElement);
      }
    },

    /**
     * Toggle between master and detail view, only works on small screens
     */
    toggle: function() {
      if (this.smallSelected == 0) {
        this.smallSelected = 1;
      } else {
        this.smallSelected = 0;
      }
    },

    /**
     * Fires on small screens when master/detail view is switched
     */
    _smallSelectedChanged: function() {
      if(this.smallSelected==0) {
        this.entryanimation = 'slide-from-right-animation';
        this.exitanimation = 'slide-left-animation';
      } else {
        this.entryanimation = 'slide-from-left-animation';
        this.exitanimation = 'slide-right-animation';
      }
    }
});
