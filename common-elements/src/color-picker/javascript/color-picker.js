Polymer({
    is: 'color-picker',
    properties: {
        color: {
            type: String,
            value: "",
            notify: true
        }
    },
    ready: function() {
        //test support for color input
        //at this stage the color property is not yet bound, so the input has the default value
        //if the browser supports color input, the default value is "#000000"
        //if the browser does not support it, the default value is ""
        this._loadPolyfill = this.$.input.value.charAt(0) !== "#";
    },
    attached: function() {
        if (this._loadPolyfill) {
            console.log("loading color picker polyfill");
            var css = document.createElement("link");
            css.href = "/extern/bower_components/spectrum/spectrum.css";
            css.rel = "stylesheet";
            this.appendChild(css);
            // console.log(this.color);
            $(this.$.input).spectrum({
                showPalette: true,
                showInput: true,
                localStorageKey: "spectrum.history",
                showInitial: true,
                preferredFormat: "rgb",
                move: function(color) {
                    this.parentNode.color = color.toHexString();
                },
                hide: function(color) {
                    this.parentNode.color = color.toHexString();
                }
            });
        }
    }
});
