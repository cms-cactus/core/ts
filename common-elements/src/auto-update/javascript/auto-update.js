Polymer({
    is: 'auto-update',
    /**
     * Fired when a request is sent.
     *
     * @event request
     */
    /**
     * Fired when a response is received.
     *
     * @event response
     */
    /**
     * Fired when an error is received.
     *
     * @event error
     */
    behaviors: [throwsToast],
    properties: {
      /**
       * The data you wish to update
       */
      data: {
        type: Object,
        //we notify manually, if the response data would be identical the normal notify would not fire
        notify: false
      },
      /**
       * The format of the callback data. Will probably be either json or text
       */
      handleAs: {
        type: String,
        value: 'json'
      },
      /**
       * The interval at which the data must be updated
       */
      interval: {
        type: Number,
        value: 5000,
        observer: '_intervalChanged',
        notify: true
      },
      /**
       * The name of the c++ callback you made with setEvent("name", ajax::Eventable::OnTime, this, &function);
       */
      callback: {
        type: String,
        value: "auto-update id not set"
      },
      /**
       * The timeout after which an update attempt must abort
       */
      timeout: {
        type: Number,
        value: 10000
      },
      /**
       * This disables the auto-update.
       * Note that the auto-update element will always do one refresh at the time it is created, regardless of the value of noRefresh.
       */
      noRefresh: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        observer: '_noRefreshChanged'
      },
      /**
       * Names of the parameter variables to send along with the request
       */
      parameters: {
        type: Array,
        value: function() {return [];}
      },
      /**
       * Javascript date string representing the last time an update was successful
       */
      lastUpdated: {
        type: String,
        value: "never"
      },
      /**
       * Removes the UI of this element
       */
      headless: {
        type: Boolean,
        value: false
      },
      /**
       * Only applicable if headless=false, this will prevent the user to specify the update interval via the UI
       */
      noSlider: {
        type: Boolean,
        value: false
      }
    },
    /**
     * AJAX response handler
     */
    _onResponse: function(event, detail, sender) {
      if (typeof detail.response == "string" && detail.response.indexOf("<!doctype") == 0) {
        console.error("auto refresh got back main page, your app is out of sync", detail.response.length);
        this._removeTimer();
        this.set('noRefresh', true);
      } else if(typeof detail.response == "string" && detail.response.length == 0) {
        console.warn(this, "request " + this.callback + " is unknown by the server. Did you make a typo?");
        this.throwToast({
          'type': 'warning',
          'message': "Server doesn't understand this auto-update request, setting T=0 to not spam logs"
        });
        this.set('interval', 0);
      } else {
        this.set('lastUpdated', Date());
        this.set('data', detail.response);
        // manual notify
        this.fire('data-changed', this.data);
        this.fire('response', detail, {bubbles: false});
        this.setupNextRequest();
      }
    },
    /**
     * AJAX error handler
     */
    _onError: function(event, detail, sender) {
      console.error('error in auto-update', event, detail, sender);
      this.throwToast({
        'type': 'error',
        'message': 'Something is wrong with this auto-refresh, setting T=0 to not spam logs'
      });
      // this.setupNextRequest();
      this.set('interval', 0);
      this.fire('error', detail, {bubbles: false});
    },

    /**
     * Resets the interval timer, blocked when noRefresh=true
     */
    setupNextRequest: function() {
      if (this.timer != -1) {
        this._removeTimer();
        if (!(this.noRefresh) && this.interval > 0) {
          this.timer = window.setTimeout(this.generateRequest.bind(this), this.interval);

          //animate the green bar
          this.async(function() {
            this.$.progressbar.style.animationDuration = this.interval + "ms";
            this.$.progressbar.style.animationName = "";
          }, 100);
        }
      } else {
        console.error("manually destroying auto-update, you have a memory leak!");
      }
    },

    /**
     * Equivalent to clicking the `refresh` button
     * @deprecated use generateRequest() instead
     */
    manualRefresh: function() {
      this.generateRequest();
    },

    /**
     * Forces auto-update to make an AJAX request, consider using manualRefresh() instead
     */
    generateRequest: function() {
      if (this.callback) {
        var params = {
          "_eventType_": "OnTime",
          "_id_": this.callback
        };
        if (this.parameters) {
          for (var i = 0; i < this.parameters.length; i++) {
            var currentParameter = this.parameters[i];
            params[currentParameter] = this[currentParameter];
          }
        }
        this.$.ajax.body = this._encodeParams(params);
        this.$.ajax.handleAs = this.handleAs;
        this.$.ajax.timeout = this.timeout;
        this._removeTimer();
        this.$.ajax.generateRequest();
        this.fire('request', this.$.ajax.lastRequest, {bubbles: false});
      }
    },

    _encodeParams: function(params) {
      var result = "";
      for (var key in params) {
          if (result != "") {
              result += "&";
          }
          result += key + "=" + encodeURIComponent(params[key]);
      }
      return result;
    },

    _noRefreshChanged: function(newval, oldval) {
      //do not run at initialisation or when noRefresh=true
      if (oldval != undefined && !(newval)) {
        this.generateRequest();
      } else {
        this._removeTimer();
      }
    },
    _intervalChanged: function(newval, oldval) {
      if (oldval != undefined) {
        if (this.interval > 0) {
          this.generateRequest();
        } else {
          this._removeTimer();
        }
      }
    },

    attached: function() {
      this.$.ajax.url = window.location.origin + window.location.pathname;
      this.generateRequest();
    },
    detached: function() {
      this._removeTimer();
      // in case of a memory leak, this object keeps existing after detaching
      // removing the timer will not stop the auto-update in this case
      this.timer = -1;
    },
    _removeTimer: function() {
      window.clearTimeout(this.timer);

      //set the green bar to full again
      this.$.progressbar.style.animationName = 'none';
    }
});
