/**
 * Use `responsiveBehavior` to equip an element with responsive breakpoints.
 * It implements the breakpoints from [material design](http://www.google.be/design/spec/layout/responsive-ui.html#responsive-ui-breakpoints)
 * @polymerBehavior responsiveBehavior
 */
var responsiveBehavior = {
  properties: {
    /**
     * true when the screen width is ]0-600px]
     */
    extraSmall: {
      type: Boolean,
      reflectToAttribute: true,
      notify: true
    },
    /**
     * true when the screen width is ]600-960px]
     */
    small: {
      type: Boolean,
      reflectToAttribute: true,
      notify: true
    },
    /**
     * true when the screen width is ]960-1280px]
     */
    medium: {
      type: Boolean,
      reflectToAttribute: true,
      notify: true
    },
    /**
     * true when the screen width is ]1280-1920px]
     */
    large: {
      type: Boolean,
      reflectToAttribute: true,
      notify: true
    },
    /**
     * true when the screen width is ]1920px-∞]
     */
    extraLarge: {
      type: Boolean,
      reflectToAttribute: true,
      notify: true
    },

    /**
     * the current screen size category as a String
     * possible values: extraSmall, small, medium, large, extraLarge
     */
    responsiveMode: {
      type: String,
      notify: true
    },

    /**
     * true when the screen width is ]0-600px]
     */
    lte_extraSmall: {
      type: Boolean,
      notify: true,
      computed: '_lte_extraSmall(responsiveMode)'
    },

    /**
     * true when the screen width is ]0-960px]
     */
    lte_small: {
      type: Boolean,
      notify: true,
      computed: '_lte_small(responsiveMode)'
    },

    /**
     * true when the screen width is ]0-1280px]
     */
    lte_medium: {
      type: Boolean,
      notify: true,
      computed: '_lte_medium(responsiveMode)'
    },

    /**
     * true when the screen width is ]0-1920px]
     */
    lte_large: {
      type: Boolean,
      notify: true,
      computed: '_lte_large(responsiveMode)'
    },
    /**
     * use this when idiot
     */
    lte_extraLarge: {
      type: Boolean,
      value: true,
      notify: true,
      readonly: true
    },
    /**
     * use this when idiot
     */
    gte_extraSmall: {
      type: Boolean,
      value: true,
      notify: true,
      readonly: true
    },
    /**
     * true when the screen width is ]600px-∞]
     */
    gte_small: {
      type: Boolean,
      notify: true,
      computed: '_gte_small(responsiveMode)'
    },
    /**
     * true when the screen width is ]960px-∞]
     */
    gte_medium: {
      type: Boolean,
      notify: true,
      computed: '_gte_medium(responsiveMode)'
    },
    /**
     * true when the screen width is ]1280px-∞]
     */
    gte_large: {
      type: Boolean,
      notify: true,
      computed: '_gte_large(responsiveMode)'
    },
    /**
     * true when the screen width is ]1920px-∞]
     */
    gte_extraLarge: {
      type: Boolean,
      notify: true,
      computed: '_gte_extraLarge(responsiveMode)'
    }



  },
  ready: function() {

    this.set("responsiveMode", "extraSmall");

    var modes = [
      {
        name: "extraSmall",
        query: "(max-width: 599px)"
      },
      {
        name: "small",
        query: "(min-width: 600px) and (max-width: 959px)"
      },
      {
        name: "medium",
        query: "(min-width: 960px) and (max-width: 1279px)"
      },
      {
        name: "large",
        query: "(min-width: 1280px) and (max-width: 1919px)"
      },
      {
        name: "extraLarge",
        query: "(min-width: 1920px)"
      }
    ];

    for (var i = 0; i < modes.length; i++) {
      var breakpoint = document.createElement('iron-media-query');
      breakpoint.query = modes[i].query;
      breakpoint.id = modes[i].name;
      breakpoint.addEventListener('query-matches-changed', this._queryMatchesChanged.bind(breakpoint));

      Polymer.dom(this.root.appendChild(breakpoint));
      this._setMode(modes[i].name, breakpoint.queryMatches);
      // break possible circular references
      breakpoint = null;
    }
  },
  _queryMatchesChanged: function(e) {
    this.parentNode.host._setMode(this.id, e.detail.value);
  },

  /**
   * observers do not seem to work in behaviors, do it manually
   */
  _setMode: function(mode, active) {
    this.set(mode, active);

    if (active) {
      this.set('responsiveMode', mode);
    }
  },

  /**
   * true when the screen width is ]0-600px]
   */
  _lte_extraSmall: function() {
    return this.extraSmall;
  },

  /**
   * true when the screen width is ]0-960px]
   */
  _lte_small: function() {
    return this.small || this.extraSmall;
  },

  /**
   * true when the screen width is ]0-1280px]
   */
  _lte_medium: function() {
    return this.extraSmall || this.small || this.medium;
  },

  /**
   * true when the screen width is ]0-1920px]
   */
  _lte_large: function() {
    return this.extraSmall || this.small || this.medium || this.large;
  },
  /**
   * use this when idiot
   */
  _lte_extraLarge: function() {
    // return this.extraSmall || this.small || this.medium || this.large || this.extraLarge
    return true;
  },
  /**
   * use this when idiot
   */
  _gte_extraSmall: function() {
    // return this.extraSmall || this.small || this.medium || this.large || this.extraLarge
    return true;
  },
  /**
   * true when the screen width is ]600px-∞]
   */
  _gte_small: function() {
    return this.small || this.medium || this.large || this.extraLarge;
  },
  /**
   * true when the screen width is ]960px-∞]
   */
  _gte_medium: function() {
    return this.medium || this.large || this.extraLarge;
  },
  /**
   * true when the screen width is ]1280px-∞]
   */
  _gte_large: function() {
    return this.large || this.extraLarge;
  },
  /**
   * true when the screen width is ]1920px-∞]
   */
  _gte_extraLarge: function() {
    return this.extraLarge;
  }
};
