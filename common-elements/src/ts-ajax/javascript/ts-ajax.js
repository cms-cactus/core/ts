Polymer({
    is: 'ts-ajax',
    /**
     * Fired when a request is sent.
     *
     * @event request
     */
    /**
     * Fired when a response is received.
     *
     * @event response
     */
    /**
     * Fired when an error is received.
     *
     * @event error
     */
    behaviors: [throwsToast],
    properties: {
      /**
       * The data you wish to put the result of the ajax call into
       */
      data: {
        type: Object,
        //we notify manually, if the response data would be identical the normal notify would not fire
        notify: false
      },
      /**
       * The format of the callback data. Will probably be either json or text
       */
      handleAs: {
        type: String,
        value: 'json'
      },
      /**
       * The name of the c++ callback you made with setEvent("name", ajax::Eventable::OnClick, this, &function);
       */
      callback: {
        type: String,
        value: "ts-ajax id not set"
      },
      /**
       * The timeout after which an update attempt must abort
       */
      timeout: {
        type: Number,
        value: 10000
      },
      /**
       * Names of the parameter variables to send along with the request
       * This is just a list of names, set the values like this:
       * this.push('parameters', 'myvariable');
       * this.myvariable = 5;
       */
      parameters: {
        type: Array,
        value: function() {return [];}
      },
      /**
       * Javascript date string representing the last time an update was successful
       */
      lastUpdated: {
        type: String,
        value: "never"
      },
      /**
       * if set, ts-ajax will make a request immediately
       */
      auto: {
        type: Boolean,
        value: false,
        observer: "_autoChanged"
      }
    },
    /**
     * AJAX response handler
     */
    _onResponse: function(event, detail, sender) {
      if (typeof detail.response == "string" && detail.response.indexOf("<!doctype") == 0) {
        console.error("ts-ajax request got back main page, your app is out of sync", detail.response.length);
      } else if(typeof detail.response == "string" && detail.response.length == 0) {
        console.warn(this, "request " + this.callback + " is unknown by the server. Did you make a typo?");
        this.throwToast({
          'type': 'warning',
          'message': "Server doesn't understand this ts-ajax request"
        });
      } else {
        this.set('lastUpdated', Date());
        this.set('data', detail.response);
        // manual notify
        this.fire('data-changed', this.data);
        this.fire('response', detail, {bubbles: false});
      }
    },
    /**
     * AJAX error handler
     */
    _onError: function(event, detail, sender) {
      console.error('error in ts-ajax', event, detail, sender);
      this.throwToast({
        'type': 'error',
        'message': 'Something is wrong with this ajax request'
      });
      this.fire('error', detail, {bubbles: false});
    },

    /**
     * fire a new ajax request
     */
    generateRequest: function() {
      var params = {
        "_eventType_": "OnClick",
        "_id_": this.callback
      };
      if (this.parameters) {
        for (var i = 0; i < this.parameters.length; i++) {
          var paramName = this.parameters[i];
          // if (!this[paramName]) {
          //   console.warn(this, paramName + " is in the list of parameters but is not set");
          // }
          params[paramName] = this[paramName];
        }
      }
      this.$.ajax.body = this._encodeParams(params);
      this.$.ajax.handleAs = this.handleAs;
      this.$.ajax.timeout = this.timeout;
      this.$.ajax.generateRequest();
      this.fire('request', this.$.ajax.lastRequest, {bubbles: false});
    },

    _encodeParams: function(params) {
      var result = "";
      for (var key in params) {
          if (result != "") {
              result += "&";
          }
          result += key + "=" + encodeURIComponent(params[key]);
      }
      return result;
    },

    ready: function() {
      this.$.ajax.url = window.location.origin + window.location.pathname;
      this._autoChanged();
    },

    _autoChanged: function(newval, oldval) {
      if (this.auto && this.$.ajax.url) {
        this.generateRequest();
      }
    }
});
