Polymer({
  is: "discrete-bar-chart",

  behaviors: [
    Polymer.IronResizableBehavior,
    nvd3ChartBehavior
  ],

  _makeChart: function() {
    return nv.models.discreteBarChart();
  }
});
