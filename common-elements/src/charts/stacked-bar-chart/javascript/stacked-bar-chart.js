Polymer({
  is: "stacked-bar-chart",

  behaviors: [
    Polymer.IronResizableBehavior,
    nvd3ChartBehavior
  ],

  _makeChart: function() {
    return nv.models.multiBarChart();
  }
});
