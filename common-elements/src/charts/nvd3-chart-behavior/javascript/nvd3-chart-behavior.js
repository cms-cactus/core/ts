/**
 * `nvd3ChartBehavior` implements the common javascript functions with which we build ag-charts using NVD3
 * @polymerBehavior nvd3ChartBehavior
 */
var nvd3ChartBehavior = {

  listeners: {
    'iron-resize': '_drawChart'
  },
  properties: {
    /**
     * Data to display in the chart.
     */
    data: {
      type: Object,
      observer: '_dataChanged'
    },
    /**
     * Config element. Used to setup the graph (labels, ...)
     * see https://nvd3-community.github.io/nvd3/examples/documentation.html
     */
    config: {
      type: Object
    },
    /**
     * SVG element holding the chart
     */
    _d3chart: {
      type: Object
    },
    /**
     * D3 object holding the chart
     */
    _chart: {
      type: Object
    },

    /**
     * Function to do manual JS configuration
     */
    configureChart: {
      type: Function,
      value: function() {return function() {}}
    },

    _menu: {
      type: Object,
      value: function() {
        var box = document.createElement('paper-material');
        box.setAttribute("hidden", "hidden");
        var boxDOM = Polymer.dom(box);
        var menu = document.createElement('paper-menu');
        var menuDOM = Polymer.dom(menu);

        var saveJson = document.createElement('paper-item');
        saveJson.textContent = "save as JSON";
        saveJson.addEventListener("click", function(e) {
          this.saveJson();
        }.bind(this))
        menuDOM.appendChild(saveJson);

        var savePng = document.createElement('paper-item');
        savePng.textContent = "save as PNG";
        savePng.addEventListener("click", function(e) {
          this.savePng();
        }.bind(this))
        menuDOM.appendChild(savePng);

        var saveSvg = document.createElement('paper-item');
        saveSvg.textContent = "save as SVG";
        saveSvg.addEventListener("click", function(e) {
          this.saveSvg();
        }.bind(this))
        menuDOM.appendChild(saveSvg);

        var saveHtml = document.createElement('paper-item');
        saveHtml.textContent = "save as HTML";
        saveHtml.addEventListener("click", function(e) {
          this.saveHtml();
        }.bind(this))
        menuDOM.appendChild(saveHtml);

        boxDOM.appendChild(menu);

        return box;
      }
    }
  },

  observers: ['_configChanged(config.*)'],

  _configChanged: function(newval, oldval) {
    // ignore if chart not yet initialised
    if (this._chart) {
      this._configureChart();
      this._drawChart();
    }
  },

  /**
   * Event listener for changing chart data
   */
  _dataChanged: function() {
    if (this._chart) {
      this._populateGraph();
    }
  },

  /**
   * Generates a new chart. Based on data and config provided
   */
  _generateGraph: function() {
    nv.addGraph(function() {
      this._chart = this._makeChart();

      // this._chart = this._configurateGraph(this._chart);
      this._configureChart();
      this.configureChart();

      this._populateGraph();

      return this._chart;
    }.bind(this));
  },

  attached: function() {
    this.async(this.notifyResize, 1);
    this._d3chart = d3.select(this.$.chart);
    this._generateGraph();

    Polymer.dom(this.root).appendChild(this._menu);

    this.addEventListener( "contextmenu", function(e) {
      e.preventDefault();
      this._showMenu(e.layerY, e.layerX);
    });

    document.addEventListener("click",function(e){
      if (e.which != 3) {
        this._hideMenu();
      }
    }.bind(this));
  },

  _showMenu: function(top, left) {
    this._menu.style.top = top + "px";
    this._menu.style.left = left + "px";
    this._menu.removeAttribute("hidden");
  },

  _hideMenu: function() {
    this._menu.setAttribute("hidden", "hidden");
  },


  /**
   *  Adds configuration elements to the graph. Uses the nvd3 pie chart properties
   */
  _configureChart: function() {
    var config = this.config;
    if (config) {
      for (var property in config) {
        if (this._chart[property]) {
          this._chart[property](config[property]);
        } else {
          console.warn(this, 'unknown property ' + property)
        }
      }
    }
  },

  /**
   * Helper function. Binds data to the chart
   */
  _populateGraph: function() {
    this._d3chart
      .datum(this.data)
      .transition().duration(350)
      .call(this._chart);
  },


  /**
   * Redraws the chart (new data, new config, new size, ...)
   */
  _drawChart: function() {
    if (this._chart) {
      this._d3chart.call(this._chart);
    }
  },


  _applyStyles: function() {
    var applyStyles = function(cssquery, styles) {
      var elems = this.$.chart.querySelectorAll(cssquery);
      for (var i = 0; i < elems.length; i++) {
        for (var style in styles) {
          elems[i].style[style] = styles[style];
        }
      }
    }.bind(this);
    applyStyles(".nvtooltip[class]", {
      position: "fixed",
      bottom: "auto",
      right: "auto",
      display: "inline-block"
    });

    applyStyles(".nvtooltip[class] td.legend-color-guide", {
      position: "relative",
      width: "14px"
    });

    applyStyles("div", {
      position: "absolute",
      top: "0",
      bottom: "0",
      left: "0",
      right: "0"
    });

    applyStyles(".nvd3 .nv-axis line, .nvd3 .nv-axis path", {
      fill: "none",
      shapeRendering: "crispEdges"
    });

    applyStyles(".nv-brush .extent, .nvd3 .background path, .nvd3 .nv-axis line, .nvd3 .nv-axis path", {
      shapeRendering: "crispEdges"
    });

    applyStyles(".nvd3 .nv-axis", {
      opacity: "1"
    });

    applyStyles(".nvd3 .nv-axis.nv-disabled, .nvd3 .nv-controlsWrap .nv-legend .nv-check-box .nv-check", {
      opacity: "0"
    });

    applyStyles(".nvd3 .nv-axis path", {
      stroke: "#000",
      strokeOpacity: "0.75"
    });

    applyStyles(".nvd3 .nv-axis path.domain", {
      strokeOpacity: "0.75"
    });

    applyStyles(".nvd3 .nv-axis.nv-x path.domain", {
      strokeOpacity: "0"
    });

    applyStyles(".nvd3 .nv-axis line", {
      stroke: "#e5e5e5"
    });

    applyStyles(".nvd3 .nv-axis .zero line, .nvd3 .nv-axis line.zero", {
      strokeOpacity: "0.75"
    });

    applyStyles(".nvd3 .nv-axis .nv-axisMaxMin text", {
      fontWeight: "700"
    });

    applyStyles(".nvd3 .x .nv-axis .nv-axisMaxMin text, .nvd3 .x2 .nv-axis .nv-axisMaxMin text, .nvd3 .x3 .nv-axis .nv-axisMaxMin text", {
      textAnchor: "middle"
    });

    applyStyles(".nvd3 .nv-bars rect", {
      fillOpacity: "0.75"
    });

    applyStyles(".nvd3 .nv-bars text", {
      fill: "transparent"
    });

    applyStyles(".nvd3 .nv-discretebar .nv-groups rect, .nvd3 .nv-multibar .nv-groups rect, .nvd3 .nv-multibarHorizontal .nv-groups rect", {
      strokeOpacity: "0"
    });

    applyStyles(".nvd3 .nv-discretebar .nv-groups text, .nvd3 .nv-multibarHorizontal .nv-groups text", {
      fontWeight: "700",
      fill: "#000",
      stroke: "transparent"
    });

    applyStyles(".nvd3 .nv-boxplot circle", {
      fillOpacity: "0.5"
    });

    applyStyles(".nvd3 line.nv-boxplot-median", {
      stroke: "#000"
    });

    applyStyles(".nvd3.nv-bullet", {
      font: "10px sans-serif"
    });

    applyStyles(".nvd3.nv-bullet .nv-measure", {
      fillOpacity: "0.8"
    });

    applyStyles(".nvd3.nv-bullet .nv-marker", {
      stroke: "#000",
      strokeWidth: "2px"
    });

    applyStyles(".nvd3.nv-bullet .nv-markerTriangle", {
      stroke: "#000",
      fill: "#fff",
      strokeWidth: "1.5px"
    });

    applyStyles(".nvd3.nv-bullet .nv-tick line", {
      stroke: "#666",
      strokeWidth: "0.5px"
    });

    applyStyles(".nvd3.nv-bullet .nv-range.nv-s0", {
      fill: "#eee"
    });

    applyStyles(".nvd3.nv-bullet .nv-range.nv-s1", {
      fill: "#ddd"
    });

    applyStyles(".nvd3.nv-bullet .nv-range.nv-s2", {
      fill: "#ccc"
    });

    applyStyles(".nvd3.nv-bullet .nv-title", {
      fontSize: "14px",
      fontWeight: "700"
    });

    applyStyles(".nvd3.nv-bullet .nv-subtitle", {
      fill: "#999"
    });

    applyStyles(".nvd3.nv-bullet .nv-range", {
      fill: "#bababa",
      fillOpacity: "0.4"
    });

    applyStyles(".nvd3.nv-candlestickBar .nv-ticks .nv-tick", {
      strokeWidth: "1px"
    });

    applyStyles(".nvd3.nv-candlestickBar .nv-ticks .nv-tick.positive rect", {
      stroke: "#2ca02c",
      fill: "#2ca02c"
    });

    applyStyles(".nvd3.nv-candlestickBar .nv-ticks .nv-tick.negative rect", {
      stroke: "#d62728",
      fill: "#d62728"
    });

    applyStyles(".nvd3.nv-candlestickBar .nv-ticks line", {
      stroke: "#333"
    });

    applyStyles(".nvd3 .nv-check-box .nv-box", {
      fillOpacity: "0",
      strokeWidth: "2"
    });

    applyStyles(".nvd3 .nv-check-box .nv-check", {
      fillOpacity: "0",
      strokeWidth: "4"
    });

    applyStyles(".nvd3 .nv-series.nv-disabled .nv-check-box .nv-check", {
      fillOpacity: "0",
      strokeOpacity: "0"
    });

    applyStyles(".nvd3.nv-linePlusBar .nv-bar rect", {
      fillOpacity: "0.75"
    });

    applyStyles(".nvd3 .nv-groups path.nv-line", {
      fill: "none"
    });

    applyStyles(".nvd3 .nv-groups path.nv-area", {
      stroke: "none"
    });

    applyStyles(".nvd3.nv-line .nvd3.nv-scatter .nv-groups .nv-point", {
      fillOpacity: "0",
      strokeOpacity: "0"
    });

    applyStyles(".nvd3.nv-scatter.nv-single-point .nv-groups .nv-point", {
      fillOpacity: "0.5",
      strokeOpacity: "0.5"
    });

    applyStyles(".nvd3 .nv-point-paths path", {
      stroke: "#aaa",
      strokeOpacity: "0",
      fill: "#eee",
      fillOpacity: "0"
    });

    applyStyles(".nvd3 text", {
      font: "400 12px Arial"
    });

    applyStyles(".nvd3 .title", {
      font: "700 14px Arial"
    });

    applyStyles(".nvd3 .nv-background", {
      fill: "#fff",
      fillOpacity: "0"
    });

    applyStyles(".nvd3.nv-noData", {
      fontSize: "18px",
      fontWeight: "700"
    });

    applyStyles(".nv-brush .extent", {
      fillOpacity: "0.125"
    });

    applyStyles(".nv-brush .resize path", {
      fill: "#eee",
      stroke: "#666"
    });

    applyStyles(".nvd3 .nv-legend .nv-disabled circle", {
      fillOpacity: "0"
    });

    applyStyles(".nvd3 .nv-brushBackground rect", {
      stroke: "#000",
      strokeWidth: "0.4",
      fill: "#fff",
      fillOpacity: "0.7"
    });

    applyStyles(".nvd3.nv-ohlcBar .nv-ticks .nv-tick", {
      strokeWidth: "1px"
    });

    applyStyles(".nvd3.nv-ohlcBar .nv-ticks .nv-tick.hover", {
      strokeWidth: "2px"
    });

    applyStyles(".nvd3.nv-ohlcBar .nv-ticks .nv-tick.positive", {
      stroke: "#2ca02c"
    });

    applyStyles(".nvd3.nv-ohlcBar .nv-ticks .nv-tick.negative", {
      stroke: "#d62728"
    });

    applyStyles(".nvd3 .background path", {
      fill: "none",
      stroke: "#EEE",
      strokeOpacity: "0.4"
    });

    applyStyles(".nvd3 .foreground path", {
      fill: "none",
      strokeOpacity: "0.7"
    });

    applyStyles(".nvd3 .nv-parallelCoordinates-brush .extent", {
      fill: "#fff",
      fillOpacity: "0.6",
      stroke: "gray",
      shapeRendering: "crispEdges"
    });

    applyStyles(".nvd3 .missingValuesline line", {
      fill: "none",
      stroke: "#000",
      strokeWidth: "1",
      strokeOpacity: "1",
      strokeDasharray: "5, 5"
    });

    applyStyles(".nvd3.nv-pie .nv-pie-title", {
      fontSize: "24px",
      fill: "rgba(19, 196, 249, 0.59)"
    });

    applyStyles(".nvd3.nv-pie .nv-slice text", {
      stroke: "#000",
      strokeWidth: "0"
    });

    applyStyles(".nvd3.nv-pie path", {
      stroke: "#fff",
      strokeWidth: "1px",
      strokeOpacity: "1",
      fillOpacity: "0.7"
    });

    applyStyles(".nvd3.nv-pie .nv-label rect", {
      fillOpacity: "0",
      strokeOpacity: "0"
    });

    applyStyles(".nvd3.nv-sparkline path", {
      fill: "none"
    });

    applyStyles(".nvd3.nv-sparklineplus .nv-xValue, .nvd3.nv-sparklineplus .nv-yValue", {
      strokeWidth: "0",
      fontSize: "0.9em",
      fontWeight: "400"
    });

    applyStyles(".nvd3.nv-sparklineplus .nv-yValue", {
      stroke: "#f66"
    });

    applyStyles(".nvd3.nv-sparklineplus .nv-maxValue", {
      stroke: "#2ca02c",
      fill: "#2ca02c"
    });

    applyStyles(".nvd3.nv-sparklineplus .nv-minValue", {
      stroke: "#d62728",
      fill: "#d62728"
    });

    applyStyles(".nvd3.nv-sparklineplus .nv-currentValue", {
      fontWeight: "700",
      fontSize: "1.1em"
    });

    applyStyles(".nvtooltip h3, .nvtooltip table td.key", {
      fontWeight: "400"
    });

    applyStyles(".nvd3.nv-stackedarea path.nv-area", {
      fillOpacity: "0.7",
      strokeOpacity: "0"
    });

    applyStyles(".nvd3.nv-stackedarea .nv-groups .nv-point", {
      strokeOpacity: "0",
      fillOpacity: "0"
    });

    applyStyles(".nvd3 line.nv-guideline", {
      stroke: "#ccc"
    });
  },

  saveSvg: function(filename) {
    if (typeof filename == "undefined") {
      filename = window.prompt("please choose a file name", "chart.svg");
    }
    if (filename != null) {
      this._applyStyles();
      saveSvg(this.$.chart, filename);
    }
  },

  savePng: function(filename) {
    if (typeof filename == "undefined") {
      filename = window.prompt("please choose a file name", "chart.png");
    }
    if (filename != null) {
      this._applyStyles();
      saveSvgAsPng(this.$.chart, filename);
    }
  },

  saveJson: function(filename) {
    if (typeof filename == "undefined") {
      filename = window.prompt("please choose a file name", "chart.json");
    }
    if (filename != null) {
      var blob = new Blob([JSON.stringify(this.data)], {type: "application/json"});
      saveAs(blob, filename);
    }
  },

  saveHtml: function(filename) {
    if (typeof filename == "undefined") {
      filename = window.prompt("please choose a file name", "chart.html");
    }
    if (filename != null) {
      var tagname = this.tagName.toLowerCase();
      // fix to not have script end tag inside javascript, confusing the compiler
      var scripttag = 'script';
      var html = '\
      <!DOCTYPE html>\n\
      <html>\n\
        <head>\n\
          <title>' + filename + '</title>\n\
          <' + scripttag + ' src="http://' + window.location.host + '/extern/bower_components/webcomponentsjs/webcomponents-lite.min.js"></' + scripttag + '>\
          <link rel="import" href="http://' + window.location.host + '/ts/common-elements/charts/' + tagname + '/' + tagname + '.html">\n\
          <style>\n\
            html,body {height: 100%}\n\
            #chart {position: absolute; top:0; bottom: 0; left: 0; right: 0;}\n\
          </style>\n\
        </head>\n\
        <body>\n\
          <' + tagname + ' id="chart"></' + tagname + '>\n\
          <' + scripttag + '>\n\
            var chart = document.getElementById("chart");\n\
            chart.data = ' + JSON.stringify(this.data) + ';\n\
            chart.config = ' + JSON.stringify(this.config) + ';\n\
            chart.configureChart = ' + this.configureChart + '\n\
          </' + scripttag + '>\n\
        </body>\n\
      </html>\n';
      var blob = new Blob([html], {type: "text/html"});
      saveAs(blob, filename);
    }
  }
};
