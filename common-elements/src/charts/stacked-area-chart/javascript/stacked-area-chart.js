Polymer({
  is: "stacked-area-chart",

  behaviors: [
    Polymer.IronResizableBehavior,
    nvd3ChartBehavior
  ],

  _makeChart: function() {
    return nv.models.stackedAreaChart();
  }
});
