Polymer({
  is: "candlestick-chart",

  behaviors: [
    Polymer.IronResizableBehavior,
    nvd3ChartBehavior
  ],

  _makeChart: function() {
    return nv.models.candlestickBarChart();
  }
});
