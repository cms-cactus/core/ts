Polymer({
  is: "state-diagram",

  behaviors: [
    Polymer.IronResizableBehavior
    // nvd3ChartBehavior
  ],

  listeners: {
    'iron-resize': '_onResize'
  },

  properties: {
    _chart: Object
  },

  // _makeChart: function() {
  //   return nv.models.cumulativeLineChart();
  // }

  attached: function() {
    this._chart = cytoscape({
      container: this.$.chart, // container to render in
      elements: [ // list of graph elements to start with


        {
          data: {id: 'halted'}
        },
        {
          data: {id: 'configured'}
        },
        {
          data: {id: 'running'}
        },
        {
          data: {id: 'paused'}
        },

        {
          data: { id: 'coldReset', source: 'halted', target: 'halted' }
        },
        {
          data: { id: 'configure', source: 'halted', target: 'configured' }
        },
        {
          data: { id: 'start', source: 'configured', target: 'running' }
        },
        {
          data: { id: 'stop', source: 'running', target: 'configured' }
        },
        {
          data: { id: 'pause', source: 'running', target: 'paused' }
        },
        {
          data: { id: 'resume', source: 'paused', target: 'running' }
        },
        {
          data: { id: 'stop2', source: 'paused', target: 'configured' }
        }
      ],
      style: [ // the stylesheet for the graph
        {
          selector: 'node',
          style: {
            'background-color': '#666',
            'label': 'data(id)'
          }
        },

        {
          selector: 'edge',
          style: {
            'width': 3,
            'line-color': '#ccc',
            'target-arrow-color': '#ccc',
            'target-arrow-shape': 'triangle'
          }
        }
      ],

      layout: {
        name: 'circle',
        nodeRepulsion       : function( node ){ return 400000; },
        fit: true,
        avoidOverlap: true
      }
    });
  },

  _onResize: function() {
    if (this._chart) {
      this._chart.layout();
    }
  }
});
