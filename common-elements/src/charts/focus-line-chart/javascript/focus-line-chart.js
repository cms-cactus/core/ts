Polymer({
  is: "focus-line-chart",

  behaviors: [
    Polymer.IronResizableBehavior,
    nvd3ChartBehavior
  ],

  _makeChart: function() {
    return nv.models.lineWithFocusChart();
  }
});
