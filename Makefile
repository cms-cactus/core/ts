include config/vars.mk

Project=cactuscore
Packages=\
	ts/toolbox \
	ts/ajaxell \
	ts/common-elements \
	ts/framework \
	ts/candela \
	ts/runcontrol \
	ts/xdaqclient \
	extern/icons \
	extern/bower-components 

include config/rules.mk
