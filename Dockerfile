ARG BASE_IMAGE=gitlab-registry.cern.ch/cms-cactus/core/cactus-buildenv/base-xdaq15:master-954f9778 

FROM ${BASE_IMAGE}
LABEL maintainer="Cactus <cactus@cern.ch>"

COPY ci_rpms /rpms
RUN yum install -y /rpms/*.rpm && \
    yum clean all
