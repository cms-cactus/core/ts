/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Evangelos Paradas
 * @author: Christos Lazaridis
 *************************************************************************/


#ifndef _tsruncontrol_FEDMap_h_
#define _tsruncontrol_FEDMap_h_

#include <string>
#include <vector>
#include <map>

namespace tsruncontrol
{
class FEDMap
{
  public:
    enum SubsystemFEDs { uGT, uGMT, CaloL1, CaloL2, BMTF, EMTF, OMTF, TwinMux, CPPF, RPC };
  
  protected:
    std::map<int, unsigned int> mValuesMap;

  public:
    FEDMap();
    ~FEDMap();

    //!Reads the input fedmap string, decodes it and fills the map
    void init( const std::string& fedMap = "0" );

    //!Returns the map as a string
    std::string toString() const;

    /**
     * Returns the size of the FEDMap
     */
    unsigned int size() const;

    /**
     * Returns the active status of a given FED (an OR between SLINK/TTS active)
     * 
     * @param aIndex FED index
     * 
     * @return true if FED is enabled, false if not
     */
    bool operator[] ( int aIndex ) const;

    /**
     * SLINK status
     * 
     * @param aIndex FED index
     * 
     * @return true if SLINK is active
     */    
    bool activeSLink( int aIndex ) const;

    /**
     * TTS status
     * 
     * @param aIndex FED index
     * 
     * @return true if TTS is active
     */    
    bool activeTTS( int aIndex ) const;

    /**
     * Returns the status bit value of a given FED
     *
     * @param aIndex FED index
     *
     * @return the bit value for the given FED
     */
    unsigned int fedBitValue( int aIndex ) const;
    
    /**
     * Returns the enable status of a given subsystem
     * 
     * @param aSubsystem subsystem
     * 
     * @return true if one of subsystem FED is enabled, false if not
     */    
    bool isEnabled ( SubsystemFEDs aSubsystem ) const;

    /**
     * Forces the enable status of a FED to a specific value
     *
     * @param aIndex FED index
     * @param aValue FED enable value
     */
    void forceFed( int aIndex, unsigned int aValue );
    
    /**
     * Get the FEDs in the map
     *
     * @return a std::vector<int> of the FEDs defined in the map
     */    
    std::vector<int> getFeds() const;
    
    

  protected:
    //! Decodes the input string and creates the map 
    void parseRcmsFed ( const std::string& str );

};
}

#endif
