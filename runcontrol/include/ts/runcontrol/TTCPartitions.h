/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer Jun.
 * @author: Evangelos Paradas
 * @author: Christos Lazaridis
 *************************************************************************/


#ifndef _tsruncontrol_TTCPartitions_
#define _tsruncontrol_TTCPartitions_

#include "ts/exception/CellException.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellXhannel.h"

#include "log4cplus/logger.h"

#include <string>
#include <vector>
#include <map>

namespace tsruncontrol
{
  class TTCPartitions
  {
  private:
    log4cplus::Logger mLogger;
    tsframework::CellAbstractContext* mContext;

  public:
    typedef std::map<std::string, std::string> PartitionSystem; //key is the partition and value is the system
    typedef std::map<std::string, std::vector<std::string> > SystemPartitions; //key is the system and value the vector of partitions

    TTCPartitions();
    ~TTCPartitions();
    
    //! Reads the database in order to retrieve the partitions and the systems and fill the maps
    void init(log4cplus::Logger& aLog, tsframework::CellAbstractContext* aContext);
    
    //! Returns the system in which the partition belongs
    const std::string system(const std::string& partitionName); 
    
    //! Returns the partions that belong to the system
    const std::vector<std::string> partitions(const std::string& systemName); 
    
    //! Returns the map where the system is the key and a vector of strings (partitions) is the value
    const SystemPartitions& getSystemPartitions();
    
    //! Returns the map where the partition is the key and the system is the value
    const PartitionSystem& getPartitionSystem();
    
    //! Returns the map as a string
    std::string toString();

    //! Returns true if a system exists
    bool partitionIncluded( const std::string& partitionName );

    DEFINE_TS_EXCEPTION ( EmptyKey );
    DEFINE_TS_EXCEPTION ( InvalidParameter );

  protected:
    PartitionSystem partitionSystem;
    SystemPartitions systemPartitions;
    
  private:
    void getPartitionAndSystem();
    std::string getLatestKeyFromDB();
    void getPartitionAndSystemFromDB(std::string aLatestKey);
  };
  
}
#endif
