/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Christos Lazaridis
 *************************************************************************/

#ifndef _tsruncontrol_ConfigurationBase_h_
#define _tsruncontrol_ConfigurationBase_h_

#include "ts/runcontrol/FEDMap.h"
#include "ts/runcontrol/TTCMap.h"

#include "ts/framework/CellOperation.h"
#include "ts/framework/CellXhannelCell.h"
#include "ts/framework/CellXhannelRequestCell.h"
#include "ts/framework/CellXhannelRequestHandler.h"

#include "xcept/tools.h"

#include <boost/shared_ptr.hpp>
#include <string>

#include <sstream>

namespace tsruncontrol
{
/**
 * The base class for all configuration classes. 
 * Basically, it defines the FSM and provides some basic helper methods.
 */
class ConfigurationBase: public tsframework::CellOperation
{
protected:
  FEDMap mFedMap;
  TTCMap mTtcMap;
  
  static const std::string kConfigKey;
  static const std::string kRSKey;
  static const std::string kRunNumber;
  static const std::string kTtcMap;
  static const std::string kFedMap;
  static const std::string kDcsLhcFlags;
  static const std::string kAutoMode;
  static const std::string kUsePrimaryTcds;

  static const std::string kTrEngage;
  static const std::string kTrSetup;
  static const std::string kTrConfigure;
  static const std::string kTrAlign;
  static const std::string kTrStart;
  static const std::string kTrStop;
  static const std::string kTrPause;
  static const std::string kTrResume;  
  static const std::string kTrColdReset;
  static const std::string kTrReset;

  static const std::string kSubUGT;
  static const std::string kSubUGMT;
  static const std::string kSubCALOL1;
  static const std::string kSubCALOL2;
  static const std::string kSubBMTF;
  static const std::string kSubEMTF;
  static const std::string kSubOMTF;
  static const std::string kSubTWINMUX;
  static const std::string kSubCPPF;
  static const std::string kSubRPC;

  
public:
  virtual ~ConfigurationBase();
  void resetting();

  //! Writes an error message to both the warning box and the log
  virtual void publishError ( const std::string& aMessage );

  //! Writes a warning message to both the warning box and the log
  virtual void publishWarning ( const std::string& aMessage );
  
protected:
  ConfigurationBase ( log4cplus::Logger& aLog, tsframework::CellAbstractContext* aContext );
  typedef tsframework::CellXhannelRequestHandler<tsframework::CellXhannelCell> RequestHandler;
  typedef boost::shared_ptr<tsframework::CellXhannelRequestHandler<tsframework::CellXhannelCell> > RequestHandlerPtr;

  /**
    * The default implementations for the transitions. Please override them as/if required.
    * If some of these transitions MUST be implemented, please declare them abstract here.
    */
  virtual bool checkColdReset() = 0;
  virtual bool checkConfigure() = 0;
  virtual bool checkStart() = 0;
  virtual bool checkStop() = 0;
  virtual bool checkPause() = 0;
  virtual bool checkResume() = 0;
  virtual void execColdReset() = 0;
  virtual void execConfigure() = 0;
  virtual void execStart() = 0;
  virtual void execStop() = 0;
  virtual void execPause() = 0;
  virtual void execResume() = 0;


  //! True if the cell is in error level
  bool hasErrors();

  //! True if the cell is in warning level
  bool hasWarnings();

  //! Removes a parameter (if available)
  void removeParameter ( const std::string& aName );

  //! Returns the parameter as a trimmed string
  std::string getParam ( const std::string& aName, bool aMustBeAvailable = true );

  //! Returns the bool representation of the parameter
  bool getBoolParam ( const std::string& aName );

  //! True if a parameter exists
  bool hasParam ( const std::string& aName );
  
  //! Returns the current state of operation opId in a given xhannel
  std::string getState(const std::string& opId, const std::string& xhannel);
  
  //! Returns a vector of the worker cells 
  std::vector<std::string> getWorkers();
  
  //! Returns a pointer to the CellXhannelCell instance of a given xhannel
  tsframework::CellXhannelCell* getCell(const std::string& xhannel);
  
  /** 
    * Send an operation transition request
    * 
    * @param opId operation name
    * @param xhannel the worker to request the transition
    * @param transitions transition name
    * @param params operation parameters
    * 
    * @return pointer to the request handler
    */
  RequestHandlerPtr sendTransition(const std::string& opId, const std::string& xhannel, const std::string& transition, std::map<std::string,xdata::Serializable*>& params);

  /** 
    * Send a command to a worket cell
    * 
    * @param command command name
    * @param xhannel worker to send the command to
    * @param params command parameters
    * 
    * @return pointer to the request handler
    */
  RequestHandlerPtr sendCommand(const std::string& command, const std::string& xhannel, std::map<std::string,xdata::Serializable*>& params);
  
  /** 
    * Reset a single xhannel
    * 
    * @param aOpId operation name
    * @param aXhannel the worker to request the transition
    */
  void reset(const std::string& aOpId, const std::string& aXhannel);
  
  void exceptionReport( const std::string & aTransition, xcept::Exception& aException );
    
  void errorReport( const std::string & aTransition );
    
  std::string fedToString(const FEDMap::SubsystemFEDs & aSubsystem) const;
  
  FEDMap::SubsystemFEDs stringToFed(const std::string & aSubsystem) const;

private:
  /**
  * By default, these methods simply call the check* & exec* ones above.
  * May be used to implement default behavior that is not supposed to be overridden.
  */
  bool cColdReset();
  bool cConfigure();
  bool cStart();
  bool cStop();
  bool cPause();
  bool cResume();
  void eColdReset();
  void eConfigure();
  void eStart();
  void eStop();
  void ePause();
  void eResume();
  
  void reportBody(std::ostringstream& aOut);
  void sendReport(const std::string& aBody,  const std::string & aTransition, const std::string& aType);
};
}

#endif
