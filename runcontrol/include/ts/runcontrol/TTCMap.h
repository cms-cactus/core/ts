/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer Jun.
 * @author: Evangelos Paradas
 * @author: Christos Lazaridis
 *************************************************************************/

#ifndef _tsruncontrol_TTCMap_
#define _tsruncontrol_TTCMap_

#include "ts/framework/CellAbstractContext.h"

#include "ts/toolbox/Tools.h"
#include "ts/runcontrol/TTCPartitions.h"

#include "log4cplus/logger.h"

#include "xcept/Exception.h"

#include <boost/shared_ptr.hpp>

#include <string>
#include <map>

namespace tsruncontrol
{
  class TTCMap
  {
  private:
    log4cplus::Logger mLogger;
    tsframework::CellAbstractContext* mContext;
    TTCPartitions mTtcPartitions;
    std::map<std::string, unsigned int> bitValues;
    std::string inputString;

  public:
    static const int FED_BITMASK = 1;
    static const int FMM_BITMASK = 2;

    TTCMap ();
    ~TTCMap();

    void init(log4cplus::Logger& aLogger, tsframework::CellAbstractContext* context, std::string inputfromFM = "" );

    DEFINE_TS_EXCEPTION ( EmptyTTCPartitionMap );
    DEFINE_TS_EXCEPTION ( IndexOutOfRange );
    DEFINE_TS_EXCEPTION ( NoSystem );
    DEFINE_TS_EXCEPTION ( InvalidArgument );
    DEFINE_TS_EXCEPTION ( KeyNotFoundInMap );

    //! Returns the TTCPartitions object
    TTCPartitions& mapping();

    //! Returns the size of the map
    unsigned int size() const;

    //! Checks if a partition has FED enabled
    bool hasFedEnabled ( const std::string& partitionName );

    //! Checks if a partition has FMM enabled
    bool hasFmmEnabled ( const std::string& partitionName );

    //! Checks if a system has at least one partition has FED enabled
    bool systemHasFedEnabled ( const std::string& systemName );

    //! Checks if a system has at least one partition has FMM enabled
    bool systemHasFmmEnabled ( const std::string& systemName );

    //! It sets the FED enabled of a system
    void enableSystemFed ( const std::string& systemName );

    //! Returns the map where the partition is the key and the bit is the value
    std::map<std::string,unsigned int>  getBitMap();

    //! Returns the map as a string
    std::string toString();

    //! Returns the string that was received
    std::string getInputString();

    //! Returns a string with the list of partitions that do not correspond to a system
    std::string getPartitionsWithoutSystem();

  private:
    //! Format the input string 
    std::string formatInput(std::string s);

    std::map<std::string, unsigned int> getPartitionsAndBits(const std::string& text);

    std::string partitionsWithoutSystem;
  };
}

#endif
