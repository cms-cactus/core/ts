/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun.
 * @author: Christos Lazaridis 
 *************************************************************************/

#ifndef _tsruncontrol_SequenceItem_h_
#define _tsruncontrol_SequenceItem_h_

#include "ts/runcontrol/ToStream.h"

#include <string>

namespace tsruncontrol
{

class SequenceItem : public ToStream
{
public:
  std::string xhannel;
  int timeout;
  std::string dependsOnXhannel;
  int secondsToWait;

  SequenceItem ( const std::string& xhannel = "",
      const int timeout = -1,
      const std::string& dependsOnXhannel = "",
      const int secondsToWait = WAIT_UNTIL_DONE() );

  static SequenceItem fromString ( const std::string& sequenceItem );

  std::string toString() const;

  bool operator== ( const std::string& findXhannel ) const;

  static int WAIT_UNTIL_DONE();
};
}

#endif
