/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun.
 * @author: Christos Lazaridis
 *************************************************************************/

#ifndef _tsruncontrol_Sequences_h_
#define _tsruncontrol_Sequences_h_

#include "ts/runcontrol/ToStream.h"

#include "ts/runcontrol/Sequence.h"

#include <string>
#include <map>

namespace tsruncontrol
{

  /**
   * A container for sequences
   */
  class Sequences : public ToStream
  {
    public:
      typedef std::map<std::string, Sequence> List_t;


    protected:
      List_t _sequences;


    public:
      Sequences ( const std::string& sequences = "" );

      ~Sequences();

      Sequence& operator[] ( const std::string& transition );

      void clear();

      bool isEmpty() const;

      std::string toString() const;

  };
}

#endif
