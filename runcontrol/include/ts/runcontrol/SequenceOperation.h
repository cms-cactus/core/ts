 /** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer jun.
 * @author: Christos Lazaridis
 *************************************************************************/

#ifndef _tsruncontrol_SequenceOperation_h_
#define _tsruncontrol_SequenceOperation_h_

#include "ts/runcontrol/Resource.h"

#include "boost/utility.hpp"

namespace tsframework {
  class CellXhannelCell;
  class CellAbstractContext;
  class CellXhannelXdaqSimple;
}

namespace tsruncontrol
{
class UserInterface;
class SequenceAction;

class SequenceOperation : boost::noncopyable
{
protected:
  UserInterface& _ui;
  const std::string _xhannel;
  const std::string _opName;
  const Resource _resource;
  const bool _isXdaq;


public:
  SequenceOperation ( UserInterface& ui,
      const std::string& xhannel,
      const std::string& opName,
      const Resource& resource = Resource ( "", "" ) );

  virtual ~SequenceOperation();

  bool isCell()    const;

  bool isXdaq()    const;

  UserInterface&  ui()        const;
  
  const std::string& xhannel()    const;

  const std::string& opName()    const;

  const std::string  url()       const;

  tsframework::CellXhannelCell& getXhannel() const;

  tsframework::CellAbstractContext* cellContext() const;

  tsframework::CellXhannelXdaqSimple& xdaqXhannel() const;
  
  std::string getState();

  std::string reset();

  // REVIEW
  //
  SequenceAction* transition ( const std::string& transition );

};
}

#endif
