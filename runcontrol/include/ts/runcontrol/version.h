#ifndef _ts_configuration_version_h_
#define _ts_configuration_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define TS_TSRUNCONTROL_VERSION_MAJOR 5
#define TS_TSRUNCONTROL_VERSION_MINOR 2
#define TS_TSRUNCONTROL_VERSION_PATCH 4

//
// Template macros
//
#define TS_TSRUNCONTROL_VERSION_CODE PACKAGE_VERSION_CODE(TS_TSRUNCONTROL_VERSION_MAJOR,TS_TSRUNCONTROL_VERSION_MINOR,TS_TSRUNCONTROL_VERSION_PATCH)
#ifndef TS_TSRUNCONTROL_PREVIOUS_VERSIONS
#define TS_TSRUNCONTROL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TS_TSRUNCONTROL_VERSION_MAJOR,TS_TSRUNCONTROL_VERSION_MINOR,TS_TSRUNCONTROL_VERSION_PATCH)
#else
#define TS_TSRUNCONTROL_FULL_VERSION_LIST  TS_TSRUNCONTROL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TS_TSRUNCONTROL_VERSION_MAJOR,TS_TSRUNCONTROL_VERSION_MINOR,TS_TSRUNCONTROL_VERSION_PATCH)
#endif

namespace tsruncontrol
{
  const std::string project = "ts";
  const std::string package  =  "tsruncontrol";
  const std::string versions =  TS_TSRUNCONTROL_FULL_VERSION_LIST;
  const std::string description = "Trigger Supervisor Configuration Component";
  const std::string authors = "Josef Hammer jun.";
  const std::string summary = "Trigger Supervisor Configuration Component";
  const std::string link = "https://savannah.cern.ch/projects/l1ts/";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif


