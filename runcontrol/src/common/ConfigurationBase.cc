/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Christos Lazaridis
 *************************************************************************/

#include "ts/runcontrol/ConfigurationBase.h"

#include "ts/framework/CellAbstract.h"

#include "ts/toolbox/CellToolbox.h"
#include "ts/toolbox/SmtpMessenger.h"
#include "ts/toolbox/Timer.h"
#include "ts/toolbox/Tools.h"

#include "xcept/Exception.h"
#include "xdata/xdata.h"

#include "log4cplus/loggingmacros.h"

#include <algorithm>
#include <unistd.h>

#include <boost/algorithm/string.hpp>

using namespace std;

namespace tsruncontrol {
  
const string ConfigurationBase::kConfigKey      = "Configuration Key";
const string ConfigurationBase::kRSKey          = "Run Settings Key";
const string ConfigurationBase::kRunNumber      = "Run Number";
const string ConfigurationBase::kTtcMap         = "TTC Map";
const string ConfigurationBase::kFedMap         = "FED Map";
const string ConfigurationBase::kDcsLhcFlags    = "DCS LHC Flags";
const string ConfigurationBase::kAutoMode       = "AutoMode";
const string ConfigurationBase::kUsePrimaryTcds = "Use Primary TCDS";

const string ConfigurationBase::kTrEngage    = "engage";
const string ConfigurationBase::kTrSetup     = "setup";
const string ConfigurationBase::kTrConfigure = "configure";
const string ConfigurationBase::kTrAlign     = "align";
const string ConfigurationBase::kTrStart     = "start";
const string ConfigurationBase::kTrStop      = "stop";
const string ConfigurationBase::kTrPause     = "pause";
const string ConfigurationBase::kTrResume    = "resume";
const string ConfigurationBase::kTrColdReset = "coldReset";
const string ConfigurationBase::kTrReset     = "reset";

const string ConfigurationBase::kSubUGT     = "UGT";
const string ConfigurationBase::kSubUGMT    = "UGMT";
const string ConfigurationBase::kSubCALOL1  = "CALOL1";
const string ConfigurationBase::kSubCALOL2  = "CALOL2";
const string ConfigurationBase::kSubBMTF    = "BMTF";
const string ConfigurationBase::kSubEMTF    = "EMTF";
const string ConfigurationBase::kSubOMTF    = "OMTF";
const string ConfigurationBase::kSubTWINMUX = "TWINMUX";
const string ConfigurationBase::kSubCPPF    = "CPPF";
const string ConfigurationBase::kSubRPC     = "RPC";
  
// --------------------------------------------------------
ConfigurationBase::ConfigurationBase ( log4cplus::Logger& aLog, tsframework::CellAbstractContext* aContext )
          : tsframework::CellOperation ( aLog, aContext )
{
  LOG4CPLUS_DEBUG( getLogger(), "#ConfigurationBase()"); 

  try {

    const string stateHalted         = "halted";
    const string stateConfigured     = "configured";
    const string stateRunning        = "running";
    const string statePaused         = "paused";

    getFSM().clear();
    getFSM().addState ( stateHalted );
    getFSM().addState ( stateConfigured );
    getFSM().addState ( stateRunning );
    getFSM().addState ( statePaused );
    getFSM().addTransition ( stateHalted,        stateHalted,        "coldReset",    this, 
                                                   &ConfigurationBase::cColdReset,    &ConfigurationBase::eColdReset );
    getFSM().addTransition ( stateHalted,        stateConfigured,    "configure",    this, 
                                                   &ConfigurationBase::cConfigure,    &ConfigurationBase::eConfigure );
    getFSM().addTransition ( stateConfigured,    stateRunning,       "start",        this, 
                                                   &ConfigurationBase::cStart,        &ConfigurationBase::eStart );
    getFSM().addTransition ( stateRunning,       stateConfigured,    "stop",         this, 
                                                   &ConfigurationBase::cStop,         &ConfigurationBase::eStop );
    getFSM().addTransition ( stateRunning,       statePaused,        "pause",        this, 
                                                   &ConfigurationBase::cPause,        &ConfigurationBase::ePause );
    getFSM().addTransition ( statePaused,        stateRunning,       "resume",       this, 
                                                   &ConfigurationBase::cResume,       &ConfigurationBase::eResume );
    getFSM().addTransition ( statePaused,        stateConfigured,    "stop",         this, 
                                                   &ConfigurationBase::cStop,         &ConfigurationBase::eStop );
    getFSM().setInitialState ( stateHalted );
    getFSM().reset();
  }
  catch ( xcept::Exception& e ) {

    XCEPT_RETHROW ( tsexception::CellException,"Failed to initialize the FSM!", e );
  }

  // Add/initialize parameters
  //
  //! L1 configuration keys
  getParamList() [kConfigKey] = new xdata::String ( "" );
  getParamList() [kRSKey] = new xdata::String ( "" );
  //! Run number
  getParamList() [kRunNumber]      = new xdata::UnsignedLong ( 0 );
  getParamList() [kTtcMap]         = new xdata::String( "{dummy=0}" );
  getParamList() [kFedMap]         = new xdata::String ( "0&0%" );
  getParamList() [kDcsLhcFlags]    = new xdata::String ( "" );
  getParamList() [kAutoMode]       = new xdata::Boolean ( false );
  getParamList() [kUsePrimaryTcds] = new xdata::Boolean ( true );
}


// --------------------------------------------------------
ConfigurationBase::~ConfigurationBase()
{
  //
}


// --------------------------------------------------------
bool ConfigurationBase::cColdReset()
{
  tstoolbox::Timer timer; 
  bool toReturn;
  const string transition = "check " + kTrColdReset;
  
  try {
    
    toReturn = checkColdReset();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" );
  return toReturn;   
}


// --------------------------------------------------------
bool ConfigurationBase::cConfigure()
{
  tstoolbox::Timer timer;   
  bool toReturn;
  const string transition = "check " + kTrConfigure;
  
  try {

    toReturn = checkConfigure();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" );
  return toReturn;   
}


// --------------------------------------------------------
bool ConfigurationBase::cStart()
{
  tstoolbox::Timer timer; 
  bool toReturn;
  const string transition = "check " + kTrStart;
  
  try {
    
    toReturn = checkStart();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" );
  return toReturn;   
}


// --------------------------------------------------------
bool ConfigurationBase::cStop()
{
  tstoolbox::Timer timer; 
  bool toReturn = checkStop();
  const string transition = "check " + kTrStop;
  
  try {
    
    toReturn = checkStop();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }  
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" );
  return toReturn;   
}


// --------------------------------------------------------
bool ConfigurationBase::cPause()
{
  tstoolbox::Timer timer; 
  bool toReturn;
  const string transition = "check " + kTrPause;
  
  try {
    
    toReturn = checkPause();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" );
  return toReturn;   
}


// --------------------------------------------------------
bool ConfigurationBase::cResume()
{
  tstoolbox::Timer timer; 
  bool toReturn;
  const string transition = "check " + kTrResume;
  
  try {
    
    toReturn = checkResume();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" );
  return toReturn;   
}


// --------------------------------------------------------
void ConfigurationBase::eColdReset()
{
  tstoolbox::Timer timer; 
  const string transition = kTrColdReset;
  
  try {
    
    execColdReset();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" );
}


// --------------------------------------------------------
void ConfigurationBase::eConfigure()
{
  tstoolbox::Timer timer; 
  const string transition = kTrConfigure;
  
  try {
    
    execConfigure();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" ); 
}


// --------------------------------------------------------
void ConfigurationBase::eStart()
{
  tstoolbox::Timer timer; 
  const string transition = kTrStart;
  
  try {
    
    execStart();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }

  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" ); 
}


// --------------------------------------------------------
void ConfigurationBase::eStop()
{
  tstoolbox::Timer timer; 
  const string transition = kTrStop;
  
  try {
    
    execStop();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" ); 
}


// --------------------------------------------------------
void ConfigurationBase::ePause()
{
  tstoolbox::Timer timer; 
  const string transition = kTrPause;
  
  try {
    
    execPause();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" ); 
}


// --------------------------------------------------------
void ConfigurationBase::eResume()
{
  tstoolbox::Timer timer; 
  const string transition = kTrResume;
  
  try {
    
    execResume();
  }
  catch (xcept::Exception& e) {
    
    exceptionReport(transition, e);
    
    XCEPT_RETHROW(tsexception::CellException,"Cell " + transition + " exception",e);
  }
  
  LOG4CPLUS_INFO ( getLogger(), transition + " operation done in " << timer.ms() << " ms" ); 
}


// --------------------------------------------------------
void ConfigurationBase::resetting()
{
  tstoolbox::Timer timer; 

  LOG4CPLUS_INFO ( getLogger(), "reset operation done in " << timer.ms() << " ms" ); 
}


// --------------------------------------------------------
void ConfigurationBase::publishError ( const string& aMessage )
{
  getWarning().append ( aMessage + "\n", tsframework::CellWarning::ERROR );
  LOG4CPLUS_ERROR ( getLogger(), aMessage );
}


// --------------------------------------------------------
void ConfigurationBase::publishWarning ( const string& aMessage )
{
  getWarning().append ( aMessage + "\n", tsframework::CellWarning::WARNING );
  LOG4CPLUS_WARN ( getLogger(), aMessage );
}


// --------------------------------------------------------
bool ConfigurationBase::hasErrors()
{
  return ( tsframework::CellWarning::ERROR <= getWarning().getLevel() );
}


// --------------------------------------------------------
bool ConfigurationBase::hasWarnings()
{
  return ( tsframework::CellWarning::WARNING == getWarning().getLevel() );
}


// --------------------------------------------------------
void ConfigurationBase::removeParameter ( const string& aName )
{
  map<string, xdata::Serializable*>::iterator it = getParamList().find ( aName );

  if ( it != getParamList().end() )
  {
    delete it->second;
    getParamList().erase ( it );
  }
}


// --------------------------------------------------------
string ConfigurationBase::getParam ( const string& aName,bool aMustBeAvailable)
{
  if ( hasParam ( aName ) )
  {
    return boost::trim_copy ( getParamList() [aName]->toString() );
  }

  iAssert ( !aMustBeAvailable && "getParam() was called for a non-existing parameter!" );
  return "";
}


// --------------------------------------------------------
bool ConfigurationBase::getBoolParam ( const string& aName )
{
  return ( dynamic_cast<xdata::Boolean*> ( getParamList() [aName] ) )->toString () == "true";
}


// --------------------------------------------------------
bool ConfigurationBase::hasParam ( const string& aName )
{
  return ( getParamList().find ( aName ) != getParamList().end() ) && getParamList() [aName];
}


// --------------------------------------------------------
string ConfigurationBase::getState(const string& opId, const string& xhannel)
{
  tsframework::CellXhannelCell* x = getCell(xhannel);
  
  RequestHandlerPtr h_ptr(new RequestHandler(x));
  
  bool async=false;
  h_ptr->getRequest()->doOpGetState(getSessionId(), async, opId);
  
  try {
    x->send(h_ptr->getRequest());
  } catch (xcept::Exception& e) {
    XCEPT_RAISE(tsexception::CellException,"Error requesting state to operation '" + opId + "' of xhannel '" 
                                                                                                    + xhannel + "'");
  }
  
  string state = h_ptr->getRequest()->opGetStateReply();
  return state;
}


// --------------------------------------------------------
vector<string> ConfigurationBase::getWorkers() 
{
  vector<string> result;
  map<string, tsframework::CellXhannel*> xhannels = getContext()->getXhannelList();
  
  for(map<string, tsframework::CellXhannel*>::iterator i(xhannels.begin()); i != xhannels.end(); ++i) 
  {
    if (dynamic_cast<tsframework::CellXhannelCell*>(i->second))
      result.push_back(i->first);
  }
  
  return result;
}


// --------------------------------------------------------
tsframework::CellXhannelCell* ConfigurationBase::getCell(const string& xhannel) 
{
  tsframework::CellXhannelCell* x = dynamic_cast<tsframework::CellXhannelCell*>(getContext()->getXhannel(xhannel));
  
  if (!x) {
    ostringstream msg;
    msg << "Can not convert xhannel \'" << xhannel << "\' to CellXhannelCell";
    XCEPT_RAISE(tsexception::CellException,msg.str());
  }
  
  return x;
}


// --------------------------------------------------------
ConfigurationBase::RequestHandlerPtr ConfigurationBase::sendTransition(const string& opId, const string& xhannel, 
                                                                       const string& transition, 
                                                                       map<string,xdata::Serializable*>& params)
{
  tsframework::CellXhannelCell* x = getCell(xhannel);
  
  RequestHandlerPtr h_ptr(new RequestHandler(x));

  bool async=true;
  h_ptr->getRequest()->doOpSendCommand(getSessionId(), async, opId, transition, params);

  try {
    x->send(h_ptr->getRequest());
  } 
  catch (xcept::Exception& e) {
    XCEPT_RETHROW(tsexception::CellException,"Cannot execute transition '" + transition + "' in xhannel '" + xhannel 
                                                                                + "' (operation : '" + opId + "')",e);
  }
  
  return h_ptr;
}


// --------------------------------------------------------
ConfigurationBase::RequestHandlerPtr ConfigurationBase::sendCommand(const string& command, const string& xhannel, 
                                                                    map<string,xdata::Serializable*>& params)
{
  tsframework::CellXhannelCell* x = getCell(xhannel);
  
  RequestHandlerPtr h_ptr(new RequestHandler(x));
  
  bool async = true;
  h_ptr->getRequest()->doCommand(getSessionId(), async, command, params);
  
  try {
    x->send(h_ptr->getRequest());
  } 
  catch (xcept::Exception& e) {
    XCEPT_RETHROW(tsexception::CellException,"Cannot send command '" + command + "' in xhannel '" + xhannel + "'",e);
  }
  
  return h_ptr; 
}


// --------------------------------------------------------
void ConfigurationBase::reset(const string& aOpId, const string& aXhannel) 
{
  tsframework::CellXhannelCell* x = getCell(aXhannel);
  
  RequestHandlerPtr h_ptr(new RequestHandler(x));
  
  bool async=false;
  h_ptr->getRequest()->doOpReset(getSessionId(), async, aOpId);
  
  try {
    
    x->send(h_ptr->getRequest());
  } 
  catch (xcept::Exception& e) {
    
    //reset should not throw
    getWarning().append("@" + aXhannel + ".reset : " + xcept::stdformat_exception_history(e), 
                        tsframework::CellWarning::ERROR);
    return;
  }

  tsframework::CellWarning warning = h_ptr->getRequest()->getWarning();

  if (warning.getLevel() >= tsframework::CellWarning::ERROR)
    getWarning().append("@" + aXhannel + ".reset : " + warning.getMessage()+"\n", warning.getLevel());
}


// --------------------------------------------------------
string ConfigurationBase::fedToString(const FEDMap::SubsystemFEDs & aSubsystem) const
{
  if ( aSubsystem == FEDMap::uGT )
    return kSubUGT;
  else if ( aSubsystem == FEDMap::uGMT )
    return kSubUGMT;
  else if ( aSubsystem == FEDMap::CaloL1 )
    return kSubCALOL1;
  else if ( aSubsystem == FEDMap::CaloL2 )
    return kSubCALOL2;
  else if ( aSubsystem == FEDMap::BMTF )
    return kSubBMTF;
  else if ( aSubsystem == FEDMap::EMTF )
    return kSubEMTF;
  else if ( aSubsystem == FEDMap::OMTF )
    return kSubOMTF;
  else if ( aSubsystem == FEDMap::TwinMux )
    return kSubTWINMUX;
  else if ( aSubsystem == FEDMap::CPPF )
    return kSubCPPF;
  else if ( aSubsystem == FEDMap::RPC )
    return kSubRPC;
  else 
    XCEPT_RAISE(tsexception::CellException,"Unexpected FEDMap::SubsystemFEDs value encountered.");
}


// --------------------------------------------------------
FEDMap::SubsystemFEDs ConfigurationBase::stringToFed(const std::string & aSubsystem) const
{
  if ( aSubsystem == kSubUGT)
    return FEDMap::uGT ;
  else if ( aSubsystem == kSubUGMT )
    return FEDMap::uGMT;
  else if ( aSubsystem == kSubCALOL1 )
    return FEDMap::CaloL1;
  else if ( aSubsystem == kSubCALOL2 )
    return FEDMap::CaloL2;
  else if ( aSubsystem == kSubBMTF )
    return FEDMap::BMTF;
  else if ( aSubsystem == kSubEMTF )
    return FEDMap::EMTF;
  else if ( aSubsystem == kSubOMTF )
    return FEDMap::OMTF;
  else if ( aSubsystem == kSubTWINMUX )
    return FEDMap::TwinMux;
  else if ( aSubsystem == kSubCPPF )
    return FEDMap::CPPF;
  else if ( aSubsystem == kSubRPC )
    return FEDMap::RPC;
  else 
    XCEPT_RAISE(tsexception::CellException,"Unexpected subsystem string value encountered.");
}


// --------------------------------------------------------
void ConfigurationBase::exceptionReport( const string & aTransition, xcept::Exception& aException ) 
{
  ostringstream out;
  string name = getContext()->getCell()->getName();
  
  out << name << "\n"
      << "An exception occured during " + boost::algorithm::to_upper_copy( aTransition ) << " on "
      << tstoolbox::Timer().startTimeAsString(true)
      << "\n\n"
      << "Exception information:\n"
      << "~~~~~~~~~~~~~~~~~~~~~\n";
      
      for ( xcept::Exception::const_iterator itr = aException.begin(); itr != aException.end(); itr++ ) {
        out << "Name        : " << itr->getProperty("identifier") << "\n";
        out << "Module      : " << itr->getProperty("module") << "\n";
        out << "Function    : " << itr->getProperty("function") << "\n";
        out << "Line        : " << itr->getProperty("line") << "\n";
        out << "Message     : " << itr->getProperty("message") << "\n\n";
      }
      
  reportBody(out);
  sendReport(out.str(), aTransition, "Exception");
}



// --------------------------------------------------------
void ConfigurationBase::errorReport( const string & aTransition )
{
  ostringstream out;
  string name = getContext()->getCell()->getName();
  
  out << name << "\n"
      << "An error occured during " + boost::algorithm::to_upper_copy( aTransition ) << " on "
      << tstoolbox::Timer().startTimeAsString(true)
      << "\n\n";
      
  reportBody(out);
  sendReport(out.str(), aTransition, "Error");
}


// --------------------------------------------------------
void ConfigurationBase::reportBody(ostringstream& aOut)
{
  
  aOut << "Error message:\n"
       << "~~~~~~~~~~~~~\n"
       << getWarning().getMessage()
       << "\n\n"
       << "Current operation messages:\n"
       << "~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
       << getResult()
       << "\n\n"
       << "Further information:\n"
       << "~~~~~~~~~~~~~~~~~~~\n";
       
  if ( hasParam(kAutoMode) )
      aOut << kAutoMode << "         : " << getParamList() [kAutoMode]->toString() << "\n";
  
  if ( hasParam(kUsePrimaryTcds) )
      aOut << kUsePrimaryTcds << "  : " << getParamList() [kUsePrimaryTcds]->toString() << "\n";
  
  if ( hasParam(kConfigKey) )
      aOut <<  kConfigKey << " : " << getParamList() [kConfigKey]->toString() << "\n";
  
  if ( hasParam(kRSKey) )
      aOut <<  kRSKey << "  : " << getParamList() [kRSKey]->toString() << "\n";
  
  if ( hasParam(kRunNumber) )
      aOut <<  kRunNumber << "        : " << getParamList() [kRunNumber]->toString() << "\n";
  
  if ( hasParam(kDcsLhcFlags) )
      aOut <<  kDcsLhcFlags << "     : " << getParamList() [kDcsLhcFlags]->toString() << "\n";
  
  if ( hasParam(kTtcMap) )
      aOut <<  kTtcMap << "           : " << getParamList() [kTtcMap]->toString() << "\n";
  
  if ( hasParam(kFedMap) ) {
    
      aOut << "Enabled FEDs for  : ";

      if ( mFedMap.isEnabled(FEDMap::uGT) ) aOut << kSubUGT << ",";
      if ( mFedMap.isEnabled(FEDMap::uGMT) ) aOut << kSubUGMT << ",";
      if ( mFedMap.isEnabled(FEDMap::CaloL1) ) aOut << kSubCALOL1 << ",";
      if ( mFedMap.isEnabled(FEDMap::CaloL2) ) aOut << kSubCALOL2 << ",";
      if ( mFedMap.isEnabled(FEDMap::BMTF) ) aOut << kSubBMTF << ",";
      if ( mFedMap.isEnabled(FEDMap::EMTF) ) aOut << kSubEMTF << ",";
      if ( mFedMap.isEnabled(FEDMap::OMTF) ) aOut << kSubOMTF << ",";
      if ( mFedMap.isEnabled(FEDMap::TwinMux) ) aOut << kSubTWINMUX << ",";
      if ( mFedMap.isEnabled(FEDMap::CPPF) ) aOut << kSubCPPF << ",";
      if ( mFedMap.isEnabled(FEDMap::RPC) ) aOut << kSubRPC;
      
      aOut << "\n" << kFedMap << "           : " << getParamList() [kFedMap]->toString() << "\n";
  }
}

// --------------------------------------------------------
void ConfigurationBase::sendReport(const string& aBody, const string & aTransition, const string& aType)
{
  string name = getContext()->getCell()->getName();
  
  string lSubject    = name + " - " + aType +" during " + boost::algorithm::to_upper_copy( aTransition );
  string lRecipients = tstoolbox::getEnvironment ("REPORT_RECIPIENT_LIST");
  tstoolbox::SmtpMessenger m(lRecipients, lSubject, aBody);
  
  std::replace(name.begin(), name.end(), ' ' , '_');
  name += "@noreply.cern.ch";
  
  m.send(name);  
}

} // end ns tsruncontrol
