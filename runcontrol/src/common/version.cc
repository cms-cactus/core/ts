#include "ts/runcontrol/version.h"

#include "config/version.h"

GETPACKAGEINFO ( tsruncontrol )

void tsruncontrol::checkPackageDependencies()
{
  CHECKDEPENDENCY ( config );
}

std::set<std::string, std::less<std::string> > tsruncontrol::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;
  ADDDEPENDENCY ( dependencies,config );
  return dependencies;
}
