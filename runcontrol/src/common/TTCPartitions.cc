/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer Jun.
 * @author: Evangelos Paradas
 * @author: Christos Lazaridis
 *************************************************************************/

#include "ts/runcontrol/TTCPartitions.h"

#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellXhannelTB.h"
#include "ts/framework/CellXhannelRequestTB.h"
#include "ts/framework/CellXhannelRequestHandler.h"

#include "ts/toolbox/Tools.h"
#include "ts/toolbox/FsTools.h"
#include "ts/toolbox/CellToolbox.h"

#include "ts/candela/DatabaseConnection.h"
#include "ts/candela/DBJob.h"

#include <boost/algorithm/string.hpp>
#include <boost/spirit/include/classic.hpp>

#include "xdata/Table.h"
#include "xdata/TableIterator.h"
#include "xdata/String.h"

#include "xcept/tools.h"

#include <memory>

using namespace std;
using namespace tscandela;
using namespace boost::spirit::classic;

namespace tsruncontrol {

// --------------------------------------------------------
TTCPartitions::TTCPartitions()
{
  ;
}


// --------------------------------------------------------
TTCPartitions::~TTCPartitions()
{
  ;
}


// --------------------------------------------------------
void TTCPartitions::init(log4cplus::Logger& aLog, tsframework::CellAbstractContext* aContext)
{
  mLogger = aLog;
  mContext = aContext;

  getPartitionAndSystemFromDB(getLatestKeyFromDB());
}


// --------------------------------------------------------
const string TTCPartitions::system(const string& partitionName)
{
  try {

    return partitionSystem.at(boost::to_upper_copy(partitionName));
  }
  catch (exception& e) {

    XCEPT_RAISE( InvalidParameter, "Partition " + partitionName + " was not found in database. " + e.what() );
  }
}


// --------------------------------------------------------
const vector<string> TTCPartitions::partitions(const string& systemName)
{
  try {

    return systemPartitions.at(boost::to_upper_copy(systemName));
  }
  catch (exception& e) {

    XCEPT_RAISE( InvalidParameter, "System " + systemName + " was not found in database. " + e.what() );
  }
}


// --------------------------------------------------------
const TTCPartitions::SystemPartitions& TTCPartitions::getSystemPartitions()
{
  return systemPartitions;
}


// --------------------------------------------------------
const TTCPartitions::PartitionSystem& TTCPartitions::getPartitionSystem()
{
  return partitionSystem;
}


// --------------------------------------------------------
string TTCPartitions::toString()
{
  ostringstream msg;
  
  for (PartitionSystem::iterator i = partitionSystem.begin(); i != partitionSystem.end(); i++)
    msg << "TTC Partition : '" << i->first << "' : '" << i->second << "'\n";
  
  return msg.str();
}


// --------------------------------------------------------
bool TTCPartitions::partitionIncluded( const string& partitionName )
{
  if ( partitionSystem.count( boost::to_upper_copy(partitionName) ) != 0 )
    return true;

  return false;
}


// --------------------------------------------------------
std::string TTCPartitions::getLatestKeyFromDB()
{
  DatabaseConnection *lDb = new DatabaseConnection (mLogger, mContext);
  map<string, string> parameters;
  DBJobSelect lDbJobSelect(lDb, "urn:tstore-view-SQL:runcontrol", "getLatestPartSystKey", parameters);

  try {

    lDbJobSelect.run();
  }
  catch (xcept::Exception& e) {

    delete lDb;
    XCEPT_RAISE( tsexception::DbAccessError, "Exception while trying to read latest TTC Partitions key from the database.\n" + xcept::stdformat_exception_history(e));
  }

  std::auto_ptr<xdata::Table> lTable( lDbJobSelect.getResult() );

  if ( lTable->getColumns().empty() )
    XCEPT_RAISE( tsexception::DbReturnedObjectError, "Received no columns while trying to read latest TTCPartitions key from the database.");

  if ( lTable->getRowCount() == 0 )
    XCEPT_RAISE( tsexception::DbReturnedObjectError, "Received no rows while trying to read latest TTCPartitions key from the database.");

  xdata::TableIterator it = lTable->begin();
  return it->getField("KEY")->toString();
}


// --------------------------------------------------------
void TTCPartitions::getPartitionAndSystemFromDB(std::string aLatestKey)
{
  LOG4CPLUS_INFO ( mLogger, "TTCPartitions latest key found '" + aLatestKey + "'");

  DatabaseConnection *lDb = new DatabaseConnection (mLogger, mContext);
  map<string, string> parameters;
  parameters["key"] = aLatestKey;
  DBJobSelect lDbJobSelect(lDb, "urn:tstore-view-SQL:runcontrol", "getActivePartitions", parameters);

  try {

    lDbJobSelect.run();
  }
  catch (xcept::Exception& e) {

    delete lDb;
    XCEPT_RAISE( tsexception::DbAccessError, "Exception while trying to read TTC Partitions configuration from the database.\n" + xcept::stdformat_exception_history(e));
  }

  std::auto_ptr<xdata::Table> lTable( lDbJobSelect.getResult() );

  if ( lTable->getColumns().empty() )
    XCEPT_RAISE( tsexception::DbReturnedObjectError, "Received no columns while trying to read TTC Partitions configuration from the database.");

  if ( lTable->getRowCount() == 0 )
    XCEPT_RAISE( tsexception::DbReturnedObjectError, "Received no rows while trying to read TTC Partitions configuration from the database.");

  for(xdata::TableIterator it = lTable->begin(); it != lTable->end(); it++)
      partitionSystem[boost::to_upper_copy(it->getField("PARTITION")->toString())] = boost::to_upper_copy(it->getField("SYSTEM")->toString());

    for(PartitionSystem::iterator i = partitionSystem.begin(); i != partitionSystem.end(); i++)
      systemPartitions[i->second].push_back( i->first );
}

}
