/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer Jun.
 * @author: Evangelos Paradas
 * @author: Christos Lazaridis
 *************************************************************************/

#include "ts/runcontrol/TTCMap.h"
#include "ts/toolbox/Tools.h"

#include "log4cplus/logger.h"

#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/spirit/include/classic.hpp>
#include <boost/spirit/include/classic_insert_at_actor.hpp>
#include <boost/tokenizer.hpp>

#include <exception>
#include <sstream>

using namespace std;
using namespace boost::spirit::classic;

namespace tsruncontrol {

// --------------------------------------------------------
TTCMap::TTCMap()
{
  //
}


// --------------------------------------------------------
TTCMap::~TTCMap()
{
  ;
}


// --------------------------------------------------------
void TTCMap::init(log4cplus::Logger& aLogger, tsframework::CellAbstractContext* aContext, string aInputStr )
{
  mLogger = aLogger;
  mContext = aContext;

  mTtcPartitions.init(aLogger, aContext);

  if ( mTtcPartitions.getPartitionSystem().empty() )
    XCEPT_RAISE ( TTCMap::EmptyTTCPartitionMap, "Got no TTCPartitions from  database." );

  boost::algorithm::trim( aInputStr );

  if( aInputStr == "" )
    aInputStr = "{dummy=0}";
  
  try {

    inputString = formatInput(aInputStr);
   
    bitValues = getPartitionsAndBits( inputString.substr (inputString.find_first_of("{")+1) ) ; 
  }
  catch ( exception& e ) {

    string exc = e.what();
    XCEPT_RAISE ( TTCMap::InvalidArgument, "Invalid TTCMap input (" + aInputStr + "). Caught: " + exc );
  }

  ostringstream partsWithoutSystem;
  for(map<string, unsigned int>::iterator it=bitValues.begin(); it!=bitValues.end(); it++) {

    if( !mTtcPartitions.partitionIncluded(boost::to_upper_copy(it->first) ) ) {

      partsWithoutSystem << "Partition: " << it->first << " not found in L1 DB. Will be ignored." << endl;
      bitValues.erase(it->first);
    }
  }
  
  partitionsWithoutSystem = partsWithoutSystem.str();

  for(map<string, string>::const_iterator it = mTtcPartitions.getPartitionSystem().begin(); it != mTtcPartitions.getPartitionSystem().end(); it++) {

    if ( bitValues.count(it->first) == 0 )
      bitValues[it->first] = 0;
  }    
}


// --------------------------------------------------------
TTCPartitions& TTCMap::mapping()
{
  return mTtcPartitions;
}


// --------------------------------------------------------
unsigned int TTCMap::size() const
{
  return bitValues.size();
}


// --------------------------------------------------------
bool TTCMap::hasFedEnabled ( const string& partitionName )
{
  return ( FED_BITMASK & bitValues[ boost::to_upper_copy(partitionName) ] ); 
}


// --------------------------------------------------------
bool TTCMap::hasFmmEnabled ( const string& partitionName )
{
  return ( FMM_BITMASK & bitValues[ boost::to_upper_copy(partitionName) ] );  
}


// --------------------------------------------------------
bool TTCMap::systemHasFedEnabled ( const string& systemName )
{
  vector<string> sysFeds;
  
  try {

    sysFeds = (mTtcPartitions.getSystemPartitions()).at(boost::to_upper_copy(systemName));
  }
  catch (exception& e) {

    XCEPT_RAISE ( TTCMap::KeyNotFoundInMap, "System " + systemName + " not found in L1 DB. " + e.what());
  }
  
  for ( vector<string>::iterator i = sysFeds.begin(); i != sysFeds.end(); ++i ) {

    if ( hasFedEnabled ( *i ) )
      return true;
  }

  return false;
}


// --------------------------------------------------------
bool TTCMap::systemHasFmmEnabled ( const string& systemName )
{
  vector<string> sysFeds;
  
  try {

    sysFeds = mTtcPartitions.getSystemPartitions().at(boost::to_upper_copy(systemName));
  }
  catch (exception& e) {

    XCEPT_RAISE( TTCMap::KeyNotFoundInMap, "System " + systemName + " not found in L1 DB. " + e.what());
  }

  for ( vector<string>::iterator i = sysFeds.begin(); i != sysFeds.end(); ++i ) {

    if ( hasFmmEnabled ( *i ) )
      return true;
  }

  return false;
}


// --------------------------------------------------------
void TTCMap::enableSystemFed ( const string& systemName )
{
  if( mTtcPartitions.getSystemPartitions().count(boost::to_upper_copy(systemName)) == 0 ) {

    XCEPT_RAISE ( TTCMap::NoSystem, "System does not exist in L1 DB." );
  }
  else {

    bitValues[ *mTtcPartitions.getSystemPartitions().at(boost::to_upper_copy(systemName)).begin() ] |= FED_BITMASK;
    iAssert ( systemHasFedEnabled ( boost::to_upper_copy(systemName) ) );
  }
}


// --------------------------------------------------------
map<string,unsigned int> TTCMap::getBitMap()
{
  return bitValues;
}


// --------------------------------------------------------
string TTCMap::toString()
{
  ostringstream res;

  for( map<string,unsigned int>::iterator it=bitValues.begin(); it!=bitValues.end(); it++)
    res << it->first << "\t" << it->second << endl;
  
  return res.str();
}


// --------------------------------------------------------
string TTCMap::getInputString()
{
  return inputString;
}


// --------------------------------------------------------
string TTCMap::formatInput(string s)
{
  string toReturn = "{";

  s = s.substr(s.find_first_of("{")+1, s.find_first_of("}")-1);

  boost::char_separator<char> sep(", ");
  boost::tokenizer< boost::char_separator<char> > tokens(s, sep);

  BOOST_FOREACH (const string& t, tokens) {

    toReturn += t + ", ";
  }

  toReturn = toReturn.substr(0, toReturn.size() - 2 );
  toReturn += "}";

  return toReturn;
}


// --------------------------------------------------------
map<string, unsigned int> TTCMap::getPartitionsAndBits(const string& text)
{
  string partition;
  unsigned int bit;
  string text_n = text;
  size_t fnd = text_n.find('}');
  text_n.replace(fnd,3,", ");

  map<string, unsigned int> PartitionsAndBits, tempPartitionsAndBits;
  
  if ( !parse(text_n.c_str(), 
    *( ( * (anychar_p - '=') )[assign_a(partition)] >> '=' >> uint_p[assign_a (bit)] >> ',' >> (+ space_p) [insert_at_a (tempPartitionsAndBits, partition, bit) ] ), 
              nothing_p).full )
    XCEPT_RAISE ( InvalidArgument, "Illegal TTCMap definition: " + text );
  
  for(map<string, unsigned int>::iterator it = tempPartitionsAndBits.begin();  it != tempPartitionsAndBits.end(); it++) {

    PartitionsAndBits[boost::to_upper_copy(it->first)] = it->second;
  }
  
  return PartitionsAndBits;
}


// --------------------------------------------------------
string TTCMap::getPartitionsWithoutSystem()
{
  return partitionsWithoutSystem;
}

}
