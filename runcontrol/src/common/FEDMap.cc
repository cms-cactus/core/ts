/** **********************************************************************
 * Trigger Supervisor Component
 *
 * @author: Josef Hammer Jun.
 * @author: Evangelos Paradas
 * @author: Christos Lazaridis
 *************************************************************************/

#include "ts/runcontrol/FEDMap.h"

#include "ts/toolbox/Tools.h"

#include "ts/exception/CellException.h"

#include <boost/lexical_cast.hpp>
#include <boost/spirit/include/classic.hpp>
#include <boost/spirit/include/classic_insert_at_actor.hpp>

using namespace std;
using namespace tsexception;
using namespace boost::spirit::classic;

namespace tsruncontrol {

// --------------------------------------------------------
FEDMap::FEDMap (): mValuesMap()
{
  ;
}


// --------------------------------------------------------
FEDMap::~FEDMap ()
{
  ;
}


// --------------------------------------------------------
void FEDMap::init(const std::string& fedMap)
{
  if ( fedMap == "" )
    XCEPT_RAISE ( ParsingError, "Received empty FEDMap." );
  
  if ( fedMap.find ( "&" ) != std::string::npos && fedMap.find ( "%" ) != std::string::npos )
    parseRcmsFed ( fedMap );
  else
    XCEPT_RAISE ( ParsingError, "Error in parsing provided FEDMap." );
}


// --------------------------------------------------------
std::string FEDMap::toString() const
{
  string lString = "";

  for(map<int, unsigned int>::const_iterator it = mValuesMap.begin(); it != mValuesMap.end(); it++) {
    
    lString += boost::lexical_cast<string> ( it->first ) + "&" + boost::lexical_cast<string> ( it->second ) + "%";
  }
  
  return lString;
}


// --------------------------------------------------------
unsigned int FEDMap::size() const
{
  return mValuesMap.size();
}



// --------------------------------------------------------
void FEDMap::forceFed( int aIndex, unsigned int aValue )
{
  try {

    mValuesMap.at(aIndex) = aValue;
  }
  catch ( std::exception& e ) {

    mValuesMap.insert ( std::pair<int, unsigned int>(aIndex,aValue) );
  }
}


// --------------------------------------------------------
bool FEDMap::operator[] ( int aIndex ) const
{
  return ( activeSLink(aIndex) || activeTTS(aIndex) );
}


// --------------------------------------------------------
bool FEDMap::activeSLink( int aIndex ) const
{
  try {
    
    unsigned int lValue = mValuesMap.at ( aIndex );

    if ( ( lValue & 0x5 ) == 0x1 )
      return true;
  }
  catch ( std::exception& e ) {
    
    // FED not in map, so not active
    return false;  
  } 

  return false; 
}


// --------------------------------------------------------
bool FEDMap::activeTTS( int aIndex ) const
{
  try {
    
    unsigned int lValue = mValuesMap.at ( aIndex );

    if ( ( lValue & 0xa ) == 0x2 )
      return true;
  }
  catch ( std::exception& e ) {
    
    // FED not in map, so not active
    return false;  
  } 

  return false; 
}


// --------------------------------------------------------
unsigned int FEDMap::fedBitValue( int aIndex ) const
{
  try {

    return mValuesMap.at ( aIndex );
  }
  catch ( std::exception& e ) {

    return 0;
  }
}


// --------------------------------------------------------
void FEDMap::parseRcmsFed ( const string& str )
{
  mValuesMap.clear();
  
  map<int, unsigned int> bitValues;
  int id;
  unsigned int maskValue;

  // syntax is: "id1&mask1%id2&mask2%...maskN%"
  if ( !parse ( str.c_str(),
                (* ( uint_p[assign_a ( id ) ] >> '&' >> uint_p[assign_a ( maskValue ) ] >> '%' ) [insert_at_a (bitValues, id, maskValue)] ), nothing_p ).full )  {
    
    XCEPT_RAISE ( ParameterError, "FEDMap contains illegal values!" );
  }

  for (map<int, unsigned int>::iterator it=bitValues.begin(); it!=bitValues.end(); it++ ) {
    
     mValuesMap.insert ( std::pair<int, unsigned int>(it->first,it->second) );
  }  
}


// --------------------------------------------------------
bool FEDMap::isEnabled ( SubsystemFEDs aSubsystem ) const
{
/*
 * As defined in https://twiki.cern.ch/twiki/bin/viewauth/CMS/L1TriggerFEDs
 * 0&0%1404&1%1402&1%1354&1%1360&0%1376&0%1384&1%1367&1%1390&0%
 * 0&0%1404&1%1402&1%1354&1%1360&1%1376&0%1384&1%1390&1%
 * 0&0%1404&0%1402&0%1354&1%1360&0%1376&0%1384&0%1390&0%
 *
 * uGT  1404 - 1406
 * uGMT 1402 - 1403
 * CALOL1 1354 - 1359
 * CALOL2 1360 - 1367
 * BMTF 1376 - 1379
 * EMTF 1384 - 1389
 * OMTF 1380 - 1383 
 * TwinMux 1390 - 1395
 */

  try {
    switch ( aSubsystem ) {
    case uGT:
      return ( (*this)[ 1404 ] || (*this)[ 1405 ] || (*this)[ 1406 ] );
      break;
    case uGMT:
      return (*this)[ 1402 ] || (*this)[ 1403 ] ;
      break;
    case CaloL1:
      return (*this)[ 1354 ] || (*this)[ 1355 ] || (*this)[ 1356 ] ||
             (*this)[ 1357 ] || (*this)[ 1358 ] || (*this)[ 1359 ] ;
      break;
    case CaloL2:
      return (*this)[ 1360 ] || (*this)[ 1361 ] || (*this)[ 1362 ] ||
             (*this)[ 1363 ] || (*this)[ 1364 ] || (*this)[ 1365 ] ||
             (*this)[ 1366 ] || (*this)[ 1367 ] ;
      break;
    case BMTF:
      return (*this)[ 1376 ] || (*this)[ 1377 ] ||
             (*this)[ 1378 ] || (*this)[ 1379 ];
      break;
    case EMTF:
      return (*this)[ 1384 ] || (*this)[ 1385 ] || (*this)[ 1387 ] ||
             (*this)[ 1388 ] || (*this)[ 1389 ] ;
      break;
    case OMTF:
      return (*this)[ 1380 ] || (*this)[ 1381 ] ||
             (*this)[ 1382 ] || (*this)[ 1383 ];
      break;
    case TwinMux:
      return (*this)[ 1390 ] || (*this)[ 1391 ] || (*this)[ 1392 ] || (*this)[ 1393 ] ||
             (*this)[ 1394 ] || (*this)[ 1395 ] ;
      break;  
    case CPPF:      // https://svnweb.cern.ch/trac/cactus/ticket/2180
      return (*this)[ 1386 ] ;
      break;
    case RPC:
      return (*this)[ 790 ] || (*this)[ 791 ] || (*this)[ 792 ] || (*this)[ 793 ];
    default:
      return false;
    }
  }
  catch ( std::exception& e ) {

    return false;  
  }
}


// --------------------------------------------------------
vector<int> FEDMap::getFeds() const
{
  vector<int> toReturn;
  
  for(map<int, unsigned int>::const_iterator it = mValuesMap.begin(); it != mValuesMap.end(); it++ ) {
    
    toReturn.push_back(it->first);
  }
  
  return toReturn;
}

}
