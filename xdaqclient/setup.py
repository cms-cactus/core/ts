#!/usr/bin/python

from distutils.core import setup, Command, Extension
from os import environ

setup(name="cactuscore-tsxdaqclient",
      version=environ['PACKAGE_VER_MAJOR'] + '.' + environ['PACKAGE_VER_MINOR'] + '.' + environ['PACKAGE_VER_PATCH'],
      description="XDAQ and TS SOAP Client for functional testing",
      author="Marc Magrans de Abril",
      author_email="marc@cern.ch",
      url="https://twiki.cern.ch/twiki/bin/viewauth/CMS/TriggerSupervisor",
      packages=['xdaqclient'],
      )
