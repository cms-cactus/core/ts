#!/usr/bin/python
"""This file stores the different SOAP templates for TS Cells.

The command names are checked against the regex of the first element of each template.
The first match is the selected template. 

Remember to put the more generic regex last (e.g. '.*' in the last place)."""

tmpl = []

tmpl += [["OpSendCommand",
           """<?xml version='1.0' ?>
<SOAP-ENV:Envelope
SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"
xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
<SOAP-ENV:Header>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
<cell:%(_command)s xmlns:cell=\"%(_ns)s\" cid=\"%(_cid)s\" sid=\"%(_sid)s\" async=\"%(_async)s\">               
<operation>%(_opname)s</operation>
<command>%(transition)s</command>
%(_params)s
<callbackFun>%(_reply_callback)s</callbackFun>
<callbackUrl>%(_reply_url)s</callbackUrl>
<callbackUrn>%(_reply_urn)s</callbackUrn>
</cell:%(_command)s>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""]]

tmpl += [["OpReset|OpGetState",
           """<?xml version='1.0' ?>
<SOAP-ENV:Envelope
SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"
xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"
xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"
xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
<SOAP-ENV:Header/>
<SOAP-ENV:Body>
<cell:%(_command)s async=\"%(_async)s\" cid=\"%(_cid)s\" sid=\"%(_sid)s\" xmlns:cell=\"%(_ns)s\">
<operation>%(_opname)s</operation>
%(_params)s
<callbackFun>%(_reply_callback)s</callbackFun>
<callbackUrl>%(_reply_url)s</callbackUrl>
<callbackUrn>%(_reply_urn)s</callbackUrn>
</cell:%(_command)s>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""]]

tmpl += [[".*",
           """<?xml version='1.0'?>
<SOAP-ENV:Envelope
SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"
xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
<SOAP-ENV:Header>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
<cell:%(_command)s xmlns:cell=\"%(_ns)s\" cid=\"%(_cid)s\" sid=\"%(_sid)s\" async=\"%(_async)s\">
%(_params)s
<callbackFun>%(_reply_callback)s</callbackFun>
<callbackUrl>%(_reply_url)s</callbackUrl>
<callbackUrn>%(_reply_urn)s</callbackUrn>
</cell:%(_command)s>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""]]


