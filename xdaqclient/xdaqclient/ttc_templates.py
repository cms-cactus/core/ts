#!/usr/bin/python
"""This file stores the different SOAP templates for XDAQ and TTC nodes.

The command names are checked against the regex of the first element of each template.
The first match is the selected template. 

Remember to put the more generic regex last (e.g. '.*' in the last place)."""

tmpl = []

tmpl += [['ttcciUserCommandOldStyleExecuteSequence',
          """<?xml version='1.0' ?>
<SOAP-ENV:Envelope 
    SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Header />
<SOAP-ENV:Body 
    xdaq:CommandPar="Execute Sequence" 
    xdaq:sequence_name="%(sequenceName)s" 
    xmlns:xdaq="%(_ns)s"><xdaq:userCommand/></SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""]]

tmpl += [['ttcciUserCommandOldStyle',
          """<?xml version='1.0' ?>
<SOAP-ENV:Envelope 
    SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Header />
<SOAP-ENV:Body 
    xdaq:CommandPar="%(commandPar)s" 
    xmlns:xdaq="%(_ns)s"><xdaq:userCommand/></SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""]]

tmpl += [['ttcciUserCommandExecuteSequence', 
          """<?xml version='1.0' ?>
<SOAP-ENV:Envelope 
    SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Header />
<SOAP-ENV:Body><xdaq:userCommand 
    xdaq:CommandPar="Execute Sequence" 
    xdaq:sequence_name="%(sequenceName)s" 
    xmlns:xdaq="%(_ns)s"/></SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""]]

tmpl += [['ltcCommand',
          """<?xml version='1.0' ?>
<SOAP-ENV:Envelope 
    SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Header />
<SOAP-ENV:Body><xdaq:%(cmd)s 
    xdaq:Param="%(param)s" 
    xmlns:xdaq="%(_ns)s" /></SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""]]

tmpl += [['ttcciUserCommand',
          """<?xml version='1.0' ?>
<SOAP-ENV:Envelope 
    SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Header />
<SOAP-ENV:Body><xdaq:userCommand 
    xdaq:CommandPar="%(commandPar)s" 
    xmlns:xdaq="%(_ns)s"/></SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""]]
