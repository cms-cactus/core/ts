#!/usr/bin/python

import random
import urllib2
import socket
import re
import sys
import time
import xdaq_templates as xdaqt
import ts_templates as tst
import ttc_templates as ttct
import logging
from xml.dom.minidom import parseString
import BaseHTTPServer
import threading
import SocketServer
import traceback
import getopt

TS_WNG_THERESHOLD = 3000
TIMEOUT = 50
SERVER_PORT = random.randint(10000,50000)
EMPTY_SOAP = """<?xml version='1.0'?>
<SOAP-ENV:Envelope
SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"
xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"
xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
<SOAP-ENV:Header>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""

def logsetup():
    global logger
    logger = logging.getLogger("root")
    logger.setLevel(logging.FATAL)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    h = logging.StreamHandler()
    h.setFormatter(formatter)
    logger.addHandler(h)

logsetup()

#######SERVER CODE 
class RequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """Handles the SOAP and throws an exception if it does not conform"""
    reply = ""
    def do_GET(self):
        raise Exception("GET request received on the asynchronous listener from %s" % self.client_address)

    def  do_POST(self):
        """Receives an HTTP POST reply and writes the resulting string (without headers) in RequestHandler.reply"""
        clen = self.headers.getheader('content-length')
        if clen:
            clen = int(clen)
        else:
            raise Exception("Asynchrnous SOAP reply from %s is empty" % self.client_address)
        
        RequestHandler.reply = self.rfile.read(clen)

        #send acknowledge
        self.send_response(200)
        self.end_headers()
        self.wfile.write(EMPTY_SOAP)

    def log_message(self, format, *args):
        """Override to silent output"""
        pass

class Server(BaseHTTPServer.HTTPServer):
    """HTTP Server that timeouts and throws if there is no request received in TIMEOUT sec"""
    def __init__(self,*args,**kr):
        BaseHTTPServer.HTTPServer.__init__(self,*args,**kr)
        
    def handle_request(self):
        """Handle one reques. Socke timeout has been added to avoid blocking"""

        self.socket.settimeout(TIMEOUT)
        request, client_address = self.get_request()

        if self.verify_request(request, client_address):
            try:
                self.process_request(request, client_address)
            except Exception:
                self.close_request(request)
                raise

class ServerThread(threading.Thread):
    """Non-blocking server """
    def run(self):
        self.reply = None
        self.exception = None

        try:
            server = Server(('',SERVER_PORT),RequestHandler)
            server.handle_request()
            
            self.reply = RequestHandler.reply
        except socket.timeout:
            self.exception = Exception("socket timeout of %d sec" % TIMEOUT)
        except Exception,e:
            self.exception = e



#######DATA TYPES      
class WrapType:
    def __init__(self,value=""):
        self._d = {}
        self._d['value'] = str(value)
        self._d['type'] = None
        self._d['name'] = None

    def toXDAQ(self, name):
        self._d['name'] = name
        return "<p:%(name)s xsi:type=\"%(type)s\">%(value)s</p:%(name)s>" % self._d

    def toCell(self, name):
        self._d['name'] = name
        return "<cell:param name=\"%(name)s\" xsi:type=\"%(type)s\">%(value)s</cell:param>" % self._d
    
class UnsignedShort(WrapType):
    def __init__(self,value=""):
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:unsignedShort"

class UnsignedLong(WrapType):
    def __init__(self,value=""):
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:unsignedLong"

class UnsignedInt(WrapType):
    def __init__(self,value=""):
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:unsignedInt"

class String(WrapType):
    def __init__(self,value=""):
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:string"

class Bool(WrapType):
    def __init__(self,value=""):
        if value:
            value = "true"
        else:
            value= "false"
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:boolean"

class Int(WrapType):
    def __init__(self,value=""):
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:integer"

class Short(WrapType):
    def __init__(self,value=""):
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:short"

class Long(WrapType):
    def __init__(self,value=""):
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:long"

class Float(WrapType):
    def __init__(self,value=""):
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:float"

class Double(WrapType):
    def __init__(self,value=""):
        WrapType.__init__(self,value)
        self._d['type'] = "xsd:double"

#######XDAQ CLIENT
class XDAQ:
    def __init__(self, url,urn, ns='urn:xdaq-soap:3.0', tmpl=None):
        socket.setdefaulttimeout(TIMEOUT)
        
        self._url = url
        self._urn = urn
        self._ns = ns
        
        #Add the template
        self._tmpl = []
        if tmpl:
            self._tmpl = tmpl 
        self._tmpl += xdaqt.tmpl
        
    def __getattr__(self, name):
        return XDAQProxy(self, name)

class XDAQProxy:
    def __init__(self,xdaq,cmd):
        self._xdaq = xdaq
        self._cmd = cmd
                
    def _convertParams(self,params):
        p = {}
        for n,v in params.items():
            if type(v) is str:
                v = String(v)

            if type(v) is bool:
                v = Bool(v)

            if not isinstance(v,WrapType):
                raise TypeError("Unable to convert parameter %s to WrapType" % n)

            p[n] = v
        return p


    def _createSoap(self,**kwargs):

        p = {}
        p['_ns'] = self._xdaq._ns

        for k,v in kwargs.items():
            if k == 'params' and isinstance(v,dict):
                params = self._convertParams(v)
                p['_params'] = ''.join([s.toXDAQ(n) for n,s in params.items()])
            else:
                p[k] = str(v)

        if not p.has_key('_params'):
            p['_params'] = ''
        
        p["_command"] = self._cmd
        for r_str,soap in self._xdaq._tmpl:
            r = re.compile(r_str)
            if r.search(p["_command"]):
                msg = soap
                break
        else:
            raise Exception("SOAP template not found for '%s'" % p["_command"])
        
        try:
            msg = msg % p
        except KeyError,e:
            raise Exception("Parameter '%s' not found" % str(e))

        return msg

    def _handleFaults(self,d):
        if d.has_key('faultstring'):
            raise Exception("%s" % d['faultstring'].replace("\n"," "))
        
    def _parseDomReply(self,dom,d):
        for n in dom.childNodes:
            if n.nodeType == n.TEXT_NODE:
                d[str(n.parentNode.localName)] = str(n.data)
            else:
                self._parseDomReply(n,d)

        return d

    def _parseReply(self,reply):
        dom = parseString(reply)
        d = {}

        return self._parseDomReply(dom,d)
        
    def __call__(self,**kwargs):
        data = self._createSoap(**kwargs)
        logger.info(data)

        header = {"SOAPAction":self._xdaq._urn}
        req = urllib2.Request(self._xdaq._url,data,header)

        reply = urllib2.urlopen(req).read()
        
        logger.info(reply)
        
        d = self._parseReply(reply)
        logger.info(d)
        self._handleFaults(d)

        return d

#######TTC CLIENT
class TTC(XDAQ):
    def __init__(self, url,urn='urn:xdaq-application:lid=13', ns='urn:xdaq-soap:3.0'):
        XDAQ.__init__(self,url,urn,ns,tmpl=ttct.tmpl)

#######TS CLIENT
class Cell(XDAQ):
    def __init__(self, url,urn='urn:xdaq-application:lid=13', ns='urn:ts-soap:3.0',async=True):
        XDAQ.__init__(self,url,urn,ns,tmpl=tst.tmpl)
        
        if async:
            self._async = "true"
            self._reply_url = "http://%s:%d" % (socket.gethostname(),SERVER_PORT)
            self._reply_urn = 'undetermined'
            self._reply_callback = 'undetermined'
        else:
            self._async = "false"
            self._reply_url = ''
            self._reply_urn = ''
            self._reply_callback = ''
        
        self._sid = "xdaqclient" + time.asctime().replace(' ','_')
       
    def Operation(self,opname):
        return CellOperationProxy(self,opname)
    
    def __getattr__(self, name):
        return CellProxy(self, name)
    
class CellProxy(XDAQProxy):
    def __init__(self,xdaq,cmd,opname='',transition=''):
        XDAQProxy.__init__(self,xdaq,cmd)
        self._opname = opname
        self._transition = transition

    def _handleFaults(self,d):
        XDAQProxy._handleFaults(self,d)

        if d.has_key('warningLevel') and d.has_key('warningMessage'):
            wlevel = int(d['warningLevel'])
            if wlevel >= TS_WNG_THERESHOLD:
                raise Exception(d['warningMessage'])

    def _createSoap(self,**kwargs):

        p = {}
        p['_ns'] = self._xdaq._ns
        p['_sid'] = self._xdaq._sid
        p['_cid'] = time.asctime().replace(' ','_')
        p['_async'] = self._xdaq._async
        p['_reply_url'] = self._xdaq._reply_url
        p['_reply_urn'] = self._xdaq._reply_urn
        p['_reply_callback'] = self._xdaq._reply_callback
        if self._opname:
            p['_opname'] = self._opname
        if self._transition:
            p['transition'] = self._transition
        if not kwargs.has_key('_params'):
            p['_params'] = ''

        for k,v in kwargs.items():
            if k == 'params' and isinstance(v,dict):
                params = self._convertParams(v)
                p['_params'] = ''.join([s.toCell(n) for n,s in params.items()])
            else:
                p[k] = str(v)
        
        p["_command"] = self._cmd
        for r_str,soap in self._xdaq._tmpl:
            r = re.compile(r_str)
            if r.search(p["_command"]):
                msg = soap
                break
        else:
            raise Exception("SOAP template not found for '%s'" % p["_command"])
        
        try:
            msg = msg % p
        except KeyError,e:
            raise Exception("Parameter '%s' not found" % str(e))

        return msg

    def __call__(self,**kwargs):
        data = self._createSoap(**kwargs)
        logger.info(data)

        #Start server before async communication
        t=None
        d={}
        try:
            if self._xdaq._async == "true":
                t = ServerThread()
                t.start()
            
            #Send/recive sync message
            header = {"SOAPAction":self._xdaq._urn}
            req = urllib2.Request(self._xdaq._url,data,header)
            sync = urllib2.urlopen(req).read()
        
            logger.info(sync)
        
            d = self._parseReply(sync)
            logger.info(d)
            self._handleFaults(d)
        finally:
            #Wait for the server to receive the reply or timeout
            t.join()
            
        #Receive asynchrnous Message
        if t:
            if t.exception:
                raise t.exception
            elif t.reply:
                async = t.reply
                logger.info(async)
                d = self._parseReply(async)
                logger.info(d)
                self._handleFaults(d)
 
        return d


class CellOperationProxy:
    def __init__(self,xdaq,opname):
        self._xdaq = xdaq
        self._opname = opname

    
    def __getattr__(self, name):
        if name == 'reset':
            cmd = 'OpReset'
            transition = ''
        elif name == 'getState':
            cmd = 'OpGetState'
            transition = ''
        else:
            cmd = 'OpSendCommand'
            transition = name
        
        return CellProxy(self._xdaq, cmd, self._opname,transition)


#######SCRIPTs TO EXECUTE THE TESTS

def __testCommand(name, cmd):
    try: 
        # Execute the actual command
        start = time.time()
        cmd()
        end = time.time()
        elapsed_time=end-start
        print "TEST OK, %s, %f sec" % (name, elapsed_time)
    except KeyboardInterrupt:
        print "Interrupting test..."
        sys.exit(1)
    except Exception,e:
        print "TEST FAILED, %s: %s" % (name,str(e))
        global logger
        if (logger.getEffectiveLevel() == logging.INFO or logger.getEffectiveLevel() == logging.DEBUG):
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_tb(exc_traceback)

def usage():
    msg = """usage: %s [options]
    
    Where:
        -h,--help               Prints this help
        -v,--verbose            Verbose output
        -t <timeout sec>        Sets the socket timeout in seconds (default 50 sec)
        --timeout=<timeout sec> Sets the socket timeout in seconds (default 50 sec)
    """ 
    return msg % sys.argv[0]

def verbose():
    global logger
    logger.setLevel(logging.INFO)

def settimeout(timeout_sec):
    global TIMEOUT
    TIMEOUT = timeout_sec
    socket.setdefaulttimeout(TIMEOUT)
   
    
def test():
    """Executes all the test_xyz functions of the module (__main__ by default)
    prints "TEST OK, xyz, <seconds> s" is the test succeeds
    and
    prints "TEST FAIL, xyz, <seconds> s" if the """
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hvt:", ["help", "verbose","timeout="])
    except Exception,e:
        print str(err) 
        print usage()
        sys.exit(1)
    
    for o, a in opts:
        if o in ("-v","--verbose"):
            verbose()
        elif o in ("-h", "--help"):
            print usage()
            sys.exit(1)
        elif o in ("-t", "--timeout"):
            settimeout(int(a))
       
            
    module = __import__('__main__')

    def_names = []

    for element in dir(module):
        if re.search('test_+.?',str(element)):
            def_names.append(element)

    if not def_names:
        print "TEST FAILED, No tests defined in %s" % module.__name__
        print "Aborting test"
        sys.exit(1)
    
    for def_name in def_names:
        try:
            testname = " ".join(def_name.split('_')[1:])
        except IndexError:
            print "TEST FAILED, %s" % def_name
            print "Malformated subtest function name"
            print "The test functions should be named 'test_xyz'"
            sys.exit(1)

        __testCommand(testname,getattr(module,def_name))


def check(result,regex):
    '''Check if results matches the regex. If not throws a RegexCheckFailed exception'''
    if not re.search(regex,str(result),re.DOTALL):
        msg = "Regular expression check failed. "
        msg += "We were expecting: '%s'. "
        msg += "However, the result was: '%s'" % (regex,str(result)) 
        
        raise  Exception(msg)


