# Auto-set build home, unless overridden
BUILD_HOME ?= $(shell cd $(shell dirname $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)))/../..; pwd)
# Sanitize build home
BUILD_HOME := $(shell cd ${BUILD_HOME}; pwd)

CACTUS_RPM_ROOT ?= $(BUILD_HOME)

#Python include path  
PYTHON_INCLUDE_PREFIX ?= $(shell python -c "import distutils.sysconfig; print distutils.sysconfig.get_python_inc()")
PYTHON_LIB_PREFIX ?= $(shell python -c "from distutils.sysconfig import get_python_lib; import os.path; print os.path.split(get_python_lib(standard_lib=True))[0]")

#Trigger Supervisor Toolbox Library
TRIGGER_TS_TOOLBOX_PREFIX = $(BUILD_HOME)/ts/toolbox
TRIGGER_TS_TOOLBOX_INCLUDE_PREFIX=$(TRIGGER_TS_TOOLBOX_PREFIX)/include
TRIGGER_TS_TOOLBOX_LIB_PREFIX=$(TRIGGER_TS_TOOLBOX_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

#Trigger Supervisor Ajaxell Library
TRIGGER_TS_AJAXELL_PREFIX = $(BUILD_HOME)/ts/ajaxell
TRIGGER_TS_AJAXELL_INCLUDE_PREFIX=$(TRIGGER_TS_AJAXELL_PREFIX)/include
TRIGGER_TS_AJAXELL_LIB_PREFIX=$(TRIGGER_TS_AJAXELL_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

#Trigger Supervisor Framework Library
TRIGGER_TS_FRAMEWORK_PREFIX = $(BUILD_HOME)/ts/framework
TRIGGER_TS_FRAMEWORK_INCLUDE_PREFIX=$(TRIGGER_TS_FRAMEWORK_PREFIX)/include
TRIGGER_TS_FRAMEWORK_LIB_PREFIX=$(TRIGGER_TS_FRAMEWORK_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

#Trigger Supervisor Run Control Library                                                                                                                                                                                                      
TRIGGER_TS_RUNCONTROL_PREFIX = $(BUILD_HOME)/ts/runcontrol
TRIGGER_TS_RUNCONTROL_INCLUDE_PREFIX=$(TRIGGER_TS_RUNCONTROL_PREFIX)/include
TRIGGER_TS_RUNCONTROL_LIB_PREFIX=$(TRIGGER_TS_RUNCONTROL_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

#TCDS Library for the Trigger Supervisor Applications
TRIGGER_TCDS_TS_UTILS_PREFIX = $(BUILD_HOME)/cactusprojects/tcds/TCDSUtils
TRIGGER_TCDS_TS_UTILS_INCLUDE_PREFIX=$(TRIGGER_TCDS_TS_UTILS_PREFIX)/include
TRIGGER_TCDS_TS_UTILS_LIB_PREFIX=$(TRIGGER_TCDS_TS_UTILS_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

#Trigger Configuration DB
TRIGGER_CANDELA_PREFIX = $(BUILD_HOME)/ts/candela
TRIGGER_CANDELA_INCLUDE_PREFIX=$(TRIGGER_CANDELA_PREFIX)/include
TRIGGER_CANDELA_LIB_PREFIX=$(TRIGGER_CANDELA_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

#ROOT
ROOTSYS=/opt/wbm-support-root

include /opt/cactus/build-utils/mfCommonDefs.mk
include $(XDAQ_ROOT)/build/mfAutoconf.rules
include $(XDAQ_ROOT)/build/mfDefs.$(XDAQ_OS)
